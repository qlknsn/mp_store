// assets/components/homePop/index1.js.js
const app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    zIndex: {
      type: Number,
      value: 0
    },
    bottom: {
      type: String,
      value: "56"
    },
    show: {
      type: Boolean,
      value: false
    },
    moveViewX: {
      type: Number,
      value: 0
    },
    moveViewY: {
      type: Number,
      value: 0
    }
  },
  externalClasses: ['custom-class'],

  /**
   * 组件的初始数据
   */
  data: {
    cartNum: 0,
    isLogin: false,
  },
  lifetimes: {
    ready: function () {
      this.setData({
        cartNum: app.globalData.cartNum,
        isLogin: app.globalData.isLogin,
      })
      app.$watch('cartNum', (val, old) => {
        this.setData({
          cartNum: val
        })
      })
      app.$watch('isLogin', (val, old) => {
        this.setData({
          isLogin: val
        })
      })
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
    },
    show: function () {
    },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    /* homePage() {
      wx.switchTab({
        url: '/pages/index/index'
      })
    }, */
    onHome: function () {
      if (!this.data.isLogin) {
        wx.navigateTo({
          url: "/pages/userLogin/index?switchTabIndex=1",
        })
        return
      } else {
        if (!app.globalData.deliverableAddress.addrId) {
          wx.navigateTo({
            url: '/pages/user/address/list/index?type=2'
          })
          return
        }
      }
      wx.switchTab({ url: '/pages/cart/index' })
    }
  }
})
