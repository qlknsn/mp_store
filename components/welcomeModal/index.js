var app = getApp();
Component({

    /**
     * 组件的属性列表
     */
    properties: {
    },

    /**
     * 组件的初始数据
     */
    data: {
        show: false,
        toTop: false
    },
    lifetimes: {
        attached: function () {
            const isWelcome = wx.getStorageSync("isWelcome");
            if (!isWelcome) {
                this.setData({
                    show: true
                })
            }
        },
        detached: function () {
            // 在组件实例被从页面节点树移除时执行
        },
    },
    /**
     * 组件的方法列表
     */
    methods: {
        // 取消函数
        next() {
            wx.setStorageSync("isWelcome", true);
            app.globalData.isWelcome = true;
            this.setData({
                show: false
            }, () => {
                this.triggerEvent('next')
            })
        },
        animationEnd() {
            this.setData({
                toTop: true
            })
        },
        goAgreement() {
            wx.navigateTo({
                url: `/module_base/privacyAgreement/index`,
            })
        },
        goUserAgreement() {
            wx.navigateTo({
                url: `/module_base/userAgreement/index`,
            })
        }
    }
})