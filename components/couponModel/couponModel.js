// components/couponModel.js
const { httpResource } = require("../../assets/js/common.js");
const app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    autoLoad: {
      type: 'boolean',
      value: true
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    bgUrl: '',
    show: false, // 遮罩关闭
    showView: false, // 弹窗元素关闭
    isLoad: false,
    actId: '',
    password: '', // 口令密码
    couponWay: 1, // 直领取，口令领
    couponInfoResps: [],
    modelArr: [],
  },
  pageLifetimes: {
    // 组件所在页面的生命周期函数
    show: function () {
      let isLogin = wx.getStorageSync("u-token");
      if (isLogin && this.data.autoLoad) {
        this.getData();
      }
    },
    hide: function () {
      this.clear();
    },
    resize: function () { },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    clear() {
      this.setData({
        bgUrl: '',
        show: false, // 遮罩关闭
        showView: false, // 弹窗元素关闭
        actId: '',
        password: '', // 口令密码
        couponWay: 1, // 直领取，口令领
        couponInfoResps: [],
        modelArr: [],
      })
    },
    pwdInput(e) {
      this.setData({
        password: e.detail.value
      })
    },
    getData() {
      const isLogin = wx.getStorageSync("u-token");
      if (!isLogin) {
        this.data.isLoad = true;
        return
      }
      return new Promise((resolve, reject) => {
        try {
          httpResource('popCoupon', {}, (data) => {
            if (data && data.actId) {
              const { rewardChannel } = data;
              const isShow = rewardChannel.includes(0);
              // 加载完成
              this.data.isLoad = true;
              this.setData({
                actId: data.actId,
                couponWay: data.couponWay, // 直领取，口令领
                couponInfoResps: data.couponInfoResps || [],
                bgUrl: data.popImg,
                show: isShow
              })
              if(isShow){
                wx.setPageStyle({
                  style: {
                    overflow: 'hidden'
                  }
                })
              }
              resolve(data);
            } else {
              // 加载完成
              this.data.isLoad = true;
            }
          }, 'POST', () => {
            this.data.isLoad = true;
          })
        } catch (error) {
          // 加载完成
          this.data.isLoad = true;
        }
      })
    },
    // 领取优惠券
    async getCoupon() {
      const { actId, couponInfoResps, couponWay, password } = this.data;
      const templateIds = couponInfoResps.map(e => ({ tempId: e.rewardId, canRewardNum: e.canRewardNum }));
      let params = {
        actId, templateIds
      }
      if (couponWay == 2) {
        if (!password) {
          wx.showToast({
            icon: 'none',
            title: '请输入口令'
          })
          return;
        } else {
          params.pwd = password;
        }
      }
      try {
        const data = await httpResource('receiveCoupon', params);
        if (data) {
          this.setData({
            showView: true
          })
          wx.showModal({
            title: '领取成功',
            content: data.isAllReward ? '所有券已经领取成功咯，请到券包查看或使用。' : '部分券已经领完了，其它券将放入您的券包中，请到券包查看或使用。',
            confirmText: '知道了',
            confirmColor: '#FF3B30',
            showCancel: false,
            success: (res) => {
              // 关闭弹窗
              this.close();

            }
          })
        }
        // 执行成功逻辑
      } catch (error) {
        console.log(error)
      }
    },
    close() {
      console.log(this.data.modelArr)
      this.setData({
        show: false
      })
      wx.setPageStyle({
        style: {
          overflow: ''
        }
      })
      this.triggerEvent('close');
    },
    next() {
      let arr = this.data.modelArr;
      if (arr.length == 0) return;
      let data = arr.shift()[0];
      this.triggerEvent('next', data);
      this.setData({
        modelArr: arr
      })
    },
    move() {
      return
    },
  }
})
