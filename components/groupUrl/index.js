var app = getApp();
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        show: {
            type: 'boolean',
            value: false
        },
        top: {
            type: 'number',
            value: 0
        },
        src: {
            type: 'string',
            value: ''
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
    },
    lifetimes: {
        attached: function () {
        },
        detached: function () {
            // 在组件实例被从页面节点树移除时执行
        },
    },
    /**
     * 组件的方法列表
     */
    methods: {
        close() {
            this.triggerEvent('close')
        }
    }
})