const { httpResource } = require('../../assets/js/common.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderNo: null,
    list: [],
    badTagList: [],
    goodTagList: [],
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // this.getLabel();
    this.initData(options);
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },
  initData(opt) {
    if (opt.orderNo) {
      this.setData({
        orderNo: opt.orderNo
      }, () => {
        this.getLabel();
        this.getStoreLabel();
      })
    }
  },
  // 返回商品列表
  async getData() {
    const { orderNo, goodTagList, badTagList } = this.data;
    const data = await httpResource('skuList', { orderNo }, null, "GET");
    const list = (data || []).map(e => {
      e.ja = false;
      e.jian = false;
      if (e.commentType == 1) {
        e.ja = true;
      }
      if (e.commentType == 2) {
        e.jian = true;
      }
      e.tagList = []
      if (e.commentLabel) {
        if (e.commentType == 1) {
          e.tagList = JSON.parse(JSON.stringify(goodTagList)).map(v => {
            if (e.commentLabel.split(',').includes(v.label)) {
              v.isActive = true
            }
            if (e.commentContent && v.label == '写评价') {
              v.isActive = true
            }
            return v
          })
        }
        if (e.commentType == 2) {
          e.tagList = JSON.parse(JSON.stringify(badTagList)).map(v => {
            if (e.commentLabel.split(',').includes(v.label)) {
              v.isActive = true
            }
            if (e.commentContent && v.label == '写评价') {
              v.isActive = true
            }
            return v
          })
        }
      }
      return e
    })
    this.setData({
      list
    })
  },
  // 获取好坏标签
  async getLabel() {
    const data = await httpResource('getCommentLabel', {}, null, "GET");
    if (data && data.length) {
      const goodTagList = data.filter(e => e.commentType === 1).map(item => {
        return {
          label: item.commentLabel,
          isActive: false
        }
      })
      goodTagList.push({
        label: '写评价',
        isActive: false
      });
      const badTagList = data.filter(e => e.commentType === 2).map(item => {
        return {
          label: item.commentLabel,
          isActive: false
        }
      })
      badTagList.push({
        label: '写评价',
        isActive: true
      });
      this.setData({
        goodTagList,
        badTagList
      }, this.getData)
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  // 获取门店数据
  async getStoreLabel() {
    const { orderNo } = this.data;
    const data = await httpResource('getOrderabel', { orderNo }, null, "GET");
    if (data) {
      const { commentContent, shopName, shopCode, storeData } = data;
      this.setData({
        commentContent, shopName, shopCode, storeData: storeData || []
      })
    }
    console.log('门店数据', data)
  },
})