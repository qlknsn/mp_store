const app = getApp();
const {
  httpResource,
  pxTorpx
} = require("../../assets/js/common.js");
import {
  createBarcode,
  createQrcode2
} from "../../assets/js/codeUtil";
import Dialog from "@vant/weapp/dialog/dialog";
Page({
  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      title: "订单详情",
      return: 1,
      home: 1
    },
    background: "#F62C1E",
    titleColor: "#FFFFFF",
    status: 3,
    showQrcode: false,
    // 送达
    serviceshow: false,
    // 待支付
    waitPayshow: false,
    // 取消支付
    cancelPayshow: false,
    // 订单流程
    flowShow: false,
    orderDetail: {},
    countdown: 0,
    countdownTime: "",
    // 门店码
    shopCodeImg: "",
    evalutionshow: false,
    qrcodestr: "AHTM337389029783492087",
    orderList: [{
      picDetails: [{
        samplePic: "",
      },],
    },],
    showRefundlist: false,
    // 不可退款
    showunRefund: false,
    isTakeOut: false, //先死配货规则
    isShowTakeTheir: false, //是否转自提
    groupBuyDetail: null, //团购信息
    takeTheirWidth: null,
    groupBuyTimeStatus: null, //团购时间状态：1.未结束，2.已结束，3.商家强制结束
    groupBuyStatus: null, //拼团结果 20.已成团，10.未成团
    groupBuyEndTime: null, //团购结束时间
    countDownTimeHtm: null, //退款倒计时
    templateList: null, //消息模板
    id: null, //订单详情ID，
    singleGoodsrefund: {},
    // 订单跟踪
    isShowTrack: false,
    flowMiniList: [],// 轨迹list
    opcity: 0,
    showMap: false, // 是否显示地图
    //默认上海人民广场
    longitude: 121.310403,
    latitude: 31.195169,
    markers: [],// 地图点位
    deliveryTextObj: {
      65: '骑手已接单',
      70: '骑手已取货',
      75: '骑手已送达',
    },
    courierCoordinateTextObj: {
      65: '骑手正赶往商家',
      70: '骑手正在送货',
      75: '骑手已送达',
    },
    coordinateInfo: {},
    coordinateTimer: null,
    giftAmount: 0, // 惊喜礼卡支付金额
    isOrderCode: false,
    orderCodeSum: '',
  },
  textpaste(code) {
    wx.setClipboardData({
      data: code.currentTarget.dataset.code,
      success: function (res) {
        wx.getClipboardData({
          // 这个api是把拿到的数据放到电脑系统中的
          success: function (res) {
            console.log(res.data); // data
          },
        });
      },
    });
  },
  applyRefund(event) {
    httpResource('checkCanRefund', {
      orderNo: this.data.orderDetail.orderNo
    }, (res) => {
      let { status, orderTime, sourceType, orderStatus, orderType } = event.currentTarget.dataset.status;
      if (res.canRefund) {
        if (status !== '配送中(外卖)') {
          if (this.data.orderDetail.orderStatusStr == '已送达(到家)' || this.data.orderDetail.orderStatusStr == '已送达' || this.data.orderDetail.orderStatusStr == '已完成' || this.data.orderDetail.orderStatusStr == '已提货(自提）' || this.data.orderDetail.orderStatusStr == '已送达(外卖)' || (this.data.orderDetail.sourceType == '1' && this.data.orderDetail.orderStatusStr == '支付完成') || this.data.orderDetail.orderStatusStr == '订单已送达') {
            wx.navigateTo({
              url: '/module_order/selectService/index?orderId=' + this.data.orderDetail.orderId + '&shopCode=' + this.data.orderDetail.shopNo + '&isAftermarket=true' + '&orderTime=' + orderTime + '&sourceType=' + sourceType + '&orderStatus=' + orderStatus + '&orderType=' + orderType,
            })
          } else {
            wx.navigateTo({
              url: '/module_order/selectService/index?orderId=' + this.data.orderDetail.orderId + '&shopCode=' + this.data.orderDetail.shopNo + '&isAftermarket=false' + '&orderTime=' + orderTime + '&sourceType=' + sourceType + '&orderStatus=' + orderStatus + '&orderType=' + orderType,
            })
          }
        } else {
          this.setData({
            showunRefund: true
          })
        }
      } else {
        let content = ''
        switch (res.cannotRefundType) {
          case 1:
            content = '订单已经全部退款'
            break;
          case 2:
            content = '您已错过申请退款的时间段(交易成功后7天内)，如果您有退货需求，建议您联系门店'
            break;
          case 3:
            content = '您有一笔退款申请正在审核中，不能发起新的退款申请，感谢您的理解!'
            break;
        }
        wx.showModal({
          title: '无法申请退款',
          content: content,
          confirmText: '知道了',
          showCancel: false,
          success(res) { }
        })
      }
    }, "GET", null)
  },
  closepeisongzhong() {
    this.setData({
      showunRefund: false
    })
  },
  closeRefundlist() {
    this.setData({
      showRefundlist: false,
    });
  },
  showRefund({
    currentTarget: {
      dataset: {
        barcode
      },
    },
  }) {

    let filter = {
      orderId: this.data.orderDetail.orderId,
      barCode: barcode,
    };
    httpResource(
      "singleDetail",
      filter,
      (data) => {
        console.log(data.length);
        if (data.records.length > 1) {
          // 超过两笔订单
          this.setData({
            showRefundlist: true,
          });
          this.setData({
            singleGoodsrefund: data.records,
          });
        } else {
          // 只有一笔直接跳详情
          wx.navigateTo({
            url: '/module_order/refundDetail/index?returnOrderNo=' + data.records[0].returnOrderNo,
          })
        }
      },
      "POST",
      null
    );
  },
  torefundDetail({
    currentTarget: {
      dataset: {
        item
      },
    },
  }) {
    console.log(item);
    wx.navigateTo({
      url: '/module_order/refundDetail/index?returnOrderNo=' + item.returnOrderNo,
    })
  },
  showOrCODE() {
    let filter = {
      shopCode: this.data.orderDetail.shopNo,
    };
    httpResource(
      "getCommunityManagerCode",
      filter,
      (data) => {
        this.setData({
          showQrcode: true,
          shopCodeImg: data.shopManagerImgUrl,
        });
      },
      "GET",
      null
    );
  },
  closeOrCODE() {
    this.setData({
      showQrcode: false,
    });
  },
  cancelPay({
    currentTarget: {
      dataset: {
        orderid,
        orderno,
        type
      },
    },
  }) {
    let that = this;
    wx.showModal({
      title: "",
      content: "确认要取消订单吗？",
      success(res) {
        if (res.confirm) {
          console.log("用户点击确定");
          let filter = {
            orderId: orderid,
            orderNo: orderno,
            cancelType: type,
          };
          httpResource(
            "cancelOrder",
            filter,
            (data) => {
              let pages = getCurrentPages();
              pages.splice(pages.length - 2, 1);
              console.log(pages);
              wx.reLaunch({
                url: "/module_order/orderList/orderList?orderType=4",
              });
            },
            "POST",
            null
          );
        } else if (res.cancel) {
          console.log("用户点击取消");
        }
      },
    });
  },
  countDownTimer: null,
  // 倒计时
  setCountDown() {
    let time = 1000;
    let formatTime = this.getFormat(this.data.countdown);
    this.data.countdown -= time;
    if (this.data.countdown == -2000) {
      let filter = {
        orderId: this.data.orderDetail.orderId,
        orderNo: this.data.orderDetail.orderNo,
        cancelType: 2,
      };
      httpResource(
        "cancelOrder",
        filter,
        (data) => {
          this.getOrderDetail(this.data.orderDetail.orderId);
        },
        "POST",
        null
      );
      return;
    } else {
      let times = `${formatTime.mm}:${formatTime.ss}`;
      this.setData({
        countdownTime: times,
      });
      clearTimeout(this.countDownTimer);
      this.countDownTimer = setTimeout(this.setCountDown, time);
    }
  },
  /**
   * 格式化时间
   */
  getFormat: function (msec) {
    let ss = parseInt(msec / 1000);
    let ms = parseInt(msec % 1000);
    let mm = 0;
    let hh = 0;
    if (ss > 60) {
      mm = parseInt(ss / 60);
      ss = parseInt(ss % 60);
      if (mm > 60) {
        hh = parseInt(mm / 60);
        mm = parseInt(mm % 60);
      }
    }
    ss = ss > 9 ? ss : `0${ss}`;
    mm = mm > 9 ? mm : `0${mm}`;
    hh = hh > 9 ? hh : `0${hh}`;
    return {
      ms,
      ss,
      mm,
      hh,
    };
  },
  getOrderDetail(id) {
    let that = this;
    //进度条宽度
    let takeTheirWidth = null;
    this.setData({ showMap: false, coordinateTimer: null });
    httpResource("goodsDetails", null, (data) => {
      if (data.sourceType == "3" && data.orderStatusStr !== '全部退货') {
        setTimeout(() => {
          this.getqrcode(data.pickUpCode);
        }, 1000);
      }
      let {
        nowOrderNum,
        maxOrderNum,
        groupBuyTimeStatus,
        groupBuyStatus,
        groupBuyEndTime,
        countDownTime
      } = data.groupBuyDetail || {};
      //兼容进度条只有1的时候，出现图片变形不能展开的问题
      if (nowOrderNum >= maxOrderNum) takeTheirWidth = 100;
      if (nowOrderNum < maxOrderNum)
        takeTheirWidth = (nowOrderNum / maxOrderNum) * 100;
      // 是否存在惊喜礼卡支付信息
      const giftInfo = data.payDetails.find(item => item.orderChannelZh == '惊喜礼卡');
      let giftAmount = 0;
      if (giftInfo) {
        giftAmount = giftInfo.amount;
      }
      that.setData({
        orderDetail: data,
        takeTheirWidth: takeTheirWidth,
        groupBuyDetail: data.groupBuyDetail || {},
        groupBuyTimeStatus: groupBuyTimeStatus,
        groupBuyStatus: groupBuyStatus,
        groupBuyEndTime: groupBuyEndTime,
        countDownTimeHtm: countDownTime * 1000,
        giftAmount,
      });
      let newTime = Date.parse(new Date());
      let reg = new RegExp("-", "g");
      let oldTime = new Date(data.orderTime.replace(reg, "/")).getTime();
      if (newTime - oldTime > 300000) {
        that.setData({
          countdown: 0,
          countdownTime: "00:00",
          waitPayshow: false,
        });
        if (that.data.serviceshow) {
          that.setData({
            cancelPayshow: false,
          });
        } else {
          that.setData({
            cancelPayshow: true,
          });
        }
      } else {
        that.setData({
          countdown: 300000 - (newTime - oldTime),
        });
        that.setCountDown();
      }
      // 轨迹
      this.getflowMini(data.orderNo, data.orderType);
      // 物流状态 才会有骑手信息
      // || data.deliveryStatus == 75
      console.log(data.orderStatus != 35);
      if (data.orderStatus != 35 && (data.deliveryStatus == 65 || data.deliveryStatus == 70)) {
        // 显示地图
        this.setData({ showMap: true });
        this.getCourierCoordinate(data.orderId, data.shopNo)
      }
    },
      "GET",
      (res) => {
        // 请求失败，报错提示
        wx.showToast({
          title: res.msg,
          icon: "none",
        });
      },
      `/${id}`
    );
  },
  toEvaluation({
    currentTarget: {
      dataset: {
        orderno
      },
    },
  }) {
    wx.navigateTo({
      url: `/module_order/orderEvaluation/index?orderNo=${orderno}&edit=true`,
    });
  },
  // 退款信息
  goRefunddetail({
    currentTarget: {
      dataset: {
        hasNoFinishReturnApply,
        refundapplyid,
        refundStatus
      },
    },
  }) {
    wx.navigateTo({
      url: `/pages/user/refundOrderList/index?orderNo=${this.data.orderDetail.orderNo}&orderId=${this.data.orderDetail.orderId}&return=1`,
    });
  },
  //订单码
  orderCode(event) {
    let _this = this;
    let { channelOrderNo } = event.currentTarget.dataset.channelorderno;
    _this.setData({
      isOrderCode: true,
      orderCodeSum: channelOrderNo
    }, () => {
      createBarcode("orderBarcode", channelOrderNo, pxTorpx(504), pxTorpx(132));
    });
  },
  //关闭订单码
  onCloseToast() {
    let _this = this;
    _this.setData({
      isOrderCode: false,
    });
  },
  // 开发票
  showModal(e) {
    let isInvoice = e.currentTarget.dataset.isinvoice
    console.log(isInvoice);
    if(isInvoice){
      wx.navigateTo({
        url: `/pages/invoice/index?o=${this.data.orderDetail.channelOrderNo}&s=${this.data.orderDetail.shopNo}&p=${this.data.orderDetail.posCode}`,
      });
    }else{
      wx.navigateTo({
        url: `/pages/noinvoice/noinvoice`,
      });
    }
   
  },
  toPay({
    currentTarget: {
      dataset: {
        orderid,
        sourcetype
      },
    },
  }) {
    wx.showLoading({
      title: "支付中...",
      mask: true,
    });
    this.getCartSettlement(orderid, sourcetype);
  },
  //打包结算
  async getCartSettlement(orderId, sourcetype) {
    let params = {
      orderId: orderId,
    };
    let data = await httpResource("payOrder", params, null, "post");
    console.log(data);
    wx.hideLoading();
    if (data && data.payInfo != null) {
      wx.requestPayment({
        timeStamp: data.payInfo.timeStamp,
        nonceStr: data.payInfo.nonceStr,
        package: data.payInfo.package,
        signType: data.payInfo.signType,
        paySign: data.payInfo.paySign,
        success: function (res) {
          if (res.errMsg == "requestPayment:ok") {
            wx.showToast({
              title: "支付成功",
              success: () => {
                wx.removeStorageSync("GOODS_PAY_LIST");
                if (sourcetype == '3') {
                  wx.redirectTo({
                    url: '/module_order/pay-successSelf/index?takeCode=' + data.takeCode + "&orderId=" + data.orderId + "&orderNo=" + data.orderNo + "&mobile=" + data.mobile
                  })
                } else {
                  wx.redirectTo({
                    url: "/module_order/pay-success/index" + "?orderType=" + data.orderType,
                  });
                }

              },
            });
          }
        },
        fail: function (err) {
          wx.showToast({
            title: "您已取消支付",
            icon: "none",
            duration: 2000,
          });
        },
      });
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    this.setData({
      id: options.id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () { },
  getqrcode(data) {
    let that = this;
    createQrcode2("qrcodes", data, pxTorpx(350), pxTorpx(350), that);
    let str2 = createBarcode("barcode", data, pxTorpx(674), pxTorpx(186));
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getOrderDetail(this.data.id);
    let pages = getCurrentPages();
    let currPage = pages[pages.length - 1]; //当前页面
    if (currPage.data.evalutionshow) {
      console.log("zoule");
    }
    this.getTemplateList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () { },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    if (this.data.coordinateTimer) {
      clearTimeout(this.data.coordinateTimer);
      this.setData({
        coordinateTimer: null
      })
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () { },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () { },
  goDetail(e) {
    const barcode = e.currentTarget.dataset.barcode;
    const {
      shopNo: shopCode
    } = this.data.orderDetail;
    wx.navigateTo({
      url: `/pages/productDetail/index?barcode=${barcode}&shopCode=${shopCode}`,
    });
  },
  //tost显示门店名称
  shopTost(event) {
    let {
      text
    } = event.currentTarget.dataset;
    wx.showToast({
      title: `${text}的团购订单`,
      icon: "none",
      duration: 3000,
    });
  },
  //显示拼团规则
  showTakeOut() {
    this.setData({
      isTakeOut: true,
    });
  },
  //关闭拼团规则
  hideTakeOut() {
    this.setData({
      isTakeOut: false,
    });
  },
  //显示转自提
  showTakeTheir() {
    this.setData({
      isShowTakeTheir: true,
    });
  },
  //关闭转自提
  hideTakeTheir() {
    this.setData({
      isShowTakeTheir: false,
    });
  },
  //确认转自提
  async takeTheirOk() {
    let _this = this;
    let {
      id
    } = _this.data;
    let {
      orderId,
      orderNo,
      groupBuyDetail: {
        groupBuyId
      }
    } = _this.data.orderDetail;
    let {
      nickName
    } = wx.getStorageSync("userInfo");
    let parasm = JSON.parse(JSON.stringify({
      orderId,
      orderNo,
      groupBuyId,
      receiver: nickName
    }));
    let data = await httpResource("convertPickup", parasm, null, "post");
    wx.requestSubscribeMessage({
      tmplIds: _this.getTemplateCode("changeSelfPick"),
      success(res) {
        if (_this.getTemplateCode("changeSelfPick").some(v => res[v] === 'accept')) {
          httpResource("sendMessage", {
            code: "changeSelfPick",
            templateCodeList: _this.getTemplateCode("changeSelfPick"),
          });
        }
        _this.setData({
          isShowTakeTheir: false,
        }, () => {
          wx.showToast({
            title: "配送费会自动返还请注意查看",
            icon: "none",
            duration: 2000,
            complete: () => {
              _this.getOrderDetail(id);
            },
          });
        });
      },
      fail: (err) => {
        console.log("订阅失败！");
      },
    });
  },
  //获取模板消息列表
  async getTemplateList() {
    let data = await httpResource("getTemplateList", {}, null, "GET");
    if (data) {
      this.setData({
        templateList: data || [],
      });
    }
  },
  // 获取模板消息ID
  getTemplateCode(code) {
    let {
      templateList
    } = this.data;
    let list = [];
    try {
      list = templateList.find((e) => e.code == code) ?
        templateList
          .find((e) => e.code == code)
          .templateList.map((v) => v.templateId) : [];
    } catch (e) {
      list = [];
    }
    return list;
  },
  //订阅
  subscription() {
    let _this = this;
    let id = wx.getStorageSync("DING_YUE_XIAO_XI");
    if (!id && id != _this.data.id) {
      wx.requestSubscribeMessage({
        tmplIds: _this.getTemplateCode("groupClickShare"),
        success(res) {
          if (_this.getTemplateCode("groupClickShare").some(v => res[v] === 'accept')) {
            httpResource("sendMessage", {
              code: "groupClickShare",
              templateCodeList: _this.getTemplateCode("groupClickShare"),
            });
          }
          wx.setStorage({
            key: "DING_YUE_XIAO_XI",
            data: _this.data.id,
            complete: () => {
              console.log("订阅成功！");
            },
          });
        },
        fail: (err) => {
          console.log("订阅失败！");
        },
      });
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    let {
      address
    } = this.data.groupBuyDetail;
    let shopAddress = address ? address : '您的邻居正在邀请您一起参加好特卖拼团，快快加入';
    if (res.from == "button") {
      return {
        title: `我在${shopAddress}小区进行好特卖拼团，快来加入我一起成团`,
        path: "/pages/productList/index?navIndex=0",
        imageUrl: "https://img-cdn.hotmaxx.cn/web/common/mp-store/order/mmrndy.jpg",
      };
    }
    return {
      title: `为你推荐一家宝藏折扣店，全场1折起！`,
      path: "/pages/index/index",
      imageUrl: "https://img-cdn.hotmaxx.cn/web/common/mp-store/common/share-base-img.jpg",
    }
  },
  // 打开订单跟踪
  openTarck() {
    this.setData({
      isShowTrack: true
    })
  },
  // 关闭
  closeTarck() {
    this.setData({
      isShowTrack: false
    })
  },
  onPageScroll(e) {
    if (e.scrollTop >= 150) {
      if (this.data.opcity < 1) {
        this.setData({
          opcity: 1,
        });
      }
    } else {
      const opcity = e.scrollTop / 150;
      this.setData({
        opcity,
      });
    }
  },
  // 打电话
  callPhone() {
    const { coordinateInfo: { courierPhone } } = this.data;
    wx.makePhoneCall({
      phoneNumber: courierPhone,
    });
  },
  // 获取订单轨迹
  async getflowMini(orderNo, orderType) {
    try {
      const data = await httpResource('flowMini', {
        orderNo,
        orderType
      }, null, "GET");
      this.setData({
        flowMiniList: (data || []).map(e => { e.date = this.getflowMiniTime(e.time); return e })
      })
      console.log('轨迹', data);
    } catch (error) {

    }
  },
  // 回显轨迹时间
  getflowMiniTime(time) {
    time = parseInt(Number(time) * 1000);
    let date = new Date(time);
    let month = date.getMonth() + 1;
    month = month > 9 ? month : `0${month}`;
    let day = date.getDate() + 1;
    let hours = date.getHours();
    hours = hours > 9 ? hours : `0${hours}`;
    let min = date.getMinutes();
    min = min > 9 ? min : `0${min}`;
    return `${month}-${day} ${hours}:${min}`

  },
  getCourierCoordinateTime(time) {
    let date = new Date(+time);
    let hours = date.getHours();
    hours = hours > 9 ? hours : `0${hours}`;
    let min = date.getMinutes();
    min = min > 9 ? min : `0${min}`;
    return `${hours}:${min}`
  },
  // 获取骑手坐标信息
  async getCourierCoordinate(orderId, shopCode) {
    try {
      const data = await httpResource('courierCoordinate', { shopCode, orderId }, null, "GET");
      if (data) {
        console.log('成功', data)
        let markers = [];
        if (data.courierLocation) {
          markers.push({
            latitude: +data.courierLocation.latitude,
            longitude: +data.courierLocation.longitude,
            id: 1,
            type: 'rider',
            iconPath: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/order/map-rider.png',
            display: 'ALWAYS',
            customCallout: {
              anchorY: 0,
              anchorX: 20,
              display: 'ALWAYS',
            },
            width: 78,
            height: 43
          })
        }
        if (data.shopLocation) {
          markers.push({
            latitude: +data.shopLocation.latitude,
            longitude: +data.shopLocation.longitude,
            id: 2,
            type: 'store',
            iconPath: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/order/map-store.png',
            width: 41,
            height: 45
          })
        }
        if (data.userLocation) {
          markers.push({
            latitude: +data.userLocation.latitude,
            longitude: +data.userLocation.longitude,
            id: 3,
            type: 'home',
            iconPath: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/order/map-home.png',
            width: 44,
            height: 44
          })
        }
        this.setData({
          coordinateInfo: Object.assign(data, { predictDeliveryTime: this.getCourierCoordinateTime(data.predictDeliveryTime) }),
          markers
        })
      }
      this.setData({
        coordinateTimer: setTimeout(() => {
          this.getCourierCoordinate(orderId, shopCode)
        }, 30000)
      })
    } catch (error) {
      console.log('失败', error)
      this.setData({
        coordinateTimer: setTimeout(() => {
          this.getCourierCoordinate(orderId, shopCode)
        }, 5000)
      })
    }
  }
});