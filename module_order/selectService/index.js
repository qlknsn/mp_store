Page({
  /**
   * 页面的初始数据
   */
  data: {
    orderId: "",
    shopCode: '',
    isAftermarket: '',
    orderTime: '',
    orderStatus: '',
    orderType: '',
  },
  // 退货退款
  salesReturn() {
    let _this = this;
    let { orderStatus, orderType } = _this.data;
    wx.navigateTo({
      url: `/module_order/typeService/index?orderId=${_this.data.orderId}&shopCode=${_this.data.shopCode}&isAftermarket=${_this.data.isAftermarket}&orderTime=${_this.data.orderTime}&orderStatus=${orderStatus}&orderType=${orderType}`,
    })
  },
  // 无需退货
  unsalesReturn() {
    let _this = this
    wx.redirectTo({
      url: '/module_order/refundGoodslist/index?orderId=' + _this.data.orderId + "&shopCode=" + _this.data.shopCode + '&isAftermarket=' + _this.data.isAftermarket,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      orderId: options.orderId,
      shopCode: options.shopCode,
      isAftermarket: options.isAftermarket,
      orderTime: options.orderTime,
      sourceType: options.sourceType,
      orderStatus: options.orderStatus,
      orderType: options.orderType,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})