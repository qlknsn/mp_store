import {
    subscribeMessage,
    getSubState
} from "../../config/subConfig";
const app = getApp();
const {
    httpResource,
} = require('../../assets/js/common.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        background: '#ff0000',
        parameter: {
            'title': '订单列表  ',
            return: 1,
        },
        tabTitleArr: [{
            tabTitle: '全部',
            navIndex: 1
        },
        {
            tabTitle: '待支付',
            navIndex: 4
        },
        {
            tabTitle: '待送达',
            navIndex: 5
        },
        {
            tabTitle: '待取货',
            navIndex: 6
        },
        {
            tabTitle: '待评价',
            navIndex: 2
        },

        ],
        navIndex: 0,
        tabLeft: "20",
        contentHeight: 600,
        orderList: [],
        pageIndex: 1,
        pageCount: 0,
        // 高度
        navbarheight: 0,
        scrollTopNum: 0,
        noMore: false, // 是否到底
        refresher: false, // 下拉刷新,
        navType: 0, //当前类型
        respones: false,
        detailChange: false,
        // 评论成功列表跳转
        evalutionshow: false,

    },
    getStatus(e) {
        console.log(e);
    },
    getHeight() {
        const query = wx.createSelectorQuery();
        query.select('.header-bg').boundingClientRect()
        query.exec((res) => {
            const height = app.globalData.screenHeight - res[0].height - 16;
            this.setData({
                contentHeight: height
            })
            console.log(height)
        })
    },
    //tab切换
    changeTab: function (e) {
        console.log(e)
        this.setData({
            navIndex: e.currentTarget.dataset.index,
            navType: e.currentTarget.dataset.nav,
            pageIndex: 1,
            orderList: [],
            noMore: false,
            respones: false
        })
        this.getOrderList(this.data.pageIndex, e.currentTarget.dataset.nav)
        this.changeline()
    },
    changeline: function () {
        var _this = this;
        const query = wx.createSelectorQuery();
        query.select('.tabTrue').boundingClientRect()
        query.exec(function (res) {
            console.log(res)
            _this.setData({
                tabLeft: _this.data.navIndex == 0 ? (res[0].left + 20) : (res[0].left + 20)
            })
        })
    },
    getOrderList(pageIndex, type) {
        console.log(type);
        let that = this
        var filter
        switch (type) {
            case 5:
                filter = {
                    orderType: type,
                    currentPage: pageIndex,
                    pageSize: 20,
                    sourceType: 4
                };
                break;
            case 6:
                filter = {
                    orderType: type,
                    currentPage: pageIndex,
                    pageSize: 20,
                    sourceType: 3
                };
                break;
            case 0:
                filter = {
                    orderType: 1,
                    currentPage: pageIndex,
                    pageSize: 20,
                };
                break;
            default:
                filter = {
                    orderType: type,
                    currentPage: pageIndex,
                    pageSize: 20,
                };
                break;
        }

        wx.showLoading({
            title: '请求中...',
            mask: true
        });
        console.log(filter);
        httpResource('getAllOrderList', filter, (data) => {
            this.setData({
                evalutionshow: false
            })
            // console.log(data);
            if (data?.records && data.records.length >= 0) {
                wx.hideLoading({
                    success: (res) => { },
                })
                console.log('走这儿')
                if (data.currentPage == 1) {
                    that.setData({
                        orderList: data ? data.records : [],
                        refresher: false,
                    })
                    console.log(this.data.orderList);
                    if (data.total < data.pageSize) {
                        that.setData({
                            noMore: true,
                        })
                    }
                } else {
                    this.setData({
                        orderList: this.data.orderList.concat(data.records),
                        refresher: false,
                    })

                    if (data.records.length == 0) {
                        that.setData({
                            noMore: true,
                        })
                    }
                }

            } else {
                wx.hideLoading({
                    success: (res) => { },
                })
                console.log('走这儿')
                if (data?.currentPage == 1 || data == null) {
                    this.setData({
                        respones: true,
                        refresher: false,
                    })

                } else {
                    if (data) {
                        that.setData({
                            noMore: data.currentPage == 1 ? false : true,
                            refresher: false,
                        })
                    } else {
                        that.setData({
                            noMore: false,
                            refresher: false,
                        })
                    }
                }

            }

        }, "POST", null)
    },
    toDetail({
        currentTarget: {
            dataset: {
                item
            }
        }
    }) {
        console.log(item);
        wx.navigateTo({
            url: `/module_order/orderDetail/orderDetail?id=${item.orderId}`,
        })
    },
    toEnergy() {
        wx.uma.trackEvent("member_medal");
        wx.navigateTo({
            url: '/pages/user/energy/index',
        })
    },
    toEvaluation({
        currentTarget: {
            dataset: {
                orderno
            }
        }
    }) {
        wx.navigateTo({
            url: `/module_order/orderEvaluation/index?orderNo=${orderno}&edit=true`,
        })
    },
    goEvaluationDetail({
        currentTarget: {
            dataset: {
                orderno
            }
        }
    }) {
        wx.navigateTo({
            url: `/module_order/orderEvaluationDetail/index?orderNo=${orderno}`,
        })
    },
    goRefunddetail({
        currentTarget: {
            dataset: {
                hasNoFinishReturnApply,
                refundapplyid,
                refundStatus
            }
        }
    }) {

        // if(hasNoFinishReturnApply){
        wx.navigateTo({
            url: `/pages/user/refundApplyDetail/index?id=${refundapplyid}&return=1`,
        })
        // }
        // if(refundStatus==1){
        //     wx.navigateTo({
        //         url: `/pages/user/refundApplyDetail/index?id=${refundApplyId}&return=3`,
        //     })
        // }

    },
    bindrefresherrefresh() {
        this.setData({
            refresher: true,
            pageIndex: 1,
            noMore: false
        }, () => {
            console.log(this.data.navType)
            setTimeout(() => {
                this.getOrderList(this.data.pageIndex, this.data.navType)
            }, 300)
        })
    },
    scrollToLower() {
        this.data.pageIndex++;
        this.getOrderList(this.data.pageIndex, this.data.navType)
    },
    // 去支付
    async toPayDetail({
        currentTarget: {
            dataset: {
                time,
                orderid,
                sourcetype
            }
        }
    }) {
        // let newTime = Date.parse(new Date())
        // let oldTime = (new Date(time)).getTime()
        wx.showLoading({
            title: '支付中...',
            mask: true
        });
        console.log(orderid);
        this.getCartSettlement(orderid, sourcetype);
        // if (newTime - oldTime > 300000) {
        //     // 超时取消刷新列表
        //     wx.hideLoading();
        //     wx.showToast({
        //         title: '订单超时，已经取消',
        //         icon: 'none',
        //         duration: 2000
        //     });
        //     setTimeout(() => {
        //         this.getOrderList(1, this.data.navType)
        //     }, 2000)

        // } else {
        //     // 可以支付
        //     // 支付操作
        // }
    },

    //打包结算
    async getCartSettlement(orderId, sourcetype) {
        console.log(orderId);
        let params = {
            orderId: orderId
        }
        let data = await httpResource('payOrder', params, null, "post");
        console.log(data);
        wx.hideLoading();
        if (data && data.payInfo != null) {
            wx.requestPayment({
                'timeStamp': data.payInfo.timeStamp,
                'nonceStr': data.payInfo.nonceStr,
                'package': data.payInfo.package,
                'signType': data.payInfo.signType,
                'paySign': data.payInfo.paySign,
                'success': function (res) {
                    if (res.errMsg == 'requestPayment:ok') {
                        wx.showToast({
                            title: '支付成功',
                            success: () => {
                                wx.removeStorageSync('GOODS_PAY_LIST');
                                if (sourcetype == 3) {
                                    wx.redirectTo({
                                        url: '/module_order/pay-successSelf/index?takeCode=' + data.takeCode + "&orderId=" + data.orderId + "&orderNo=" + data.orderNo + "&mobile=" + data.mobile
                                    })
                                } else {
                                    wx.redirectTo({
                                        url: '/module_order/pay-success/index' + "?orderType=" + data.orderType
                                    })
                                }

                            }
                        })
                    }
                },
                'fail': function (err) {
                    wx.showToast({
                        title: '支付失败，请稍后再试！',
                        icon: 'none',
                        duration: 2000,
                        success: () => {
                            wx.removeStorageSync('GOODS_PAY_LIST');
                            setTimeout(() => {
                                wx.redirectTo({
                                    url: `/module_order/orderList/orderList?orderType=4`
                                })
                            }, 2000)
                        }
                    })
                }
            })
        }
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(options);
        // let query = wx.createSelectorQuery();
        // query.select('.navbar').boundingClientRect(rect => {
        //     console.log(rect.height);
        //     this.setData({
        //         navbarheight: rect.height
        //     })
        // }).exec();
        if (options.orderType) {
            this.getOrderList(this.data.pageIndex, Number(options.orderType))
            this.setData({
                navType: options.orderType
            })
            switch (options.orderType) {
                case "0":
                    this.setData({
                        navIndex: 0
                    })
                    break;
                case "4":
                    this.setData({
                        navIndex: 1
                    })
                    break;
                case "5":
                    this.setData({
                        navIndex: 2
                    })
                    break;
                case "2":
                    this.setData({
                        navIndex: 4
                    })
                    break;
                case "6":
                    this.setData({
                        navIndex: 3
                    })
                    break;
            }
            this.changeline()
        } else {
            console.log('初次进来')
            this.getOrderList(this.data.pageIndex, 1)
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function (e) {
        let pages = getCurrentPages();
        let currPage = pages[pages.length - 1]; //当前页面
        console.log(currPage.data.evalutionshow)
        if (currPage.data.evalutionshow) {
            this.setData({
                pageIndex: 1,
                navType: 1,
                navIndex: 0
            })
            this.changeline()
            this.getOrderList(1, 1)
        } else {
            // this.setData({
            //     pageIndex: 1,
            // })
            // this.getOrderList(1, this.data.navType)
        }

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
})