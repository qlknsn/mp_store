const app = getApp();
const {
  httpResource
} = require('../../assets/js/common.js');
const {
  throttl
} = require('../../assets/js/eventUtil');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderNo: null,
    list: [],
    badTagList: [],
    goodTagList: [],
    options: {},
    rateTitleList: ['非常差', '差', '一般', '满意', '超赞'],
    shopName: '',
    shopCode: '',
    storeData: [],
    commentContent: '',
    showMoreGoods: true,
    spliceData: [],
    listLength: 0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    // this.getLabel();
    this.setData({
      options
    })
    this.initData(options);

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  changeGoods() {
    let that = this
    const {
      showMoreGoods
    } = this.data;
    if (!showMoreGoods) {
      this.setData({
        showMoreGoods: true
      })
      let sliceData = that.data.list.slice(0, 3)
      let spliceData = that.data.list.splice(3, that.data.list.length)
      this.setData({
        list: sliceData,
        spliceData
      })
    } else {
      this.setData({
        showMoreGoods: false
      })
      console.log(that.data.list);
      console.log(that.data.spliceData);
      this.setData({
        list: [...that.data.list, ...that.data.spliceData],
      })
    }
  },
  initData(opt) {
    console.log(opt);
    if (opt.orderNo) {
      this.setData({
        orderNo: opt.orderNo
      }, () => {
        this.getLabel();
        this.getStoreLabel();
      })
    }
  },
  // 门店评价
  tagTapbottom(e) {
    const index = e.currentTarget.dataset.index;
    this.data.goodTagList[index].isActive = !this.data.goodTagList[index].isActive
    this.setData({
      goodTagList: this.data.goodTagList,
    })
  },
  // tag点击事件
  tagTap(e) {
    const {
      list
    } = this.data;
    const arr = JSON.parse(JSON.stringify(list));
    const row = e.currentTarget.dataset.row;
    const index = e.currentTarget.dataset.index;
    const tag = e.currentTarget.dataset.tag;
    arr[row].tagList[index].isActive = !arr[row].tagList[index].isActive
    this.setData({
      [`list[${row}]`]: arr[row],
    })
    if (tag == '写评价') {
      this.setData({
        [`list[${row}].commentContent`]: '',
      })
    }
  },
  // 返回商品列表
  async getData() {
    const {
      orderNo,
      goodTagList,
      badTagList
    } = this.data;
    const data = await httpResource('skuList', {
      orderNo
    }, null, "GET");
    const list = (data || []).map(e => {
      e.ja = false;
      e.animation = false
      e.jian = false;
      e.tagList = []
      return e
    })
    let l = [...list]
    this.setData({
      listLength: l.length
    })
    if (l.length > 3) {
      let sliceData = l.slice(0, 3)
      let spliceData = l.splice(3, l.length)
      this.setData({
        list: sliceData,
        spliceData
      })
    } else {
      this.setData({
        list: l
      })
    }

  },
  // 获取好坏标签
  async getLabel() {
    const data = await httpResource('getCommentLabel', {}, null, "GET");
    if (data && data.length) {
      const goodTagList = data.filter(e => e.commentType === 1).map(item => {
        return {
          label: item.commentLabel,
          isActive: false
        }
      })
      goodTagList.push({
        label: '写评价',
        isActive: false
      });
      const badTagList = data.filter(e => e.commentType === 2).map(item => {
        return {
          label: item.commentLabel,
          isActive: false
        }
      })
      badTagList.push({
        label: '写评价',
        isActive: true
      });
      this.setData({
        goodTagList,
        badTagList
      }, this.getData)
    }
  },
  // 根据状态返回好坏tagList
  getTagList(bol) {
    const {
      badTagList,
      goodTagList
    } = this.data;
    return bol ? goodTagList : badTagList
  },
  // 评价内容赋值
  textAreaInput(e) {
    const value = e.detail.value;
    const index = e.currentTarget.dataset.index;
    this.setData({
      [`list[${index}].commentContent`]: value,
    })
  },
  // 点赞修改事件
  jaChange(e) {
    const {
      list
    } = this.data;
    const index = e.currentTarget.dataset.index;
    const bol = list[index].ja;
    const jian = list[index].jian;
    let tagList = [];
    if (!bol) {
      tagList = this.getTagList(true);
    }
    if (!list[index].ja) {
      this.setData({

        [`list[${index}].animation`]: !list[index].animation,
      })
      setTimeout(() => {
        this.setData({

          [`list[${index}].animation`]: !list[index].animation,
        })
      }, 2000)
    }
    this.setData({
      [`list[${index}].ja`]: !list[index].ja,
      [`list[${index}].jian`]: false,
      [`list[${index}].tagList`]: tagList,
    })

  },
  jianChange(e) {
    const {
      list
    } = this.data;
    const index = e.currentTarget.dataset.index;
    const bol = list[index].jian;
    const ja = list[index].ja;
    let tagList = [];

    if (!bol) {
      tagList = this.getTagList(false);
    }
    console.log(this.data.list[index].animation);
    if (this.data.list[index].animation) {

    } else {
      this.setData({
        [`list[${index}].ja`]: false,
        [`list[${index}].jian`]: !list[index].jian,
        [`list[${index}].tagList`]: tagList,
      })
    }

  },
  // 提交按钮事件
  submit: throttl(async function () {
    const {
      list,
      orderNo,
      storeData,
      commentContent,
      shopName,
      shopCode
    } = this.data;
    let valid = this.validateData(list, storeData, commentContent);
    if (valid.status) {
      let params = Object.assign(this.setParamsData(list, orderNo, storeData, commentContent), {
        shopName,
        shopCode
      });
      console.log(params)
      await httpResource('comment', params, null, "POST");
      console.log(this.data.options.orderNo);
      wx.redirectTo({
        url: `/module_order/orderEvaluationSuccess/index?orderNo=${this.data.options.orderNo}`,
      })
    } else {
      wx.showToast({
        title: valid.msg,
        icon: 'none',
        duration: 2000
      })
    }
  }, 800),
  // 校验数据
  validateData(data, storeData, commentContent) {
    let status = true;
    let msg = '';
    const arr = data.filter(item => item.ja || item.jian);
    if (arr.length) {
      arr.forEach(item => {
        const select = item.tagList.filter(e => e.isActive).map(v => v.label);
        // if (!select.length) {
        //   status = false;
        //   msg = '请选择标签';
        // }
        if (select.includes('写评价')) {
          if (!item.commentContent) {
            status = false;
            msg = "请填写评价内容"
          }
        }
      })
    } else {
      status = false
      msg = '请选择评价'
    }
    const rateArr = storeData.filter(item => item.value > 0);
    if (commentContent || rateArr.length) {
      status = true
      msg = ''
    }
    // if (rateArr.length) {
    //   status = false
    //   msg = '请选择评分'
    // }
    return {
      status,
      msg
    }
  },
  // 设置请求数据
  setParamsData(list, orderNo, storeData, commentContent) {
    let data = JSON.parse(JSON.stringify(list));
    data = data.filter(e => (e.ja || e.jian));
    const skuCommentList = data.map(e => {
      const type = e.ja ? 1 : e.jian ? 2 : ''; // 好坏评价
      let commentLabel = '';
      if (e.tagList) {
        commentLabel = e.tagList.filter(e => e.isActive).map(v => v.label).join(',');
      }
      return {
        inputCode: e.inputCode,
        inputName: e.productName,
        commentType: type,
        commentLabel,
        commentContent: e.commentContent
      }
    })
    storeData = storeData.map(e => {
      if (e.value > 3) {
        e.tagList = e.tagList.map(v => {
          v.isActive = false;
          return v
        })
      }
      return e;
    })
    return {
      orderNo,
      skuCommentList,
      commentContent,
      storeData
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  // 门店
  rateChange(e) {
    const index = e.currentTarget.dataset.index;
    const value = e.detail;
    this.setData({
      [`storeData[${index}].value`]: value
    })
    console.log(value)
  },
  storeTagTap(e) {
    const {
      storeData
    } = this.data;
    const arr = JSON.parse(JSON.stringify(storeData));
    const row = e.currentTarget.dataset.row;
    const index = e.currentTarget.dataset.index;
    const tag = e.currentTarget.dataset.tag;
    arr[row].tagList[index].isActive = !arr[row].tagList[index].isActive
    this.setData({
      [`storeData[${row}]`]: arr[row],
    })
  },
  commentContentInput(e) {
    const value = e.detail.value;
    this.setData({
      commentContent: value,
    })
  },
  // 获取门店数据
  async getStoreLabel() {
    const {
      orderNo
    } = this.data;
    const data = await httpResource('getOrderabel', {
      orderNo
    }, null, "GET");
    if (data) {
      const {
        commentContent,
        shopName,
        shopCode,
        storeData
      } = data;
      this.setData({
        commentContent,
        shopName,
        shopCode,
        storeData: storeData || []
      })
    }
    console.log('门店数据', data)
  },

})