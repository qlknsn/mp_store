const { httpResource } = require('../../assets/js/common.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderTime: '',//七无理由时间
    maxReturnDay: '',//几天过期
    orderDayTime: '',//具体过期时间
    isPermission: false,//是否显示7天无理由退货
    orderId: '',//订单ID
    shopCode: '',//门店code
    isAftermarket: '',
    orderStatus: '',
    orderType: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _this = this;
    _this.setData({
      orderTime: options.orderTime,
      orderId: options.orderId,
      shopCode: options.shopCode,
      isAftermarket: options.isAftermarket,
      orderStatus: options.orderStatus,
      orderType: options.orderType,
    })
  },
  // 七天无理由协议
  goSevenDaysAgreement() {
    wx.navigateTo({
      url: '/module_base/sevenDaysAgreement/index'
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let _this = this;
    _this.getCheckPermission();
  },

  //时间转换
  dayTime(date) {
    let _this = this;
    let { maxReturnDay } = _this.data;
    if (!date) return `--月--后不可享`;
    let dateTime = date.replace(/-/g, "/");
    let time = new Date(dateTime).getTime();
    let atDay = new Date(time + maxReturnDay * 24 * 3600 * 1000);
    _this.setData({
      orderDayTime: `${atDay.getMonth() + 1}月${atDay.getDate()}后不可享`,
    });
  },

  //检查用户无理由退货权限
  async getCheckPermission() {
    let _this = this;
    let { orderTime } = _this.data;
    let data = await httpResource('checkPermission', {}, null, "post");
    if (data) {
      _this.setData({
        isPermission: data.permission,
        maxReturnDay: data.maxReturnDay,
      }, () => {
        _this.dayTime(orderTime);
      });
    }
  },

  //跳转7天无理由退款页面
  routerUrl() {
    let _this = this;
    let { orderStatus } = _this.data;
    wx.navigateTo({
      url: `/module_order/goodsRefund/index?orderId=${_this.data.orderId}&shopCode=${_this.data.shopCode}&isAftermarket=${_this.data.isAftermarket}`,
    })
  },
  //普通退款
  routerGeneral() {
    wx.redirectTo({
      url: '/module_order/storerefundGoods/index',
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})