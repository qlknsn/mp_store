const {
  httpResource,
  accMul
} = require('../../assets/js/common.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderId: null, //订单ID
    orderItem: null, //商品列表
    remainingQuantity: '', //7天无理由剩余能退商品
    refundedQuantityList: [], //已退商品
    newGoodsList: [], //新选商品
    orderNo: '',
    shopCode: '',
    sumTotal: 0, //新的商品数量
    refundedSum: 0, //已退商品数量
    isAftermarket: '',
    refundableSkuQuantity: '',//总体可退
    gradeName: '',//会员等级
    gradeId: '',//会员ID
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _this = this;
    _this.setData({
      orderId: options.orderId,
      shopCode: options.shopCode,
      isAftermarket: options.isAftermarket,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let _this = this;
    let { gradeName, gradeId } = wx.getStorageSync('GRADE');
    _this.setData({
      gradeName: gradeName,
      gradeId: gradeId,
    }, () => {
      _this.getOrderItemData();
    });

  },

  //获取订单内的无理由退货商品
  async getOrderItemData() {
    let _this = this;
    let sum = 0;
    let { orderId } = _this.data;
    let data = await httpResource('getOrderItem', { orderId: orderId }, null, "get");
    if (data) {
      (data.orderItem || []).forEach((item) => {
        item.isSelect = false;
        item.itemCount = 0;
        item.isYTSelect = false;
        item.totalPrice = accMul(item.sellPrice, item.itemCount);
        //一共退过多少商品
        if (item.refundedQuantity > 0) {
          sum = sum + 1;
          item.isYTSelect = true;
        }
      })
      _this.setData({
        orderItem: data.orderItem,
        refundableSkuQuantity: data.refundableSkuQuantity,
        remainingQuantity: data.remainingQuantity,
        sumTotal: Number(data.refundableSkuQuantity - sum),
        refundedSum: sum,
      });
    }
  },
  //选择商品
  selectGoods(event) {
    let _this = this;
    let { index } = event.currentTarget.dataset;
    //商品规格数量
    let barcode = _this.data.orderItem[index].barcode;
    //实际规格数量
    let canRefundQty = _this.data.orderItem[index].canRefundQty;
    //添加数量
    let itemCount = _this.data.orderItem[index].itemCount;
    //单个sku可退商品数量
    let refundableQuantity = _this.data.orderItem[index].refundableQuantity;
    //单价
    let sellPrice = _this.data.orderItem[index].sellPrice;
    // 可以新的商品退货数量
    if (!_this.data.orderItem[index].isYTSelect) {
      if (_this.data.newGoodsList.length < _this.data.sumTotal) {
        if (_this.data.orderItem[index].isSelect) {
          _this.data.orderItem[index].isSelect = false;
          _this.data.newGoodsList.forEach((item, index) => {
            if (item.barcode == barcode) {
              _this.data.newGoodsList.splice(index, 1);
            }
          })
        } else {
          _this.data.orderItem[index].isSelect = true;
          if (itemCount == 0) {
            if (refundableQuantity > canRefundQty) {
              _this.data.orderItem[index].itemCount = canRefundQty;
              _this.data.orderItem[index].totalPrice = accMul(sellPrice, canRefundQty);
            }
            if (refundableQuantity < canRefundQty) {
              _this.data.orderItem[index].itemCount = refundableQuantity;
              _this.data.orderItem[index].totalPrice = accMul(sellPrice, refundableQuantity);
            }
          }
          if (itemCount > canRefundQty) {
            _this.data.orderItem[index].itemCount = refundableQuantity;
            _this.data.orderItem[index].totalPrice = accMul(sellPrice, refundableQuantity);
          }
          if (itemCount > refundableQuantity) {
            _this.data.orderItem[index].itemCount = canRefundQty;
            _this.data.orderItem[index].totalPrice = accMul(sellPrice, canRefundQty);
          }
          _this.data.newGoodsList.push(_this.data.orderItem[index]);
        }
      } else {
        _this.data.orderItem[index].isSelect = false;
        _this.data.newGoodsList.forEach((item, index) => {
          if (item.barcode == barcode) {
            _this.data.newGoodsList.splice(index, 1);
          }
        })
      }
    }
    //已退商品数量
    if (_this.data.orderItem[index].isYTSelect) {
      if (_this.data.refundedQuantityList.length < _this.data.refundedSum) {
        if (_this.data.orderItem[index].isSelect) {
          _this.data.orderItem[index].isSelect = false;
          _this.data.refundedQuantityList.forEach((item, index) => {
            if (item.barcode == barcode) {
              _this.data.refundedQuantityList.splice(index, 1);
            }
          })
        } else {
          _this.data.orderItem[index].isSelect = true;
          if (itemCount == 0) {
            if (refundableQuantity > canRefundQty) {
              _this.data.orderItem[index].itemCount = canRefundQty;
              _this.data.orderItem[index].totalPrice = accMul(sellPrice, canRefundQty);
            }
            if (refundableQuantity < canRefundQty) {
              _this.data.orderItem[index].itemCount = refundableQuantity;
              _this.data.orderItem[index].totalPrice = accMul(sellPrice, refundableQuantity);
            }
          }
          if (itemCount > canRefundQty) {
            _this.data.orderItem[index].itemCount = refundableQuantity;
            _this.data.orderItem[index].totalPrice = accMul(sellPrice, refundableQuantity);
          }
          if (itemCount > refundableQuantity) {
            _this.data.orderItem[index].itemCount = canRefundQty;
            _this.data.orderItem[index].totalPrice = accMul(sellPrice, canRefundQty);
          }
          _this.data.refundedQuantityList.push(_this.data.orderItem[index]);
        }
      } else {
        _this.data.orderItem[index].isSelect = false;
        _this.data.refundedQuantityList.forEach((item, index) => {
          if (item.barcode == barcode) {
            _this.data.refundedQuantityList.splice(index, 1);
          }
        })
      }
    }
    _this.setData({
      orderItem: _this.data.orderItem,
    });
  },
  //数字减
  minusCount(event) {
    let _this = this;
    let { index } = event.currentTarget.dataset;
    //当前件数
    let itemCount = _this.data.orderItem[index].itemCount;
    if (itemCount > 0) {
      _this.data.orderItem[index].itemCount--;
      _this.data.orderItem[index].totalPrice = accMul(_this.data.orderItem[index].sellPrice, _this.data.orderItem[index].itemCount);
    } else {
      wx.showToast({
        title: '亲～不能再减少了',
        icon: 'none',
        duration: 1500
      })
    }
    _this.setData({
      orderItem: _this.data.orderItem
    });
  },
  //数字加
  addCount(event) {
    let _this = this;
    let { index } = event.currentTarget.dataset;
    let canRefundQty = _this.data.orderItem[index].canRefundQty;
    //当前件数
    let itemCount = _this.data.orderItem[index].itemCount;
    if (itemCount >= canRefundQty) {
      wx.showToast({
        title: '亲～超出可退件数！',
        icon: 'none',
        duration: 1500
      })
    } else {
      _this.data.orderItem[index].itemCount++;
      _this.data.orderItem[index].totalPrice = accMul(_this.data.orderItem[index].sellPrice, _this.data.orderItem[index].itemCount);
    }
    _this.setData({
      orderItem: _this.data.orderItem
    });
  },
  //确认提交
  async submitOk() {
    let _this = this;
    let arr = []
    let { newGoodsList, refundedQuantityList } = _this.data;
    let goodsList = [...newGoodsList, ...refundedQuantityList];
    if (goodsList.length != 0) {
      goodsList.forEach(item => {
        if (item.isSelect) {
          arr.push({
            barcode: item.barcode,
            quantity: item.itemCount
          })
        }
      })
      const params = {
        orderId: _this.data.orderId,
        orderNo: _this.data.orderNo,
        refundItemList: arr,
        shopCode: _this.data.shopCode
      }
      const data = await httpResource('refundConfirm', params);
      if (data) {
        data.shopCode = _this.data.shopCode
        data.isAftermarket = _this.data.isAftermarket
        wx.setStorage({
          key: "refundGoodInfo",
          data: data,
          complete: () => {
            wx.redirectTo({
              url: `/module_order/refund-apply/index?orderId=${_this.data.orderId}&orderNo=${_this.data.orderNo}&state=QI_TIAN_WU_LI_YOU`,
            })
          }
        })
      }
    } else {
      wx.showToast({
        title: '请勾选要退款的商品!',
        icon: 'none',
        duration: 1500
      })
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})