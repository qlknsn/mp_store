// pages/user/orderEvaluationSuccess/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderNo:null
  },
  goHome(){
    wx.reLaunch({
      url:'/pages/index/index'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      orderNo:options.orderNo
    })
    let pages = getCurrentPages()
    console.log(pages);
    let page = pages.filter(item=>{
      return item.route=='module_order/orderList/orderList'
    })
    let page2 = pages.filter(item=>{
      return item.route=='module_order/orderDetail/orderDetail'
    })
    console.log(page2);
    if(page2.length>0){
      page2[0].setData({
        evalutionshow:true
      })
    }
    page[0].setData({
      evalutionshow:true
    })
    if(options.orderNo){
      this.setData({
        orderNo:options.orderNo
      })
    }
  },
  goDetail(){
    const { orderNo } = this.data;
    wx.redirectTo({
      url: `/module_order/orderEvaluationDetail/index?orderNo=${orderNo}`,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  onUnload: function () {
      
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})