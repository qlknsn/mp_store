// pages/pay-successSelf/index.js
import { createBarcode, createQrcode } from '../../assets/js/codeUtil';
const { pxTorpx } = require('../../assets/js/common.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        takeCode: '',
        orderId: '',
        orderNo: null,
        mobile: null,
    },
    routerOrder() {
        let that = this
        wx.reLaunch({
            url: `/module_order/orderDetail/orderDetail?id=${that.data.orderId}`
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(options);
        this.setData({
            takeCode: options.takeCode,
            orderId: options.orderId,
            orderNo: options.orderNo,
            mobile: options.mobile,
        })
        createQrcode('qrcode', options.takeCode, pxTorpx(260), pxTorpx(260));
        createBarcode('barcode', options.takeCode, pxTorpx(500), pxTorpx(194));
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
})