const {
  httpResource,
  accMul
} = require("../../assets/js/common.js");
// const { httpResource } = require("../../assets/js/common.js");
import {
  throttl
} from '../../assets/js/eventUtil.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsList: [],
    selectType: 2, //选择type 2 正常多选 3 全选 
    isSelectAll: false, //全选状态
    selectedItemIds: [], //选中的购物车条目ids selectType = 2 时必传
    isOverallSelect: false, //全局管理,
    orderId: '',
    orderNo: '',
    shopCode: '',
    isAftermarket: ''
  },
  // 获取可退款商品列表
  async getGoodsList(orderId) {
    try {
      const data = await httpResource('refundProductList', {
        orderId
      }, null, 'GET');
      console.log(data)
    } catch (error) { }
  },
  //单选选择
  selectGoods(event) {
    let _this = this;
    let {
      index
    } = event.currentTarget.dataset;
    let status = _this.data.goodsList[index].status;
    let isOverallSelect = _this.data.isOverallSelect;
    _this.data.goodsList[index].isSelect = !_this.data.goodsList[index].isSelect;
    _this.data.selectedItemIds = [];
    let isSelectAll = _this.data.goodsList.every(item => item.isSelect);
    _this.data.selectType = 2;
    _this.data.goodsList.forEach((item) => {
      if (item.isSelect) _this.data.selectedItemIds.push(item.cartItemId);
    })
    _this.setData({
      goodsList: _this.data.goodsList,
      isSelectAll: isSelectAll
    }, () => {
      // if (!_this.data.isOverallSelect) _this.getCartCalculate();
    });

  },
  //input添加商品
  InputRemark(event) {
    let _this = this;
    let {
      index
    } = event.currentTarget.dataset;
    let {
      value
    } = event.detail;
    let {
      canRefundQty
    } = _this.data.goodsList[index];
    if (value != 0 && value <= canRefundQty) {
      _this.data.goodsList[index].itemCount = value;
      _this.data.goodsList[index].totalPrice = accMul(_this.data.goodsList[index].sellPrice, _this.data.goodsList[index].itemCount);
      _this.setData({
        goodsList: _this.data.goodsList,
      });
    } else {
      _this.setData({
        goodsList: _this.data.goodsList
      });
    }
  },
  //全选
  tapSelectAll() {
    let _this = this;
    let isOverallSelect = _this.data.isOverallSelect;
    _this.data.selectedItemIds = [];
    _this.data.selectType = 2;
    _this.data.isSelectAll = !_this.data.isSelectAll;
    _this.data.goodsList.forEach((item) => {
      item.isSelect = _this.data.isSelectAll ? true : false;

      if (item.isSelect) _this.data.selectedItemIds.push(item.cartItemId);
    })
    _this.setData({
      isSelectAll: _this.data.isSelectAll,
      goodsList: _this.data.goodsList
    }, () => {
      // if (!_this.data.isOverallSelect) _this.getCartCalculate();
    });
  },
  //数字加
  addCount(event) {
    let _this = this;
    let {
      index
    } = event.currentTarget.dataset;
    if (_this.data.goodsList[index].itemCount < _this.data.goodsList[index].canRefundQty) {
      let {
        packageCode,
        categoryOneCode,
        itemCount
      } = _this.data.goodsList[index];
      itemCount = ++itemCount;
      // _this.getCartAdd(packageCode, categoryOneCode, itemCount).then(() => {
      _this.data.goodsList[index].itemCount = itemCount;
      _this.data.goodsList[index].totalPrice = accMul(_this.data.goodsList[index].sellPrice, itemCount);
      _this.setData({
        goodsList: _this.data.goodsList
      });
      // })
    }
  },
  //数字减
  minusCount(event) {
    let _this = this;
    let {
      index
    } = event.currentTarget.dataset;
    if (_this.data.goodsList[index].itemCount > 1) {
      let {
        packageCode,
        categoryOneCode,
        itemCount
      } = _this.data.goodsList[index];
      itemCount = --itemCount;
      _this.data.goodsList[index].itemCount = itemCount;
      _this.data.goodsList[index].totalPrice = accMul(_this.data.goodsList[index].sellPrice, itemCount);
      _this.setData({
        goodsList: _this.data.goodsList
      });
    }
  },
  //加购物车
  getCartAdd(packageCode, categoryOneCode, itemCount) {
    return new Promise(async (resolve, reject) => {
      // let _this = this;
      // // let shopCode = app.globalData.currentStore.shopCode;
      // let parasm = JSON.parse(JSON.stringify({
      //     shopCode,
      //     packageCode,
      //     categoryOneCode,
      //     itemCount,
      //     addType: 1
      // }));
      // await httpResource('cartAdd', parasm, null, "post");
      // resolve()
      // let num = await httpResource('cartCount', {
      //     shopCode
      // });
      // app.globalData.cartNum = num;
      // _this.getCartCalculate();
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    // refundProductList
    this.setData({
      orderId: options.orderId,
      shopCode: options.shopCode,
      isAftermarket: options.isAftermarket
    })

  },
  // 退款确认
  refundConfirm: throttl(async function () {
    let goodsSelected = this.data.goodsList.filter(item => {
      return item.isSelect;
    })
    if (goodsSelected.length > 0) {
      let arr = []
      goodsSelected.forEach(item => {
        arr.push({
          barcode: item.barcode,
          quantity: item.itemCount
        })
      })
      const params = {
        orderId: this.data.orderId, //this.data.orderId,
        orderNo: this.data.orderNo, //this.data.orderNo,
        refundItemList: arr,
        shopCode: this.data.shopCode
      }
      console.log(params);
      const data = await httpResource('refundConfirm', params)
      data.shopCode = this.data.shopCode
      data.isAftermarket = this.data.isAftermarket
      wx.setStorageSync("refundGoodInfo", data);
      wx.redirectTo({
        url: `/module_order/refund-apply/index?orderId=${this.data.orderId}&orderNo=${this.data.orderNo}`,
      })
    } else {
      wx.showToast({
        title: '请勾选要退款的商品',
        icon: 'none',
        duration: 1500
      })
    }
  }, 600),
  goodsMap(arr) {
    let newArr = [];
    arr.forEach(item => {
      newArr.push({
        detailId: item.id,
        payableAmount: item.id,
        barcode: item.barcode,
        productId: item.productId,
        productName: item.productName,
        productIcon: item.productIcon,
        productQpc: item.productQpc,
        productUnit: item.id,
        quantity: item.id,
        sellPrice: item.id,
        discountPrice: item.id,
        returnAmount: item.id,
        discountAmount: item.id,
        isAllRefundFlag: item.id,
      })
    })
    return newArr;
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      isSelectAll: false
    })
    httpResource("refundProductList", {
      orderId: this.data.orderId,
      shopCode: this.data.orderId,
    }, (data) => {
      console.log(data);
      (data.goodsList || []).forEach(item => {
        item.isSelect = false
        if (item.canRefundQty == 0) {
          item.itemCount = 0
          item.totalPrice = 0
        } else {
          item.itemCount = item.canRefundQty
        }
        item.totalPrice = accMul(item.sellPrice, item.itemCount);
      })
      this.setData({
        goodsList: data.goodsList,
        // orderId: data.orderId,
        orderNo: data.orderNo
      })
    }, "GET", null)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})