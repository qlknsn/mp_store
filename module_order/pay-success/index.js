Page({

  /**
   * 页面的初始数据
   */
  data: {
    payType: 0,
    orderType: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      payType: options.payTape,
      orderType: options.orderType,
      deliveryType: options.deliveryType,
    })
  },
  //跳转支付成功
  routerOrder() {
    if (this.data.orderType == 4 && this.data.deliveryType == 0) {
      wx.redirectTo({
        url: `/module_order/orderList/orderList?orderType=5`
      })
    } else {
      wx.redirectTo({
        url: `/module_order/orderList/orderList?orderType=1`
      })
    }

    // }

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})