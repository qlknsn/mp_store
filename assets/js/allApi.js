module.exports.API = {
  mpLogin: "act/user/wx/login", // 登录 ------>曹嘉兴的新接口
  register: "act/user/wx/register", // 注册 ------>曹嘉兴的新接口
  modifyInfo: "act/user/wx/info/modify", // 修改用户信息 --------->曹嘉兴的新接口
  // 会员中心轮播
  receivingNoteList: "tdc/v1/online/receivingNoteList", //待取货轮播列表

  getAllOrderList: "tdc/v1/order/es/list", //查询所有订单
  orderListCount: "tdc/v1/order/es/count", // 根据类型查询订单数量
  queryOrderDetail: "tdc/v1/order/es/info", //订单详情
  goodsDetails: 'tdc/v2/online/info',//订单详情新接口
  // goodsDetails: 'tdc/v1/order/es/info',//订单详情旧接口
  // wxPayOpen: "pay/api/app/ver1_0/offlinePayView/open", //获取微信支付签名
  wxPayOpen: "member/wechat/mini/getPayCodeSign", //获取微信支付签名

  getRefundList: "tdc/v1/order/es/refund/list", //退货订单列表查询
  queryRefundDetail: "tdc/v1/order/es/refund/info", //退货单详情查询
  refundApplyList: "tdc/v1/online/order/refund/list", //退货申请列表查询
  refundApplyAdd: "tdc/v1/online/order/refund/apply", //退货申请
  refundApplyDetail: "tdc/v1/online/order/refund/info", //退货申请详情查询
  cancelRefund: "tdc/v1/online/order/refund/cancel", //取消退款
  cancelOrder: "tdc/v1/online/cancelOrder",
  convertPickup: "tdc/v1/online/convertPickup", //转自提

  // 退款-选择商品列表
  refundProductList: 'tdc/v1/online/can/refund/productList',
  orderInfo: "member/shop/order/getOrderByChannel", // 查询订单金额
  addInvoice: "tdc/posInvoice/addInvoice", // 申请开票
  ivvoiceList: "member/user/invoice/title", // 历史发票列表
  removeInvoice: "member/user/title/remove", // 删除历史发票

  // 会员
  selectMemberChannel: 'member/member/selectMemberChannel', // 查询用户是否存在
  userRegister: 'member/member/register', // 注册信息
  login: 'member/member/login', // 登录
  getUserInfo: 'member/member/getUserInfo', // 查询用户信息
  saveMemberExtendInfo: 'member/extend/saveMemberExtendInfo', // 保存用户扩展信息
  // selectEnergyLog:'member/member/selectEnergyLog', // 能量明细
  selectEnergyLog: "member/miniApp/member/findMemberEnergyLog", // 能量明细
  findMemberEnergy: "member/miniApp/member/findMemberEnergy", // 用户总能量
  userMergeInfo: "member/member/selectUserMergeInfoVO", // 首页信息接口
  couponList: "member/coupon/selectUserCouponList", // 平台券列表
  thirdPartyCouponList: "member/coupon/getUserThirdPartyCouponList", // 第三方券列表
  couponDetail: "member/coupon/getSingleUserCoupon", // 优惠券详情
  receiveMedal: "member/miniApp/member/claimMemberMedal", // 领取勋章
  findMemberMedal: "member/miniApp/member/findMemberMedal", // 勋章列表
  getNextMedal: "member/miniApp/member/getNextMedal", // 获取下一等级勋章
  getShopByRange: "member/miniApp/shop/getShopByRange", // 附近门店
  getShopByRangeWithPage: "member/miniApp/shop/getShopByRangeWithPage", // 附近门店
  getShopByKeywords: "member/miniApp/shop/getShopByKeywords", // 模糊查询
  getShopByKeywordsWithPage: "member/miniApp/shop/getShopByKeywordsWithPage", // 模糊查询
  // 订阅消息
  getTemplateList: "msgc/sm/selectSubscribeSceneList", // 查询订阅消息列表
  sendMessage: "msgc/msg/sendMessage", // 订阅成功后 通知订阅
  clickMsg: "msgc/msg/clickMsg", // 点击消息
  // 订单评价
  skuList: "provider-base-odc/order/comment/sku/list", // 查询商品+标签:get
  comment: "provider-base-odc/order/comment", // 新增评价:post
  getCommentLabel: "provider-base-odc/order/comment/label", // 查询评价标签
  getOrderabel: "provider-base-odc/order/comment/site/label", // 查询门店评价标签

  miniappPage: "member/miniappPage/pages", // 查询配置页面
  // 商品上新
  newGoods: "mall/shop/newGood/list",
  getPopup: "member/popup/getPopup", // 首页弹窗
  // 新到货商品
  allGoods: "mall/shop/allGood/list", // 获取门店所有在售商品
  favorite: "mall/member/favorite", // 喜欢
  unFavorite: "mall/member/unFavorite", // 取消喜欢
  favoriteList: "mall/member/goodsList", // 获取喜欢的商品
  // 重构商品
  sendHome: "mall/mallGoods/sendHome", //配送到家分页接口
  dayGoods: "mall/mallGoods/new", //今日新品 昨日上新 分页接口
  categoryAll: "mall/category/childrenList", // 分类 根据code传入
  categoryAll2: "mall/category/childrenListWithInventory", // 分类 根据code传入
  clearFavorite: "mall/member/clearFavorite", // 清空喜欢列表
  deliverableAddress: "mall/shop/deliverableAddress", // 查询可配送货地址
  favoriteGoodsCount: "mall/member/count", // 喜欢商品数量
  addressDefault: "provider-base-member/user/address/default", // 默认地址
  checkAddress: 'mall/user/address/check', // 校验门店是否在配送范围
  // 社群
  getShopCommunityCode: "member/miniApp/shop/getShopCommunityCode", // 查找门店社群码
  // 查找门店码
  getCommunityManagerCode: "member/miniApp/shop/getCommunityManagerCode",

  //购物车
  cartList: "mall/shopping/cart/list", //查询购物车列表
  cartAdd: "mall/shopping/cart/add", //加购物车
  cartDelete: "mall/shopping/cart/delete", //删除购物车商品
  cartCalculate: "mall/shopping/cart/calculate", //计算金额 每次选中购物车商品调用
  cartCount: "mall/shopping/cart/count", //查询购物车总条数
  cartSettlement: "mall/shopping/cart/settlement", //打包结算
  cartAdd2: "mall/shopping/cart/addv2", //添加购物车
  cartAddHome: "mall/shopping/cart/addvHome", //首页 添加购物车


  // 地址管理
  addAddress: "provider-base-member/user/address/add", // 添加地址
  addressList: "mall/user/address/list", // 地址列表
  addDelete: "provider-base-member/user/address/delete", // 删除
  addDeleteV2: "provider-base-member/user/address/remove",
  setAddrDefault: "provider-base-member/user/address/setDefault", // 设置默认地址
  addrDetail: "provider-base-member/user/address/one", // 地址详情
  updateAddr: "provider-base-member/user/address/update", // 更新地址信息

  //确认订单支付
  onlineSubmitOrder: "mall/v1/online/submitOrder", //提交订单
  queryNearbyAddress: "mall/user/address/queryNearbyAddress",//查询附近地址

  // 订单支付
  payOrder: "tdc/v1/online/payOrder",
  calculateOnlineOrderFee: "mall/v1/online/calculateOnlineOrderFee", //计算线上订单费用,打包费和配送费

  // 商品详情 商品分享
  goodsInfo: "mall/mallGoods/info",
  shareNum: "mall/share", //分享统计
  searchHistoryList: "mall/search/list", // 用户历史搜索记录
  searchHistorySave: "mall/search/save", // 保存搜索词
  searchHistoryDelete: "mall/search/delete", // 清除
  searchGoods: "mall/mallGoods/search", // 搜索商品


  // 上传图片 simpleUpload?code=shopOrderRefund
  uploadImage: "support/oss/api/upload",

  // 退款相关
  refundProductList: 'tdc/v1/online/can/refund/productList', // 可退款商品列表 get

  // 检查是否可退款
  checkCanRefund: 'tdc/applet/refund/checkCanRefund',

  refundConfirm: 'tdc/applet/refund/confirm', // 可退款确认 post
  refundOrderDetail: 'tdc/v1/order/business/order/refund/details', // 订单退款详情  get
  singleDetail: 'tdc/applet/refund/goods/one/detail', //单个退款商品详情
  refundDetail: 'tdc/applet/refund/order/refund/details', //退款订单详情
  refundSubmit: 'tdc/applet/refund/submit', // 退款申请

  // 获取预约时间
  getExpectDateTime: 'mall/shopping/cart/expectDateTime',
  flowMini: 'tdc/online/order/flow/record/mini', // 小程序端物流轨迹
  courierCoordinate: 'tdc/v1/delivery/deliveryCoordinate',// 骑手坐标信息

  //邀请有礼
  generateMiniQRCode: 'member/wechat/mini/generateMiniQRCode',//获取太阳码
  getActImgById: 'member/invite/gift/getActInfoById',//根据id查询活动图片
  getMyInviteRecordTotal: 'member//invite/gift/getMyInviteRecordTotal',//我的邀请-总数量
  getMyInvite: 'member/invite/gift/getMyInvite',//我的邀请-邀请明细
  // 获取小程序Banner
  listShopRangeModule: 'member/miniappPage/listShopRangeModule',


  // 抽奖活动
  checkInit: 'member/lottery/check/init',//进首页初始化检测
  winningRecord: 'member/lottery/winning/record',//中奖信息查询
  doLottery: 'member/lottery/doLottery',//抽奖
  getlotteryInfo: 'member/lottery/get/info',// 获取抽奖活动信息
  lotteryUsableCount: 'member/lottery/usable/count',// 获取抽奖次数
  doTask: 'member/lottery/do/task',//完成任务回调
  getWinninginfo: 'member/lottery/get/winning/info',//获取跑马灯信息
  getaskInfo: 'member/lottery/get/task/info',//获取任务信息
  getShareImgs: 'member/lottery/get/share/imgs',//分享图片
  generateMiniQRCode: 'member/wechat/mini/generateMiniQRCode',//生成太阳码

  // 九宫格抽奖






  // 公共字典
  dictList: 'mall/dictionary/dictList',
  // 惊喜礼卡
  balanceBind: 'wallet/member/card/bind', // 绑定卡 post
  authCode: 'wallet/member/balance/authCode', // 支付码 get
  balance: 'wallet/member/balance/query', // 查询会员余额 get
  balanceInfo: 'wallet/member/balance/usable/info', // 查询会员惊喜卡余额有效明细 get
  balanceUnInfo: 'wallet/member/balance/unusable/info', // 查询会员惊喜卡余额无效分页明细 get
  balanceFlow: 'wallet/member/balance/flow', // 查询会员余额流水 get
  balanceAdd: 'mall/buycardinfo/add', // 惊喜礼卡申请 post

  // 领券活动
  actInfo: 'member/coupon/act/actInfo',//查询活动明细信息
  showCoupon: 'member/coupon/act/couponShow',//小程序，优惠券页面是否展示
  popCoupon: 'member/coupon/act/popCoupon',//领券页面弹框
  receiveCoupon: 'member/coupon/act/receiveCoupon',//领取优惠券

  // 会员体系
  signList: 'member/sign/list', // 查询当月与上月签到详情
  signIn: 'member/sign/signIn', // 签到
  saveShareRecord: 'member/share/saveShareRecord', // 分享记录保存
  queryBymember: 'member/member/grade/queryByUserId', // 查询当前会员等级相关信息
  gradeInfo: 'member/grade/gradeInfo', // 查询等级列表
  growInfo: 'member/grade/growInfo', // 查询成长值互动列表
  listIconByShopRangeModule: 'member/miniappPage/listIconByShopRangeModule', // 获取小程序栏目位

  //七天无理由退货
  checkPermission: 'tdc/applet/refund/without/checkPermission',//检查用户无理由退货权限
  getOrderItem: 'tdc/applet/refund/without/getOrderItem',//获取订单内的无理由退货商品
  withoutSubmit: 'tdc/applet/refund/without/submit',//无理由退货提交
};