import { Subtr } from "./common.js";
const couponTypeList = [
  { label: "满减券", value: 2 },
  { label: "满赠券", value: 14 },
  // { label: '折扣券', value: 2 },
  { label: "立减券", value: 3 },
  { label: "赠品券", value: 14 },
  { label: "实物券", value: 11 },
  { label: "第三方权益券", value: 12 },
];
const setCouponDetailInfo = (data) => {
  const { discountType, discountValue } = data;
  let newData = { ...data };
  let valueData = {};
  try {
    valueData = JSON.parse(discountValue);
  } catch (error) {
    valueData = {};
  }
  const { overAmount, discount, amount, goodsPic, pic, goodsName, barcode } =
    valueData;
  // newData.cUrl = 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/coupon/coupon-base.png';
  newData.cPrice = "";
  newData.startTime = newData.startTime.substring(0, 10).replace(/-/g, ".");
  if(newData.consumeTime){
    newData.consumeTime = newData.consumeTime.substring(0, 10).replace(/-/g, ".");
  }
  
  newData.endTime = newData.endTime.substring(0, 10).replace(/-/g, ".");
  newData.code = discountType
  if (discountType == 2) {
    newData.cUrl =
      "https://img.hotmaxx.cn/web/common/mp-store/member2/user/full-reduction.png";
    newData.typeName = "满减券";
    
    newData.cPrice = discount;
  }
  if (discountType == 3) {
    newData.cUrl =
      "https://img.hotmaxx.cn/web/common/mp-store/member2/user/pendant.png";
    newData.typeName = "立减券";
    newData.cPrice = amount;
  }
  if (discountType == 11) {
    newData.typeName = "实物券";
    newData.cUrl = goodsPic;
  }
  if (discountType == 14) {
    newData.typeName = "赠品券";
    newData.cUrl =  "https://img.hotmaxx.cn/web/common/mp-store/member2/user/pendant.png";
  }
  if (discountType == 12) {
    newData.typeName = "第三方权益券";
    newData.cUrl = pic;
  }
  if (discountType == 14) {
    newData.typeName = "满赠券";
    newData.cUrl = "https://img.hotmaxx.cn/web/common/mp-store/member2/user/pendant.png";
  }
  return newData;
};
//扫码进入字符串截取
const getQuery = function (locationUrl, key) {
  if (!locationUrl) return "";
  var locationUrl = locationUrl.indexOf('?') > -1 ? locationUrl.split('?')[1] : locationUrl;
  var arr = locationUrl.split("&");
  for (var i = 0; i < arr.length; i++) {
    var ar = arr[i].split("=");
    if (ar[0] == key) {
      return ar[1];
    }
  }
  return "";
};

//查看你的授权状态
const getUserLocation = () => {
  return new Promise((reslove, reject) => {
    wx.getSetting({
      success: (res) => {
        if (
          res.authSetting["scope.userLocation"] != undefined &&
          res.authSetting["scope.userLocation"] != true
        ) {
          // wx.showModal({
          //   title: '请求授权当前位置',
          //   content: '需要获取您的地理位置，请确认授权',
          //   success: function (res) {
          //     if (res.cancel) {
          //       wx.showToast({
          //         title: '拒绝授权',
          //         icon: 'none',
          //         duration: 1000
          //       })
          //     } else if (res.confirm) {
          //       wx.openSetting({
          //         success: function (dataAu) {
          //           if (dataAu.authSetting["scope.userLocation"] == true) {
          //             // wx.showToast({
          //             //   title: '授权成功',
          //             //   icon: 'success',
          //             //   duration: 1000
          //             // })
          //             //再次授权，调用wx.getLocation的API
          //             // return userLocation();
          //             reslove(userLocation());
          //           } else {
          //             // wx.showToast({
          //             //   title: '授权失败',
          //             //   icon: 'none',
          //             //   duration: 1000
          //             // })
          //           }
          //         }
          //       })
          //     }
          //   }
          // })
          reslove(userLocation());
        } else if (res.authSetting["scope.userLocation"] == undefined) {
          //调用wx.getLocation的API
          // return userLocation();
          reslove(userLocation());
        } else {
          //调用wx.getLocation的API
          // return userLocation();
          reslove(userLocation());
        }
      },
    });
  });
};
// 校验 是否授权定位
const checkLocation = async () => {
  return new Promise((reslove, reject) => {
    wx.getSetting({
      success: function (dataAu) {
        if (dataAu.authSetting["scope.userLocation"] == true) {
          reslove(true);
        } else {
          reslove(false);
        }
      },
    });
  });
};
//获取用户定位信息
const userLocation = () => {
  return new Promise((reslove, reject) => {
    wx.getLocation({
      type: "gcj02",
      success(res) {
        reslove(res);
      },
      fail(err) {
        if (err.errMsg.indexOf("频繁调用会增加电量") == -1) {
          // wx.showToast({
          //   icon: 'none',
          //   title: '未授权定位信息,请授权'
          // })
        }
        reject(err);
      },
    });
  });
};
// 获取配置页面详情数据
const setModulesData = (list) => {
  return list.map((e) => {
    const { jsonContent, ...newData } = e;
    const { data } = jsonContent;
    if (Array.isArray(data)) {
      newData.data = data;
    } else {
      if (data) {
        for (let key in data) {
          newData[key] = data[key];
        }
      }
    }
    return newData;
  });
};
// 公共跳转
// 低碳/新增模块点击事件
const commonTap = (e) => {
  const { type, url, appid } = e.currentTarget.dataset;
  // 无
  if (type === 0) {
    return;
  }
  // 内部页面
  if (type === 1) {
    wx.navigateTo({
      url,
    });
    return;
  }
  // 外部小程序
  if (type === 2) {
    wx.navigateToMiniProgram({
      appId: appid,
      // path:url
    });
    return;
  }
  // H5
  if (type === 3) {
    wx.navigateTo({
      url: `/pages/webview/index?url=${url}`,
    });
    return;
  }
};
// 搜索高亮
function getHighlightStrArray(content, key, result) {
  if (result == undefined) {
    result = [];
  }

  key = key.toUpperCase();
  let keyLen = key.length;
  let tmp = content.toUpperCase();

  if (tmp.length >= keyLen && keyLen > 0) {
    let index = -1;
    index = tmp.indexOf(key);

    if (index != -1) {
      let text = content.substring(0, index);
      result.push({
        type: 2,
        text: text,
      });
      let keyText = content.substring(index, index + keyLen);
      result.push({
        type: 1,
        text: keyText,
      });
      content = content.substring(index + keyLen, content.length);
      getHighlightStrArray(content, key, result);
    } else {
      result.push({
        type: 2,
        text: content,
      });
    }
  } else {
    result.push({
      type: 2,
      text: content,
    });
  }
  return result;
}
function getImgUrl(url, num = 400) {
  if (url.indexOf("?") > -1) {
    return url;
  }
  if (url.indexOf("m//") > -1) {
    url = url.replace("m//", "m/");
  }
  if (url.indexOf("n//") > -1) {
    url = url.replace("n//", "n/");
  }
  if (url.indexOf("aliyuncs.com") > -1 || url.indexOf("hotmaxx.cn") > -1) {
    return `${url}?x-oss-process=image/resize,w_${num}/quality,q_80`;
  }
}
// 小数补0
function toDecimal2(x) {
  var f = parseFloat(x);
  if (isNaN(f)) {
    return false;
  }
  var f = Math.round(x * 100) / 100;
  var s = f.toString();
  var rs = s.indexOf(".");
  if (rs < 0) {
    rs = s.length;
    s += ".";
  }
  while (s.length <= rs + 1) {
    s += "0";
  }
  return s;
}
// 处理商品数据
function getProductDetail(row) {
  for (let key in row) {
    if (row[key] === null) {
      row[key] = "";
    }
    row.difference = "";
    row.priceInteger = "";
    row.priceRight = "";
    if (row.salePrice && row.marketPrice) {
      row.difference = Subtr(Number(row.marketPrice), Number(row.salePrice))
    }
    if (row.salePrice) {
      const price = (row.salePrice + "").split(".");
      row.priceInteger = price[0];
      if (price[1]) {
        row.priceRight = `.${price[1]}`;
      }
    }
    row.image = "";
    if (row.coverImage) {
      const arr = row.coverImage.split(",");
      row.image = getImgUrl(arr[0]);
    }
  }
  // row.id = index;
  return row;
}
// 会员成长体系
function setSharePagePath(path, app, name) {
  if (!name) name = path;
  const shareUserId = app.globalData.userInfo.userId;
  if (shareUserId) {
    if (path.indexOf('?') > -1) {
      path = `${path}&shareUserId=${shareUserId}&sharePage=${name}`
    } else {
      path = `${path}?shareUserId=${shareUserId}&sharePage=${name}`
    }
  }
  return path;
}


module.exports = {
  couponTypeList: couponTypeList,
  setCouponDetailInfo: setCouponDetailInfo,
  getQuery: getQuery,
  getUserLocation: getUserLocation,
  checkLocation: checkLocation,
  setModulesData: setModulesData,
  commonTap: commonTap,
  getHighlightStrArray: getHighlightStrArray,
  getProductDetail: getProductDetail,
  toDecimal2: toDecimal2,
  getImgUrl: getImgUrl,
  setSharePagePath: setSharePagePath
};
