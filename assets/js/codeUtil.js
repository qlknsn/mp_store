import qrcode from './weapp.qrcode.min.js';
import drawQrcode from './weapp.qrcode.min2.js';
import barcode from './barcode';

// 插件内部是根据width, height参数的rpx值来进行绘画
// 把数字转换成条形码
function createBarcode(canvasId, code, width, height) {
  barcode.code128(wx.createCanvasContext(canvasId), code, width, height);
}

// 把数字转换成二维码
function createQrcode(canvasId, code, width, height) {
  qrcode({
    width: width,
    height: height,
    canvasId: canvasId,
    text: code,
    _this: this
  })
}
// 生成canvas 2d 码
function createQrcode2(canvasId, code, width, height, page) {
  const query = wx.createSelectorQuery().in(page)
  console.log(query.select(`#${canvasId}`));
  query.select(`#${canvasId}`)
    .fields({
      node: true,
      size: true
    })
    .exec((res) => {
      console.log(res);
      if (res[0]) {
        var canvas = res[0].node

        // 调用方法drawQrcode生成二维码
        drawQrcode({
          canvas: canvas,
          canvasId: canvasId,
          width: width,
          height: height,
          // padding: 30,
          background: '#ffffff',
          // foreground: '#000000',
          text: code,
        })
      }


      // // 获取临时路径（得到之后，想干嘛就干嘛了）
      // wx.canvasToTempFilePath({
      //     canvasId: 'myQrcode',
      //     canvas: canvas,
      //     x: 0,
      //     y: 0,
      //     width: 260,
      //     height: 260,
      //     destWidth: 260,
      //     destHeight: 260,
      //     success(res) {
      //         console.log('二维码临时路径：', res.tempFilePath)
      //     },
      //     fail(res) {
      //         console.error(res)
      //     }
      // })
    })
}

module.exports = {
  createBarcode,
  createQrcode,
  createQrcode2
}