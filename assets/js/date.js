
/**
 * 日期格式转转时间戳
 * 一般用于倒计时
 */
export function getTimestamp(time) {
    if (time != "") {
        var date = new Date(time.replace(/-/g, '/'));
        return date.getTime();
    }
    return "";
}

/** 
  * 时间戳转化为年 月 日 时 分 秒 
  * number: 传入时间戳 
  * format：返回格式，支持自定义，但参数必须与formateArr里保持一致 
  */
 export function timespampFormat(number, format) {
    // console.log(number, 'number')
    var formateArr = ['Y', 'M', 'D', 'h', 'm', 's']
    var returnArr = []
    var date = new Date(number)
    returnArr.push(date.getFullYear())
    returnArr.push(this.formatNumber(date.getMonth() + 1))
    returnArr.push(this.formatNumber(date.getDate()))

    returnArr.push(this.formatNumber(date.getHours()))
    returnArr.push(this.formatNumber(date.getMinutes()))
    returnArr.push(this.formatNumber(date.getSeconds()))
    console.log(returnArr, 'returnArr')
    for (var i = 0; i < returnArr.length; i++) {
      format = format.replace(formateArr[i], returnArr[i])
    }
    return format;
}