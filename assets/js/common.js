var { API } = require('allApi.js');
const app = getApp();
var yunlv = {};
import { baseUrl } from '../../config/config';
//基本配置
yunlv.base = {
  gAppid: "wx35e6b745bcb73b2c", //小程序AppID
  gYunlvServer: baseUrl, //测式
  // gYunlvServer: "http://192.168.0.13:8810", // 本地
  isLoding: false,
  gToken: function () { //获取token
    try {
      var value = wx.getStorageSync('u-token');
      if (value) {
        return value;
      } else {
        return "";
      }
    } catch (e) {
      return "getStorageSync token error";
    }
  },
  gUserId: function () { //获取token
    try {
      var userInfo = wx.getStorageSync('userInfo');
      if (userInfo && userInfo.userId) {
        return userInfo.userId;
      } else {
        return "";
      }
    } catch (e) {
      return "";
    }
  },
  //执行用户授权
  doLoginFun: function () {
    let pages = getCurrentPages(); // 保存当前页面
    if (pages.length) {
      let currentPage = pages[pages.length - 1];
      "pages/login/index" != currentPage.route && wx.setStorageSync("currentPage", currentPage);
    }
    wx.redirectTo({
      url: "/pages/login/index" // 跳转授权登录页面
    });
  },
  //授权成功 跳转回原页面
  navigateBackFun: function () {
    var pages = getCurrentPages();
    let currentPage = wx.getStorageSync('currentPage');
    if (JSON.stringify(currentPage.options) != '{}') {
      var toUrl = currentPage.route + yunlv.tools.jsonIfyFun(currentPage.options);
    } else {
      var toUrl = currentPage.route
    }
    wx.redirectTo({
      url: '/' + toUrl,
      fail: function () {
        wx.reLaunch({
          url: '/' + toUrl,
        });
      }
    });
  }
};

//后台请求封装
yunlv.httpResource = {
  httpResource: function (service, filter, success, method, error, prefix, isToast = true) {
    yunlv.base.isLoding = false;
    let type = null,
      joinPrefix = prefix || '',
      header = {
        'content-type': 'application/json',
        'Authorization': `${yunlv.base.gToken()}`,
      };
    var timeLoding = setTimeout(() => {
      if (yunlv.base.isLoding) { } else {
        // wx.showLoading({
        //     title: '加载中...',
        //     mask: true
        // });
      }
    }, 500);
    // userId
    if (yunlv.base.gUserId()) {
      filter = Object.assign({ userId: yunlv.base.gUserId() }, filter)
    }
    return new Promise((reslove, reject) => {
      wx.request({
        url: yunlv.base.gYunlvServer + "/" + API[service] + joinPrefix,
        data: filter,
        method: method || "POST",
        timeout: 20000,
        header: header,
        success: function (data) {
          yunlv.base.isLoding = true;
          let res = data.data;
          console.log('接口结果', res.code !== 200 ? '错误' : '成功', API[service], filter, res)
          if (res.code !== 200) { // 失败
            const err = Object.assign(res, filter);
            if (service == 'login') {
              // yunlv.base.doLoginFun();
              error && error(err);
              reject(err);
              return
            }
            if (res.code == 401 || res.code == 402) {
              yunlv.business.getCode().then((code) => {
                yunlv.business.mpLogin({ code: code }, () => {
                  let pages = getCurrentPages();
                  let currPage = null;
                  if (pages.length) {
                    currPage = pages[pages.length - 1];
                  }
                  currPage.onShow();
                })
              });
              error && error(err);
              reject(err);
              return
            }
            if (res.msg && isToast) {
              wx.showToast({
                title: res.msg || '当前服务繁忙，正努力解决中，请耐心等待',
                icon: 'none',
                mask: true,
              });
            }
            error && error(err);
            reject(err);
          } else {
            success && success(res.data, res.msg);
            reslove(res.data, res.msg)
          }
        },
        fail: function () {
          error && error();
          reject();
        },
        complete: function (e) {
          clearTimeout(timeLoding);
        }
      })
    })
  },
  uploadImg({ filePath, code = 'shopOrderRefund' }) {
    return new Promise((resolve, reject) => {
      wx.uploadFile({
        url: `${baseUrl}/${API['uploadImage']}`,
        filePath: filePath,
        formData: {
          code
        },
        name: 'file',
        header: {
          'content-type': 'application/json',
          'Authorization': `${yunlv.base.gToken()}`,
        },
        success(res) {
          const data = JSON.parse(res.data);
          if (data.code == 200 && data.success) {
            resolve(data.data)
          } else {
            reject(data)
          }
        },
        fail(err) {
          reject(err)
        }
      })
    })
  }
};
yunlv.tools = {
  // 拨打电话
  callPhone(phoneNumber) {
    wx.makePhoneCall({
      phoneNumber: phoneNumber,
      success: function () {
        console.log("拨打成功！")
      },
      fail: function () {
        console.log("拨打失败！")
      }
    })
  },
  getQuery(locationUrl, key) {
    if (!locationUrl) return "";
    var locationUrl = locationUrl.indexOf('?') > -1 ? locationUrl.split('?')[1] : locationUrl;
    var arr = locationUrl.split("&");
    for (var i = 0; i < arr.length; i++) {
      var ar = arr[i].split("=");
      if (ar[0] == key) {
        return ar[1];
      }
    }
  },
  //将字符串转换成对象
  queryURL(url) {
    var params = url.split("&"); //进行分割成数组
    var obj = {}; //声明对象
    for (var i = 0; i < params.length; i++) {
      var param = params[i].split("="); //进行分割成数组
      obj[param[0]] = param[1]; //为对象赋值
    }
    return obj;
  },
  //手机正则校验
  checkPhone(text) {
    return text.match(/((((13[0-9])|(15[^4])|(18[0,1,2,3,5-9])|(17[0-8])|(147))\d{8})|((\d3,4|\d{3,4}-|\s)?\d{7,14}))?/g);
  },

  //将http地址转换为https
  httpTohttps: function (url, type) {
    let urls = url;
    if (!url) {
      urls = "";
    } else {
      if (url.indexOf("http://") > -1) {
        urls = url.replace("http:", "https:");
      } else {
        urls = url;
      }
    }
    if (!type) {
      urls = urls + "?x-oss-process=style/width640_mark";
    } else {
      urls = urls + type;
    }
    return urls;
  },

  //倒计时2；
  timeClock: function (endtime, that) {
    var interval = null;
    endtime = endtime.replace(/-/g, "/");
    var totalSecond = (new Date(endtime).getTime() - new Date().getTime()) / 1000;
    var interval = setInterval(function () {
      // 秒数  
      var second = totalSecond;
      // // 天数位  
      var day = Math.floor(second / 3600 / 24);
      var dayStr = day.toString();
      if (dayStr.length == 1) dayStr = '0' + dayStr;
      // 小时位  
      var hr = Math.floor((second - day * 3600 * 24) / (60 * 60));
      var hrStr = hr.toString();
      if (hrStr.length == 1) hrStr = '0' + hrStr;

      // 分钟位  
      // var min = Math.floor((second - hr * 3600) / 60);
      var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
      var minStr = min.toString();
      if (minStr.length == 1) minStr = '0' + minStr;

      // 秒位  
      // var sec = second - hrNew * 3600 - min * 60;
      var sec = Math.floor(second - day * 3600 * 24 - hr * 3600 - min * 60);
      var secStr = sec.toString();
      if (secStr.length == 1) secStr = '0' + secStr;

      that.setData({
        countDownDay: dayStr,
        countDownHour: hrStr,
        countDownMinute: minStr,
        countDownSecond: secStr,
      });
      totalSecond--;
      if (totalSecond <= 0) {
        that.setData({
          endState: 1,
          countDownDay: '00',
          countDownHour: '00',
          countDownMinute: '00',
          countDownSecond: '00',
        });
        clearInterval(interval);
      }
    }.bind(that), 1000);
    that.setData({
      interval: interval
    });
  },

  /**
   * 获取当前时间的日期部分
   */
  getNowFormatDate: function () {
    var date = new Date();
    var year = date.getFullYear(); //完整4位年份， getYear(); //获取当前年份(2位)
    var month = date.getMonth() + 1; //date.getMonth()月份0~11，0代表1月
    var day = date.getDate(); //获取当前日(1-31)

    if (month >= 1 && month <= 9) {
      month = "0" + month;
    }
    if (day >= 0 && day <= 9) {
      day = "0" + day;
    }
    return year + "-" + month + "-" + day;

  },
  //日期比较：日期字符串格式2017-10-26 
  compareDate: function (dateStart, dateEnd) {
    var d1 = new Date(dateStart.replace(/\-/g, "\/"));
    var d2 = new Date(dateEnd.replace(/\-/g, "\/"));
    if (d1 > d2) {
      return false;
    } else {
      return true;
    }
  },
  parseDate(date) {
    let tDate = new Date(parseInt(date) * 1000)
    return tDate.getFullYear() + '-' + (tDate.getMonth() + 1) + '-' + tDate.getDate() + ' ' + tDate.getHours() + ':' + tDate.getMinutes() + ':' + tDate.getSeconds()
  },
  jsonIfyFun(data) {
    if (JSON.stringify(data) != '{}') {
      var q = "?";
      for (var key in data) {
        q = q + key + "=" + data[key] + "&";
      }
      q = q.substr(0, q.length - 1)
      return q
    }
  },
  /**
   * 计算两个时间相差多少秒
   */
  getSecondByTowDate(date1, date2) {
    var dateStart = new Date(date1);
    var dateEnd = new Date(date2);
    var second = (dateEnd.getTime() - dateStart.getTime()) / 1000;
    return second;
  },
  //px转换rpx
  pxTorpx: function (width) {
    let clientWidth = wx.getSystemInfoSync().screenWidth;
    return Math.floor(clientWidth * width / 750);
  },
  add0: function (m) {
    return m < 10 ? "0" + m : m;
  },
  RndNum: function (n) {
    var rnd = "";
    for (var i = 0; i < n; i++)
      rnd += Math.floor(Math.random() * 10);
    return rnd;
  },

  /**
   * 变量不存在
   */
  isNull: function (value) {
    if (value == null || typeof (value) == 'undefined' ||
      (typeof (value) == 'string' && value.trim() === '')) {
      return true
    } else {
      return false
    }
  },
  /**
   * 变量不存在并返回默认值
   */
  isNullOrDefault: function (value, defaultValue) {
    if (value == null || typeof (value) == 'undefined' ||
      (typeof (value) == 'string' && value.trim() === '')) {
      return defaultValue
    } else {
      return value
    }
  },
  /**
   * 小数位补零
   */
  zeroize: function (number, length = 2) {
    var str = number.toString();
    var pos_decimal = str.indexOf('.');
    if (pos_decimal < 0) {
      pos_decimal = str.length;
      str += '.';
    }
    while (str.length <= pos_decimal + length) {
      str += '0';
    }
    return str;
  },
  /**
   * jpg,png图片处理
   */
  imageFunction: function (image) {
    if (image.includes('.png') || image.includes('.jpg')) {
      return image + "?x-oss-process=style/q_80"
    } else {
      return image
    }
  }
};

/**
 * 地理位置模块
 */
yunlv.location = {
  /**
   * 计算两个经纬度之间的距离
   */
  GetDistance: function (lat1, lng1, lat2, lng2) {
    var radLat1 = lat1 * Math.PI / 180.0;
    var radLat2 = lat2 * Math.PI / 180.0;
    var a = radLat1 - radLat2;
    var b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0;
    var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
      Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
    s = s * 6378.137; // EARTH_RADIUS;
    s = Math.round(s * 10000) / 10000;
    return s;
  }
},

  yunlv.business = {
    //小程序登录
    mpLogin: function (data, resolve, reject) {
      wx.showLoading({ title: '加载中' });
      let { code, type, actId, inviteUserId, isActivity } = data;
      let param = {};
      if (isActivity) param = { code, type, actId, inviteUserId, channel: 'WX' };
      if (!isActivity) param = { code, channel: 'WX' };
      yunlv.httpResource.httpResource("login", param, function (data) {
        if (app && app.globalData) app.globalData.isLogin = true;
        wx.setStorageSync('u-token', data.token);
        wx.setStorageSync("openId", data.appUserId);
        wx.setStorageSync("userInfo", {
          ...data,
          avatar: data.headUrl,
          userNo: data.memNo
        }); // 新操作
        wx.hideLoading();
        resolve && resolve();
      }, "POST", (err) => {
        wx.hideLoading();
        reject && reject(err);
      })
    },
    //获取code
    getCode: function () {
      return new Promise((resolve, reject) => {
        wx.login({
          success(res) {
            console.log(res);
            resolve(res.code);
          },
          fail(res) {
            wx.showToast({
              title: '微信登录错误',
              duration: 1500,
              mask: true,
              icon: "none"
            })
            reject(res);
          }
        })
      });
    },
    getUserInfo(resolve) {
      yunlv.httpResource.httpResource("getUserInfo", {}, function (data) {
        wx.setStorageSync("userInfo", data);
        resolve()
      }, 'GET')
    },

    //获取位置信息
    getLocationByMP: function () {
      return new Promise((resolve) => {
        wx.getLocation({
          type: 'gcj02', //返回可以用于wx.openLocation的经纬度
          success: function (res) {
            wx.setStorageSync("userLocationInfo", res);
            resolve(res);
          },
          fail: function (res) {
            resolve(res);
            console.log('微信定位失败:' + JSON.stringify(res));
            wx.showToast({
              title: '微信定位失败',
            })
          }
        })
      })
    },
    //判断是否需要授权
    isNeedAutho: function () {
      return new Promise((resolve, reject) => {
        const userId = wx.getStorageSync("userInfo").userId;
        const token = wx.getStorageSync('u-token');
        console.log('用户信息', userId, token);
        if (token && userId) {
          resolve()
        }
        if (!token) { //未过授权
          yunlv.business.getCode().then((code) => {
            yunlv.business.mpLogin({ code: code }, resolve, reject)
          });
        }
        if (token && !userId) { //未获取到用户信息
          // yunlv.business.getUserInfo(resolve)
          yunlv.business.getCode().then((code) => {
            yunlv.business.mpLogin({ code: code }, resolve, reject)
          });
        }
      });
    },
  };
yunlv.data = {
  sort: function (jsonData, colId, sequence) {
    //对json进行升序排序函数  
    var asc = function (x, y) {
      return (x[colId] > y[colId]) ? 1 : -1
    }
    //对json进行降序排序函数  
    var desc = function (x, y) {
      return (x[colId] < y[colId]) ? 1 : -1
    }
    if (sequence == "asc") {
      return jsonData.sort(asc);
    }
    if (sequence == "desc") {
      return jsonData.sort(desc);
    } else {
      return jsonData.sort(asc); //默认升序
    }
  },
  //从对象数组中【查找】指定属性名值的对象数组,这个方法有问题item.attrKey无效
  queryObjFromObjArr: function (jsonObjArr, attrKey, attrVal) {
    if (jsonObjArr.length) {
      var newArr = [];
      jsonObjArr.forEach((item, index) => {
        if (item[attrKey] == attrVal) {
          newArr.push(item);
        }
      })
      return newArr;
    }
  },
  //从对象数组中【移除】指定属性名值的对象数组
  removeObjFromObjArr: function (jsonObjArr, attrKey, attrVal) {
    for (var i = 0; i < jsonObjArr.length; i++) {
      for (var n in jsonObjArr[i]) {
        if (n == attrKey && jsonObjArr[i][n] == attrVal) {
          jsonObjArr.splice(i, 1);
        }
      }
    }
    return jsonObjArr;
  },
  makeTreeData: function (jsonData, StartID) {
    var children = [];
    for (var i = 0; i < jsonData.length; i++) {
      if (jsonData[i].PID == StartID) {
        children.push(jsonData[i]);
        jsonData[i].children = this.makeTreeData(jsonData, jsonData[i].NID);
      }
    }
    return children;
  }
};
//加法函数
function accAdd(arg1, arg2) {
  var r1, r2, m;
  try {
    r1 = arg1.toString().split(".")[1].length;
  }
  catch (e) {
    r1 = 0;
  }
  try {
    r2 = arg2.toString().split(".")[1].length;
  }
  catch (e) {
    r2 = 0;
  }
  m = Math.pow(10, Math.max(r1, r2));
  return (arg1 * m + arg2 * m) / m;
}
//给Number类型增加一个add方法，，使用时直接用 .add 即可完成计算。
Number.prototype.add = function (arg) {
  return accAdd(arg, this);
};


//减法函数
function Subtr(arg1, arg2) {
  var r1, r2, m, n;
  try {
    r1 = arg1.toString().split(".")[1].length;
  }
  catch (e) {
    r1 = 0;
  }
  try {
    r2 = arg2.toString().split(".")[1].length;
  }
  catch (e) {
    r2 = 0;
  }
  m = Math.pow(10, Math.max(r1, r2));
  //last modify by deeka
  //动态控制精度长度
  n = (r1 >= r2) ? r1 : r2;
  return ((arg1 * m - arg2 * m) / m).toFixed(n);
}

//给Number类型增加一个add方法，，使用时直接用 .sub 即可完成计算。
Number.prototype.sub = function (arg) {
  return Subtr(this, arg);
};


//乘法函数
function accMul(arg1, arg2) {
  var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
  try {
    m += s1.split(".")[1].length;
  }
  catch (e) {
  }
  try {
    m += s2.split(".")[1].length;
  }
  catch (e) {
  }
  return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
}
//给Number类型增加一个mul方法，使用时直接用 .mul 即可完成计算。
Number.prototype.mul = function (arg) {
  return accMul(arg, this);
};
function getAllRect(context, selector) {
  return new Promise(function (resolve) {
    wx.createSelectorQuery()
      .in(context)
      .selectAll(selector)
      .boundingClientRect()
      .exec(function (rect) {
        if (rect === void 0) { rect = []; }
        return resolve(rect[0]);
      });
  });
}
function getRect(context, selector) {
  return new Promise(function (resolve) {
    wx.createSelectorQuery()
      .in(context)
      .select(selector)
      .boundingClientRect()
      .exec(function (rect) {
        if (rect === void 0) { rect = []; }
        return resolve(rect[0]);
      });
  });
}
//使用时：common.
module.exports = {
  gAppid: yunlv.base.gAppid,
  gYunlvServer: yunlv.base.gYunlvServer,
  gToken: yunlv.base.gToken,
  doLoginFun: yunlv.base.doLoginFun,
  navigateBackFun: yunlv.base.navigateBackFun,
  httpResource: yunlv.httpResource.httpResource,
  checkPhone: yunlv.tools.checkPhone,
  callPhone: yunlv.tools.callPhone,
  httpTohttps: yunlv.tools.httpTohttps,
  timeClock: yunlv.tools.timeClock,
  getNowFormatDate: yunlv.tools.getNowFormatDate,
  compareDate: yunlv.tools.compareDate,
  parseDate: yunlv.tools.parseDate,
  jsonIfyFun: yunlv.tools.jsonIfyFun,
  getSecondByTowDate: yunlv.tools.getSecondByTowDate,
  pxTorpx: yunlv.tools.pxTorpx,
  add0: yunlv.tools.add0,
  RndNum: yunlv.tools.RndNum,
  isNull: yunlv.tools.isNull,
  isNullOrDefault: yunlv.tools.isNullOrDefault,
  zeroize: yunlv.tools.zeroize,
  queryURL: yunlv.tools.queryURL,
  getQuery: yunlv.tools.getQuery,
  GetDistance: yunlv.location.GetDistance,
  getLocationByMP: yunlv.business.getLocationByMP,
  mpLogin: yunlv.business.mpLogin,
  getCode: yunlv.business.getCode,
  getUserInfo: yunlv.business.getUserInfo,
  isNeedAutho: yunlv.business.isNeedAutho,
  arraySort: yunlv.data.sort,
  queryObjFromObjArr: yunlv.data.queryObjFromObjArr,
  removeObjFromObjArr: yunlv.data.removeObjFromObjArr,
  makeTreeData: yunlv.data.makeTreeData,
  imageFunction: yunlv.tools.imageFunction,
  Subtr: Subtr,
  accAdd: accAdd,
  accMul: accMul,
  uploadImg: yunlv.httpResource.uploadImg,
  getAllRect: getAllRect,
  getRect: getRect
}