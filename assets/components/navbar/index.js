var app = getApp();
Component({
  properties: {
    parameter: {
      type: Object,
      value: {
        class: '0',
        title: ''
      },
    },
    opacity: {
      type: String,
      value: "1"
    },
    background: {
      type: String,
      value: "#FFFFFF"
    },
    type: {
      type: String,
      value: "block"
    },
    titleColor: { type: String, value: "#333" }
  },
  data: {
    navH: "",
    statusBar: "",
  },
  ready: function () {
    var pages = getCurrentPages();
    var currPage = null;
    if (pages.length) {
      currPage = pages[pages.length - 1].route;
    }
    console.log(pages, 'his.data.parameter', this.data.parameter)
    console.log('currPage', currPage)
    if (pages.length <= 1 || currPage == 'pages/login/index"' || currPage == 'pages/phoneAuth/index') {
      this.setData({ 'parameter.return': 0 });
    } else if (this.data.parameter.return > 1) {
      this.setData({ 'parameter.return': this.data.parameter.return });
    } else {
      this.setData({ 'parameter.return': 1 });
    }

    if (currPage == 'pages/index/index' || currPage == 'pages/user/orderList/index' ||
      currPage == 'pages/login/index' || currPage == 'module_base/code/index' || currPage == 'pages/mine/index') {
      this.setData({ 'parameter.home': 0 });
    } else {
      if (pages.length > 2) {
        this.setData({ 'parameter.home': 1 });
      }
    }
  },
  attached: function () {
    if (app.globalData.navHeight == 0) {
      this.resetHeight();
    }
    this.setData({
      navH: app.globalData.navHeight,
      statusBar: app.globalData.statusBar
    });
  },
  methods: {
    resetHeight() {
      //获取设备信息
      wx.getSystemInfo({
        success: function (res) {
          wx.setStorageSync("systemInfo", res);
          wx.setStorageSync("systemPlatform", res.platform);
          app.globalData.statusBar = res.statusBarHeight;
          let rect = wx.getMenuButtonBoundingClientRect(); //胶囊按钮位置信息;
          if (!rect || rect.height == 0) {
            rect = wx.getMenuButtonBoundingClientRect();
          }
          // wx.getMenuButtonBoundingClientRect();
          let navBarHeight = (function () {
            //导航栏高度
            let gap = rect.top - res.statusBarHeight; //动态计算每台手机状态栏到胶囊按钮间距
            return 2 * gap + rect.height;
          })();
          console.log('navbar', navBarHeight, res.statusBarHeight)
          app.globalData.navHeight = navBarHeight + res.statusBarHeight;
          app.globalData.viewHeight = res.screenHeight - app.globalData.navHeight;
          app.globalData.screenHeight = res.screenHeight;
          app.globalData.fachHeight = res.windowHeight - app.globalData.navHeight;
          app.globalData.tabbarHeight =
            (res.screenHeight - res.windowHeight) * (750 / res.windowWidth);
          this.setData({
            navH: app.globalData.navHeight,
            statusBar: app.globalData.statusBar
          });

        },
      });
    },
    return: function () {
      let that = this
      wx.navigateBack({
        delta: parseInt(that.data.parameter.return)
      })
    },
    //回主页
    home: function () {
      wx.reLaunch({
        url: '/pages/index/index'
      })
    },
    //切换组织单元
  }
})

