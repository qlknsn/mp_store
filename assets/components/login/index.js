import {
  gAppid,
  httpResource,
  isNeedAutho
} from '../../../assets/js/common.js';
import {
  throttl
} from '../../../assets/js/eventUtil.js'
var app = getApp();
Component({
  properties: {
    userInfo: {
      type: Object,
      value: {}
    },
    showLogin: {
      type: Boolean,
      value: false
    },
    showPhone: {
      type: Boolean,
      value: false
    },
    authUserInfo: {
      type: Object,
      value: {}
    },
    authPhoneInfo: {
      type: Object,
      value: {}
    },
    lcode: {
      type: String,
      value: ''
    },
  },
  data: {
    screenHeight: wx.getStorageSync("systemInfo").screenHeight + 'px'
  },
  lifetimes: {
    created() {

    },
    ready() {

    }
  },
  methods: {
    hideModal() {
      this.setData({
        showLogin: false
      })
      wx.reLaunch({
        url: '/pages/index/index',
        success: function () {
          app.globalData.isFirstLogin = 0;
        }
      })
    },
    hideModalphone() {
      this.setData({
        showPhone: false
      })
      wx.reLaunch({
        url: '/pages/index/index',
        success: function () {
          app.globalData.isFirstLogin = 0;
        }
      })
    },
    getUserInfo() {
      wx.getUserProfile({
        desc: '用于完善会员资料',
        success: (res) => {
          this.setData({
            authUserInfo: res.userInfo,
            showLogin: false,
            showPhone: true,
          })
        }
      })
    },
    getPhoneNumber: throttl(function (res) {
      if (res.detail.errMsg === 'getPhoneNumber:ok') {
        this.setData({ authPhoneInfo: res.detail });
        const shopCode = wx.getStorageSync("shopCode") || '';
        const shopName = wx.getStorageSync("shopName") || '';
        const source = wx.getStorageSync("source") || '';
        const { type, inviteUserId, actId } = app.globalData;
        let parms = {
          code: this.data.lcode,
          encreptedData: this.data.authPhoneInfo.encryptedData,
          iv: this.data.authPhoneInfo.iv,
          nickName: this.data.authUserInfo.nickName,
          headUrl: this.data.authUserInfo.avatarUrl,
          shopCode,
          shopName,
          phone: '',
          source,
          type,
          inviteUserId,
          actId,
          channel: 'WX'
        };
        wx.showLoading({ title: '加载中' });
        httpResource('userRegister', parms, (data) => {
          wx.hideLoading()
          console.log(this.data.authUserInfo.nickName);
          wx.setStorageSync("userInfo", {
            avatar: this.data.authUserInfo.avatarUrl,
            sex: this.data.authUserInfo.gender,
            nickName: this.data.authUserInfo.nickName
          }) // 新操作
          app.globalData.isFirstLogin = 1;
          this.setData({
            showPhone: false
          })
          // 默认跳转用户信息页面
          this.triggerEvent('init', {});
        }, "POST", () => { wx.hideLoading() })
      }
    }, 800)
  }
})