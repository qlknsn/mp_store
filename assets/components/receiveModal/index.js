var app = getApp();
Component({
    externalClasses: ['i-class'],
    /**
     * 组件的属性列表
     */
    properties: {
        //是否显示modal
        show: {
            type: Boolean,
            value: false
        },
    },

    /**
     * 组件的初始数据
     */
    data: {

    },
    lifetimes: {
        attached: function () {
            // 在组件实例进入页面节点树时执行
            this.setData({
                navH: app.globalData.navHeight
            })
        },
        detached: function () {
            // 在组件实例被从页面节点树移除时执行
        },
    },
    /**
     * 组件的方法列表
     */
    methods: {
        move(){

        },
        // 取消函数
        cancel() {
            this.setData({
                show: false
            })
            this.triggerEvent('cancel')
        },
        // 确认函数
        confirm() {
            this.setData({
                show: false
            })
            this.triggerEvent('confirm')
        }
    }
})