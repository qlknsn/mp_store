const app = getApp();
Component({
  data: {
    cartNum: 0,
    show: true,
    selected: 0,
    "color": "#333",
    "selectedColor": "#EE482F",
    "backgroundColor": "#fff",
    "borderStyle": "black",
    "position": "bottom",
    "list": [{
      "pagePath": "/pages/index/index",
      "text": "首页",
      "iconPath": "/assets/images/tabbar/index.png",
      "selectedIconPath": "/assets/images/tabbar/index-active.png"
    },
    {
      "pagePath": "/pages/categories/index",
      "text": "分类",
      "iconPath": "/assets/images/tabbar/category.png",
      "selectedIconPath": "/assets/images/tabbar/category-active.png"
    },
    {
      "pagePath": "/pages/cart/index",
      "text": "购物车",
      "iconPath": "/assets/images/tabbar/cart.png",
      "selectedIconPath": "/assets/images/tabbar/cart-active.png"
    },
    {
      "pagePath": "/pages/mine/index",
      "text": "我的",
      "iconPath": "/assets/images/tabbar/user.png",
      "selectedIconPath": "/assets/images/tabbar/user-active.png"
    }
    ]
  },
  lifetimes: {
    ready: function () {
      this.setData({
        cartNum: app.globalData.cartNum
      })
      app.$watch('cartNum', (val, old) => {
        this.setData({
          cartNum: val
        })
      })
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
    },
    show: function () {
    },
  },
  methods: {
    switchTab(e) {
      const { selected } = this.data;
      const isLogin = app.globalData.isLogin;
      const { path, index } = e.currentTarget.dataset;
      const toLoction = wx.getStorageSync('toLoction');
      const selectTaber = wx.getStorageSync("SELECT_TABER");
      if (selected == index) {
        wx.pageScrollTo({
          scrollTop: 0
        })
        return
      }
      if (path === '/pages/mine/index') {
        // if (!isLogin) {
        //   wx.navigateTo({
        //     url: "/pages/userLogin/index",
        //   })
        //   return
        // }
      }
      if (path === '/pages/cart/index') {
        // if (!isLogin) {
        //   wx.navigateTo({
        //     url: "/pages/userLogin/index?switchTabIndex=1",
        //   })
        //   return
        // } else {
        //外卖
        if (selectTaber == 'WAI_MAI') {
          if (!app.globalData.deliverableAddress.addrId) {
            wx.navigateTo({
              url: '/pages/user/address/list/index?type=2'
            })
            return
          }
        }
        //自提
        if (selectTaber == 'ZI_TI') {
          //未授权地址跳转授权地址页
          if (toLoction && !app.globalData.currentStore.shopCode) {
            wx.navigateTo({
              url: "/pages/location/fail",
            });
            return;
          }
          //未选择门店
          if (!app.globalData.currentStore.shopCode) {
            wx.navigateTo({
              url: "/pages/map/index",
            });
            return;
          }
        }
        // }
      }
      if (path === '/pages/categories/index') {
        // if (!isLogin) {
        //   wx.navigateTo({
        //     url: "/pages/userLogin/index?switchTabIndex=2",
        //   })
        //   return
        // } else {
        //外卖
        // if (selectTaber == 'WAI_MAI') {
        //   if (!app.globalData.deliverableAddress.addrId) {
        //     wx.navigateTo({
        //       url: '/pages/user/address/list/index?type=2'
        //     })
        //     return
        //   }
        // }
        //自提
        if (selectTaber == 'ZI_TI') {
          //未授权地址跳转授权地址页
          if (toLoction && !app.globalData.currentStore.shopCode) {
            wx.navigateTo({
              url: "/pages/location/fail",
            });
            return;
          }
          //未选择门店
          if (!app.globalData.currentStore.shopCode) {
            wx.navigateTo({
              url: "/pages/map/index",
            });
            return;
          }
        }
        // }
      }
      wx.switchTab({
        url: path, complete: () => {
          wx.removeStorageSync('classify_home')
        }
      })
    }
  }
})