// module_games/sharelottery/index.js
// import img from './base64Img'
// getShareImgs
const {
  httpResource
} = require("../../assets/js/common.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navbarData: {
      home: '1'
    },
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 5000,
    duration: 500,
    processList: [{
      name: "保存海报"
    }, {
      name: "朋友圈分享海报"
    }, {
      name: "至少一人扫码进小程序"
    }, {
      name: "完成任务获得一次抽奖机会"
    }],
    iconIndex: '1',
    base64: '',
    base64Img: '',
    shartImgList: [],
    actId: '',
    taskId: '',
    // 当前项
    currentIndex: 0,
    type: '',
    taskChainId:'',
    srcUserId:''
  },
  // 保存海报到相册
  savePoster() {
    wx.uma.trackEvent("click_draw_save",{actId: this.data.actId});
    let that = this
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.writePhotosAlbum']) {
          that.drawAndShareImage();
        } else if (res.authSetting['scope.writePhotosAlbum'] === undefined) {
          wx.authorize({
            scope: 'scope.writePhotosAlbum',
            success() {
              that.drawAndShareImage();
            },
            fail() {
              that.authConfirm()
            }
          })
        } else {
          that.authConfirm()
        }
      }
    })
  },
  // 授权拒绝后，再次授权提示弹窗
  authConfirm() {
    let that = this
    wx.showModal({
      content: '检测到您没打开保存图片权限，是否去设置打开？',
      confirmText: "确认",
      cancelText: "取消",
      success: function (res) {
        if (res.confirm) {
          wx.openSetting({
            success(res) {
              if (res.authSetting['scope.writePhotosAlbum']) {
                that.saveImg();
              } else {
                wx.showToast({
                  title: '您没有授权，无法保存到相册',
                  icon: 'none'
                })
              }
            }
          })
        } else {
          wx.showToast({
            title: '您没有授权，无法保存到相册',
            icon: 'none'
          })
        }
      }
    });
  },
  //保存图片
  saveImg: function (imgUrl) {
    var that = this;
    // var imgUrl = 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/lotteryDraw/%E7%BC%96%E7%BB%84%E5%88%86%E4%BA%AB%2012%E5%A4%87%E4%BB%BD%402x.png'; //需要保存的图片地址
    console.log(imgUrl)
    wx.downloadFile({
      url: imgUrl,
      success(res) {
        // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
        console.log('downloadFileres', res);
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success(res) {
            console.log("保存图片：success");
            wx.showToast({
              title: '保存成功',
            });
          },
          fail: res => {
            console.log(res);
            wx.showToast({
              title: '保存失败',
            });
          }
        })
      }
    })
  },
  drawAndShareImage() {
    let that = this;
    wx.showLoading({
      title: '正在生成海报...',
      mask: true
    });
    wx.getImageInfo({
      src: that.data.shartImgList[that.data.currentIndex].imgUrl,
      success: function (res) {
        console.log(that.data.base64Img);
        let path = res.path;
        let context = wx.createCanvasContext('myshcanvas');
        // 绘制主背景白色
        context.setFillStyle("#ffffff")
        context.fillRect(0, 0, 562, 814);
        context.drawImage(path, 0, 0, 562, 814);
        // 绘制二维码
        const fsm = wx.getFileSystemManager();
        let showImgData = that.data.base64Img; //二维码数据
        showImgData = showImgData.replace(/\ +/g, "");
        showImgData = showImgData.replace(/[\r\n]/g, "");
        const buffer = wx.base64ToArrayBuffer(showImgData);
        const fileName = wx.env.USER_DATA_PATH + '/share_img.png'
        fsm.writeFileSync(fileName, buffer, 'binary')

        showImgData = fileName; //处理完成
        console.log(showImgData);
        context.beginPath();
        context.arc(150 / 2 + 410, 150 / 2 + 650, 150 / 2, 0, Math.PI * 2);
        context.setLineWidth(10);
        context.setStrokeStyle('#fff');
        context.stroke(); //画空心圆
        context.closePath();
        context.save();
        context.arc(150 / 2 + 390, 150 / 2 + 650, 150 / 2, 0, Math.PI * 2);
        context.clip(); //裁剪上面的圆形
        context.drawImage(showImgData, 390, 650, 150, 150);

        context.draw();
        setTimeout(function () {
          wx.canvasToTempFilePath({
            canvasId: 'myshcanvas',
            x: 0,
            y: 0,
            destWidth: 562 * 4,
            destHeight: 814 * 4,
            success: function (r) {
              console.log(r);
              wx.hideToast();
              wx.saveImageToPhotosAlbum({
                filePath: r.tempFilePath,
                success(res) {
                  wx.hideLoading();
                  wx.showToast({
                    title: '保存成功！',
                    icon: 'none',
                    duration: 1500
                  })
                }
              })
            },
            fail: function (res) {
              wx.hideLoading();
              wx.showToast({
                title: '保存失败！',
                icon: 'none',
                duration: 1500
              })
            }
          })
        }, 200)
      }
    })
    // setTimeout(() => {
    // code_url = this.canvasToTempImage(); 
    //获取临时缓存合成照片路径，存入data中
    // wx.canvasToTempFilePath({
    //     canvasId: 'shareCanvas',
    //     success: function (res) {
    //         var tempFilePath = res.tempFilePath;
    //         console.log(tempFilePath)
    //         that.saveImg(tempFilePath)
    //     },
    //     fail: function (res) {
    //         console.log(res);
    //     }
    // });
    // }, 1000);
  },
  //获取太阳码
  async getCode() {
    let _this = this;
    // let {
    //     actId
    // } = _this.data;
    let {
      userId
    } = wx.getStorageSync("userInfo");
    console.log(_this.data.type);
    let page = ''
    if (_this.data.type == '1') {
      page = 'module_games/lotteryDraw/lotteryDraw';
    } else if(_this.data.type == '2'){
      page = 'module_games/gasiapon/index';
    }else if(_this.data.type == '3'){
      page = 'module_games/blindbox/blindbox';
    }else if(_this.data.type == '4'){
      page = 'module_games/sudoku/sudoku';
    }

    let data = await httpResource('generateMiniQRCode', {
      params: {
        act: `${_this.data.actId}_${userId}_${_this.data.taskChainId}`
      },
      page
    }, null, "post");
    console.log(data);
    _this.setData({
      base64: 'data:image/png;base64,' + data,
      base64Img: data,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    this.setData({
      actId: options.actId,
      taskId: options.taskId,
      type: options.type,
      taskChainId:options.taskChainId,
      srcUserId:options.srcUserId
    })
    this.getCode()
    httpResource('getShareImgs', {
      actId: options.actId,
      taskId: options.taskId
    }, (data) => {
      console.log(data);
      this.setData({
        shartImgList: data
      })
    }, 'get')

    // this.drawAndShareImage()
  },
  currentChange(e) {
    // let i = e.detail.current - 1   
    this.setData({
      currentIndex: e.detail.current
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  checkInit() {
    let _this = this
    httpResource('checkInit', {
      actId: 31
    }, (data) => {
      console.log(data);
    }, "post");
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let _this = this
    _this.checkInit()

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})