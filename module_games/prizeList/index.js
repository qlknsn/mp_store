// module_games/prizeList/index.js
var app = getApp();
const {
  httpResource
} = require("../../assets/js/common.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navbarData: {
      home: '1'
    },
    lotteryInfo: {},
    prizeList: [],
    scrollTopNum: 0,
    refresher: false, // 下拉刷新,
    pageIndex: 1,
    noMore: false,
    actId: ''
  },
  goCoupon() {
    wx.uma.trackEvent("click_coupon",{actId: this.data.actId});
    wx.navigateTo({
      url: "/pages/user/coupon/index",
    });
  },
  bindrefresherrefresh() {
    this.setData({
      refresher: true,
      pageIndex: 1,
      noMore: false
    }, () => {
      setTimeout(() => {
        this.getwinningRecord(this.data.pageIndex, this.data.actId)
      }, 300)
    })
  },
  scrollToLower() {
    this.data.pageIndex++;
    this.getwinningRecord(this.data.pageIndex, this.data.actId)
  },
  getwinningRecord(page, actId) {
    let that = this
    wx.showLoading({
      title: '请求中...',
      mask: true
    });
    httpResource('winningRecord', {
      actId: actId,
      currentPage: page,
      pageSize: 10
    }, (data) => {
      // this.setData({
      //   prizeList: data
      // })
      if (data?.records && data.records.length >= 0) {
        wx.hideLoading()
        if (data.currentPage == 1) {
          that.setData({
            prizeList: data ? data.records : [],
            refresher: false,
          })
          if (data.total < data.pageSize) {
            that.setData({
              noMore: true,
            })
          }
        } else {
          this.setData({
            prizeList: this.data.prizeList.concat(data.records),
            refresher: false,
          })

          if (data.records.length == 0) {
            that.setData({
              noMore: true,
            })
          }
        }
      } else {
        wx.hideLoading()
        if (data?.currentPage == 1 || data == null) {
          this.setData({
            respones: true,
            refresher: false,
          })

        } else {
          if (data) {
            that.setData({
              noMore: data.currentPage == 1 ? false : true,
              refresher: false,
            })
          } else {
            that.setData({
              noMore: false,
              refresher: false,
            })
          }
        }
      }
    }, 'post');
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(app.globalData);
    this.setData({
      lotteryInfo: app.globalData.lotteryInfo
    })
    // options.actId
    this.setData({
      actId: options.actId,
      pageIndex: 1
    })
    // winningRecord
    this.getwinningRecord(1, options.actId)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})