// module_games/sudoku/sudoku.js
let timer = null
const {
  httpResource,
} = require("../../assets/js/common.js");
var app = getApp();
import {
  getUserLocation,
  getHighlightStrArray
} from '../../assets/js/commonData.js';
const {
  debounce
} = require('../../assets/js/eventUtil');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lottery: null, //活动id
    // loading
    showLoading: true,
    // 规则
    showRule: false,
    // 未抽中
    shownone: false,
    // 抽中奖品
    showPrize: false,


    sudokuData: {
      prizeName: '',
      prizeImgUrl: ""
    },
    winningQrc: 1, //1:社群吗  2：门店吗  3：隐藏
    shopCodeImg: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg', //门店吗图片
    qrcodeUrl: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg', // 二维码地址

    lightshow: true,
    t: 500,

    prize_arr: [{
      id: '1',
      type: 'prize',
      name: '奖品1',
      isSelected: false
    },
    {
      id: '2',
      type: 'prize',
      name: '奖品2',
      isSelected: false
    },
    {
      id: '3',
      type: 'prize',
      name: '奖品3',
      isSelected: false
    },
    {
      id: '4',
      type: 'prize',
      name: '奖品8',
      isSelected: false
    },
    {
      id: '5',
      type: 'btn',
      name: '按钮',
      isSelected: false
    },
    {
      id: '6',
      type: 'prize',
      name: '奖品4',
      isSelected: false
    },
    {
      id: '7',
      type: 'prize',
      name: '奖品7',
      isSelected: false
    },
    {
      id: '8',
      type: 'prize',
      name: '奖品6',
      isSelected: false
    },
    {
      id: '9',
      type: 'prize',
      name: '奖品5',
      isSelected: false
    },
    ],
    // 抽奖状态，是否转完了
    isTurnOver: true,
    taskList: [{
      taskDesc: 'fsadgfdas',
      ableCount: 1,
      ableTotalCount: 1,
      actId: '12',
      taskLevel: '1',
      rewardLottery: 2,
      id: 1,
      taskType: 1
    }, {
      taskDesc: 'fsadgfdas',
      ableCount: 0,
      ableTotalCount: 1,
      actId: '12',
      taskLevel: '1',
      rewardLottery: 2,
      id: 2,
      taskType: 2
    }], //任务列表,
    bgColor: '#06185d',
    handstatus: true,
    value: '',



    lottery_page: '', //大背景图片
    horse_race_lamp_1: '', //灯亮灭
    horse_race_lamp_2: '', //灯亮灭
    biaopai: '', //标牌
    jiugongge: '', //九宫格
    checkedborder: '', //选中框
    lottery_button: '', //抽奖按钮
    click_icon: '', //点击图标
    task_border_top: '',//任务框上
    task_border_middle: '',//任务框中
    task_border_bottom: '',//任务框下
    task_border_unfinish: '',//任务框未完成
    task_border_finished: '',//任务框已完成
    activity_rule: '',//活动规则
    wining_record: '',//中奖记录
    wining_record_btn: '',//中奖记录按钮
    unissued: '',//发放状态（未发放）
    issued: '',//发放状态（已发放）

    actRule: '', //活动规则文字
    wining_tips_wined: '', // 中奖弹框 中奖
    wining_tips_no_win: '', // 中奖弹框 未中奖


    prizeNumber: 0,//几等分
    bgColor: '',//背景色
    isShoWinningInfo: '',//是否展示走马灯

    taskChainId: null, //分享的id
    currentTaskid: null, //分享出去的id
    prizeType: '', //抽奖类型0：谢谢  1：优惠券
    prizeName: '', //奖品名称
    prizeImgUrl: '', //奖品图片
    usableCount:0,//抽奖次数
  },

  showRulek() {
    wx.uma.trackEvent("click_draw_rule", { actId: this.data.lottery });
    this.setData({
      showRule: true
    })
  },
  hideRulek() {
    this.setData({
      showRule: false
    })
  },
  showPrizek() {
    this.setData({
      showPrize: true
    })
  },
  hidePrizek() {
    this.setData({
      showPrize: false
    })
  },
  hideNone() {
    this.setData({
      shownone: false
    })
  },
  showNone() {
    this.setData({
      shownone: true
    })
  },
  // 跳转中奖记录
  toPrizelist() {
    // wx.uma.trackEvent("click_draw_record", { actId: this.data.lottery });
    wx.navigateTo({
      url: `/module_games/sudokuList/sudokuList?actId=${this.data.lottery}`,
    })
  },
  toMine() {
    if (app.globalData.lotterynavigate) {
      wx.switchTab({
        url: app.globalData.lotterynavigate
      })
    } else {
      wx.switchTab({
        url: '/pages/index/index'
      })
    }
    app.globalData.lotterynavigate = ''
  },
  // 点击抽奖
  clickPrize() {

    // 如果不在抽奖状态中，则执行抽奖旋转动画
    if (this.data.isTurnOver) {
      // 把抽奖状态改为未完成
      this.setData({
        isTurnOver: false
      })
      // 这里开始假设已经调取后端接口拿到抽奖后返回的ID
      let prize_id = 0;
      // 调用抽奖方法
      // this.lottery(prize_id);
      httpResource('doLottery', {
        actId: this.data.lottery,
        nickName: wx.getStorageSync('userInfo').nickName
      }, (data) => {

        if (data) {
          prize_id = data.prizeSortNo
          this.setData({
            prizeType: data.prizeType,
            prizeName: data.prizeName,
            prizeImgUrl: data.prizeImgUrl,
            gasiaponData:data
          })

          this.lottery(prize_id)
          this.getusablecount();

          this.setData({
            active: false
          })
          setTimeout(() => {
            if (this.data.gasiaponData.prizeType == 0) {
              this.showNone()
            } else {
              this.showPrizek()
            }
          },7600)

        }
      }, "POST", () => {
        // 请求活动不在范围内
        this.setData({
          handstatus: false
        })
        this.setData({
          isTurnOver: true
        })
      });
    } else {
      wx.showToast({
        title: '请勿重复点击',
        icon: 'none'
      })
    }
  },

  // 抽奖旋转动画方法
  lottery(prize_id) {
    console.log('中奖ID：' + prize_id)
    /*
     * 数组的长度就是最多所转的圈数，最后一圈会转到中奖后的位置
     * 数组里面的数字表示从一个奖品跳到另一个奖品所需要的时间
     * 数字越小速度越快
     * 想要修改圈数和速度的，更改数组个数和大小即可
     */
    let num_interval_arr = [90, 80, 70, 60, 50, 50, 50, 100, 150, 250];
    // 旋转的总次数
    let sum_rotate = num_interval_arr.length;
    // 每一圈所需要的时间
    let interval = 0;
    num_interval_arr.forEach((delay, index) => {
      setTimeout(() => {
        this.rotateCircle(delay, index + 1, sum_rotate, prize_id);
      }, interval)
      //因为每一圈转完所用的时间是不一样的，所以要做一个叠加操作
      interval += delay * 8;
      console.log(interval);
    })
  },

  /*
   * 封装旋转一圈的动画函数，最后一圈可能不满一圈
   * delay:表示一个奖品跳到另一个奖品所需要的时间
   * index:表示执行到第几圈
   * sum_rotate：表示旋转的总圈数
   * prize_id：中奖后的id号
   */
  rotateCircle(delay, index, sum_rotate, prize_id) {
    // console.log(index)
    let _this = this;
    /*
     * 页面中奖项的实际数组下标
     * 0  1  2
     * 3     5
     * 6  7  8
     * 所以得出转圈的执行顺序数组为 ↓
     */
    let order_arr = [0, 1, 2, 5, 8, 7, 6, 3];
    // 页面奖品总数组
    let prize_arr = this.data.prize_arr;
    // 如果转到最后以前，把数组截取到奖品项的位置
    if (index == sum_rotate) {
      order_arr.splice(prize_id)
    }
    for (let i = 0; i < order_arr.length; i++) {
      setTimeout(() => {
        // 清理掉选中的状态
        prize_arr.forEach(e => {
          e.isSelected = false
        })
        // 执行到第几个就改变它的选中状态
        prize_arr[order_arr[i]].isSelected = true;
        // 更新状态
        _this.setData({
          prize_arr: prize_arr
        })
        // 如果转到最后一圈且转完了，把抽奖状态改为已经转完了
        if (index == sum_rotate && i == order_arr.length - 1) {
          _this.setData({
            isTurnOver: true
          })
        }
      }, delay * i)
    }
  },
  //根据id查询活动图片
  async getActivity() {
    let _this = this;
    try {
      let data = await httpResource('getlotteryInfo', {
        actId: _this.data.lottery
      }, null, "get");
      // _this.setData({
      //     allImg:data
      // })
      console.log(data);
      data.actImgUrls.forEach(item => {
        switch (item.pagePosition) {
          case 100:
            _this.setData({
              lottery_page: item.imgUrl
            })
            break;
          case 201:
            _this.setData({
              horse_race_lamp_1: item.imgUrl
            })
            break;
          case 202:
            _this.setData({
              horse_race_lamp_2: item.imgUrl
            })
            break;
          case 300:
            _this.setData({
              biaopai: item.imgUrl
            })
            break;
          case 301:
            _this.setData({
              jiugongge: item.imgUrl
            })
            break;
          case 302:
            _this.setData({
              checkedborder: item.imgUrl
            })
            break;
          case 400:
            _this.setData({
              lottery_button: item.imgUrl
            })
            break;
          case 500:
            _this.setData({
              click_icon: item.imgUrl
            })
            break;
          case 501:
            _this.setData({
              task_border_top: item.imgUrl
            })
            break;
          case 502:
            _this.setData({
              task_border_middle: item.imgUrl
            })
            break;
          case 503:
            _this.setData({
              task_border_bottom: item.imgUrl
            })
            break;
          case 504:
            _this.setData({
              task_border_unfinish: item.imgUrl
            })
            break;
          case 505:
            _this.setData({
              task_border_finished: item.imgUrl
            })
            break;
          case 600:
            console.log(item.imgUrl);
            _this.setData({
              activity_rule: item.imgUrl
            })
            break;
          case 700:
            _this.setData({
              wining_record: item.imgUrl
            })

            app.globalData.lotteryInfo.wining_record = item.imgUrl
            break;
          case 701:
            _this.setData({
              wining_record_btn: item.imgUrl
            })
            app.globalData.lotteryInfo.wining_record_button = item.imgUrl
            break;
          case 703:
            _this.setData({
              unissued: item.imgUrl
            })
            app.globalData.lotteryInfo.unissued = item.imgUrl
            break;
          case 702:
            _this.setData({
              issued: item.imgUrl
            })
            app.globalData.lotteryInfo.issued = item.imgUrl
            break;
          case 704:
            _this.setData({
              wining_tips_wined: item.imgUrl
            })
            break;
          case 705:
            _this.setData({
              wining_tips_no_win: item.imgUrl
            })
            break;
          default:
            break;
        }
      })
      _this.setData({
        actRule: data.actRule,
        prizeNumber: data.prizeConfigNum,
        bgColor: data.backgroundColor,
        isShoWinningInfo: data.isShoWinningInfo,
        winningQrc: data.winningQrc
      })
      app.globalData.lotteryInfo.bgColor = data.backgroundColor
      // _this.setData({
      //     actRule:data.actRule
      // })
    } catch (error) {

    }
  },

  /**
   * @param {Object} 
   * 获取门店列表
   */
  getData: debounce(async function (e) {
    const _this = this;
    const {
      value,
      latitude,
      longitude,
      pageSize,
      currentPage
    } = this.data;
    let data = await httpResource('getShopByKeywordsWithPage', {
      currentPage: currentPage,
      pageSize: pageSize,
      keywords: value,
      latitude,
      longitude
    });
    let list = [];
    if (data && data.records && data.records.length) {
      data = (data.records || []).map(item => {
        item.shopNameList = getHighlightStrArray(item.shopName, value);
        // 处理高亮
        return item
      });
      list = [...data];
      if (e == 'ok') {
        _this.setData({
          value: list[0].shopName,
          shopName: list[0].shopName || '会员小程序',
        })
        _this.getShopQrCode(list[0].shopCode)
      }
    }
    _this.setData({
      shopList: list,
      currentIndex: ''
    });
  }, 200),
  checkUserLocation: function () {
    let _this = this;
    wx.getSetting({
      success: (res) => {
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
          wx.showModal({
            title: '请求授权当前位置',
            content: '需要获取您的地理位置，请确认授权',
            success: function (res) {
              if (res.cancel) {
                wx.showToast({
                  title: '拒绝授权',
                  icon: 'none',
                  duration: 1000
                })
              } else if (res.confirm) {
                wx.openSetting({
                  success: function (dataAu) {
                    if (dataAu.authSetting["scope.userLocation"] == true) {
                      wx.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 1000
                      })
                      //再次授权，调用wx.getLocation的API
                      getUserLocation().then(res => {
                        let {
                          latitude,
                          longitude
                        } = res;
                        _this.setData({
                          latitude: latitude,
                          longitude: longitude,
                        }, _this.getData('ok'));
                      })
                    } else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'none',
                        duration: 1000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) {
          //调用wx.getLocation的API
          getUserLocation().then(res => {
            let {
              latitude,
              longitude
            } = res;
            _this.setData({
              latitude: latitude,
              longitude: longitude,
            }, _this.getData('ok'));
          })
        } else {
          //调用wx.getLocation的API
          getUserLocation().then(res => {
            let {
              latitude,
              longitude
            } = res;
            _this.setData({
              latitude: latitude,
              longitude: longitude,
            }, _this.getData('ok'));
          })
        }
      }
    })
  },
  /**
  * 获取社群二维码
  */
  async getShopQrCode(shopCode) {
    const _this = this;
    const {
      shopName
    } = this.data;
    let data = await httpResource('getShopCommunityCode', {
      shopCode
    }, null, 'GET');
    if(this.data.winningQrc!==3){
      _this.setData({
      qrcodeUrl: data || 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
      qrName: data ? shopName : '会员小程序',
    })
    }else{
      _this.setData({
        qrcodeUrl: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
        qrName: '会员小程序',
      })
    }
    
  },
  // 任务列表
  async getTaskInfo() {
    let _this = this;
    try {
      let data = await httpResource('getaskInfo', {
        actId: _this.data.lottery
      }, null, "get");
      console.log(data);
      _this.setData({
        taskList: data
      })
    } catch (error) {

    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(`****************888******************`);
    let actCode = null;
    if (options?.scene) {
      actCode = decodeURIComponent(options.scene).split("=")[1];
    }
    let _this = this;

    let isLogin = wx.getStorageSync("u-token");
    if (options?.taskChainId || actCode) {
      this.setData({
        taskChainId: options.taskChainId ? options.taskChainId : actCode.split('_')[2],
      })
    }
    if (options?.srcUserId || actCode) {
      this.setData({

        srcUserId: options.srcUserId ? options.srcUserId : actCode.split('_')[1]
      })
    }

    _this.setData({
      lottery: options?.actId ? (options.actId.indexOf('?') !== -1) ? options.actId.split('?')[0] : options.actId : actCode ? actCode.split('_')[0] : ''
    }, () => {

      if (_this.data.lottery && _this.data.lottery != null) {
        if (!isLogin) {
          wx.redirectTo({
            url: `/pages/userLogin/index?sudoku=${_this.data.lottery}&taskChainId=${_this.data.taskChainId}&srcUserId=${_this.data.userId}`,
          });
        }
      }
    })

    wx.setPageStyle({
      style: {
        overflow: 'hidden'
      }
    })
    setTimeout(() => {
      this.setData({
        showLoading: false
      })
      wx.setPageStyle({
        style: {
          overflow: 'auto'
        }
      })
      // this.startMarquee()
    }, 2000)
    // 经纬度
    getUserLocation().then(res => {
      let {
        latitude,
        longitude
      } = res;
      this.setData({
        latitude: latitude,
        longitude: longitude,
      }, this.getData('ok'));
    }).catch(() => {
      this.checkUserLocation()
    });
  },

  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log(`****************888******************`);
    let that = this
    if (timer) {
      clearInterval(timer)
    }
    // 闪灯
    timer = setInterval(() => {
      that.setData({
        lightshow: !that.data.lightshow
      })
    }, that.data.t)
    that.checkInit()
    console.log(that.data.lottery_page);
    if (that.data.lottery_page) { } else {
      that.getActivity();
    }
    // 禁止分享
    wx.hideShareMenu()

    let currentStore = wx.getStorageSync('current-store');

    if (currentStore) {
      that.setData({
        shopCode: currentStore.shopCode
      })
      that.showOrCODE()
    }
    
    that.getTaskInfo()
    that.getusablecount();
    that.newInterval(that.like, 20000);
  },
  like() {
    let _this = this
    httpResource('getWinninginfo', {
      actId: _this.data.lottery
    }, (data) => {
      _this.setData({
        runHorselist: data
      })
    }, "get");
  },
  // 获取门店码
  showOrCODE() {
    let filter = {
      shopCode: this.data.shopCode,
    };
    httpResource(
      "getCommunityManagerCode",
      filter,
      (data) => {
        this.setData({
          shopCodeImg: data.shopManagerImgUrl || 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
        });
      },
      "GET",
      null
    );
  },
   // 做任务
   todotask(e) {
    let item = e.currentTarget.dataset.item;
    console.log(item);
    httpResource('doTask', {
      actId: item.actId,
      taskId: item.id,
      level: item.taskLevel,
      rewardLottery: item.rewardLottery
    }, (data) => {
      console.log(data);
      if (data) {
        switch (item.taskType) {
          case 1:
            // wx.navigateTo({
            //     url: `/module_games/sharelottery/index?actId=${item.actId}&taskId=${item.id}`,
            // })
            console.log(item.id);
            this.setData({
              currentTaskid: item.id
            })
            break;
          case 2:
            wx.uma.trackEvent("click_pyq_complete", {
              actId: this.data.lottery
            });
            let userInfo = wx.getStorageSync('userInfo');
            wx.navigateTo({
              url: `/module_games/sharelottery/index?actId=${item.actId}&taskId=${item.id}&taskChainId=${data.taskChainId}&type=4&srcUserId=${userInfo.userId}`,
            })
            break;
          case 3:
            wx.uma.trackEvent("click_evaluate_complete", {
              actId: this.data.lottery
            });
            wx.navigateTo({
              url: '/pages/orderList/orderList?orderType=2',
            })
            break;
          case 4:
            wx.uma.trackEvent("click_consumption_complete", {
              actId: this.data.lottery
            });
            wx.switchTab({
              url: '/pages/index/index',
            })
            break;
        }
      }
    }, 'post')
  },
  newInterval(func, millisecond) {
    function inside() {
      func()
      setTimeout(inside, millisecond) //第二次往后调用，每秒后调用自身
    }
    setTimeout(inside, 0) // 第一次运行调用
  },
  checkInit() {

    let _this = this
    console.log(_this.data.taskChainId);
    httpResource('checkInit', {
      actId: _this.data.lottery,
      taskChainId: _this.data.taskChainId,
      srcUserId: _this.data.srcUserId
    }, (data) => {
      // console.log(data,msg);
      if (data.code) {
        this.setData({
          activeStatus: data.code
        })
        wx.showToast({
          title: data.message,
          icon: 'none',
          duration: 1000
        })
      }

    }, "post");
  },
  getusablecount() {
    let _this = this
    httpResource('lotteryUsableCount', {
      actId: _this.data.lottery
    }, (data) => {
      _this.setData({
        usableCount: data
      })
    }, "get");
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
   onShareAppMessage: async function (e) {
    wx.uma.trackEvent("click_wechat_complete", {
      actId: this.data.lottery
    });

    let taskinfo = await httpResource('doTask', {
      actId: e.target.dataset.actid,
      taskId: e.target.dataset.id,
      level: e.target.dataset.tasklevel,
      rewardLottery: e.target.dataset.rewardlottery
    }, null, 'post')
    let data = await httpResource('getShareImgs', {
      actId: this.data.lottery,
      taskId: e.target.dataset.id
    }, null, 'get')

    let userInfo = wx.getStorageSync('userInfo');
    return {
      title: data[0].titleDesc,
      path: `/module_games/sudoku/sudoku?actId=${this.data.lottery}&taskChainId=${taskinfo.taskChainId}&srcUserId=${userInfo.userId}`,
      imageUrl: data[0].imgUrl
    }

  },
})