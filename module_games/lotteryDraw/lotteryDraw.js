// module_games/lotteryDraw/lotteryDraw.js
let timer = null
let interval = null
let horseTIme = null
import {
  createQrcode2
} from "../../assets/js/codeUtil";
const {
  httpResource,
  pxTorpx
} = require("../../assets/js/common.js");
import {
  getUserLocation,
  getHighlightStrArray
} from '../../assets/js/commonData.js';
const {
  debounce
} = require('../../assets/js/eventUtil');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    start: false,
    end: true,
    startTime: 0,
    endTime: 0,

    // 九宫格
    prize_arr: [{
        id: '1',
        type: 'prize',
        name: '奖品1',
        isSelected: false
      },
      {
        id: '2',
        type: 'prize',
        name: '奖品2',
        isSelected: false
      },
      {
        id: '3',
        type: 'prize',
        name: '奖品3',
        isSelected: false
      },
      {
        id: '4',
        type: 'prize',
        name: '奖品8',
        isSelected: false
      },
      {
        id: '5',
        type: 'btn',
        name: '按钮',
        isSelected: false
      },
      {
        id: '6',
        type: 'prize',
        name: '奖品4',
        isSelected: false
      },
      {
        id: '7',
        type: 'prize',
        name: '奖品7',
        isSelected: false
      },
      {
        id: '8',
        type: 'prize',
        name: '奖品6',
        isSelected: false
      },
      {
        id: '9',
        type: 'prize',
        name: '奖品5',
        isSelected: false
      },
    ],
    // 抽奖状态，是否转完了
    isTurnOver: true,
    showRule: false,
    showPrize: false,
    active: true,
    // 抽奖状态，是否转完了
    isTurnOver: true,
    shownone: false,
    // 初始位置
    offsetLeft: 750,
    pace: 1.2, //滚动速度
    interval: 20, //时间间隔
    size: 24, //字体大小in px
    length: 0, //字体宽度
    windowWidth: 0,
    //根据viewId查询view的宽度
    queryViewWidth: 0,
    handstatus: false,
    // loading
    showLoading: true,
    lottery: null,
    taskList: [], //任务列表,\
    allImg: {}, //获取转盘图片
    usableCount: 0, //获取抽奖次数
    actRule: '', //获取抽奖次数
    runHorselist: [], //获取跑马灯数据
    prizeNumber: 0, //几等分

    // 图片
    lottery_page: '', //大背景图片
    horse_race_lamp_1: '', //灯亮灭
    horse_race_lamp_2: '', //灯亮灭
    turntable_content: '', //转盘
    lottery_button: '', //抽奖按钮
    click_icon: '', //小手
    task_up: '', //任务框上
    task_center: '', //任务框lotter中
    task_down: '', //任务框下
    task_not_finished: '', //未完成按钮
    task_finished: '', //完成按钮
    act_rule: '', //活动规则
    act_rule: '', //活动规则
    wining_tips_wined: '', //中奖弹框(中奖)
    prizeType: '', //抽奖类型0：谢谢  1：优惠券
    prizeName: '', //奖品名称
    prizeImgUrl: '', //奖品图片
    communityUrl: '', //社区二维码图片
    wining_record: '', //中奖记录标
    wining_record_button: '', //中奖记录按钮
    unissued: '', //发放
    issued: '', //未发放f
    taskChainId: null, //分享的id
    currentTaskid: null, //分享出去的id

    latitude: null,
    longitude: null,
    shopList: [],
    shopName: '会员小程序',
    qrcodeUrl: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg', // 二维码地址
    qrName: '会员小程序',
    value: '',
    currentPage: 1,
    pageSize: 6,
    lcode: null,
    activeStatus: '',
    bgColor: '',
    srcUserId: '',
    wining_no_wined: "",
    isShoWinningInfo: 1, //是否显示走马灯
    winningQrc: 1, //1:社群吗  2：门店吗  3：隐藏
    shopCode: '', //门店码
    shopCodeImg: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg', //门店吗图片
  },
  // 获取门店码
  showOrCODE() {
    let filter = {
      shopCode: this.data.shopCode,
    };
    httpResource(
      "getCommunityManagerCode",
      filter,
      (data) => {
        this.setData({
          shopCodeImg: data.shopManagerImgUrl || 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
        });
      },
      "GET",
      null
    );
  },
  queryViewWidth() {
    let _this = this
    return new Promise(function (resolve) {
      resolve(_this.data.runHorselist.length * 800 - 50);
    });
  },
  //停止跑马灯
  stopMarquee: function () {
    var that = this;
    //清除旧的定时器
    if (that.data != null) {
      clearInterval(interval);
    }
  },
  //执行跑马灯动画
  excuseAnimation: function () {
    var that = this;
    if (that.data.length > that.data.windowWidth) {
      //设置循环
      interval = setInterval(function () {
        if (that.data.offsetLeft <= 0) {
          if (that.data.offsetLeft >= -that.data.length) {
            that.setData({
              offsetLeft: that.data.offsetLeft - that.data.pace,
            })
          } else {
            that.setData({
              offsetLeft: that.data.windowWidth * 2,
            })
          }
        } else {
          that.setData({
            offsetLeft: that.data.offsetLeft - that.data.pace,
          })
        }
      }, that.data.interval);
    }
  },
  //开始跑马灯
  startMarquee: function () {
    var that = this;
    that.stopMarquee();
    //初始化数据
    that.data.windowWidth = wx.getSystemInfoSync().windowWidth;
    that.queryViewWidth().then(function (resolve) {
      that.data.length = resolve;
      console.log(that.data.length + "/" + that.data.windowWidth);
      that.excuseAnimation();
    });
  },
  startAction() {
    this.setData({
      start: true
    })
    setTimeout(() => {
      this.setData({
        start: false,
      })
      //其他代码部分
      //time是接口请求开始到结束的时间
    }, 7000)
    var timestamp = new Date().getTime()
    this.setData({
      startTime: timestamp
    })
  },
  endAction() {
    var timestamp = new Date().getTime()
    this.setData({
      endTime: timestamp
    })
    let dvalueTime = this.data.endTime - this.data.startTime
    let currentval = (dvalueTime / 3000).toPrecision(3)
    let goalval = Math.ceil(currentval);
    let timeval = (goalval - currentval) * 1000 + 1000
    console.log(timeval);
    let _this = this
    setTimeout(() => {
      _this.setData({
        start: false,
        end: true
      })
      //其他代码部分
      //time是接口请求开始到结束的时间
    }, timeval)
  },

  // 点击抽奖
  // clickPrize() {

  //   // 如果不在抽奖状态中，则执行抽奖旋转动画
  //   if (this.data.isTurnOver) {
  //     // 把抽奖状态改为未完成
  //     this.setData({
  //       isTurnOver: false
  //     })
  //     // 这里开始假设已经调取后端接口拿到抽奖后返回的ID
  //     let prize_id = 8;
  //     // 调用抽奖方法
  //     this.lottery(prize_id);
  //   } else {
  //     wx.showToast({
  //       title: '请勿重复点击',
  //       icon: 'none'
  //     })
  //   }
  // },

  // 九宫格抽奖旋转动画方法
  lottery2(prize_id) {

    console.log('中奖ID：' + prize_id)
    /*
     * 数组的长度就是最多所转的圈数，最后一圈会转到中奖后的位置
     * 数组里面的数字表示从一个奖品跳到另一个奖品所需要的时间
     * 数字越小速度越快
     * 想要修改圈数和速度的，更改数组个数和大小即可
     */
    let num_interval_arr = [90, 80, 70, 60, 50, 50, 50, 100, 150, 250];
    // 旋转的总次数
    let sum_rotate = num_interval_arr.length;
    // 每一圈所需要的时间
    let interval = 0;
    num_interval_arr.forEach((delay, index) => {
      setTimeout(() => {
        this.rotateCircle(delay, index + 1, sum_rotate, prize_id);
      }, interval)
      //因为每一圈转完所用的时间是不一样的，所以要做一个叠加操作
      interval += delay * 8;
    })
  },

  /*
   * 封装旋转一圈的动画函数，最后一圈可能不满一圈
   * delay:表示一个奖品跳到另一个奖品所需要的时间
   * index:表示执行到第几圈
   * sum_rotate：表示旋转的总圈数
   * prize_id：中奖后的id号
   */
  rotateCircle(delay, index, sum_rotate, prize_id) {
    // console.log(index)
    let _this = this;
    /*
     * 页面中奖项的实际数组下标
     * 0  1  2
     * 3     5
     * 6  7  8
     * 所以得出转圈的执行顺序数组为 ↓
     */
    let order_arr = [0, 1, 2, 5, 8, 7, 6, 3];
    // 页面奖品总数组
    let prize_arr = this.data.prize_arr;
    // 如果转到最后以前，把数组截取到奖品项的位置
    if (index == sum_rotate) {
      order_arr.splice(prize_id)
    }
    for (let i = 0; i < order_arr.length; i++) {
      setTimeout(() => {
        // 清理掉选中的状态
        prize_arr.forEach(e => {
          e.isSelected = false
        })
        // 执行到第几个就改变它的选中状态
        prize_arr[order_arr[i]].isSelected = true;
        // 更新状态
        _this.setData({
          prize_arr: prize_arr
        })
        // 如果转到最后一圈且转完了，把抽奖状态改为已经转完了
        if (index == sum_rotate && i == order_arr.length - 1) {
          _this.setData({
            isTurnOver: true
          })

        }
      }, delay * i)
    }
  },
  showRulek() {
    wx.uma.trackEvent("click_draw_rule", {
      actId: this.data.lottery
    });
    this.setData({
      showRule: true
    })
  },
  hideRulek() {
    this.setData({
      showRule: false
    })
  },
  showPrizek() {
    this.setData({
      showPrize: true
    })
  },
  hideNone() {
    this.setData({
      shownone: false
    })
  },
  showNone() {
    this.setData({
      shownone: true
    })
  },
  hidePrizek() {
    this.setData({
      showPrize: false
    })
  },
  toPrizelist() {
    wx.uma.trackEvent("click_draw_record", {
      actId: this.data.lottery
    });
    wx.navigateTo({
      url: `/module_games/prizeList/index?actId=${this.data.lottery}`,
    })
  },
  // 点击抽奖按钮
  lottery() {
    console.log('点击抽奖');
    wx.uma.trackEvent("click_draw", {
      actId: this.data.lottery
    });
    let prize_id = 0;
    // if (this.data.usableCount > 0) {
    //   this.setData({
    //     handstatus: true
    //   })
    //   console.log(this.data.isTurnOver);
    //   console.log(wx.getStorageSync('userInfo').nickName);
    // 如果不在抽奖状态中，则执行抽奖旋转动画
    if (this.data.isTurnOver) {
      // 加速闪灯状态
      // 把抽奖状态改为未完成
      this.setData({
        isTurnOver: false,
        handstatus: true
      })
      httpResource('doLottery', {
        actId: this.data.lottery,
        nickName: wx.getStorageSync('userInfo').nickName
      }, (data) => {
        console.log(data);
        if (data) {
          prize_id = data.prizeSortNo
          this.setData({
            prizeType: data.prizeType,
            prizeName: data.prizeName,
            prizeImgUrl: data.prizeImgUrl,
          })

          this.rotate(prize_id)
          this.getusablecount();

          this.setData({
            active: false
          })
          clearInterval(timer)
          timer = setInterval(() => {
            this.setData({
              active: !this.data.active
            })
            // console.log(that.data.active);
          }, 100)
        }
      }, "POST", () => {
        // 请求活动不在范围内
        this.setData({
          handstatus: false
        })
        this.setData({
          isTurnOver: true
        })
      });
    } else {
      wx.showToast({
        title: '请勿重复点击',
        icon: 'none'
      })
    }
    // } else {
    //   wx.showToast({
    //     title: '可以通过完成任务赢抽奖机会',
    //     icon: 'none'
    //   })
    // }

  },
  toMine() {
    if (app.globalData.lotterynavigate) {
      wx.switchTab({
        url: app.globalData.lotterynavigate
      })
    } else {
      wx.switchTab({
        url: '/pages/index/index'
      })
    }
    app.globalData.lotterynavigate = ''
  },
  // 旋转动画方法
  rotate(prize_id) {
    console.log(prize_id);
    // 执行完动画所需要的时间
    let _duration = 10000;
    let animationRotate = wx.createAnimation({
      duration: _duration,
      timingFunction: 'ease', //动画以低速开始，然后加快，在结束前变慢
    })
    // 解决二次点击抽奖后不再旋转的问题
    animationRotate.rotate(0).step({
      duration: 1
    })
    /*
     * 旋转的角度
     * 这转盘有6个奖项，所以一个奖项的度数为：360除以6等于60、
     * 要转完一圈 → 60 * 6
     * 为了让动画看起来舒服我设置了20圈 → 60 * 6 * 20
     * 又因为转盘是顺时针旋转的，默认指定奖品1
     * 所以需要减去 → 60 * (prize_id - 1) 方可在最后一圈转到对应的位置
     * 可以根据自己的设计稿奖品的个数进行调整
     * */
    let angle = (360 * 20) - (360 / this.data.prizeNumber) * (prize_id - 1);
    animationRotate.rotate(angle).step()
    this.setData({
      animationRotate: animationRotate.export()
    })
    // 设置倒计时，保证最后一圈执行完了，才更改状态
    setTimeout(() => {
      this.setData({
        isTurnOver: true,
        active: false,
        handstatus: false
      })
      // 减速闪灯状态
      clearInterval(timer)
      timer = setInterval(() => {
        this.setData({
          active: !this.data.active
        })
        // console.log(that.data.active);
      }, 500)
      if (this.data.prizeType == 0) {
        this.showNone()
      } else {
        this.showPrizek()
      }


      this.setData({
        handstatus: false
      })
    }, _duration)
  },
  sharepengyou() {
    wx.navigateTo({
      url: '/module_games/sharelottery/index',
    })
  },
  todotask(e) {
    let item = e.currentTarget.dataset.item;
    console.log(item);
    httpResource('doTask', {
      actId: item.actId,
      taskId: item.id,
      level: item.taskLevel,
      rewardLottery: item.rewardLottery
    }, (data) => {
      console.log(data);
      if (data) {
        switch (item.taskType) {
          case 1:
            // wx.navigateTo({
            //     url: `/module_games/sharelottery/index?actId=${item.actId}&taskId=${item.id}`,
            // })
            console.log(item.id);
            this.setData({
              currentTaskid: item.id
            })
            break;
          case 2:
            wx.uma.trackEvent("click_pyq_complete", {
              actId: this.data.lottery
            });
            let userInfo = wx.getStorageSync('userInfo');
            wx.navigateTo({
              url: `/module_games/sharelottery/index?actId=${item.actId}&taskId=${item.id}&taskChainId=${data.taskChainId}&type=1&srcUserId=${userInfo.userId}`,
            })
            break;
          case 3:
            wx.uma.trackEvent("click_evaluate_complete", {
              actId: this.data.lottery
            });
            wx.navigateTo({
              url: '/module_order/orderList/orderList?orderType=2',
            })
            break;
          case 4:
            wx.uma.trackEvent("click_consumption_complete", {
              actId: this.data.lottery
            });
            wx.switchTab({
              url: '/pages/index/index',
            })
            break;
        }
      }
    }, 'post')

  },
  checkUserLocation: function () {
    let _this = this;
    wx.getSetting({
      success: (res) => {
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
          wx.showModal({
            title: '请求授权当前位置',
            content: '需要获取您的地理位置，请确认授权',
            success: function (res) {
              if (res.cancel) {
                wx.showToast({
                  title: '拒绝授权',
                  icon: 'none',
                  duration: 1000
                })
              } else if (res.confirm) {
                wx.openSetting({
                  success: function (dataAu) {
                    if (dataAu.authSetting["scope.userLocation"] == true) {
                      wx.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 1000
                      })
                      //再次授权，调用wx.getLocation的API
                      getUserLocation().then(res => {
                        let {
                          latitude,
                          longitude
                        } = res;
                        _this.setData({
                          latitude: latitude,
                          longitude: longitude,
                        }, _this.getData('ok'));
                      })
                    } else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'none',
                        duration: 1000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) {
          //调用wx.getLocation的API
          getUserLocation().then(res => {
            let {
              latitude,
              longitude
            } = res;
            _this.setData({
              latitude: latitude,
              longitude: longitude,
            }, _this.getData('ok'));
          })
        } else {
          //调用wx.getLocation的API
          getUserLocation().then(res => {
            let {
              latitude,
              longitude
            } = res;
            _this.setData({
              latitude: latitude,
              longitude: longitude,
            }, _this.getData('ok'));
          })
        }
      }
    })
  },
  /**
   * @param {Object} 
   * 获取门店列表
   */
  getData: debounce(async function (e) {
    const _this = this;
    const {
      value,
      latitude,
      longitude,
      pageSize,
      currentPage
    } = this.data;
    let data = await httpResource('getShopByKeywordsWithPage', {
      currentPage: currentPage,
      pageSize: pageSize,
      keywords: value,
      latitude,
      longitude
    });
    let list = [];
    if (data && data.records && data.records.length) {
      data = (data.records || []).map(item => {
        item.shopNameList = getHighlightStrArray(item.shopName, value);
        // 处理高亮
        return item
      });
      list = [...data];
      if (e == 'ok') {
        _this.setData({
          value: list[0].shopName,
          shopName: list[0].shopName || '会员小程序',
        })
        _this.getShopQrCode(list[0].shopCode)
      }
    }
    _this.setData({
      shopList: list,
      currentIndex: ''
    });
  }, 200),
  /**
   * 获取社群二维码
   */
  async getShopQrCode(shopCode) {
    const _this = this;
    const {
      shopName
    } = this.data;
    let data = await httpResource('getShopCommunityCode', {
      shopCode
    }, null, 'GET');
    _this.setData({
      qrcodeUrl: data || 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
      qrName: data ? shopName : '会员小程序',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    // const pages = getCurrentPages()    // 获取加载的页面
    // const currentPage = pages[pages.length - 1]    // 获取当前页面的对象
    // const url = currentPage.route    // 当前页面url
    // const op= currentPage.options    // 如果要获取url中所带的参数可以查看options


    // console.log(url,op,currentPage);

    let actCode = null;
    if (options?.scene) {
      actCode = decodeURIComponent(options.scene).split("=")[1];
    }
    let _this = this;

    let isLogin = wx.getStorageSync("u-token");
    if (options?.taskChainId || actCode) {
      this.setData({
        taskChainId: options.taskChainId ? options.taskChainId : actCode.split('_')[2],
      })
    }
    if (options?.srcUserId || actCode) {
      this.setData({

        srcUserId: options.srcUserId ? options.srcUserId : actCode.split('_')[1]
      })
    }
    // console.log(options.actId);
    // if(options?.actId.indexOf('?')!==-1){
    //   let a = options.actId.split('?')
    //   console.log(a);
    //   options.actId=a[0]
    // }
    // console.log(actCode);
    _this.setData({
      lottery: options?.actId ? (options.actId.indexOf('?') !== -1) ? options.actId.split('?')[0] : options.actId : actCode ? actCode.split('_')[0] : ''
    }, () => {

      if (_this.data.lottery && _this.data.lottery != null) {
        if (!isLogin) {
          wx.redirectTo({
            url: `/pages/userLogin/index?lottery=${_this.data.lottery}&taskChainId=${_this.data.taskChainId}&srcUserId=${_this.data.userId}`,
          });
        }
      }
    })

    wx.setPageStyle({
      style: {
        overflow: 'hidden'
      }
    })
    setTimeout(() => {
      this.setData({
        showLoading: false
      })
      wx.setPageStyle({
        style: {
          overflow: 'auto'
        }
      })
      this.startMarquee()
    }, 2000)
    // 经纬度
    getUserLocation().then(res => {
      let {
        latitude,
        longitude
      } = res;
      this.setData({
        latitude: latitude,
        longitude: longitude,
      }, this.getData('ok'));
    }).catch(() => {
      this.checkUserLocation()
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    let currentStore = wx.getStorageSync('current-store');
    console.log(currentStore.shopCode);
    if (currentStore) {
      this.setData({
        shopCode: currentStore.shopCode
      })
      this.showOrCODE()
      //   createQrcode2("qrcodelott", currentStore.shopCode, pxTorpx(176), pxTorpx(176), this);

    }
    wx.hideShareMenu()
    let _this = this;
    _this.checkInit()
    if (_this.data.lottery_page) {} else {
      _this.getActivity();
    }

    _this.getTaskInfo();
    _this.getusablecount();
    _this.newInterval(_this.like, 20000);
    let that = this
    timer = setInterval(() => {
      this.setData({
        active: !that.data.active
      })
      // console.log(that.data.active);
    }, 500)
  },
  //根据id查询活动图片
  async getActivity() {
    let _this = this;
    try {
      let data = await httpResource('getlotteryInfo', {
        actId: _this.data.lottery
      }, null, "get");
      // _this.setData({
      //     allImg:data
      // })
      data.actImgUrls.forEach(item => {
        switch (item.pagePosition) {
          case 100:
            _this.setData({
              lottery_page: item.imgUrl
            })
            break;
          case 201:
            _this.setData({
              horse_race_lamp_1: item.imgUrl
            })
            break;
          case 202:
            _this.setData({
              horse_race_lamp_2: item.imgUrl
            })
            break;
          case 300:
            _this.setData({
              turntable_content: item.imgUrl
            })
            break;
          case 400:
            _this.setData({
              lottery_button: item.imgUrl
            })
            break;
          case 500:
            _this.setData({
              click_icon: item.imgUrl
            })
            break;
          case 501:
            _this.setData({
              task_up: item.imgUrl
            })
            break;
          case 502:
            _this.setData({
              task_center: item.imgUrl
            })
            break;
          case 503:
            _this.setData({
              task_down: item.imgUrl
            })
            break;
          case 504:
            _this.setData({
              task_not_finished: item.imgUrl
            })
            break;
          case 505:
            _this.setData({
              task_finished: item.imgUrl
            })
            break;
          case 600:
            _this.setData({
              act_rule: item.imgUrl
            })
            break;
          case 700:
            _this.setData({
              wining_record: item.imgUrl
            })

            app.globalData.lotteryInfo.wining_record = item.imgUrl
            break;
          case 701:
            _this.setData({
              wining_record_button: item.imgUrl
            })
            app.globalData.lotteryInfo.wining_record_button = item.imgUrl
            break;
          case 702:
            _this.setData({
              unissued: item.imgUrl
            })
            app.globalData.lotteryInfo.unissued = item.imgUrl
            break;
          case 703:
            _this.setData({
              issued: item.imgUrl
            })
            app.globalData.lotteryInfo.issued = item.imgUrl
            break;
          case 704:
            _this.setData({
              wining_tips_wined: item.imgUrl
            })
            break;
          case 705:
            _this.setData({
              wining_no_wined: item.imgUrl
            })
            break;
          default:
            break;
        }
      })
      _this.setData({
        actRule: data.actRule,
        prizeNumber: data.prizeConfigNum,
        bgColor: data.backgroundColor,
        isShoWinningInfo: data.isShoWinningInfo,
        winningQrc: data.winningQrc
      })
      app.globalData.lotteryInfo.bgColor = data.backgroundColor
      // _this.setData({
      //     actRule:data.actRule
      // })
    } catch (error) {

    }
  },
  // 任务列表
  async getTaskInfo() {
    let _this = this;
    try {
      let data = await httpResource('getaskInfo', {
        actId: _this.data.lottery
      }, null, "get");
      console.log(data);
      _this.setData({
        taskList: data
      })
    } catch (error) {

    }
  },
  getusablecount() {
    let _this = this
    httpResource('lotteryUsableCount', {
      actId: _this.data.lottery
    }, (data) => {
      _this.setData({
        usableCount: data
      })
    }, "get");
  },
  getRunHorse() {
    let _this = this
    if (horseTIme) {
      clearInterval(horseTIme)
    }
    horseTIme = setInterval(() => {
      httpResource('getWinninginfo', {
        actId: _this.data.lottery
      }, (data) => {
        _this.setData({
          runHorselist: data
        })
      }, "get");
      // this.setData({
      //     timeout:5000
      // })
    }, 5000)

  },
  newInterval(func, millisecond) {
    function inside() {
      func()
      setTimeout(inside, millisecond) //第二次往后调用，每秒后调用自身
    }
    setTimeout(inside, 0) // 第一次运行调用
  },



  like() {
    let _this = this
    httpResource('getWinninginfo', {
      actId: _this.data.lottery
    }, (data) => {
      _this.setData({
        runHorselist: data
      })
    }, "get");
  },

  checkInit() {

    let _this = this
    console.log(_this.data.taskChainId);
    httpResource('checkInit', {
      actId: _this.data.lottery,
      taskChainId: _this.data.taskChainId,
      srcUserId: _this.data.srcUserId
    }, (data) => {
      // console.log(data,msg);
      if (data.code) {
        this.setData({
          activeStatus: data.code
        })
        wx.showToast({
          title: data.message,
          icon: 'none',
          duration: 1000
        })
      }

    }, "post");
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    clearInterval(timer)
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(timer)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: async function (e) {
    console.log(e);
    wx.uma.trackEvent("click_wechat_complete", {
      actId: this.data.lottery
    });
    // debugger
    let taskinfo = await httpResource('doTask', {
      actId: e.target.dataset.actid,
      taskId: e.target.dataset.id,
      level: e.target.dataset.tasklevel,
      rewardLottery: e.target.dataset.rewardlottery
    }, null, 'post')
    let data = await httpResource('getShareImgs', {
      actId: this.data.lottery,
      taskId: e.target.dataset.id
    }, null, 'get')

    // httpResource('getShareImgs', {
    //     actId: this.data.lottery,
    //     taskId: e.target.dataset.id
    // }, (data) => {
    //     console.log(data);
    //     return {
    //         title: '快来好特卖抽奖',
    //         path: `/module_games/lotteryDraw/lotteryDraw?actId=${this.data.lottery}&taskChainId=${this.data.currentTaskid}`,
    //         imageUrl: data[0].imgUrl
    //     }
    // }, 'get')
    console.log(data);
    console.log(taskinfo);
    let userInfo = wx.getStorageSync('userInfo');
    return {
      title: data[0].titleDesc,
      path: `/module_games/lotteryDraw/lotteryDraw?actId=${this.data.lottery}&taskChainId=${taskinfo.taskChainId}&srcUserId=${userInfo.userId}`,
      imageUrl: data[0].imgUrl
    }

  },
  // onPageScroll:function(e){
  //     if(e.scrollTop<0){
  //       wx.pageScrollTo({
  //         scrollTop: 0
  //       })
  //     }
  //   }
})