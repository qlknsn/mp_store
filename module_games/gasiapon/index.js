// module_games/gasiapon/index.js
let timer = null
const {
  httpResource,
  pxTorpx
} = require("../../assets/js/common.js");
import {
  createQrcode2
} from "../../assets/js/codeUtil";
var app = getApp();
import {
  getUserLocation,
  getHighlightStrArray
} from '../../assets/js/commonData.js';
const {
  debounce
} = require('../../assets/js/eventUtil');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // loading
    showLoading: true,
    // 规则
    showRule: false,
    // 抽中奖品
    showPrize: false,
    // 未抽中
    shownone: false,
    // 控制球运动
    start: false,
    // 出口闪灯状态
    active: true,
    t: 500,
    centerstart: false,
    bottomstart: false,


    showLoading: true,
    lottery: null,
    taskList: [], //任务列表,\
    taskChainId: null, //分享的id
    currentTaskid: null, //分享出去的id


    actRule: '', //获取抽奖次数
    gasiaponData: {}, //抽奖信息
    usableCount: 0, //抽奖次数,



    latitude: null,
    longitude: null,
    shopList: [],
    shopName: '会员小程序',
    qrcodeUrl: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg', // 二维码地址
    qrName: '会员小程序',
    value: '',
    currentPage: 1,
    pageSize: 6,
    lcode: null,


    lottery_page: '', //背景
    egg_twisting_machine: '', //扭蛋机
    egg_bottom_twisting_machine: '', //扭蛋机底座
    horse_race_lamp_1: '', //跑马灯
    horse_race_lamp_2: '', //跑马灯
    big_egg_twisting_machine_1: '', //大扭蛋
    big_egg_twisting_machine_2: '', //大扭蛋
    big_egg_twisting_machine_3: '', //大扭蛋
    big_egg_twisting_machine_4: '', //大扭蛋
    word_small_big_egg_twisting_machine_1: '', //小扭蛋
    word_small_big_egg_twisting_machine_2: '', //小扭蛋
    word_small_big_egg_twisting_machine_3: '', //小扭蛋
    word_small_big_egg_twisting_machine_4: '', //小扭蛋
    small_big_egg_twisting_machine_1: '', //小扭蛋
    small_big_egg_twisting_machine_2: '', //小扭蛋
    small_big_egg_twisting_machine_gun: '', //小扭蛋
    lottery_no_button: '', //抽奖按钮未点击
    lottery_button: '', //抽奖按钮 点击
    click_icon: '', //点击 图标
    task_up: '', // 任务框 上
    task_center: '', // 任务框 中
    task_down: '', // 任务框 中
    task_not_finished: '', // 任务框(未完成)
    task_finished: '', // 任务框(已完成)
    act_rule: '', // 活动规则
    wining_record: '', // 中奖记录
    wining_record_button: '', // 中奖记录按钮
    unissued: '', // 发放状态已发好
    issued: '', // 发放状态未发好
    wining_tips_wined: '', // 中奖弹框 中奖
    wining_tips_no_win: '', // 中奖弹框 未中奖
    dianActive: false, // 按钮

    isShowHand: true,
    bottomDesc: 0,
    activeStatus: "",
    bgColor: '',
    srcUserId: '',
    winningQrc: 1, //1:社群吗  2：门店吗  3：隐藏
    shopCode: '', //门店码
    shopCodeImg: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg', //门店吗图片
  },
  // 获取门店码
  showOrCODE() {
    let filter = {
      shopCode: this.data.shopCode,
    };
    httpResource(
      "getCommunityManagerCode",
      filter,
      (data) => {
        this.setData({
          shopCodeImg: data.shopManagerImgUrl || 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
        });
      },
      "GET",
      null
    );
  },
  // 去做任务
  todotask(e) {
    let item = e.currentTarget.dataset.item;
    console.log(item);
    httpResource('doTask', {
      actId: item.actId,
      taskId: item.id,
      level: item.taskLevel,
      rewardLottery: item.rewardLottery
    }, (data) => {
      console.log(data);
      if (data) {
        switch (item.taskType) {
          case 1:
            // wx.navigateTo({
            //     url: `/module_games/sharelottery/index?actId=${item.actId}&taskId=${item.id}`,
            // })

            this.setData({
              currentTaskid: item.id
            })
            break;
          case 2:
            wx.uma.trackEvent("click_pyq_complete", {
              'actId': item.actId
            });
            let userInfo = wx.getStorageSync('userInfo');
            console.log(data);
            wx.navigateTo({
              url: `/module_games/sharelottery/index?actId=${item.actId}&taskId=${item.id}&taskChainId=${data.taskChainId}&type=2&srcUserId=${userInfo.userId}`,
            })
            break;
          case 3:
            wx.uma.trackEvent("click_evaluate_complete", {
              'actId': item.actId
            });
            wx.navigateTo({
              url: '/module_order/orderList/orderList?orderType=2',
            })
            break;
          case 4:
            wx.uma.trackEvent("click_consumption_complete", {
              'actId': item.actId
            });
            wx.switchTab({
              url: '/pages/index/index',
            })
            break;
        }
      }
    }, 'post')

  },
  // 展示活动规则
  showRulek() {
    wx.uma.trackEvent("click_draw_rule", {
      actId: this.data.lottery
    });
    this.setData({
      showRule: true
    })
  },
  // 隐藏活动规则
  hideRulek() {
    this.setData({
      showRule: false
    })
  },
  // 展示中奖弹框
  showPrizek() {
    this.setData({
      showPrize: true
    })
  },
  // 隐藏中奖弹框
  hidePrizek() {
    this.setData({
      t: 500,
      showPrize: false
    })
    clearInterval(timer)
    timer = setInterval(() => {
      this.setData({
        active: !this.data.active,
        dianActive: !this.data.dianActive,
      })
      // console.log(that.data.active);
    }, this.data.t)
  },
  // 隐藏未中奖弹框
  hideNone() {
    this.setData({
      t: 500,
      shownone: false
    })
    clearInterval(timer)
    timer = setInterval(() => {
      this.setData({
        active: !this.data.active,
        dianActive: !this.data.dianActive,
      })
      // console.log(that.data.active);
    }, this.data.t)
  },
  // 展示未中奖弹框
  showNone() {
    this.setData({
      shownone: true
    })
  },
  sharepengyou() {
    wx.navigateTo({
      url: '/module_games/sharelottery/index',
    })
  },
  // 获取用户位置
  checkUserLocation: function () {
    let _this = this;
    wx.getSetting({
      success: (res) => {
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
          wx.showModal({
            title: '请求授权当前位置',
            content: '需要获取您的地理位置，请确认授权',
            success: function (res) {
              if (res.cancel) {
                wx.showToast({
                  title: '拒绝授权',
                  icon: 'none',
                  duration: 1000
                })
              } else if (res.confirm) {
                wx.openSetting({
                  success: function (dataAu) {
                    if (dataAu.authSetting["scope.userLocation"] == true) {
                      wx.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 1000
                      })
                      //再次授权，调用wx.getLocation的API
                      getUserLocation().then(res => {
                        let {
                          latitude,
                          longitude
                        } = res;
                        _this.setData({
                          latitude: latitude,
                          longitude: longitude,
                        }, _this.getData('ok'));
                      })
                    } else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'none',
                        duration: 1000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) {
          //调用wx.getLocation的API
          getUserLocation().then(res => {
            let {
              latitude,
              longitude
            } = res;
            _this.setData({
              latitude: latitude,
              longitude: longitude,
            }, _this.getData('ok'));
          })
        } else {
          //调用wx.getLocation的API
          getUserLocation().then(res => {
            let {
              latitude,
              longitude
            } = res;
            _this.setData({
              latitude: latitude,
              longitude: longitude,
            }, _this.getData('ok'));
          })
        }
      }
    })
  },
  /**
   * @param {Object} e
   * 获取门店列表
   */
  getData: debounce(async function (e) {
    const _this = this;
    const {
      value,
      latitude,
      longitude,
      pageSize,
      currentPage
    } = this.data;
    let data = await httpResource('getShopByKeywordsWithPage', {
      currentPage: currentPage,
      pageSize: pageSize,
      keywords: value,
      latitude,
      longitude
    });
    let list = [];
    if (data && data.records && data.records.length) {
      data = (data.records || []).map(item => {
        item.shopNameList = getHighlightStrArray(item.shopName, value);
        // 处理高亮
        return item
      });
      list = [...data];
      if (e == 'ok') {
        _this.setData({
          value: list[0].shopName,
          shopName: list[0].shopName || '会员小程序',
        })
        _this.getShopQrCode(list[0].shopCode)
      }
    }
    _this.setData({
      shopList: list,
      currentIndex: ''
    });
  }, 200),
  /**
   * 获取社群二维码
   */
  async getShopQrCode(shopCode) {
    const _this = this;
    const {
      shopName
    } = this.data;
    let data = await httpResource('getShopCommunityCode', {
      shopCode
    }, null, 'GET');
    _this.setData({
      qrcodeUrl: data || 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
      qrName: data ? shopName : '会员小程序',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this

    let actCode = null;
    if (options?.scene) {
      actCode = decodeURIComponent(options?.scene).split("=")[1];
    }
    let isLogin = wx.getStorageSync("u-token");
    if (options?.taskChainId || actCode) {
      this.setData({
        taskChainId: options.taskChainId ? options.taskChainId : actCode.split('_')[2],
      })
    }
    if (options?.srcUserId || actCode) {
      this.setData({

        srcUserId: options.srcUserId ? options.srcUserId : actCode.split('_')[1]
      })
    }

    that.setData({
      lottery: options?.actId ? (options.actId.indexOf('?') !== -1) ? options.actId.split('?')[0] : options.actId : actCode ? actCode.split('_')[0] : ''
    }, () => {

      if (that.data.lottery && that.data.lottery != null) {
        if (!isLogin) {
          wx.redirectTo({
            url: `/pages/userLogin/index?gasiaponcode=${that.data.lottery}&niudanShareid=${that.data.taskChainId}&srcUserId=${that.data.userId}`,
          });
        }
      }
    })
    wx.setPageStyle({
      style: {
        overflow: 'hidden'
      }
    })
    setTimeout(() => {
      this.setData({
        showLoading: false
      })
      wx.setPageStyle({
        style: {
          overflow: 'auto'
        }
      })

    }, 2000)



    getUserLocation().then(res => {
      let {
        latitude,
        longitude
      } = res;
      this.setData({
        latitude: latitude,
        longitude: longitude,
      }, this.getData('ok'));
    }).catch(() => {
      this.checkUserLocation()
    });
  },
  //根据id查询活动图片
  async getActivity() {
    let _this = this;
    try {
      let data = await httpResource('getlotteryInfo', {
        actId: _this.data.lottery
      }, null, "get");
      // _this.setData({
      //     allImg:data
      // })
      data.actImgUrls.forEach(item => {
        switch (item.pagePosition) {
          case 150:
            _this.setData({
              lottery_page: item.imgUrl
            })
            break;
          case 250:
            _this.setData({
              egg_twisting_machine: item.imgUrl
            })
            break;
          case 251:
            _this.setData({
              egg_bottom_twisting_machine: item.imgUrl
            })
            break;
          case 252:
            _this.setData({
              horse_race_lamp_1: item.imgUrl
            })
            break;
          case 253:
            _this.setData({
              horse_race_lamp_2: item.imgUrl
            })
            break;
          case 350:
            _this.setData({
              big_egg_twisting_machine_1: item.imgUrl
            })
            break;
          case 351:
            _this.setData({
              big_egg_twisting_machine_2: item.imgUrl
            })
            break;
          case 352:
            _this.setData({
              big_egg_twisting_machine_3: item.imgUrl
            })
            break;
          case 353:
            _this.setData({
              big_egg_twisting_machine_4: item.imgUrl
            })
            break;
          case 354:
            _this.setData({
              word_small_big_egg_twisting_machine_1: item.imgUrl
            })
            break;
          case 355:
            _this.setData({
              word_small_big_egg_twisting_machine_2: item.imgUrl
            })
            break;
          case 356:
            _this.setData({
              word_small_big_egg_twisting_machine_3: item.imgUrl
            })
            break;
          case 357:
            _this.setData({
              word_small_big_egg_twisting_machine_4: item.imgUrl
            })
            break;
          case 358:
            _this.setData({
              small_big_egg_twisting_machine_1: item.imgUrl
            })
            break;
          case 359:
            _this.setData({
              small_big_egg_twisting_machine_2: item.imgUrl
            })

            break;
          case 360:
            _this.setData({
              small_big_egg_twisting_machine_gun: item.imgUrl
            })
            break;
          case 450:
            _this.setData({
              lottery_no_button: item.imgUrl
            })
            break;
          case 451:
            _this.setData({
              lottery_button: item.imgUrl
            })
            break;
          case 550:
            _this.setData({
              click_icon: item.imgUrl
            })
            break;
          case 551:
            _this.setData({
              task_up: item.imgUrl
            })
            break;
          case 552:
            _this.setData({
              task_center: item.imgUrl
            })
            break;
          case 553:
            _this.setData({
              task_down: item.imgUrl
            })
            break;
          case 554:
            _this.setData({
              task_not_finished: item.imgUrl
            })
            break;
          case 555:
            _this.setData({
              task_finished: item.imgUrl
            })
            break;
          case 650:
            _this.setData({
              act_rule: item.imgUrl
            })
            break;
          case 750:
            _this.setData({
              wining_record: item.imgUrl
            })
            app.globalData.lotteryInfo.wining_record = item.imgUrl
            break;
          case 751:
            _this.setData({
              wining_record_button: item.imgUrl
            })
            app.globalData.lotteryInfo.wining_record_button = item.imgUrl
            break;
          case 752:
            _this.setData({
              unissued: item.imgUrl
            })
            app.globalData.lotteryInfo.unissued = item.imgUrl
            break;
          case 753:
            _this.setData({
              issued: item.imgUrl
            })
            app.globalData.lotteryInfo.issued = item.imgUrl
            break;
          case 850:
            _this.setData({
              wining_tips_wined: item.imgUrl
            })
            break;
          case 851:
            _this.setData({
              wining_tips_no_win: item.imgUrl
            })
            break;
          default:
            break;
        }
      })
      _this.setData({
        actRule: data.actRule,
        prizeNumber: data.prizeConfigNum,
        bgColor: data.backgroundColor,
        winningQrc: data.winningQrc
      })
      app.globalData.lotteryInfo.bgColor = data.backgroundColor
      // _this.setData({
      //     actRule:data.actRule
      // })
    } catch (error) {

    }
  },
  startAction() {

    let that = this
    this.setData({
      centerstart: true,
      bottomstart: true
    })
    setTimeout(() => {
      wx.vibrateLong();
      this.setData({
        start: false,
        t: 500,
        active: false,
        centerstart: false,
        bottomstart: false,
        isShowHand: true
      })

      clearInterval(timer)
      timer = setInterval(() => {
        this.setData({
          active: !that.data.active,

        })
        // console.log(that.data.active);
      }, this.data.t)
      if (this.data.gasiaponData.prizeType == 0) {
        this.showNone()
      } else {
        this.showPrizek()
      }



    }, 7000)
  },
  toMine() {
    if (app.globalData.lotterynavigate) {
      wx.switchTab({
        url: app.globalData.lotterynavigate
      })
    } else {
      wx.switchTab({
        url: '/pages/index/index'
      })
    }
    app.globalData.lotterynavigate = ''
  },
  dianChou() {
    console.log('点击抽奖');
    wx.uma.trackEvent("click_draw", {
      actId: this.data.lottery
    });
    let that = this
    if (this.data.start) {
      wx.showToast({
        title: '请勿重复点击',
        icon: 'none'
      })
    } else {
      // if (this.data.usableCount > 0) {
      this.setData({
        start: true,
      })
      console.log(that.data.start);
      httpResource('doLottery', {
        actId: this.data.lottery,
        nickName: wx.getStorageSync('userInfo').nickName
      }, (data) => {
        if (data) {
          this.setData({
            t: 100,
            active: false,
            gasiaponData: data,
            isShowHand: false,
            dianActive: false
          })
          clearInterval(timer)
          timer = setInterval(() => {
            this.setData({
              active: !that.data.active
            })
          }, this.data.t)
          this.startAction()
          this.getusablecount();
        } else {



        }
      }, "POST", () => {
        setTimeout(() => {
          that.setData({
            start: false,
          })
        }, 8000)
        // 请求活动不在范围内
        // this.setData({
        //     handstatus: false
        // })
      });
      // } else {
      //   wx.showToast({
      //     title: '可以通过完成任务赢抽奖机会',
      //     icon: 'none'
      //   })
      // }


    }


  },
  toPrizelist() {
    wx.uma.trackEvent("click_draw_record", {
      actId: this.data.lottery
    });
    wx.navigateTo({
      url: `/module_games/gasprizeList/index?actId=${this.data.lottery}`,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  checkInit() {

    httpResource('checkInit', {
      actId: this.data.lottery,
      taskChainId: this.data.taskChainId,
      srcUserId: this.data.srcUserId
    }, (data) => {
      if (data) {
        this.setData({
          activeStatus: data.code
        })
        wx.showToast({
          title: data.message,
          icon: 'none',
          duration: 1000
        })
      }
    }, "post");
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    let currentStore = wx.getStorageSync('current-store');
    console.log(currentStore.shopCode);
    if (currentStore) {
      this.setData({
        shopCode: currentStore.shopCode
      })
      this.showOrCODE()
      //   createQrcode2("qrcodelott", currentStore.shopCode, pxTorpx(176), pxTorpx(176), this);

    }

    let that = this
    that.checkInit()
    wx.hideShareMenu()
    if (that.data.lottery_page) {} else {
      that.getActivity();
    }

    if (that.data.start) {} else {
      wx.onAccelerometerChange(function (e) {
        if (e.x > 1.5 || e.y > 1.5 || e.z > 1.5) {
          wx.vibrateLong();
          that.setData({
            start: true,
          })
          setTimeout(() => {
            that.setData({
              start: false,
            })
          }, 7000)
        }
      })
    }
    that.getTaskInfo();
    that.getusablecount();
    if (timer) {
      clearInterval(timer)
    }
    timer = setInterval(() => {
      this.setData({
        active: !that.data.active,
        dianActive: !that.data.dianActive
      })
      // console.log(that.data.active);
    }, this.data.t)
  },
  getusablecount() {
    let _this = this
    httpResource('lotteryUsableCount', {
      actId: _this.data.lottery
    }, (data) => {
      _this.setData({
        usableCount: data
      })
    }, "get");
  },
  // 任务列表
  async getTaskInfo() {
    let _this = this;
    try {
      let data = await httpResource('getaskInfo', {
        actId: _this.data.lottery
      }, null, "get");
      console.log(data);
      _this.setData({
        taskList: data
      })
    } catch (error) {

    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    clearInterval(timer)
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(timer)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: async function (e) {
    wx.uma.trackEvent("click_wechat_complete", {
      actId: this.data.lottery
    });
    console.log(e);
    let data = await httpResource('getShareImgs', {
      actId: this.data.lottery,
      taskId: e.target.dataset.id
    }, null, 'get')
    let taskinfo = await httpResource('doTask', {
      actId: e.target.dataset.actid,
      taskId: e.target.dataset.id,
      level: e.target.dataset.tasklevel,
      rewardLottery: e.target.dataset.rewardlottery
    }, null, 'post')
    console.log(taskinfo);
    let userInfo = wx.getStorageSync('userInfo');
    console.log(this.data.lottery);
    return {
      title: data[0].titleDesc,
      path: `/module_games/gasiapon/index?actId=${this.data.lottery}&taskChainId=${taskinfo.taskChainId}&srcUserId=${userInfo.userId}`,
      imageUrl: data[0].imgUrl
    }
  }
})