// module_games/sudoku/sudoku.js
let timer = null
let radomTimer = null
const {
  httpResource,
} = require("../../assets/js/common.js");
var app = getApp();
import {
  getUserLocation,
  getHighlightStrArray
} from '../../assets/js/commonData.js';
const {
  debounce,
  throttl
} = require('../../assets/js/eventUtil');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lottery: null, //活动id
    // loading
    showLoading: true,
    // 规则
    showRule: false,
    // 未抽中
    shownone: false,
    // 抽中奖品
    showPrize: false,

    act_rule: '', // 活动规则背景
    actRule: '', //活动规则文字
    wining_tips_wined: '', // 中奖弹框 中奖
    wining_tips_no_win: '', // 中奖弹框 未中奖
    sudokuData: {
      prizeName: '',
      prizeImgUrl: ""
    },
    winningQrc: 1, //1:社群吗  2：门店吗  3：隐藏
    shopCodeImg: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg', //门店吗图片
    qrcodeUrl: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg', // 二维码地址

    lightshow: true,
    t: 300,

    prize_arr: [
      { id: '1', type: 'prize', name: '奖品1', isSelected: false },
      { id: '2', type: 'prize', name: '奖品2', isSelected: false },
      { id: '3', type: 'prize', name: '奖品3', isSelected: false },
      { id: '4', type: 'prize', name: '奖品8', isSelected: false },
      { id: '5', type: 'btn', name: '按钮', isSelected: false },
      { id: '6', type: 'prize', name: '奖品4', isSelected: false },
      { id: '7', type: 'prize', name: '奖品7', isSelected: false },
      { id: '8', type: 'prize', name: '奖品6', isSelected: false },
      { id: '9', type: 'prize', name: '奖品5', isSelected: false },
    ],
    // 抽奖状态，是否转完了
    isTurnOver: true,
    taskList: [], //任务列表,
    bgColor: '#143e87',//背景颜色
    // handstatus: true,
    selectGoods: 1, //选中的盲盒位置



    lottery_page: '', //大背景图片
    horse_race_lamp_1: '', //灯亮灭
    horse_race_lamp_2: '', //灯亮灭
    guizi: '', //盲盒柜子

    click_icon: '', //点击图标
    task_border_top: '',//任务框上
    task_border_middle: '',//任务框中
    task_border_bottom: '',//任务框下
    task_border_unfinish: '',//任务框未完成
    task_border_finished: '',//任务框已完成
    activity_rule: '',//活动规则
    wining_record: '',//中奖记录
    wining_record_btn: '',//中奖记录按钮
    unissued: '',//发放状态（未发放）
    issued: '',//发放状态（已发放）

    actRule: '', //活动规则文字
    wining_tips_wined: '', // 中奖弹框 中奖
    wining_tips_no_win: '', // 中奖弹框 未中奖

    prizeNumber: 0,//几等分
    bgColor: '',//背景色
    isShoWinningInfo: '',//是否展示走马灯

    taskChainId: null, //分享的id
    currentTaskid: null, //分享出去的id
    prizeType: '', //抽奖类型0：谢谢  1：优惠券
    prizeName: '', //奖品名称
    prizeImgUrl: '', //奖品图片
    usableCount: 0,//抽奖次数
    value: '',
  },
//展示活动规则弹框
  showRulek() {
    // wx.uma.trackEvent("click_draw_rule", { actId: this.data.lottery });
    this.setData({
      showRule: true
    })
  },
  //活动页返回上一级操作
  toMine() {
    if (app.globalData.lotterynavigate) {
      //判断是否在全局存储上一级路由，有则跳转，无则跳转默认首页路由
      wx.switchTab({
        url: app.globalData.lotterynavigate
      })
    } else {
      wx.switchTab({
        url: '/pages/index/index'
      })
    }
    app.globalData.lotterynavigate = ''
  },
  //隐藏活动规则弹框
  hideRulek() {
    this.setData({
      showRule: false
    })
  },
  //展示抽中奖弹框
  showPrizek() {
    this.setData({
      showPrize: true
    })
  },
  //隐藏抽中奖弹框
  hidePrizek() {
    this.setData({
      showPrize: false
    })
    // 添加随机扫描动画
    let that = this
    radomTimer = setInterval(function(){
      let i = parseInt(Math.random() * 8)
      console.log(i);
      that.setData({
        selectGoods:i
      })
    },3000)
  },
  // 隐藏未中奖的弹框
  hideNone() {
    this.setData({
      shownone: false
    })
    // 隐藏之后恢复随机扫描动画
    let that = this
    radomTimer = setInterval(function(){
      let i = parseInt(Math.random() * 8)
      console.log(i);
      that.setData({
        selectGoods:i
      })
    },3000)
  },
  // 展示未中奖弹框
  showNone() {
    this.setData({
      shownone: true
    })
  },
  // 跳转中奖记录
  toPrizelist() {
    // wx.uma.trackEvent("click_draw_record", { actId: this.data.lottery });
    wx.navigateTo({
      url: `/module_games/sudokuList/sudokuList?actId=${this.data.lottery}`,
    })
  },
  // 选中盲盒
  SelectGoods : throttl(function(e) {
    clearInterval(radomTimer)
    console.log(e.currentTarget.dataset.i);
    this.setData({
      selectGoods: e.currentTarget.dataset.i
    })
    // 调用接口获取盲盒奖品
    httpResource('doLottery', {
      actId: this.data.lottery,
      nickName: wx.getStorageSync('userInfo').nickName
    }, (data) => {
      console.log(data);
      if (data) {

        this.setData({
          prizeType: data.prizeType,
          prizeName: data.prizeName,
          prizeImgUrl: data.prizeImgUrl,
          gasiaponData: data
        })
        // 更新剩余抽奖次数
        this.getusablecount();
        // 扫描一秒之后弹出中奖结果
        setTimeout(() => {
          this.setData({
            selectGoods: 11
          })

          if (this.data.gasiaponData.prizeType == 0) {
            this.showNone()
            this.setData({
              selectGoods:1
            })
          } else {
            this.showPrizek()
            this.setData({
              selectGoods:1
            })
          }
        }, 1000)

      }
    })
  },3000),
  //根据id查询活动图片
  async getActivity() {
    let _this = this;
    try {
      let data = await httpResource('getlotteryInfo', {
        actId: _this.data.lottery
      }, null, "get");
// 遍历图片数组并进行赋值
      data.actImgUrls.forEach(item => {
        switch (item.pagePosition) {
          case 100:
            _this.setData({
              lottery_page: item.imgUrl
            })
            break;
          case 201:
            _this.setData({
              horse_race_lamp_1: item.imgUrl
            })
            break;
          case 202:
            _this.setData({
              horse_race_lamp_2: item.imgUrl
            })
            break;
          case 300:
            _this.setData({
              guizi: item.imgUrl
            })
            break;

          case 500:
            _this.setData({
              click_icon: item.imgUrl
            })
            break;
          case 501:
            _this.setData({
              task_border_top: item.imgUrl
            })
            break;
          case 502:
            _this.setData({
              task_border_middle: item.imgUrl
            })
            break;
          case 503:
            _this.setData({
              task_border_bottom: item.imgUrl
            })
            break;
          case 504:
            _this.setData({
              task_border_unfinish: item.imgUrl
            })
            break;
          case 505:
            _this.setData({
              task_border_finished: item.imgUrl
            })
            break;
          case 600:
            console.log(item.imgUrl);
            _this.setData({
              activity_rule: item.imgUrl
            })
            break;
          case 700:
            _this.setData({
              wining_record: item.imgUrl
            })

            app.globalData.lotteryInfo.wining_record = item.imgUrl
            break;
          case 701:
            _this.setData({
              wining_record_btn: item.imgUrl
            })
            app.globalData.lotteryInfo.wining_record_button = item.imgUrl
            break;
          case 703:
            _this.setData({
              unissued: item.imgUrl
            })
            app.globalData.lotteryInfo.unissued = item.imgUrl
            break;
          case 702:
            _this.setData({
              issued: item.imgUrl
            })
            app.globalData.lotteryInfo.issued = item.imgUrl
            break;
          case 704:
            _this.setData({
              wining_tips_wined: item.imgUrl
            })
            break;
          case 705:
            _this.setData({
              wining_tips_no_win: item.imgUrl
            })
            break;
          default:
            break;
        }
      })
      _this.setData({
        actRule: data.actRule,
        prizeNumber: data.prizeConfigNum,
        bgColor: data.backgroundColor,
        isShoWinningInfo: data.isShoWinningInfo,
        winningQrc: data.winningQrc
      })
      app.globalData.lotteryInfo.bgColor = data.backgroundColor
      // _this.setData({
      //     actRule:data.actRule
      // })
    } catch (error) {

    }
  },

  /**
   * @param {Object} 
   * 获取门店列表
   */
  getData: debounce(async function (e) {
    const _this = this;
    const {
      value,
      latitude,
      longitude,
      pageSize,
      currentPage
    } = this.data;
    let data = await httpResource('getShopByKeywordsWithPage', {
      currentPage: currentPage,
      pageSize: pageSize,
      keywords: value,
      latitude,
      longitude
    });
    let list = [];
    if (data && data.records && data.records.length) {
      data = (data.records || []).map(item => {
        item.shopNameList = getHighlightStrArray(item.shopName, value);
        // 处理高亮
        return item
      });
      list = [...data];
      if (e == 'ok') {
        _this.setData({
          value: list[0].shopName,
          shopName: list[0].shopName || '会员小程序',
        })
        _this.getShopQrCode(list[0].shopCode)
      }
    }
    _this.setData({
      shopList: list,
      currentIndex: ''
    });
  }, 200),
  // 获取当前位置
  checkUserLocation: function () {
    let _this = this;
    wx.getSetting({
      success: (res) => {
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
          wx.showModal({
            title: '请求授权当前位置',
            content: '需要获取您的地理位置，请确认授权',
            success: function (res) {
              if (res.cancel) {
                wx.showToast({
                  title: '拒绝授权',
                  icon: 'none',
                  duration: 1000
                })
              } else if (res.confirm) {
                wx.openSetting({
                  success: function (dataAu) {
                    if (dataAu.authSetting["scope.userLocation"] == true) {
                      wx.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 1000
                      })
                      //再次授权，调用wx.getLocation的API
                      getUserLocation().then(res => {
                        let {
                          latitude,
                          longitude
                        } = res;
                        _this.setData({
                          latitude: latitude,
                          longitude: longitude,
                        }, _this.getData('ok'));
                      })
                    } else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'none',
                        duration: 1000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) {
          //调用wx.getLocation的API
          getUserLocation().then(res => {
            let {
              latitude,
              longitude
            } = res;
            _this.setData({
              latitude: latitude,
              longitude: longitude,
            }, _this.getData('ok'));
          })
        } else {
          //调用wx.getLocation的API
          getUserLocation().then(res => {
            let {
              latitude,
              longitude
            } = res;
            _this.setData({
              latitude: latitude,
              longitude: longitude,
            }, _this.getData('ok'));
          })
        }
      }
    })
  },
  /**
  * 获取社群二维码
  */
  async getShopQrCode(shopCode) {
    const _this = this;
    const {
      shopName
    } = this.data;
    let data = await httpResource('getShopCommunityCode', {
      shopCode
    }, null, 'GET');
    _this.setData({
      qrcodeUrl: data || 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
      qrName: data ? shopName : '会员小程序',
    })
  },
  // 任务列表
  async getTaskInfo() {
    let _this = this;
    try {
      let data = await httpResource('getaskInfo', {
        actId: _this.data.lottery
      }, null, "get");
      console.log(data);
      _this.setData({
        taskList: data
      })
    } catch (error) {

    }
  },
  // 做任务
  todotask(e) {
    let item = e.currentTarget.dataset.item;
    console.log(item);
    httpResource('doTask', {
      actId: item.actId,
      taskId: item.id,
      level: item.taskLevel,
      rewardLottery: item.rewardLottery
    }, (data) => {
      console.log(data);
      if (data) {
        switch (item.taskType) {
          case 1:
            // wx.navigateTo({
            //     url: `/module_games/sharelottery/index?actId=${item.actId}&taskId=${item.id}`,
            // })
            console.log(item.id);
            this.setData({
              currentTaskid: item.id
            })
            break;
          case 2:
            // 跳转分享页面
            wx.uma.trackEvent("click_pyq_complete", {
              actId: this.data.lottery
            });
            let userInfo = wx.getStorageSync('userInfo');
            wx.navigateTo({
              url: `/module_games/sharelottery/index?actId=${item.actId}&taskId=${item.id}&taskChainId=${data.taskChainId}&type=3&srcUserId=${userInfo.userId}`,
            })
            break;
          case 3:
            // 去评价
            wx.uma.trackEvent("click_evaluate_complete", {
              actId: this.data.lottery
            });
            wx.navigateTo({
              url: '/pages/orderList/orderList?orderType=2',
            })
            break;
          case 4:
            // 去下单
            wx.uma.trackEvent("click_consumption_complete", {
              actId: this.data.lottery
            });
            wx.switchTab({
              url: '/pages/index/index',
            })
            break;
        }
      }
    }, 'post')
  },

  newInterval(func, millisecond) {
    function inside() {
      func()
      setTimeout(inside, millisecond) //第二次往后调用，每秒后调用自身
    }
    setTimeout(inside, 0) // 第一次运行调用
  },
  // 获取抽中奖走马灯
  like() {
    let _this = this
    httpResource('getWinninginfo', {
      actId: _this.data.lottery
    }, (data) => {
      _this.setData({
        runHorselist: data
      })
    }, "get");
  },
  // 获取门店码
  showOrCODE() {
    let filter = {
      shopCode: this.data.shopCode,
    };
    httpResource(
      "getCommunityManagerCode",
      filter,
      (data) => {
        this.setData({
          shopCodeImg: data.shopManagerImgUrl || 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
        });
      },
      "GET",
      null
    );
  },
  // 初始化活动状态
  checkInit() {

    let _this = this
    console.log(_this.data.taskChainId);
    httpResource('checkInit', {
      actId: _this.data.lottery,
      taskChainId: _this.data.taskChainId,
      srcUserId: _this.data.srcUserId
    }, (data) => {
      // console.log(data,msg);
      if (data.code) {
        this.setData({
          activeStatus: data.code
        })
        wx.showToast({
          title: data.message,
          icon: 'none',
          duration: 1000
        })
      }

    }, "post");
  },
  // 获取剩余次数
  getusablecount() {
    let _this = this
    httpResource('lotteryUsableCount', {
      actId: _this.data.lottery
    }, (data) => {
      _this.setData({
        usableCount: data
      })
    }, "get");
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let that = this
    radomTimer = setInterval(function(){
      let i = parseInt(Math.random() * 8)
      console.log(i);
      that.setData({
        selectGoods:i
      })
    },3000)
    // setTimeout(() => {
    //   this.setData({
    //     showLoading: false
    //   })
    //   wx.setPageStyle({
    //     style: {
    //       overflow: 'auto'
    //     }
    //   })
    // }, 2000)
    console.log(options);
    let actCode = null;
    // 判断是不是分享过来的，并进行参数获取
    if (options?.scene) {
      console.log(decodeURIComponent(options.scene));
      actCode = decodeURIComponent(options.scene).split("=")[1];
    }
    let _this = this;

    let isLogin = wx.getStorageSync("u-token");
    // 判断是否是做做任务过来的
    if (options?.taskChainId || actCode) {
      this.setData({
        taskChainId: options.taskChainId ? options.taskChainId : actCode.split('_')[2],
      })
    }
    // 判断谁做的活动
    if (options?.srcUserId || actCode) {
      this.setData({
        srcUserId: options.srcUserId ? options.srcUserId : actCode.split('_')[1]
      })
    }

    _this.setData({
      lottery: options?.actId ? (options.actId.indexOf('?') !== -1) ? options.actId.split('?')[0] : options.actId : actCode ? actCode.split('_')[0] : ''
    }, () => {

      if (_this.data.lottery && _this.data.lottery != null) {
        if (!isLogin) {
          wx.redirectTo({
            url: `/pages/userLogin/index?blindbox=${_this.data.lottery}&taskChainId=${_this.data.taskChainId}&srcUserId=${_this.data.userId}`,
          });
        }
      }
    })
// 控制页面有弹框时候超出不滑动
    wx.setPageStyle({
      style: {
        overflow: 'hidden'
      }
    })
    setTimeout(() => {
      this.setData({
        showLoading: false
      })
      wx.setPageStyle({
        style: {
          overflow: 'auto'
        }
      })
      // this.startMarquee()
    }, 2000)
    // 经纬度
    getUserLocation().then(res => {
      let {
        latitude,
        longitude
      } = res;
      this.setData({
        latitude: latitude,
        longitude: longitude,
      }, this.getData('ok'));
    }).catch(() => {
      this.checkUserLocation()
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let that = this
    if(timer){
      clearInterval(timer)
    }
    if(radomTimer){
      clearInterval(radomTimer)
    }

    radomTimer = setInterval(function(){
      let i = parseInt(Math.random() * 8)
      console.log(i);
      that.setData({
        selectGoods:i
      })
    },3000)
    // 闪灯
    timer = setInterval(() => {
      that.setData({
        lightshow: !that.data.lightshow
      })
    }, that.data.t)

    if (that.data.lottery_page) { } else {
      that.getActivity();
    }
    // 禁止分享
    wx.hideShareMenu()

    let currentStore = wx.getStorageSync('current-store');

    if (currentStore) {
      that.setData({
        shopCode: currentStore.shopCode
      })
      that.showOrCODE()
    }
    // 初始化接口
    that.checkInit()
    // 调用获取任务信息
    that.getTaskInfo()
    // 获取可用次数
    that.getusablecount();
    that.newInterval(that.like, 20000);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // 清除计时器
    clearInterval(radomTimer)
    clearInterval(timer)
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // 清除计时器
    clearInterval(radomTimer)
    clearInterval(timer)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: async function (e) {
    wx.uma.trackEvent("click_wechat_complete", {
      actId: this.data.lottery
    });
    // 调用任务接口
    let taskinfo = await httpResource('doTask', {
      actId: e.target.dataset.actid,
      taskId: e.target.dataset.id,
      level: e.target.dataset.tasklevel,
      rewardLottery: e.target.dataset.rewardlottery
    }, null, 'post')
    // 获取分享图片
    let data = await httpResource('getShareImgs', {
      actId: this.data.lottery,
      taskId: e.target.dataset.id
    }, null, 'get')
    // 获取缓存的用户信息，分享时候要带在路径上
    
    let userInfo = wx.getStorageSync('userInfo');
    return {
      title: data[0].titleDesc,
      path: `/module_games/blindbox/blindbox?actId=${this.data.lottery}&taskChainId=${taskinfo.taskChainId}&srcUserId=${userInfo.userId}`,
      imageUrl: data[0].imgUrl
    }

  },
})