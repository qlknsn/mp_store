import WxValidate from "/assets/js/validate.js";
import "umtrack-wx";
import { initSubState } from "/config/subConfig";
const { httpResource, getQuery } = require("/assets/js/common.js");
App({

  // 友盟统计
  umengConfig: {
    appKey: "6103bd82864a9558e6d660ca", //由友盟分配的APP_KEY
    useOpenid: true, // 是否使用openid进行统计，此项为false时将使用友盟+随机ID进行用户统计。使用openid来统计微信小程序的用户，会使统计的指标更为准确，对系统准确性要求高的应用推荐使用OpenID。
    autoGetOpenid: true, // 是否需要通过友盟后台获取openid，如若需要，请到友盟后台设置appId及secret
    debug: false, //是否打开调试模式
    uploadUserInfo: true, // 上传用户信息，上传后可以查看有头像的用户分享信息，同时在查看用户画像时，公域画像的准确性会提升。
  },
  onLaunch: function (res) {
    const scope = this;
    //小程序的path,query,scene等信息
    const appLaunchStr = res;
    //小程序更新
    if (wx.canIUse("getUpdateManager")) {
      wx.getUpdateManager().onUpdateReady(() => {
        wx.showModal({
          title: "更新提示",
          showCancel: false,
          content: "新版本已经准备好，是否重启应用？",
          success: function (res) {
            if (res.confirm) {
              wx.getUpdateManager().applyUpdate();
            }
          },
        });
      });
    }
    //获取设备信息
    wx.getSystemInfo({
      success: function (res) {
        wx.setStorageSync("systemInfo", res);
        wx.setStorageSync("systemPlatform", res.platform);
        scope.globalData.statusBar = res.statusBarHeight;
        const isIOS = !res["system"].indexOf("Android") > 0;
        let rect = wx.getMenuButtonBoundingClientRect(); //胶囊按钮位置信息;
        if (!rect || rect.height == 0) {
          rect = wx.getMenuButtonBoundingClientRect();
        }
        if (!rect || rect.width == 0) {
          // 如果没有拿到胶囊信息就给一个默认的iphonex信息，直到能拿到胶囊信息存入缓存，以后不会出现
          rect = {
            bottom: 85,
            height: 32,
            left: 296,
            right: 383,
            top: 53,
            width: 87
          }
        }
        let navBarHeight = (function () {
          //导航栏高度
          let gap = rect.top - res.statusBarHeight; //动态计算每台手机状态栏到胶囊按钮间距
          return 2 * gap + rect.height;
        })();
        console.log('navbar', navBarHeight, res.statusBarHeight)
        scope.globalData.navHeight = navBarHeight + res.statusBarHeight;
        scope.globalData.viewHeight = res.screenHeight - scope.globalData.navHeight;
        scope.globalData.screenHeight = res.screenHeight;
        scope.globalData.fachHeight = res.windowHeight - scope.globalData.navHeight;
        scope.globalData.tabbarHeight =
          (res.screenHeight - res.windowHeight) * (750 / res.windowWidth);
        console.log("系统信息", res, "是否IOS", isIOS);
        console.log("navbar", scope.globalData.navHeight);
      },
    });
    // 订阅消息默认状态
    initSubState();
  },
  onShow: function (options) {
    console.log(options, '============>sbc');
    // 是否登录
    const isLogin = wx.getStorageSync("u-token");
    if (isLogin) {
      const userInfo = wx.getStorageSync("userInfo");
      this.globalData.userInfo = userInfo || {};
      this.globalData.isLogin = true;
      // 会员体系- 会员收集 分享人会员id 当前用户会员id 分享页面
      this.saveShareRecordFn(options)
    }
    // 收集消息-公众号消息-订阅模板消息
    if (options.query.msgId) httpResource('clickMsg', options.query);
    const isWelcome = wx.getStorageSync("isWelcome");
    if (isWelcome) this.globalData.isWelcome = true;
    // 是否跳转过授权地址
    const toLoction = wx.getStorageSync("toLoction");
    if (toLoction) this.globalData.toLoction = true;
    // 当前配货地址
    const deliverableAddress = wx.getStorageSync('deliverable-address');
    if (deliverableAddress) this.globalData.deliverableAddress = deliverableAddress;
    //判断活动邀请有礼微信好友进入
    if (options.query.actId && options.query.inviteUserId) {
      this.globalData.isActivity = true;
      this.globalData.type = 1;
      this.globalData.actId = options.query.actId;
      this.globalData.inviteUserId = options.query.inviteUserId;
      console.log(wx.getStorageSync("u-token"), '====================>token');
      //如果未登录，强制跳转到登录页面
      if (!isLogin) wx.navigateTo({ url: 'pages/userLogin/index' });
    }
    //判断活动邀请有礼太阳码进入
    console.log('act',options.query.scene)
    if (options.query.scene) {
      let act = decodeURIComponent(options.query.scene);
      if(act){
        act = act.split("=")[1].split('_');
        if(act.length>2) return;
        console.log('act',act)
        // let actObj = act.split("=")[1];
        const actId = act[0];
        const inviteUserId = act[1];
        if (actId && inviteUserId) {
          console.log('actId',actId,inviteUserId)
          this.globalData.isActivity = true;
          this.globalData.type = 1;
          this.globalData.actId = actId;
          this.globalData.inviteUserId = inviteUserId;
          //如果未登录，强制跳转到登录页面
          if (!isLogin) wx.navigateTo({ url: 'pages/userLogin/index' });
        }
      }
    }
    // 当前门店
    const currentStore = wx.getStorageSync('current-store');
    // 2022 08 22 会员体系 优化为 登录状态下调用
    if (currentStore && this.globalData.isLogin) {
      this.globalData.currentStore = currentStore;
      //初始第一次必须要调用商品总条数，应该把商品总条数这个接口写在全局，临时处理
      let callBack = httpResource('cartCount', { shopCode: currentStore.shopCode });
      callBack.then((res) => {
        this.globalData.cartNum = res;
      })
    }
  },
  // 会员体系- 会员收集
  saveShareRecordFn(options) {
    // 会员体系-收集分享 分享人与点击人不同才请求
    console.log('saveShareRecord', '============>执行', options.query.shareUserId, this.globalData.userInfo.userId);
    if (options.query.shareUserId && this.globalData.userInfo.userId && (options.query.shareUserId != this.globalData.userInfo.userId)) {
      httpResource('saveShareRecord', Object.assign(options.query, { pointUserId: this.globalData.userInfo.userId }));
    }
  },
  //正则校验
  validate: (rules, messages) => new WxValidate(rules, messages),
  /**
   * @description 全局变量监听器
   * @param {key: String, cb: Function} [key-要监听的key, cb-监听后，执行的回调函数]
   * @return {any} [返回数据视情况而定]
   * @used {使用方式} App.$watch(String key, Function callback)
   * 例子: app.$watch('cartList', (val, old) => {})
   */
  $watch(key, cb) {
    // 监听回调的添加-订阅者添加
    this.globalData.$_watchCallBack = Object.assign({}, this.globalData.$_watchCallBack, {
      [key]: this.globalData.$_watchCallBack[key] || []
    })
    this.globalData.$_watchCallBack[key].push(cb)
    // key监听的添加
    if (!this.globalData.$_watchingKeys.find(x => x === key)) {
      const that = this
      this.globalData.$_watchingKeys.push(key)
      let val = this.globalData[key]
      // 定义属性
      Object.defineProperty(this.globalData, key, {
        configurable: true,
        enumerable: true,
        set(value) {
          const old = that.globalData[key] // 原本数值
          val = value // 更新get
          // 遍历调用相关的所有回调-通知订阅者们
          that.globalData.$_watchCallBack[key].map(func => func(value, old))
        },
        get() {
          return val
        }
      })
    }
  },
  globalData: {
    cartNum: 0, // 购物车总数量
    isWelcome: false,
    showLoginStatus: false,
    isFirstLogin: 1,
    statusBar: 0,
    viewHeight: 0,
    screenHeight: 0,
    navHeight: 0,
    longitude: null, // 经度
    latitude: null, // 纬度
    currentStore: {}, // 当前门店
    deliverableAddress: {}, // 默认配货地址
    toLoction: false, // 第一次去授权地理位置,
    isLogin: false, // 是否登录
    selectTaber: "ZI_TI", //自提，外卖状态
    // app私有变量 ------------------------------
    // 监听器相关数据
    $_watchCallBack: {}, // 监听回调方法存放
    $_watchingKeys: [], // 监听key存放
    isActivity: false,//是否是活动
    type: 0,//来源0: (默认)会员登录, 1: 邀请有礼登录
    inviteUserId: null,//邀请人ID
    actId: null,//活动ID

    // 抽奖信息
    lotteryInfo: {},
    // 授奖活动跳转路径
    lotterynavigate: '',
    userInfo: {}, // 当前用户信息
  },
});
