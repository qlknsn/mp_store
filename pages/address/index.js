// pages/address/index.js
const { debounce } = require('../../assets/js/eventUtil');
import { getUserLocation, getHighlightStrArray } from '../../assets/js/commonData';
const { httpResource } = require('../../assets/js/common.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      title: '搜索门店'
    },
    latitude: null,
    longitude: null,
    inputValue: '',
    addressList: [],
    options: {}, // 跳转过来参数信息
    currentIndex: '',
    isFocus: false,
    currentPage: 1,
    viewHeight: app.globalData.viewHeight - 90,
    showImage: false, // 社群码显示
    groupImgUrl: '', // 社群码url
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      options
    })
    // 经纬度
    getUserLocation().then(res => {
      let { latitude, longitude } = res;
      this.setData({
        latitude: latitude,
        longitude: longitude,
      }, this.getData);
    }).catch(() => {
      this.getData()
    });
  },
  goMap(e) {
    // const { showList } = this.data;
    // const index = e.currentTarget.dataset.index;
    // const item = showList[index];
    // const value = this.data.inputValue;
    // let pages = getCurrentPages(); //获取当前页面pages里的所有信息。
    // let prevPage = pages[pages.length - 2]; //prevPage 是获取上一个页面的js里面的pages的所有信息。 -2 是上一个页面，-3是上上个页面以此类推。
    // if(prevPage){
    //   //上一个页面内执行setData操作，将我们想要的信息保存住。当我们返回去的时候，页面已经处理完毕。
    //   prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
    //     selectInfo:item
    //   },()=>{
    //     prevPage.getMarkers()
    //   })
    //    //最后就是返回上一个页面。
    //   wx.navigateBack()
    // }
  },
  // 中心定位
  bindStore(e) {
    const { currentIndex } = this.data;
    const index = e.currentTarget.dataset.index;
    if (currentIndex === index) return;
    this.setData({
      currentIndex: index,
    })
  },
  bindscrolltolower() {
    this.data.currentPage++;
    this.getData()
    console.log('加载更多')
  },
  // 设置当前门店
  setCurrentStore() {
    const { currentIndex, addressList } = this.data;
    const item = addressList[currentIndex];
    if (item.location && item.shopCode) {
      if (item.location.latitude && item.location.latitude) {
        app.globalData.currentStore = item || {};
        wx.setStorageSync('current-store', item);
        wx.switchTab({
          url: '/pages/index/index',
        })
      } else {
        wx.showToast({
          title: '当前门店不可选取',
          icon: 'none'
        })
      }
    } else {
      wx.showToast({
        title: '当前门店不可选取',
        icon: 'none'
      })
    }
  },
  bindKeyInput(e) {
    const value = e.detail.value;
    this.setData({
      inputValue: value
    })
    // this.getData(value)
  },
  // 获得焦点
  bindKeyFocus() {
    this.setData({
      isFocus: true
    })
  },
  // 失去焦点
  bindKeyBlur() {
    // this.setData({
    //   isFocus:false
    // },this.getData)
  },
  search(e) {
    const type = e ? e.currentTarget.dataset.type : true;
    const { inputValue: keywords } = this.data;
    if (keywords == '' && !type) {
      wx.navigateBack({})
      return
    }
    this.data.currentPage = 1;
    this.setData({
      addressList: []
    }, () => {
      this.getData()
    })
  },
  getData: debounce(async function (e) {
    const { inputValue: keywords, latitude, longitude } = this.data;
    let lon = longitude;
    let lat = latitude;
    if (wx.getStorageSync("SELECT_TABER")!='ZI_TI'&&app.globalData.deliverableAddress.addrId) {
      const { longitude: lon2, latitude: lat2 } = app.globalData.deliverableAddress;
      lon = lon2;
      lat = lat2;
      console.log('取到地址', lon, lat)
    } else {
      console.log('未获取到地址')
    }
    let data = await httpResource('getShopByKeywordsWithPage', {
      currentPage: this.data.currentPage,
      pageSize: 20, keywords, latitude: lat, longitude: lon
    });
    if (data && data.records && data.records.length) {
      data = (data.records || []).map(item => {
        item.shopNameList = getHighlightStrArray(item.shopName, keywords);
        // 处理高亮
        return item
      });
      const list = [...this.data.addressList, ...data];
      this.setData({
        addressList: list,
        currentIndex: ''
      });
    }
  }, 200),
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.initData(this.data.options)
  },
  initData(opt) {
    if (opt.inputValue) {
      this.setData({
        inputValue: opt.inputValue
      })
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  async addGroup(e) {
    const shopCode = e.currentTarget.dataset.code;
    wx.uma.trackEvent('search_store');
    const url = await httpResource('getShopCommunityCode', {
      shopCode
    }, null, 'GET');
    if (url) {
      this.setData({
        groupImgUrl: url,
        showImage: true
      })
    } else {
      wx.showToast({
        title: '该门店暂无社群码哦',
        icon: 'none'
      })
    }
  },
  closeGroup() {
    this.setData({
      showImage: false,
      groupImgUrl: '',
    })
  }
})