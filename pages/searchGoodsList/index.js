// pages/searchGoodsList/index.js
const { httpResource } = require("../../assets/js/common.js");
const app = getApp();
import { getProductDetail } from "../../assets/js/commonData";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defaultImg1:
      "https://img.hotmaxx.cn/web/common/mp-store/home-index/goods-default.png",
    navbarData: {
      return: '1',
      title: '搜索商品'
    },
    searchValue: '',
    // 排序
    utilsIndex: 0,
    utilsList: [
      {
        title: '综合',
        value: ''
      },
      {
        title: '销量',
        value: ''
      },
      {
        title: '价格',
        value: 'fall' // rise 上 fall
      },
    ],
    newGoodsList: [],
    noLikesBarcode: '',
    likesBarcode: '',
    noSearchData: false,
    cartNum: 0,
    listType: 0, // 0 竖向  1 横向
    currentPage: 1,
    noMore: false
  },
  changeListType() {
    const listType = this.data.listType == 0 ? 1 : 0;
    this.setData({ listType })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.searchValue) {
      this.setData({
        searchValue: options.searchValue
      }, this.getAllGoods)
    }
    this.setData({
      cartNum: app.globalData.cartNum,
    })
  },
  // 加入购物车
  async addCart(e) {
    const selectTaber = wx.getStorageSync("SELECT_TABER");
    const status = this.checkLogin();
    if (!status) return;
    if (!app.globalData.deliverableAddress.addrId) {
      wx.showToast({
        title: '请先选择地址哦',
        duration: 1000,
        icon: 'none'
      })
      return
    }
    const shopCode = this.data.shopCode || app.globalData.currentStore.shopCode;
    try {
      wx.hideToast();
    } catch (error) { }
    if (!shopCode) {
      wx.showToast({
        title: '请先选择门店哦',
        duration: 1000,
        icon: 'none'
      })
      return
    }
    const { item } = e.currentTarget.dataset;
    const { barcode: packageCode, goodsName, goodsCode, coverImage, salePrice, marketPrice, categoryCodeFirst: categoryOneCode,
      categoryCodeSecond: categoryTwoCode,
      categoryCodeThird: categoryThreeCode } = item;
    const params = JSON.parse(JSON.stringify({
      categoryTwoCode,
      categoryThreeCode,
      version: 2,
      shopCode,
      packageCode,
      goodsName, goodsCode, coverImage, salePrice, marketPrice, categoryOneCode,
      addType: 2,
      receiveType: selectTaber == 'ZI_TI' ? 3 : 4
    }))
    let data = await httpResource('cartAdd2', params);
    if (data && data.extraMsg) {
      wx.showToast({
        title: data.extraMsg,
        icon: "none",
        duration: 3000,
      });
    } else {
      wx.showToast({
        title: "加入成功",
        icon: "none",
        duration: 1000,
      });
    }
    const num = await httpResource('cartCount', { shopCode });
    app.globalData.cartNum = num;
    this.setData({ cartNum: num })
  },

  // 商品数据
  async getAllGoods() {
    const shopCode = app.globalData.currentStore.shopCode;
    const { searchValue } = this.data;
    const parasm = JSON.parse(
      JSON.stringify({
        shopCode,
        keyword: searchValue ? searchValue : undefined,
        // pushDate: searchValue ? '2022-04-14' : undefined,
        currentPage: this.data.currentPage, pageSize: 20,
      })
    );
    try {
      const data = await httpResource('searchGoods', parasm);
      this.setData({ noMore: false });
      if (data && data.records && data.records.length) {
        const list = (data.records || []).map(getProductDetail);
        if (this.data.currentPage == 1) {
          this.setData({
            newGoodsList: list,
          });
        } else {
          this.setData({
            newGoodsList: this.data.newGoodsList.concat(list),
          })
        }
      } else {
        if (this.data.currentPage == 1) {
          this.setData({
            newGoodsList: [],
            noSearchData: true
          })
          this.getHomeGoods();
        }
        this.setData({ noMore: this.data.currentPage == 1 ? false : this.data.newGoodsList.length ? true : false, })
      }
    } catch (error) {
      this.setData({ newGoodsList: [] })
    }
  },
  // 到家商品数据
  async getHomeGoods() {
    const shopCode = app.globalData.currentStore.shopCode;
    const parasm = JSON.parse(
      JSON.stringify({
        shopCode,
        currentPage: this.data.currentPage, pageSize: 20,
      })
    );
    try {
      const data = await httpResource('sendHome', parasm);
      this.setData({ noMore: false });
      if (data && data.records && data.records.length) {
        const list = (data.records || []).map(getProductDetail);
        if (this.data.currentPage == 1) {
          this.setData({
            newGoodsList: list,
          });
        } else {
          this.setData({
            newGoodsList: this.data.newGoodsList.concat(list),
          })
        }
      } else {
        if (this.data.currentPage == 1) {
          this.setData({
            newGoodsList: [],
          })
        }
        this.setData({ noMore: this.data.currentPage == 1 ? false : this.data.newGoodsList.length ? true : false, })
      }
    } catch (error) {
      this.setData({ newGoodsList: [] })
    }
  },
  // 是否登录 未登录去登录
  checkLogin() {
    const isLogin = app.globalData.isLogin;
    if (!isLogin) {
      wx.navigateTo({
        url: "/pages/userLogin/index?type=1"
      })
    }
    return isLogin;
  },
  goDetail(e) {
    // const status = this.checkLogin();
    // if (!status) {
    //   return;
    // }
    // if (!app.globalData.isLogin) return;
    if (!app.globalData.currentStore.shopCode) return;
    const barcode = e.currentTarget.dataset.barcode;
    wx.navigateTo({
      url: `/pages/productDetail/index?barcode=${barcode}`
    })
  },
  // 跳转购物车
  goCart() {
    if (!app.globalData.deliverableAddress.addrId) {
      wx.navigateTo({
        url: '/pages/user/address/list/index?type=2'
      })
      return
    }
    try {
      wx.hideToast();
    } catch (error) {

    }
    const shopCode = this.data.shopCode || app.globalData.currentStore.shopCode;
    if (!shopCode) {
      this.getStoreData();
      return
    }
    wx.switchTab({ url: '/pages/cart/index' })
  },
  async getStoreData() {
    const { latitude, longitude } = app.globalData.deliverableAddress;
    const data = await httpResource("getShopByRange", {
      radius: 100,
      latitude,
      longitude,
    });
    if (data && data.length) {
      // 选择 门店
      app.globalData.currentStore = data[0];
      wx.setStorageSync('current-store', data[0]);
      wx.switchTab({ url: '/pages/cart/index' })
    }
  },
  // 喜欢
  async likeChange(e) {
    const _this = this;
    const status = _this.checkLogin();
    if (!status) { return; }
    // 是否喜欢
    const { favorite: isFavorite, barcode, index } = e.currentTarget.dataset;
    const api = isFavorite ? 'unFavorite' : 'favorite';
    await httpResource(api, { barcode }, null, "GET");
    _this.setData({
      [`newGoodsList[${index}].isFavorite`]: !isFavorite,
    })
  },
  goBack() {
    wx.navigateBack()
  },
  utilsChange(e) {
    const index = e.currentTarget.dataset.index;
    const { utilsIndex, utilsList } = this.data;
    const row = utilsList[index];
    // 有排序
    if (index > 1) {
      // 当前index
      if (utilsIndex === index) {
        const value = row.value == 'fall' ? 'rise' : 'fall'; // rise 上 fall
        this.setData({
          [`utilsList[${index}].value`]: value
        }, this.utilsGetData)
      } else {
        this.setData({
          utilsIndex: index
        }, this.utilsGetData)
        this.resetUtilsList();
      }

    } else {
      if (utilsIndex === index) return;
      this.setData({
        utilsIndex: index
      }, this.utilsGetData)
      this.resetUtilsList();
    }
  },
  resetUtilsList() {
    this.setData({
      utilsList: [
        {
          title: '综合',
          value: ''
        },
        {
          title: '销量',
          value: '' // rise 上 fall
        },
        {
          title: '价格',
          value: 'fall' // rise 上 fall
        },
      ]
    })
  },
  utilsGetData() {
    this.data.currentPage = 1;
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  changeLikeGoods() {
    const { noLikesBarcode, likesBarcode, newGoodsList } = this.data;
    if (noLikesBarcode) {
      const index = newGoodsList.findIndex(e => e.barcode = noLikesBarcode);
      if (index > -1) {
        this.setData({
          [`newGoodsList[${index}].isFavorite`]: false,
        })
      }
    }
    if (likesBarcode) {
      const index = goodsList.findIndex(e => e.barcode = likesBarcode);
      if (index > -1) {
        this.setData({
          [`newGoodsList[${index}].isFavorite`]: true,
        })
      }
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.changeLikeGoods();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.currentPage++;
    const { noSearchData } = this.data;
    if (noSearchData) {
      this.getAllGoods();
    } else {
      this.getAllGoods();
    }
  },
  share() {

  },
  async shareCount(params) {
    try {
      await httpResource('shareNum', params)
    } catch (error) {
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (e) {
    if (e.from === "button") {
      const { img: imageUrl, item: { goodsName, goodscode, barcode, marketPrice, salePrice: price } } = e.target.dataset;
      const shopCode = app.globalData.currentStore.shopCode || 0;
      this.shareCount({ shopCode, goodsName, marketPrice, price, barcode });
      return {
        title: goodsName,
        path: "/pages/index/index",
        imageUrl,
      };
    }
    return {
      title: `为你推荐一家宝藏折扣店，全场1折起！`,
      path: "/pages/index/index",
      imageUrl: "https://img-cdn.hotmaxx.cn/web/common/mp-store/common/share-base-img.jpg",
    }
  }
})