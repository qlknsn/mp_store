// pages/orderList/orderList.js
import {
    subscribeMessage,
    getSubState
} from "../../config/subConfig";
const app = getApp();
const {
    httpResource,
} = require('../../assets/js/common.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        background: '#ff0000',
        parameter: {
            'title': '订单列表  ',
            return: 1,
        },

        navIndex: 0,
        tabLeft: "20",
        contentHeight: 600,
        orderList: [],
        pageIndex: 1,
        pageCount: 0,
        // 高度
        navbarheight: 0,
        scrollTopNum: 0,
        noMore: false, // 是否到底
        refresher: false, // 下拉刷新,
        navType: 0, //当前类型
        respones: false,
        detailChange: false,
        // 评论成功列表跳转
        evalutionshow: false,

    },



    getOrderList(pageIndex) {
        let that = this
        var filter = {
            currentPage: pageIndex,
            pageSize: 20,
        };
   

        wx.showLoading({
            title: '请求中...',
            mask: true
        });
        console.log(filter);
        httpResource('singleDetail', filter, (data) => {
            this.setData({
                evalutionshow: false
            })
            // console.log(data);
            if (data?.records && data.records.length >= 0) {
                wx.hideLoading({
                    success: (res) => {},
                })
                console.log('走这儿')
                if (data.currentPage == 1) {
                    that.setData({
                        orderList: data ? data.records : [],
                        refresher: false,
                    })
                    if (data.total < data.pageSize) {
                        that.setData({
                            noMore: true,
                        })
                    }
                } else {
                    this.setData({
                        orderList: this.data.orderList.concat(data.records),
                        refresher: false,
                    })
                }

            } else {
                wx.hideLoading({
                    success: (res) => {},
                })
                console.log('走这儿')
                if (data?.currentPage == 1 || data == null) {
                    this.setData({
                        respones: true,
                        refresher: false,
                    })

                } else {
                    if (data) {
                        that.setData({
                            noMore: data.currentPage == 1 ? false : true,
                            refresher: false,
                        })
                    } else {
                        that.setData({
                            noMore: false,
                            refresher: false,
                        })
                    }
                }
            }

        }, "POST", null)
    },
    toDetail({
        currentTarget: {
            dataset: {
                item
            }
        }
    }) {
        console.log(item);
        wx.navigateTo({
            url: `/pages/refundDetail/index?returnOrderNo=${item.returnOrderNo}`,
        })

    },


    toEvaluation({
        currentTarget: {
            dataset: {
                orderno
            }
        }
    }) {
        wx.navigateTo({
            url: `/pages/user/orderEvaluation/index?orderNo=${orderno}&edit=true`,
        })
    },
    goEvaluationDetail({
        currentTarget: {
            dataset: {
                orderno
            }
        }
    }) {
        wx.navigateTo({
            url: `/pages/user/orderEvaluationDetail/index?orderNo=${orderno}`,
        })
    },
    goRefunddetail({
        currentTarget: {
            dataset: {
                hasNoFinishReturnApply,
                refundapplyid,
                refundStatus
            }
        }
    }) {
        wx.navigateTo({
            url: `/pages/user/refundApplyDetail/index?id=${refundapplyid}&return=1`,
        })
    },
    bindrefresherrefresh() {
        this.setData({
            refresher: true,
            pageIndex: 1,
            noMore: false
        }, () => {
            console.log(this.data.navType)
            setTimeout(() => {
                this.getOrderList(this.data.pageIndex, this.data.navType)
            }, 300)
        })
    },
    scrollToLower() {
        this.data.pageIndex++;
        this.getOrderList(this.data.pageIndex, this.data.navType)
    },



    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getOrderList(this.data.pageIndex, 1)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function (e) {
        let pages = getCurrentPages();
        let currPage = pages[pages.length - 1]; //当前页面
        if (currPage.data.evalutionshow) {
            this.setData({
                pageIndex: 1,
                navType: 1,
                navIndex: 0
            })
            this.changeline()
            this.getOrderList(1, 1)
        }

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
})