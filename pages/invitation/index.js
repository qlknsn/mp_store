
const { httpResource } = require('../../assets/js/common.js');
var app = getApp();
let move = function () { };
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bgImages: null,//背景图片
    shareImages: null,//分享朋友圈图片
    isActivity: false,//是否展示活动规则
    isShare: false,//是否显示分享弹框
    isScan: false,//是否显示面对面扫码
    base64: null,//太阳码
    actId: null,//活动ID
    actCode: null,//Boss后台配置跳转ID
    isDefault: false,//是否展示缺省图
    actName: null,//活动名称
    actStatus: null,//活动状态 0:开启，1:停止,
    actStartStatus: null,//活动开始状态：0:未开始，1:进行中，2:已结束
    wxShareTitle: null,//微信分享标题	
    buttonImages: null,//按钮图片
    backgroudColor: null,//背景色
    actRuleDesc: null,//活动规则说明
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _this = this;
    let { actCode } = _this.data;
    let isLogin = wx.getStorageSync("u-token");
    if (options.q) actCode = decodeURIComponent(options.q).split("=")[1];
    _this.setData({
      actId: options.actId ? options.actId : actCode,
    }, () => {
      //Boss页面配置进入
      if (actCode && actCode != null) {
        if (!isLogin) {
          wx.navigateTo({
            url: `/pages/userLogin/index?actCode=${actCode}`,
          });
        }
      }
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let _this = this;
    _this.getActivity();
  },

  //根据id查询活动图片
  async getActivity() {
    let _this = this;
    let { actId } = _this.data;
    try {
      let data = await httpResource('getActImgById', { actId: actId }, null, "get");
      if (data.actStatus == 0) {
        wx.setNavigationBarTitle({
          // title: data.actName ? data.actName : '邀请有礼',
          title: '邀请有礼',
        })
        _this.setData({
          actStatus: data.actStatus,
          actStartStatus: data.actStartStatus,
          isDefault: false,
          actRuleDesc: data.actRuleDesc,
        }, () => {
          //活动进行中
          if (data.actStartStatus == 1) {
            _this.setData({
              wxShareTitle: data.wxShareTitle,
              backgroudColor: data.backgroudColor,
              isDefault: true,
            }, () => {
              if (data.imgs && data.imgs.length != 0) {
                data.imgs.forEach((item) => {
                  if (item.imageType == 0) _this.setData({ bgImages: item.imgUrl });
                  if (item.imageType == 1) _this.setData({ buttonImages: item.imgUrl });
                  if (item.imageType == 2) _this.setData({ shareImages: item.imgUrl });
                });
              }
            });
          }
          //活动未开始
          if (data.actStartStatus == 0) {
            _this.setData({
              isDefault: false,
              actStartStatus: data.actStartStatus
            });
          }
          //活动已结束
          if (data.actStartStatus == 2) {
            _this.setData({
              isDefault: false,
              actStartStatus: data.actStartStatus
            });
          }
        })
      } else {
        _this.setData({
          isDefault: false,
          actStatus: data.actStatus,
          actStartStatus: data.actStartStatus,
        });
      }
    } catch (error) {
      _this.setData({
        isDefault: false,
        actStatus: 1
      });
    }
  },
  //获取太阳码
  async getCode() {
    let _this = this;
    let { actId } = _this.data;
    let { userId, headUrl, nickName } = wx.getStorageSync("userInfo");
    let page = 'pages/index/index';
    let data = await httpResource('generateMiniQRCode', { params: { act: `${actId}_${userId}` }, page }, null, "post");
    _this.setData({
      base64: 'data:image/png;base64,' + data,
      headUrl: headUrl,
      nickName: nickName
    })
  },
  //显示活动规则
  tapActivityShow() {
    wx.uma.trackEvent("click_rule", { 'actId': this.data.actId });
    this.setData({
      isActivity: true,
    });
  },
  //关闭活动规则
  tapActivityHide() {
    this.setData({
      isActivity: false,
    });
  },
  //显示分享弹框
  tapPopup() {
    wx.uma.trackEvent("click_invitation_button", { 'actId': this.data.actId });
    this.setData({
      isShare: true
    });
  },

  //关闭分享弹框
  onClose() {
    this.setData({
      isShare: false
    });
  },

  //显示面对面扫码
  tapShowScan() {
    let _this = this;
    wx.uma.trackEvent("click_ftf", { 'actId': this.data.actId });
    _this.setData({
      isScan: true,
      isShare: false
    }, () => {
      _this.getCode();
    });
  },
  //关闭面对面扫码
  onCloseScan() {
    this.setData({
      isScan: false
    });
  },
  //点击跳转邀请记录
  tapInvite() {
    let { actId } = this.data;
    wx.uma.trackEvent("click_record", { 'actId': actId });
    wx.navigateTo({
      url: `/pages/InvitedtoRecord/index?actId=${actId}`,
    })
  },
  //点击分享朋友圈
  SharingMoments() {
    let { actId } = this.data;
    wx.uma.trackEvent("click_pyq", { 'actId': actId });
    this.setData({
      isShare: false,
    }, () => {
      wx.navigateTo({
        url: `/pages/picture/index?actId=${actId}`,
      })
    });
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage(e) {
    let { shareImages, actId, wxShareTitle } = this.data;
    let { userId } = wx.getStorageSync("userInfo");
    if (e.from === "button") {
      wx.uma.trackEvent("click_wechat", { 'actId': actId });
      return {
        title: wxShareTitle,
        path: `/pages/index/index?actId=${actId}&inviteUserId=${userId}`,
        imageUrl: shareImages,
      };
    } else {
      wx.uma.trackEvent("homepage_share", { 'actId': actId });
      return {
        title: wxShareTitle,
        path: `/pages/index/index?actId=${actId}&inviteUserId=${userId}`,
        imageUrl: shareImages,
      };
    }
  }
})