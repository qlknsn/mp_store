import {
  pxTorpx,
  httpResource,
  isNeedAutho,
  gAppid,
  getCode,
} from '../../assets/js/common.js';
import {
  createBarcode,
  createQrcode
} from '../../assets/js/codeUtil';
var app = getApp();

Page({
  data: {
    parameter: {
      'title': '会员码',
    },
    titleColor: '#fff',
    showTitle: false,
    background: 'linear-gradient(222deg, #E9440E 0%, #D80000 100%)',
    rsIcon: "https://img.hpcang.com/mp_hpc/userCenter/icon_rs.png",
    wxPayImg: "https://img.hpcang.com/mp_hpc/userCenter/wxPay.png",
    vipUserDefaultPath: "https://img.hotmaxx.cn/web/common/mp-store/header.jpg",
    statusHeight: 'height:44rpx',
    statusTop: 'top:44rpx',
    lcode: null,
    userInfo: {}, // 用户基础信息
    showLogin: false, // userInfo Pop
    showPhone: false, // phoneInfo Pop
    authUserInfo: {}, //
    authPhoneInfo: {}, //
    hide: null,
    balanceMoney: 0, // 礼品卡余额
  },
  onLoad: function () {
    this.setData({
      statusHeight: 'height:' + app.globalData.statusBar + 'px',
      statusTop: 'top:' + app.globalData.statusBar + 'px'
    })
  },
  onShow: function () {
    wx.getScreenBrightness({
      success: (res) => {
        wx.setStorageSync("screenBrightness", res.value);
        wx.setScreenBrightness({
          value: 1
        })
      }
    })
    app.globalData.isFirstLogin == 1 && isNeedAutho().then(() => {
      this.setData({
        userInfo: wx.getStorageSync("userInfo")
      }, () => {
        createQrcode('qrcode', this.data.userInfo.userNo, pxTorpx(350), pxTorpx(350));
        createBarcode('barcode', this.data.userInfo.userNo, pxTorpx(610), pxTorpx(194));
        setTimeout(() => {
          createQrcode('qrcode', this.data.userInfo.userNo, pxTorpx(350), pxTorpx(350));
          createBarcode('barcode', this.data.userInfo.userNo, pxTorpx(610), pxTorpx(194));
        }, 500);
      })
    }).catch(() => {
      // this.goLogin();
    })
    getCode().then((code) => {
      this.setData({
        lcode: code
      })
    });
    //礼品卡余额
    this.getBalanceMoney();
  },
  onReady() { },
  onHide: function () {
    wx.setScreenBrightness({
      value: wx.getStorageSync("screenBrightness")
    })
    this.setData({
      showLogin: false,
      showPhone: false,
    })
  },
  onUnload: function () {
    wx.setScreenBrightness({
      value: wx.getStorageSync("screenBrightness")
    })
    this.setData({
      showLogin: false,
      showPhone: false,
    })
  },
  init() {
    this.onShow()
  },
  goLogin() {
    this.setData({
      showLogin: true
    })
  },
  copyVipCodeEvent() {
    wx.setClipboardData({
      data: this.data.userInfo.userNo,
      success: function () {
        wx.showToast({
          title: '复制成功',
          duration: 2000,
          icon: "none"
        })
      },
      fail: (e) => {
        console.log(e)
      }
    })
  },

  async payWX() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    });
    try {
      const data = await httpResource("wxPayOpen", {}, null, "GET");
      wx.openOfflinePayView({
        'appId': data.appId,
        'timeStamp': data.timeStamp,
        'nonceStr': data.nonceStr,
        'package': data.packageStr,
        'signType': data.signType,
        'paySign': data.paySign,
        'success': function (res) { },
        'fail': function (res) {
          console.log('失败', res)
        },
        'complete': function (res) {
          wx.hideLoading({
            fail: function () { }
          });
        }
      })
    } catch (error) {
      wx.hideLoading()
    }
  },
  // 获取剩余礼品卡金额
  async getBalanceMoney() {
    try {
      const data = await httpResource('balance', {}, null, "GET");
      if (data && data.balance) {
        this.setData({
          balanceMoney: data.balance
        })
      }
    } catch (error) {

    }
  },
  // 跳转惊喜礼卡支付
  giftClick() {
    const { balanceMoney } = this.data;
    if (balanceMoney == 0) {
      wx.showToast({
        title: "余额为0，暂不支持使用",
        icon: 'none',
        duration: 2000
      })
      return
    }
    wx.navigateTo({
      url: '/pages/user/giftCard/code/index'
    })
  }
})