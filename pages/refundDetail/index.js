import {
  createBarcode,
  createQrcode2
} from "../../assets/js/codeUtil";
const {
  httpResource,
  pxTorpx,
} = require('../../assets/js/common.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderDetaillist: [],
    refundOrderId: '',
    returnOrderNo: '',
    phone: '',
    refundType: '',
  },
  textpaste(code) {
    wx.setClipboardData({
      data: code.currentTarget.dataset.code,
      success: function (res) {
        wx.getClipboardData({
          // 这个api是把拿到的数据放到电脑系统中的
          success: function (res) {
            console.log(res.data) // data
          }
        })
      }
    })
  },
  // 获取退款详情
  getDetailData(params) {
    let _this = this;
    httpResource('refundDetail', params, (data) => {
      _this.setData({
        orderDetaillist: data,
        refundOrderId: data[0].refundOrderId,
        refundType: data[0].refundType,
      }, () => {
        _this.getqrcode(data[0].refundOrderId);
      })
    }, 'GET', null);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let _this = this;
    if (options.returnOrderNo) {
      wx.getStorage({
        key: 'userInfo',
        success: (data) => {
          _this.setData({
            returnOrderNo: options.returnOrderNo,
            phone: `${data.data.phone.substring(0, 3)}****${data.data.phone.substring(data.data.phone.length - 4)}`,
          }, () => {
            _this.getDetailData({ returnOrderNo: options.returnOrderNo });
          });
        }
      })
    }
  },
  showBigimg({ currentTarget: {
    dataset: {
      current, item
    }
  } }) {
    wx.previewImage({
      current: current, // 当前显示图片的http链接
      urls: item // 需要预览的图片http链接列表
    })
  },
  //获取条形码
  getqrcode(data) {
    let that = this;
    createQrcode2("qrcodes", data, pxTorpx(258), pxTorpx(258), that);
    let str2 = createBarcode("barcode", data, pxTorpx(504), pxTorpx(132));
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})