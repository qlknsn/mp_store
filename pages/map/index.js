// pages/map/index.js
import { getUserLocation } from '../../assets/js/commonData';
const { httpResource } = require('../../assets/js/common.js');
const { debounce } = require('../../assets/js/eventUtil');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navbarData: {
      class: '0',
      title: '搜索门店'
    },
    statusBar: 0,
    navHeight: 0,
    showMask: false, // 显示搜索遮罩
    footerHeight: '500rpx',
    //默认经纬度
    longitude: 121.48704,
    latitude: 31.211519,
    userLoction: [],
    inputValue: '', // 顶部搜索信息
    scale: 14,
    selectInfo: {},
    markers: [],
    desc: '门店名称（仅限中国大陆地区）',
    options: {}, // 跳转过来参数信息
    currentIndex: '',
    childrenId: '',
    currentPage: 1,
    showImage: false, // 社群码显示
    groupImgUrl: '', // 社群码url
  },
  initData(opt) {
    if (opt.inputValue) {
      this.setData({
        inputValue: opt.inputValue
      })
    }
    this.setData({
      longitude: app.globalData.longitude,
      latitude: app.globalData.latitude,
    });
  },
  // 门店导航
  bindmarkertap(e) {
    const id = e.markerId || (e.detail && e.detail.markerId) || 0;
    const { currentIndex, markers } = this.data;
    const index = markers.findIndex(item => item.id === id);
    if (currentIndex === index) return;
    const item = markers[index];
    this.setData({
      childrenId: `y${id}`, // 滚动至位置
      [`markers[${index}].width`]: 70,
      [`markers[${index}].height`]: 70,
      currentIndex: index,
      scale: 18,
      longitude: +item.location.longitude,
      latitude: +item.location.latitude,
    })
  },

  // 门店导航
  goStore(e) {
    const { markers } = this.data;
    const index = e.currentTarget.dataset.index;
    let { latitude, longitude, shopName: name, address } = markers[index];
    wx.openLocation({
      latitude: Number(latitude),
      longitude: Number(longitude),
      scale: 18,
      name: name,
      address: address.address
    })
  },
  // 中心定位
  bindStore(e) {
    const { markers, currentIndex } = this.data;
    const id = e.currentTarget.dataset.id;
    const index = markers.findIndex(item => item.id === id);
    const item = markers[index];
    if (currentIndex === index) return;
    this.setData({
      [`markers[${index}].width`]: 70,
      [`markers[${index}].height`]: 70,
      currentIndex: index,
      scale: 18,
      longitude: +item.location.longitude,
      latitude: +item.location.latitude,
    })
    if (currentIndex !== '') {
      this.setData({
        [`markers[${currentIndex}].width`]: 40,
        [`markers[${currentIndex}].height`]: 40,
      })
    }
  },
  setMakers(data) {
    return data.map((e, i) => {
      e.latitude = +e.location.latitude;
      e.longitude = +e.location.longitude;
      e.width = 40;
      e.height = 40;
      e.dataId = e.id;
      e.joinCluster = true;
      e.id = e.id || 0;
      e.iconPath = '/assets/images/common/maker-icon.png'
      e.zIndex = i === 0;
      return e
    })
  },
  bindscrolltolower() {
    this.data.currentPage++;
    this.getMarkers()
    console.log('加载更多')
  },
  // 获取全部门店
  async getMarkers() {
    const { longitude, latitude, selectInfo } = this.data;
    let lon = longitude;
    let lat = latitude;
    //检查缓存里面是否有自提状态
    if (wx.getStorageSync("SELECT_TABER") != 'ZI_TI' && app.globalData.deliverableAddress.addrId) {
      const { longitude: lon2, latitude: lat2 } = app.globalData.deliverableAddress;
      lon = lon2;
      lat = lat2;
      this.setData({
        longitude: lon2,
        latitude: lat2,
      })
      console.log('取到地址', lon, lat)
    } else {
      console.log('未获取到地址')
    }
    const data = await httpResource('getShopByRangeWithPage', {
      radius: 10000, longitude: lon, latitude: lat, currentPage: this.data.currentPage,
      pageSize: 20,
    });
    if (data && data.records && data.records.length) {
      const markers = this.setMakers(data.records);
      const list = [...this.data.markers, ...markers];
      this.setData({
        markers: list
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      statusBar: app.globalData.statusBar,
      navHeight: app.globalData.navHeight,
      options
    })
    this.userLoction()
  },
  // 去搜索门店
  goAddress() {
    wx.navigateTo({
      url: '/pages/address/index'
    })
  },
  // 设置当前门店
  setCurrentStore() {
    const { currentIndex, markers } = this.data;
    const item = markers[currentIndex];
    if (item.latitude && item.latitude && item.shopCode) {
      app.globalData.currentStore = item || {};
      wx.setStorageSync('current-store', item);
      wx.switchTab({
        url: '/pages/index/index',
      })
    } else {
      wx.showToast({
        title: '当前门店不可选取',
        icon: 'none'
      })
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () { },
  userLoction() {
    // 经纬度
    getUserLocation().then(res => {
      let { latitude, longitude } = res;
      this.setData({
        userLoction: [latitude, longitude],
        latitude: latitude,
        longitude: longitude,
      }, this.getMarkers);
    }).catch(() => {
      this.getMarkers()
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: `为你推荐一家宝藏折扣店，全场1折起！`,
      path: "/pages/index/index",
      imageUrl: "https://img-cdn.hotmaxx.cn/web/common/mp-store/common/share-base-img.jpg",
    }
  },
  async addGroup(e) {
    const shopCode = e.currentTarget.dataset.code;
    wx.uma.trackEvent('search_store');
    const url = await httpResource('getShopCommunityCode', {
      shopCode
    }, null, 'GET');
    if (url) {
      this.setData({
        groupImgUrl: url,
        showImage: true
      })
    } else {
      wx.showToast({
        title: '该门店暂无社群码哦',
        icon: 'none'
      })
    }
  },
  closeGroup() {
    this.setData({
      showImage: false,
      groupImgUrl: '',
    })
  }
})