const app = getApp();
const {
  isNeedAutho,
  httpResource,
  getCode,
} = require('../../../assets/js/common.js');

Page({
  /**
   * 页面的初始数据
   */
  data: {
    viewHeight: app.globalData.viewHeight - 50,
    parameter: {
      'title': '订单列表',
      return: 1
    },
    orderNo: 0,
    pageCount: 9999,
    orderList: [],
    pageIndex: 1,
    pageSize: 10,
    lcode: null,
    userInfo: {}, // 用户基础信息
    showLogin: false, // userInfo Pop
    showPhone: false, // phoneInfo Pop
    authUserInfo: {}, //
    authPhoneInfo: {},//
    toast: null,
    showModelVisible: false,
    redundApplyId: '',
    isFooter: false, // 是否滑动到底部
    orderType: 1, // 订单不同状态
    options: {}
  },
  // 去评价
  goEvaluation(e) {
    const orderNo = e.currentTarget.dataset.id;
    wx.uma.trackEvent('c_evaluate');
    wx.navigateTo({
      url: `/pages/user/orderEvaluation/index?orderNo=${orderNo}`
    })
  },
  // 查看评价
  goEvaluationDetail(e) {
    const orderNo = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: `/pages/user/orderEvaluationDetail/index?orderNo=${orderNo}&edit=true`
    })
  },
  closeNav() {
    this.setData({
      navShow: false
    })
  },

  showModal2(e) {
    // 跳转订单详情
    wx.navigateTo({
      url: `/pages/user/orderDetail/index?orderId=${e.currentTarget.dataset.orderid}`,
    })
  },

  showModal(e) {
    wx.navigateTo({
      url: `/pages/invoice/index?o=${e.currentTarget.dataset.channelorderno}&s=${e.currentTarget.dataset.shopno}&p=${e.currentTarget.dataset.poscode}`,
    })
  },

  //低部触发
  lower: function (e) {
    this.getOrderList(this.data.pageIndex, 1);
  },
  /**获取门店订单列表 */
  getOrderList: function (pageIndex, type) {
    var that = this;
    if ((pageIndex > that.data.pageCount) && (that.data.pageCount != 0)) {
      // wx.showToast({
      //   title: "已加载全部数据",
      //   icon: "none",
      // });
      this.setData({
        isFooter: true
      })
      return false;
    }
    var filter = {
      orderType: +this.data.orderType,
      currentPage: pageIndex,
      pageSize: that.data.pageSize,
    };
    httpResource('getAllOrderList', filter,
     (data) => {
      var orderListOld = that.data.orderList;
      var orderData = data.records || [];
      if (type == 1) {
        orderData.forEach((value) => {
          orderListOld.push(value);
        })
        that.setData({
          pageCount: data.pages,
          orderList: orderListOld,
          pageIndex: pageIndex + 1
        })
      } else if (type == 2) {
        orderData.forEach((value) => {
          orderListOld.push(value);
        })
        that.setData({
          orderList: orderData,
          pageIndex: 2
        })
      }
    }, "POST", null);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ options })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.toast = this.selectComponent("#toast");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this;
    that.setData({
      showModelVisible: false
    })
    this.initData(this.data.options);
    getCode().then((code) => {
      that.setData({
        lcode: code
      })
    });
  },
  initData(opt) {
    if (opt.orderType) {
      let obj = {
        2: '待评价',
        3: '退款/售后',
      };
      this.setData({
        orderType: opt.orderType, parameter: {
          return: 1,
          'title': obj[opt.orderType] || '订单列表',
        }
      }, this.getData)
    } else {
      this.getData();
    }
  },
  getData() {
    isNeedAutho().then(() => {
      this.loadOrderList()
    }).catch(() => {
      this.setData({
        showLogin: true
      })
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.setData({
      showLogin: false,
      showPhone: false,
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 刷新订单列表数据 
   */
  loadOrderList() {
    var that = this;
    that.setData({
      orderList: [],
    })
    isNeedAutho().then(() => {
      that.getOrderList(1, 1);
    })
  },
  init() {
    this.onShow();
  },
  requireDrawBack(e) {
    // try {
    // wx.setStorageSync("orderData", e.currentTarget.dataset.orderinfo);
    // } catch (e) { }
    let orderHandleRefundStatus = e.currentTarget.dataset.orderinfo.handleRefundStatus
    if (orderHandleRefundStatus != 1 && orderHandleRefundStatus != 2) {
      this.toast.showDialog(e.currentTarget.dataset.orderinfo.notRefundReason)
    } else {
      console.log('tips')
      this.setData({
        showModelVisible: true,
        redundApplyId: e.currentTarget.dataset.orderid
      })

      // wx.navigateTo({
      // url: `/pages/user/drawBack/index?orderId=${e.currentTarget.dataset.orderid}`,
      // })  
    }
  },
  // 退款单详情
  goRefundDetail(e) {
    wx.navigateTo({
      url: `/pages/user/refundApplyDetail/index?id=${e.currentTarget.dataset.refundapplyid}&return=1`,
    })
  },
  confirmFun() {
    console.log('confirm');
    wx.navigateTo({
      url: `/pages/user/drawBack/index?orderId=${this.data.redundApplyId}`,
    })
  }
})