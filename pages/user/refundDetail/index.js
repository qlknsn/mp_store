const app = getApp();

import {
    httpResource,
    pxTorpx
} from '../../../assets/js/common.js';

Page({
    /**
     * 页面的初始数据
     */
    data: {
        orderId: '',
        orderDetail: {},
        modal: false,
        parameter: {
            'title': '退款详情'
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            orderId: options.orderId
        })
        this.getOrderDetail()
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        ///this.getOrderDetail()
    },
    onHide: function () {

    },
    /**
     * 获取订单详情
     */
    getOrderDetail() {
        var that = this;
        httpResource('queryRefundDetail', {}, (data) => {
            let refundOrderStatus = '';
            if (data.refundOrderStatus == 1) {
                refundOrderStatus = '待退款';
            } else if (data.refundOrderStatus == 2) {
                refundOrderStatus = '退款取消';
            } else if (data.refundOrderStatus == 3) {
                refundOrderStatus = '退款完成';
            }
            data.orderDetails.forEach((item) => {
                item.productNumber = Math.abs(item.productNumber);
            })
            data.realAmount = Math.abs(data.realAmount);
            that.setData({
                orderDetail: data,
                parameter: {
                    title: refundOrderStatus,
                    return: 1,
                    home: 1
                },
            })
        }, 'GET', (res) => {
            // 请求失败，报错提示
            wx.showToast({
                title: res.msg,
                icon: 'none'
            })
        }, `/${that.data.orderId}`)
    },

    /**
     * 复制
     */
    copyVipCodeEvent() {
        wx.setClipboardData({
            data: this.data.orderDetail.channelOrderNo,
            success: function () {
                wx.showToast({
                    title: '复制成功',
                    duration: 2000,
                    icon: "none"
                })
            }
        })
    },
    copyVipCodeEvent2() {
        wx.setClipboardData({
            data: this.data.orderDetail.sourceChannelOrderNo,
            success: function () {
                wx.showToast({
                    title: '复制成功',
                    duration: 2000,
                    icon: "none"
                })
            }
        })
    },
    showModal() {
        wx.navigateTo({
            url: `/pages/invoice/index?o=${this.data.orderDetail.channelOrderNo}&s=${this.data.orderDetail.shopNo}&p=${this.data.orderDetail.posCode}`,
        })
    },
    refundBtn(){
        wx.navigateTo({
            url: `/pages/user/refundList/index?orderNo=${this.data.orderDetail.orderNo}`,
        })
    }
})