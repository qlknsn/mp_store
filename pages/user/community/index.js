const app = getApp();
const {
  isNeedAutho,
  httpResource,
  getCode,
} = require('../../../assets/js/common.js');
const { debounce } = require('../../../assets/js/eventUtil');
import { getUserLocation, getHighlightStrArray } from '../../../assets/js/commonData.js';

Page({
  /**
   * 页面的初始数据
   */
  data: {
    viewHeight: app.globalData.viewHeight - 50,
    parameter: {
      'title': 'Hotmaxx社群',
      return: 1
    },
    options: null,
    value: '',
    currentPage: 1,
    pageSize: 6,
    lcode: null,
    userInfo: {}, // 用户基础信息
    showLogin: false, // userInfo Pop
    showPhone: false, // phoneInfo Pop
    authUserInfo: {}, //
    authPhoneInfo: {},//
    toast: null,
    show: false, // 是否显示门店列表
    isFooter: false, // 是否滑动到底部
    latitude: null,
    longitude: null,
    shopList: [],
    shopName: '会员小程序',
    qrcodeUrl: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg', // 二维码地址
    qrName: '会员小程序'
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      options
    })
    // 经纬度
    getUserLocation().then(res => {
      let { latitude, longitude } = res;
      this.setData({
        latitude: latitude,
        longitude: longitude,
      }, this.getData('ok'));
    }).catch(() => {
      this.checkUserLocation()
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.toast = this.selectComponent("#toast");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  initData(opt) {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.setData({
      showLogin: false,
      showPhone: false,
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  onChange(e) {
    this.setData({
      show: true,
      value: e.detail
    }, this.getData())
  },
  /**
   * 聚焦
   */
  onFocus() {
    this.setData({
      show: true
    });
  },
  /**
   * 搜索
   */
  onClick() {
    this.setData({
      show: true,
    });
    this.getData()
  },
  /**
   * @param {Object} e
   * 选择门店
   */
  chooseShop(e) {
    this.setData({
      value: e.target.dataset.info.shopName,
      shopName: e.target.dataset.info.shopName || '会员小程序',
      show: false
    });
    this.getShopQrCode(e.target.dataset.info.shopCode)
  },
  checkUserLocation: function () {
    let _this = this;
    wx.getSetting({
      success: (res) => {
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
          wx.showModal({
            title: '请求授权当前位置',
            content: '需要获取您的地理位置，请确认授权',
            success: function (res) {
              if (res.cancel) {
                wx.showToast({
                  title: '拒绝授权',
                  icon: 'none',
                  duration: 1000
                })
              } else if (res.confirm) {
                wx.openSetting({
                  success: function (dataAu) {
                    if (dataAu.authSetting["scope.userLocation"] == true) {
                      wx.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 1000
                      })
                      //再次授权，调用wx.getLocation的API
                      getUserLocation().then(res => {
                        let { latitude, longitude } = res;
                        _this.setData({
                          latitude: latitude,
                          longitude: longitude,
                        }, _this.getData('ok'));
                      })
                    } else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'none',
                        duration: 1000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) {
          //调用wx.getLocation的API
          getUserLocation().then(res => {
            let { latitude, longitude } = res;
            _this.setData({
              latitude: latitude,
              longitude: longitude,
            }, _this.getData('ok'));
          })
        }
        else {
          //调用wx.getLocation的API
          getUserLocation().then(res => {
            let { latitude, longitude } = res;
            _this.setData({
              latitude: latitude,
              longitude: longitude,
            }, _this.getData('ok'));
          })
        }
      }
    })
  },
  /**
   * @param {Object} e
   * 获取门店列表
   */
  getData: debounce(async function (e) {
    const _this = this;
    const { value, latitude, longitude, pageSize, currentPage } = this.data;
    let data = await httpResource('getShopByKeywordsWithPage', {
      currentPage: currentPage,
      pageSize: pageSize,
      keywords: value,
      latitude, longitude
    });
    let list = [];
    if (data && data.records && data.records.length) {
      data = (data.records || []).map(item => {
        item.shopNameList = getHighlightStrArray(item.shopName, value);
        // 处理高亮
        return item
      });
      list = [...data];
      if (e == 'ok') {
        _this.setData({
          value: list[0].shopName,
          shopName: list[0].shopName || '会员小程序',
        })
        _this.getShopQrCode(list[0].shopCode)
      }
    }
    _this.setData({
      shopList: list,
      currentIndex: ''
    });
  }, 200),
  /**
   * 获取社群二维码
   */
  async getShopQrCode(shopCode) {
    const _this = this;
    const { shopName } = this.data;
    let data = await httpResource('getShopCommunityCode', {
      shopCode
    }, null, 'GET');
    _this.setData({
      qrcodeUrl: data || 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
      qrName: data ? shopName : '会员小程序',
    })
  },
  /**
   * 关闭选择店铺弹框
   */
  closeModel() {
    this.setData({
      show: false
    })
  }
})