const app = getApp();
import {
    httpResource,
} from '../../../assets/js/common.js';
import {
    throttl
} from '../../../assets/js/eventUtil';

const onThrottl = throttl((that, e) => {
    that.applyBtn(e);
}, 1500)

Page({
    /**
     * 页面的初始数据
     */
    data: {
		orderId: '',
        applyinfo: {},
        orderLines: {
            lineId: '',
            quantity: '',
            refundWay: 0,
            refundReasonType: '',
        },
        refundReasonTypeList: ["品质问题", "买错了", "价格高了", "不好吃/不好喝", "其他"],
        pickerIndex: null,
        parameter: {
            'title': '退货申请'
        },
		selectAllStatus: false,
		orderData: {}
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
		this.setData({
		    orderId: options.orderId
		})
		this.getOrderDetail()
	},
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        
    },
    onHide: function () {

    },
    cartNumDes(e) {
		let orderData = this.data.orderData;
		let quantity = orderData.orderDetails[e.currentTarget.dataset.index].quantity;
		if (orderData.mustAllRefund) {
			this.disHandleToast()
		} else {
			if (quantity > 1) {
				quantity--;
				orderData.orderDetails[e.currentTarget.dataset.index].quantity = quantity;
				this.setData({
					orderData: orderData
				})
			}
		}
        
    },
    cartNumInt(e) {
        let orderData = this.data.orderData;
        let quantity = orderData.orderDetails[e.currentTarget.dataset.index].quantity;
        if (quantity < orderData.orderDetails[e.currentTarget.dataset.index].canRefundNum) {
            quantity++;
			orderData.orderDetails[e.currentTarget.dataset.index].quantity = quantity;
            this.setData({
				orderData: orderData
            })
        }
    },
    /**
     * 复制
     */
    copyVipCodeEvent() {
        wx.setClipboardData({
            data: this.data.applyinfo.channelOrderNo,
            success: function () {
                wx.showToast({
                    title: '复制成功',
                    duration: 2000,
                    icon: "none"
                })
            }
        })
    },
    PickerChange(e) {
        this.setData({
            pickerIndex: e.detail.value,
            ['orderLines.refundReasonType']: this.data.refundReasonTypeList[e.detail.value],
        })
    },
    goback() {
        wx.navigateBack({
            delta: 1
        })
    },
    sumbitBtn(e) {
        onThrottl(this, e);
    },
    applyBtn(e) {
        this.setData({
            ['orderLines.refundReason']: e.detail.value.refundReason,
        })
        let filter = Object.assign({
            orderId: this.data.applyinfo.orderId
        }, {
            orderLines: [this.data.orderLines]
        })
        if (filter.orderLines[0].refundReasonType == '') {
            wx.showToast({
                title: "请选择退货原因!",
                duration: 2000,
                icon: "none"
            })
            return false;
        }
        if (filter.orderLines[0].refundReasonType == '其他' && filter.orderLines[0].refundReason == '') {
            wx.showToast({
                title: "请输入退货原因!",
                duration: 2000,
                icon: "none"
            })
            return false;
        }
        httpResource('refundApplyAdd', filter, (data) => {
            wx.showToast({
                title: "申请成功!",
                duration: 2000,
                icon: "none"
            })
            setTimeout(() => {
                wx.redirectTo({
                    url: `/pages/user/refundApplyDetail/index?id=${data}`,
                })
            }, 800)
        }, 'POST')
    },
	// 获取订单详情
	getOrderDetail() {
	    var that = this,
	        orderChannelZhTotal = '',
	        payNoTotal = '';
	    httpResource('queryOrderDetail', {}, (data) => {
			let dataObj = data;
			let selectAllStatus = false;
			let arr = []
			dataObj.orderDetails.map((item, i) => {
				if(dataObj.mustAllRefund) {
					item.selected = true;
					selectAllStatus = true;
				} else {
					item.selected = false;
				}
				item.quantity = item.canRefundNum;
				if(item.canRefundNum && item.canRefundNum > 0) {
					arr.push(item)
				}
			})
			dataObj.orderDetails = arr
			this.setData({
				orderData: dataObj,
				selectAllStatus: selectAllStatus
			})
		}, 'GET', (res) => {
				// 请求失败，报错提示
				wx.showToast({
					title: res.msg,
					icon: 'none'
				})
		},`/${that.data.orderId}`)
	},
	selectList(e){
		let dataObj = this.data.orderData
		if (dataObj.mustAllRefund) {
			this.disHandleToast()
		} else {
			let orderData = this.data.orderData
			orderData.orderDetails[e.currentTarget.dataset.index].selected = !orderData.orderDetails[e.currentTarget.dataset.index].selected
			let selectAllStatus = orderData.orderDetails.every(item => item.selected == true)
			this.setData({
				orderData: orderData,
				selectAllStatus: selectAllStatus
			})
		}
	},
	selectAll() {
		let dataObj = this.data.orderData
		if (dataObj.mustAllRefund) {
			this.disHandleToast()
		} else {
			let orderData = this.data.orderData
			let selectAllStatus = this.data.selectAllStatus
			if(this.data.selectAllStatus) {
				orderData.orderDetails.map(item => {
					item.selected = false
				})
			} else {
				orderData.orderDetails.map(item => {
					item.selected = true
				})
			}
			this.setData({
				orderData: orderData,
				selectAllStatus: !selectAllStatus
			})
		}
	},
	disHandleToast() {
		let mustAllRefundReason = this.data.orderData.mustAllRefundReason
		wx.showToast({
			title: mustAllRefundReason,
			icon: 'none'
		})
		
	},
	//缓存订单信息 若选择商品可进行下一步 
	goDrawBackDetail() {
		// console.log('eeee', this.data.orderData)
		let hasSelected = this.data.orderData.orderDetails.some(item => item.selected == true)
		if (hasSelected) {
			wx.setStorageSync("drawBackDataInfo", this.data.orderData);
			wx.navigateTo({
				url: `/pages/user/drawBackDetail/index`,
			})
		} else {
			wx.showToast({
				title: "请选择退款商品",
				icon: 'none'
			})
		}
		
	}
})