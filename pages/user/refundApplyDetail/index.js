const app = getApp();
import {
    httpResource,
    pxTorpx
} from '../../../assets/js/common.js';

import {
    createBarcode,
    createQrcode
} from '../../../assets/js/codeUtil';

import {
    throttl
} from '../../../assets/js/eventUtil';

const cancelRefund = throttl((that, e) => {
    that.cancelRefundFun(e);
}, 1500)

Page({
    /**
     * 页面的初始数据
     */
    data: {
		viewHeight: app.globalData.viewHeight,
        id: '',
        parameter: {
            'title': '',
        },
        orderDetail: {},
        modal: false,
		return: 1
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
		console.log('334', options)
        this.setData({
            id: options.id,
			return: options.return,
        })
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.getOrderDetail()
    },
    onHide: function () {

    },
	submitCancel(e) {
	    cancelRefund(this, e);
	},
	cancelRefundFun () {
		let that = this;
		wx.showModal({
		  title: '提示',
		  content: '请确认是否取消退款',
		  success (res) {
		    if (res.confirm) {
		      let filter = {
		        id: that.data.orderDetail.id,
		      };
		      httpResource('cancelRefund', '', (data) => {
				  wx.showToast({
					  title: "申请成功!",
					  duration: 2000,
					  icon: "none"
				  })
				  that.getOrderDetail()
		      }, "POST", (res) => {
		            // 请求失败，报错提示
		            wx.showToast({
		                title: res.msg,
		                icon: 'none'
		            })
		        }, `/${filter.id}`);
		    } else if (res.cancel) {
		      console.log('用户点击取消')
		    }
		  }
		})
	},
    /**
     * 获取订单详情
     */
    getOrderDetail() {
        var that = this;
        httpResource('refundApplyDetail', {}, (data) => {
            createQrcode('qrcode', data.id, pxTorpx(360), pxTorpx(360));
            createBarcode('barcode', data.id, pxTorpx(526), pxTorpx(168));
            setTimeout(() => {
                createQrcode('qrcode', data.id, pxTorpx(360), pxTorpx(360));
                createBarcode('barcode', data.id, pxTorpx(526), pxTorpx(168));
            }, 500);
            that.setData({
                orderDetail: data,
				parameter: {
				    title: data.showStatusDesc,
					return: that.data.return,
					home: 1
				}
            })
        }, 'GET', (res) => {
            // 请求失败，报错提示
            wx.showToast({
                title: res.msg,
                icon: 'none'
            })
        }, `/${that.data.id}`)
    },

    /**
     * 复制
     */
    copyCodeEvent(e) {
		let code = e.currentTarget.dataset.code
        wx.setClipboardData({
            data: code,
            success: function () {
                wx.showToast({
                    title: '复制成功',
                    duration: 2000,
                    icon: "none"
                })
            }
        })
    },
})