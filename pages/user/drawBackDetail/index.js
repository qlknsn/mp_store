const app = getApp();
import {
    httpResource,
} from '../../../assets/js/common.js';
import {
    throttl
} from '../../../assets/js/eventUtil';

const onThrottl = throttl((that, e) => {
    that.applyBtn(e);
}, 1500)

Page({
    /**
     * 页面的初始数据
     */
    data: {
        applyinfo: {},
		totalAmount: '',
        orderLines: {
            lineId: '',
            quantity: '',
            refundWay: 0,
            refundReasonType: '',
        },
        refundReasonTypeList: ["品质问题", "买错了", "价格高了", "不好吃/不好喝", "其他"],
        pickerIndex: null,
        parameter: {
            'title': '退货申请'
        },
		drawBackObj: {},
		drawBackList: []
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {},
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
		let drawBackObj = wx.getStorageSync('drawBackDataInfo')
		let drawBackList = drawBackObj.orderDetails.filter(item => item.selected == true)
		let totalAmount = 0
		drawBackList.map(item => {
			item.pickerIndex = null;
			item.refundReason = '';
			item.refundAmount = parseFloat(item.discountPrice).mul(parseFloat(item.quantity))
			totalAmount = totalAmount.add(item.refundAmount)
		})
        this.setData({
			drawBackObj: drawBackObj,
			drawBackList: drawBackList,
			totalAmount: totalAmount
        })
    },
    onHide: function () {

    },
    cartNumDes() {
        let quantity = this.data.orderLines.quantity;
        if (quantity > 1) {
            quantity--;
            this.setData({
                ['orderLines.quantity']: quantity
            })
        }
    },
    cartNumInt() {
        let quantity = this.data.orderLines.quantity;
        if (quantity < this.data.applyinfo.orderDetails[0].productNumber) {
            quantity++;
            this.setData({
                ['orderLines.quantity']: quantity
            })
        }
    },
    /**
     * 复制
     */
    copyVipCodeEvent() {
        wx.setClipboardData({
            data: this.data.applyinfo.channelOrderNo,
            success: function () {
                wx.showToast({
                    title: '复制成功',
                    duration: 2000,
                    icon: "none"
                })
            }
        })
    },
    PickerChange(e) {
		console.log('9999', e)
		let drawBackList = this.data.drawBackList
		let goodsCurrent = drawBackList[e.currentTarget.dataset.index]
		goodsCurrent.pickerIndex = e.detail.value
        this.setData({
            ['orderLines.refundReasonType']: this.data.refundReasonTypeList[e.detail.value],
			drawBackList: drawBackList
        })
    },
    goback() {
        wx.navigateBack({
            delta: 1
        })
    },
	reasonFun(e) {
		let drawBackList = this.data.drawBackList
		let currentItem = drawBackList[e.currentTarget.dataset.index]
		currentItem.refundReason = e.detail.value
		this.setData({
			drawBackList: drawBackList
		})
	},
    sumbitBtn(e) {
        onThrottl(this, e);
    },
	//提交退货申请
    applyBtn(e) {
        let bodyData = []
		for (let i = 0; i < this.data.drawBackList.length; i++) {
			let item = this.data.drawBackList[i]
			if (item.pickerIndex == null) {
				wx.showToast({
					title: "请选择退货原因!",
					duration: 2000,
					icon: "none"
				})
				return false;
			}
			if (item.pickerIndex == 4 && item.refundReason.trim() == '') {
				wx.showToast({
					title: "请输入退货原因!",
					duration: 2000,
					icon: "none"
				})
				return false;
			}
			let orderLineObj = {
				lineId: item.id,
				quantity: item.quantity,
				refundReasonType: this.data.refundReasonTypeList[item.pickerIndex],
				refundReason: item.refundReason
			}
			bodyData.push(orderLineObj)
		}	
        let filter = Object.assign({
            orderId: this.data.drawBackObj.orderId
        }, {
            orderLines: bodyData
        })
        httpResource('refundApplyAdd', filter, (data) => {
            wx.showToast({
                title: "申请成功!",
                duration: 2000,
                icon: "none"
            })
            setTimeout(() => {
                wx.redirectTo({
					url: `/pages/user/refundApplyDetail/index?id=${data}&return=3`,
                })
            }, 800)
        }, 'POST')
    }
})