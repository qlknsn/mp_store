// pages/user/energy/index.js
import { httpResource } from '../../../assets/js/common.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      class: '0',
      title: '我的能量'
    },
    opcity: 0,
    currentPage: 1, // 列表第几页
    energy: '',
    energyList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getData();
  },
  goMedal() {
    wx.navigateTo({
      url: "/pages/user/medal/index",
    });
  },
  getData() {
    var userInfo = wx.getStorageSync('userInfo');
    let memId = '';
    if (userInfo && userInfo.userId) {
      memId = userInfo.userId;
    }
    let params = {
      memId,
      currentPage: this.data.currentPage,
      pageSize: 20,
    }
    httpResource('selectEnergyLog', params, data => {
      const records = data.records || [];
      this.setData({
        energyList: [...this.data.energyList, ...records],
      })
    }, "POST", null)
  },
  async getEnergy() {
    var userInfo = wx.getStorageSync('userInfo');
    let memId = '';
    if (userInfo && userInfo.userId) {
      memId = userInfo.userId;
    }
    const data = await httpResource('findMemberEnergy', { memId }, '', "get");
    this.setData({
      energy: data || 0
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getEnergy();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.currentPage++
    this.getData()
  },
  onPageScroll(e) {
    const top = e.scrollTop;
    if (e.scrollTop >= 150) {
      if (this.data.opcity < 1) {
        this.setData({
          opcity: 1
        })
      }
    } else {
      const opcity = e.scrollTop / 150;
      this.setData({
        opcity
      })
    }
  }
})