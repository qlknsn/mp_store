const app = getApp();

import {
    httpResource,
    pxTorpx
} from '../../../assets/js/common.js';

Page({
    /**
     * 页面的初始数据
     */
    data: {
        orderId: '',
        orderDetail: {},
        modal: false,
        parameter: {
            'title': '订单详情'
        },
		toast: null,
		showModelVisible: false,
		redundApplyId: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            orderId: options.orderId
        })
        this.getOrderDetail()
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        ///this.getOrderDetail()
    },
    onHide: function () {

    },
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {
	  this.toast = this.selectComponent("#toast");	
	},
    /**
     * 获取订单详情
     */
    getOrderDetail() {
        var that = this,
            orderChannelZhTotal = '',
            payNoTotal = '';
        httpResource('queryOrderDetail', {}, (data) => {
            let orderState = '';
            if (data.refundStatus == 1) {
                orderState = '已完成';
            } else if (data.refundStatus == 5) {
                orderState = '部分退款';
            } else if (data.refundStatus == 10) {
                orderState = '已退款';
            }
            data.payDetails.forEach((item) => {
                orderChannelZhTotal += item.orderChannelZh + ',';
                if (item.payType == 'ALIPAY' || item.payType == 'WEIXIN' || item.payType == 'RMB') {
                    payNoTotal += item.payNo + ',';
                }
            })
            data.orderChannelZhTotal = orderChannelZhTotal.substring(0, orderChannelZhTotal.length - 1);
            if (payNoTotal == '') {
                data.payNoTotal = '-'
            } else {
                data.payNoTotal = payNoTotal.substring(0, payNoTotal.length - 1);
            }
            that.setData({
                orderDetail: data,
                parameter: {
                    title: orderState,
                    return: 1,
                    home: 1
                },
            })
        }, 'GET', (res) => {
            // 请求失败，报错提示
            wx.showToast({
                title: res.msg,
                icon: 'none'
            })
        }, `/${that.data.orderId}`)
    },
    applyBtn(e) {
        let applyinfo = {
            orderId:this.data.orderDetail.orderId,
            shopName:this.data.orderDetail.shopName,
            channelOrderNo:this.data.orderDetail.channelOrderNo,
            posCode:this.data.orderDetail.posCode,
            orderDetails:[e.currentTarget.dataset.applygoodsinfo]
        }
        wx.setStorageSync("applyinfo", applyinfo);
        wx.navigateTo({
            url: '/pages/user/refundApply/index',
        })
    },
    /**
     * 复制
     */
    copyVipCodeEvent() {
        wx.setClipboardData({
            data: this.data.orderDetail.channelOrderNo,
            success: function () {
                wx.showToast({
                    title: '复制成功',
                    duration: 2000,
                    icon: "none"
                })
            }
        })
    },
	/**
	 * 申请退款
	 * */
	requireDrawBack(e) {
		  let orderHandleRefundStatus = this.data.orderDetail.handleRefundStatus
		  if (orderHandleRefundStatus != 1 && orderHandleRefundStatus != 2) {
			  this.toast.showDialog(this.data.orderDetail.notRefundReason)
		  } else {
			 // wx.navigateTo({
				// url: `/pages/user/drawBack/index?orderId=${this.data.orderDetail.orderId}`,
			 // })  
			 this.setData({
				  showModelVisible: true,
				  redundApplyId: this.data.orderDetail.orderId
			 })
		  }
	},
    showModal() {
        wx.navigateTo({
            url: `/pages/invoice/index?o=${this.data.orderDetail.channelOrderNo}&s=${this.data.orderDetail.shopNo}&p=${this.data.orderDetail.posCode}`,
        })
    },
    refundBtn() {
		wx.navigateTo({
			url: `/pages/user/refundOrderList/index?orderNo=${this.data.orderDetail.orderNo}&orderId=${this.data.orderDetail.orderId}&return=1`,
		})
    },
	confirmFun() {
		  console.log('confirm');
		  wx.navigateTo({
		  	url: `/pages/user/drawBack/index?orderId=${this.data.redundApplyId}`,
		  })  
	}
})