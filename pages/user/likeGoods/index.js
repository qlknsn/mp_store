const { httpResource } = require("../../../assets/js/common.js");
import {
  getProductDetail
} from "../../../assets/js/commonData";
const app = getApp();
Page({

  /**
   * 组件的初始数据
   */
  data: {
    defaultImg2:
      "https://img.hotmaxx.cn/web/common/mp-store/home-index/goods-default.png",
    navbarData: { title: '我喜欢' },
    refresher: false,
    loading: false,
    noMore: false,
    loadingFailed: false,
    currentPage: 1,
    isDefault: false,
    goodsList: [],
    noLikesBarcode: ''
  },
  // 清空我喜欢
  async clearLike() {
    await httpResource('clearFavorite', {}, null, "GET");
    wx.showToast({
      title: '清空成功！',
      icon: 'none'
    })
    this.data.currentPage = 1;
    this.getAllGoods();
  },
  changeLikeGoods() {
    const { noLikesBarcode, goodsList } = this.data;
    if (noLikesBarcode) {
      const index = goodsList.findIndex(e => e.barcode = noLikesBarcode);
      if (index > -1) {
        goodsList.splice(index, 1);
        this.setData({
          goodsList,
        })
      }
    }
  },
  onLoad: function () {
    const data = wx.getSystemInfoSync();
    this.setData({
      moveViewLeft: 0,
      moveViewTop: data.windowHeight - 100,
    })
    this.getAllGoods();
  },
  onShow: function () {
    this.changeLikeGoods();
  },
  // 下拉刷新
  bindrefresherrefresh() {
    this.setData({
      refresher: true,
      currentPage: 1,
    }, () => {
      setTimeout(() => {
        this.getAllGoods()
      }, 100)
    })
  },
  getAllGoods: function () {
    let _this = this
    let obj = { currentPage: _this.data.currentPage, pageSize: 20 };
    const res = httpResource("favoriteList", obj, null, "GET");
    res.then((res) => {
      wx.hideLoading();
      let data = (res.records || []).map(getProductDetail);
      _this.setData({ loading: false, noMore: false });
      if (data && data.length != 0) {
        if (_this.data.currentPage == 1) {
          _this.setData({ goodsList: data, refresher: false });
        } else {
          if (_this.data.goodsList.length < res.total) {
            _this.setData({
              goodsList: _this.data.goodsList.concat(data),
              refresher: false
            })
          }
          if (_this.data.goodsList.length == res.total) {
            _this.setData({
              noMore: true,
            })
          }
        }
      } else {
        if (_this.data.currentPage == 1) {
          _this.setData({
            goodsList: [],
          })
        }
        _this.setData({
          noMore: _this.data.currentPage == 1 ? false : true,
          isDefault: true,
          refresher: false
        })
      }
      //如果返回的数据为空，那么就没有下一页了
      if (res.records.length == 0) {
        _this.setData({
          isDefault: true
        })
      }
    })
  },
  //到达底部
  scrollToLower: function (e) {
    let _this = this;
    if (!_this.data.loading && !_this.data.noMore) {
      _this.setData({
        loading: true,
        currentPage: _this.data.currentPage + 1,
      }, () => {
        _this.getAllGoods();
      })
    }
  },
  //删除
  async deleteGoods(event) {
    let _this = this;
    let { barcode } = event.target.dataset;
    await httpResource('unFavorite', { barcode }, null, "GET");
    //不知道goodCode 是不是唯一得，如果不是唯一那就有bug
    _this.data.goodsList.forEach((item, index) => {
      if (item.packageCode == barcode) {
        _this.data.goodsList.splice(index, 1);
      }
    })
    _this.setData({
      goodsList: _this.data.goodsList
    }, () => {
      if (_this.data.goodsList && _this.data.goodsList.length == 0) {
        _this.setData({
          isDefault: true,
        })
      }
    })
  },
  //跳转首页
  routerUrl: function () {
    wx.switchTab({ url: '/pages/index/index' });
  },
  // 加入购物车
  async addCart(e) {
    try {
      wx.hideToast();
    } catch (error) { }
    if (!app.globalData.deliverableAddress.addrId) {
      wx.showToast({
        title: '请先选择地址哦',
        duration: 1000,
        icon: 'none'
      })
      return
    }
    const shopCode = app.globalData.currentStore.shopCode;
    const selectTaber = wx.getStorageSync("SELECT_TABER");
    if (!shopCode) {
      wx.showToast({
        title: '请先选择门店哦',
        duration: 1000,
        icon: 'none'
      })
      return
    }
    const { item } = e.currentTarget.dataset;
    const { packageCode, goodsName, goodsCode, coverImage, salePrice, marketPrice, categoryOneCode,
      categoryTwoCode,
      categoryThreeCode,
    } = item;
    const params = JSON.parse(
      JSON.stringify({
        categoryTwoCode,
        categoryThreeCode,
        version: 2,
        shopCode,
        packageCode,
        goodsName, goodsCode, coverImage, salePrice, marketPrice, categoryOneCode,
        addType: 2,
        receiveType: selectTaber == 'ZI_TI' ? 3 : 4
      }))
    let data = await httpResource('cartAdd2', params);
    if (data && data.extraMsg) {
      wx.showToast({
        title: data.extraMsg,
        icon: "none",
        duration: 3000,
      });
    } else {
      wx.showToast({
        title: "加入成功",
        icon: "none",
        duration: 1000,
      });
    }
    const num = await httpResource('cartCount', { shopCode });
    app.globalData.cartNum = num;
  },
  goDetail(e) {
    // const status = this.checkLogin();
    // if (!status) {
    //   return;
    // }
    // if (!app.globalData.isLogin) return;
    if (!app.globalData.currentStore.shopCode) return;
    const barcode = e.currentTarget.dataset.barcode;
    wx.navigateTo({
      url: `/pages/productDetail/index?barcode=${barcode}`
    })
  },
})
