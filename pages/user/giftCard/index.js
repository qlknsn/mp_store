// pages/user/giftCard/index.js
const app = getApp();
import { httpResource, getAllRect, getRect } from '../../../assets/js/common.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loading: true,
    parameter: {
      'title': '惊喜礼卡',
      return: 1,
      home: 1
    },
    navHeight: 0,
    opcity: 0,
    tabLeft: 104,
    navIndex: 0,
    tabTitleArr: [ // tab
      { tabTitle: '可用卡', nums: 0, navIndex: 0 },
      { tabTitle: '不可用卡', nums: 0, navIndex: 1 },
    ],
    balanceMoney: 0, // 余额
    availableList: [], // 可用列表
    availableLoad: false,
    unusableList: [], // 不可用列表
    unusableLoad: false,
    showIllustrate: false, // 使用说明
    showHistory: false, // 记录
    currentPage: 1,
    currentHistoryPage: 1,
    gqUrl: 'https://img.hotmaxx.cn/web/common/mp-store/giftCard/card-gq.png',// 过期url
    ywUrl: 'https://img.hotmaxx.cn/web/common/mp-store/giftCard/card-yw.png',// 用完url
    wqyUrl: 'https://img.hotmaxx.cn/web/common/mp-store/giftCard/card-wqy.png',// 未启用url
    historyDefaultIcon: 'https://img.hotmaxx.cn/web/common/mp-store/giftCard/history-default.png',// 记录默认图
    cardDefaultIcon: 'https://img.hotmaxx.cn/web/common/mp-store/giftCard/card-list-default.png',//-default.png',// 卡片列表默认图
    historyList: [], // 记录列表

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      navHeight: app.globalData.navHeight,
    });

  },
  getLineLeft() {
    let _this = this;
    const { navIndex } = this.data;
    Promise.all([
      (0, getAllRect)(this, '.tab-text'),
      (0, getRect)(this, '.tab-b'),
    ]).then(function (_a) {
      var _b = _a[0], rects = _b === void 0 ? [] : _b, lineRect = _a[1];
      var rect = rects[navIndex];
      if (rect == null) {
        return;
      }
      var lineOffsetLeft = rects
        .slice(0, navIndex)
        .reduce(function (prev, curr) { return prev + curr.width; }, 0);

      lineOffsetLeft +=
        (rect.width - lineRect.width) / 2;
      _this.setData({ tabLeft: lineOffsetLeft }, () => {
        if (!_this.data.transition) {
          _this.setData({
            transition: 'all 0.2s ease'
          })
        }
      });
    });
  },
  bindscrolltolower() {
    const { navIndex } = this.data;
    if (navIndex === 0) return;
    this.data.currentPage++;
    this.getUnData()
    console.log('加载更多')
  },
  bindHistoryscrolltolower() {
    this.data.currentHistoryPage++;
    this.getHistoryData()
    console.log('加载更多')
  },
  //tab切换
  changeTab: function (e) {
    this.setData({
      navIndex: e.currentTarget.dataset.index
    })
    if (e.currentTarget.dataset.index == 1) {
      this.data.currentPage = 1;
      this.getUnData();
    } else {
      this.getData();
    }
    this.changeline();
  },
  changeline: function () {
    this.setData({
      tabLeft: this.data.navIndex == 1 ? 376 : 104
    })
  },
  copyClick: function (e) {
    const { code } = e.currentTarget.dataset;
    wx.setClipboardData({
      data: code,
      success: function () {
        wx.showToast({
          title: '复制成功',
          duration: 2000,
          icon: "none"
        })
      }
    })
  },
  goBind() {
    wx.navigateTo({
      url: '/pages/user/giftCard/bind/index'
    })
  },
  goBuy() {
    wx.navigateTo({
      url: '/pages/user/giftCard/buy/index'
    })
  },
  getItem(item) {
    for (let key in item) {
      if (item[key] == null) {
        item[key] = '';
      }
      item['beginTime'] = item['beginTime'].substring(0, 10).replace(/-/g, '.');
      item['deadline'] = item['deadline'].substring(0, 10).replace(/-/g, '.');
    }
    return item
  },
  // 获取失效数据
  async getUnData() {
    try {
      const params = {
        pageNo: this.data.currentPage,
        pageSize: 5
      }
      const data = await httpResource('balanceUnInfo', params, null, "GET");
      this.setData({
        unusableLoad: true,
      })
      wx.stopPullDownRefresh()
      if (data.unusable && data.unusable.records && data.unusable.records.length) {
        const { total, currentPage, records } = data.unusable;
        const unusableList = this.data.unusableList;
        if (currentPage == 1) {
          this.setData({ unusableList: records.map(this.getItem) })
        } else if (unusableList.length < total) {
          this.setData({
            unusableList: unusableList.concat(records.map(this.getItem)),
          })
        }

        this.setData({
          [`tabTitleArr[${1}].nums`]: total || 0
        })
      } else {
        if (this.data.currentPage == 1) {
          this.setData({
            unusableList: [], // 不可用列表,
          })
        }
      }
    } catch (e) {
      wx.stopPullDownRefresh()
      this.setData({
        unusableLoad: true,
      })
    }
  },
  // 获取数据
  async getData() {
    try {
      const data = await httpResource('balanceInfo', {}, null, "GET");
      this.setData({
        availableLoad: true
      })
      wx.stopPullDownRefresh()
      if (data) {
        if (data.unusableCount) {
          this.setData({
            [`tabTitleArr[${1}].nums`]: data.unusableCount
          })
        }
        if (data.balance) {
          this.setData({ balanceMoney: data.balance })
        }
        if (data.available && data.available.length) {
          this.setData({ availableList: data.available.map(this.getItem), [`tabTitleArr[${0}].nums`]: data.available.length })
        } else {
          this.setData({ availableList: [], [`tabTitleArr[${0}].nums`]: 0 })
        }

      }
      this.setData({
        loading: false,
      })
    } catch (error) {
      wx.stopPullDownRefresh()
      this.setData({
        balanceMoney: 0, // 余额
        availableList: [], // 可用列表
        loading: false,
        availableLoad: true,
      })
    }
  },
  // 获取数据
  async getHistoryData() {
    try {
      const data = await httpResource('balanceFlow', {
        pageNo: this.data.currentHistoryPage,
        pageSize: 20
      }, null, "GET");
      if (data) {
        if (data.consumerPageRecords && data.consumerPageRecords.records && data.consumerPageRecords.records.length) {
          const { total, currentPage, records } = data.consumerPageRecords;
          const historyList = this.data.historyList;
          if (currentPage == 1) {
            this.setData({ historyList: records })
          } else if (historyList.length < total) {
            this.setData({
              historyList: historyList.concat(records),
            })
          }

        }
      } else {
        if (this.data.currentHistoryPage == 1) {
          this.setData({
            historyList: [], // 不可用列表,
          })
        }
      }
    } catch (error) {
      console.log(error)
    }
  },
  // 打开使用说明
  openIllustrate() {
    this.setData({
      showIllustrate: true
    })
  },
  // 关闭使用说明
  closeIllustrate() {
    this.setData({
      showIllustrate: false
    })
  },
  // 打开使用说明
  openHistory() {
    this.setData({
      showHistory: true
    })
  },
  // 关闭使用说明
  closeHistory() {
    this.setData({
      showHistory: false
    })
  },
  // 跳转支付页面
  goPay() {
    const { balanceMoney } = this.data;
    if (balanceMoney == 0) {
      return
    }
    wx.navigateTo({
      url: '/pages/user/giftCard/code/index'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // this.getLineLeft()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.data.currentPage = 1;
    this.data.currentHistoryPage = 1;
    this.getData();
    this.getHistoryData();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },


  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onPullDownRefresh: function () {
    // 标题栏显示刷新图标，转圈圈
    wx.showNavigationBarLoading()

    console.log("onPullDownRefresh");

    // 请求最新数据
    this.data.currentHistoryPage = 1;
    this.data.currentPage = 1;
    const { navIndex } = this.data;
    setTimeout(() => {
      if (navIndex == 0) {
        this.getData();
      } else {
        this.getUnData();
      }
    }, 300)
    this.getHistoryData();
    // 标题栏隐藏刷新转圈圈图标
    setTimeout(() => {
      wx.hideNavigationBarLoading()
    }, 350);

  },
  onPageScroll(e) {
    const top = e.scrollTop;
    if (e.scrollTop >= 150) {
      if (this.data.opcity < 1) {
        this.setData({
          opcity: 1
        })
      }
    } else {
      const opcity = e.scrollTop / 150;
      this.setData({
        opcity
      })
    }
  }
})