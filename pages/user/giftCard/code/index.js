// pages/user/giftCard/code/index.js
import { httpResource, pxTorpx } from '../../../../assets/js/common.js';
import {
  createBarcode,
  createQrcode
} from '../../../../assets/js/codeUtil';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      'title': '惊喜礼卡支付码',
      return: 1,
    },
    code: '',
    hiddenCode: '',
    showCode: '',
    isHidden: true,
    balanceMoney: null
  },
  timer: null,
  showCodeFn() {
    this.setData({
      isHidden: false
    })
  },
  hiddenCodeFn() {
    this.setData({
      isHidden: true
    })
  },
  async getData() {
    try {
      const data = await httpResource('authCode', {}, null, "GET");
      if (data) {
        if (data.balance) {
          this.setData({
            balanceMoney: data.balance
          })
        }
        if (data.authCode) {
          this.setData({
            code: data.authCode,
            hiddenCode: this.createSplitCode(this.createHiddenCode(data.authCode), 4),
            showCode: this.createSplitCode(data.authCode, 4),
          }, this.caeateCode)
        }
      }
      this.timer = setTimeout(() => {
        this.getData();
      }, 30000)
    } catch (error) {
      this.timer = setTimeout(() => {
        this.getData();
      }, 3000);
      this.setData({
        code: '123456789123456789',
        hiddenCode: this.createSplitCode(this.createHiddenCode('123456789123456789'), 4),
        showCode: this.createSplitCode('123456789123456789', 4),
        balanceMoney: '180.00'
      }, this.caeateCode);
    }
  },
  // 处理为隐藏后的code
  createHiddenCode(code) {
    return `${code.substring(0, 3)}***********${code.substring(14, 18)}`
  },
  // 处理为分割后的code
  createSplitCode(code, len) {
    let codeArr = code.split('');
    let list = []
    while (codeArr.length > len) {
      // 倒序切割
      list.unshift(codeArr.slice(-len));
      codeArr = codeArr.slice(0, -len);
    }
    // 处理剩余长度
    list.unshift(codeArr);
    list = list.map(item => item.join(''));
    return `${list.join(' ')}`
  },
  caeateCode() {
    const { code } = this.data;
    createQrcode('qrcode', code, pxTorpx(352), pxTorpx(352));
    createBarcode('barcode', code, pxTorpx(652), pxTorpx(178));
    setTimeout(() => {
      createQrcode('qrcode', code, pxTorpx(352), pxTorpx(352));
      createBarcode('barcode', code, pxTorpx(652), pxTorpx(178));
    }, 500);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getData();
    wx.getScreenBrightness({
      success: (res) => {
        wx.setStorageSync("screenBrightness", res.value);
        wx.setScreenBrightness({
          value: 1
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    wx.setScreenBrightness({
      value: wx.getStorageSync("screenBrightness")
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearTimeout(this.timer);
    this.timer = null;
    wx.setScreenBrightness({
      value: wx.getStorageSync("screenBrightness")
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})