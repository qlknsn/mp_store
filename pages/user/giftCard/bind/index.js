// pages/user/giftCard/buy/index.js
const app = getApp();
import { httpResource } from '../../../../assets/js/common.js';
const { throttl } = require('../../../../assets/js/eventUtil');
import { getQuery } from "../../../../assets/js/commonData";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      'title': '绑定惊喜礼卡',
      return: 1,
      home: 1
    },
    cardNo: '',
    cardPwd: '',
    showBindBtn: true,
    loginLoad: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let isLogin = wx.getStorageSync("u-token");
    if (!isLogin) {
      this.setData({
        loginLoad: true
      })
      wx.navigateTo({
        url: `/pages/userLogin/index?type=1`,
      });
      return
    }
  },
  cardNoInput(e) {
    this.setData({
      cardNo: e.detail.value
    })
  },
  cardPwdInput(e) {
    this.setData({
      cardPwd: this.createSplitCode(e.detail.value, 4)
    })
  },
  // 处理为分割后的code
  createSplitCode(code, len) {
    let codeArr = code.replace(/\s*/g, '').split('');
    let list = []
    while (codeArr.length > len) {
      // 倒序切割
      list.push(codeArr.slice(0, len));
      codeArr = codeArr.slice(len, codeArr.length);
    }
    // 处理剩余长度
    list.push(codeArr);
    list = list.map(item => item.join(''));
    return `${list.join(' ')}`
  },
  // 提交资料
  formSubmit: throttl(async function (e) {
    const { cardNo, cardPwd, showBindBtn } = this.data;
    if (!(cardNo && cardPwd && showBindBtn)) {
      return
    }
    let values = e.detail.value;
    values.cardPwd = values.cardPwd.replace(/\s*/g, '');
    httpResource('balanceBind', values, data => {
      this.setData({ showBindBtn: false })
      wx.showToast({
        title: '绑定成功'
      })
      setTimeout(() => {
        wx.redirectTo({
          url: '/pages/user/giftCard/index'
        })
      }, 2000)
    }, "POST", null)
  }, 800),

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let isLogin = wx.getStorageSync("u-token");
    if (!isLogin && !this.data.loginLoad) {
      wx.navigateTo({
        url: `/pages/userLogin/index?type=1`,
      });
      return
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.setData({
      loginLoad: false
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.setData({
      loginLoad: false
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})