// pages/user/giftCard/bind/index.js
const app = getApp();
import { httpResource } from '../../../../assets/js/common.js';
const { throttl } = require('../../../../assets/js/eventUtil');
import { getQuery } from "../../../../assets/js/commonData";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      'title': '企业团购惊喜礼卡',
      return: 1,
      home: 1
    },
    navHeight: 0,
    planMoneyValue: '',
    moneyList: [],
    shopCode: '',
    shopName: '',
    showSelect: false,
    showTip: false,
    opcity: 0,
    titleColor: '#FFFFFF',
    loginLoad: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let isLogin = wx.getStorageSync("u-token");
    if (!isLogin) {
      this.setData({
        loginLoad: true
      })
      wx.navigateTo({
        url: `/pages/userLogin/index?type=1`,
      });
      return
    }
    this.setData({
      navHeight: app.globalData.navHeight,
    });
    this.getMoneyList();
    this.initStoreData(options)
  },
  // 初始化数据
  initStoreData(opt) {
    //获取二维码中参数
    if (opt.q) {
      let Url = decodeURIComponent(opt.q);
      let shopCode = getQuery(Url, "shopCode");
      let shopName = getQuery(Url, "shopName");
      console.log('参数', shopCode, shopName)
      if (shopCode) {
        this.setData({
          shopCode
        })
      }
      if (shopName) {
        this.setData({
          shopName
        })
      }
    }
  },
  async getMoneyList() {
    try {
      const data = await httpResource('dictList', { categoryCode: 'count_amount' });
      this.setData({ moneyList: data || [] })
    } catch (error) {
      this.setData({ moneyList: [] })
    }
  },
  openSelect() {
    this.setData({
      showSelect: true
    })
  },
  closeSelect() {
    this.setData({
      showSelect: false
    })
  },
  selectOk(e) {
    const { dictionaryValue } = e.detail.value;
    this.setData({ planMoneyValue: dictionaryValue });
    this.closeSelect();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  // 提交资料
  formSubmit: throttl(async function (e) {
    console.log(e);
    const values = e.detail.value;
    if (values.name === '') {
      wx.showToast({
        title: "请输入昵称",
        icon: "none",
      });
      return false;
    }
    const reg = /^((0\d{2,3}-\d{7,8})|(1[3456789]\d{9}))$/;
    if (!reg.test(values.phone)) {
      wx.showToast({
        title: "请输入\r\n正确的手机号或座机号",
        icon: "none",
      });
      return false;
    }
    if (values.location === '') {
      wx.showToast({
        title: "请输入城市",
        icon: "none",
      });
      return false;
    }
    if (values.company === '') {
      wx.showToast({
        title: "请输入公司/单位",
        icon: "none",
      });
      return false;
    }
    if (values.planMoney === '') {
      wx.showToast({
        title: "请选择计划购买惊喜礼卡金额",
        icon: "none",
      });
      return false;
    }
    if (values.others === '') {
      wx.showToast({
        title: "请输入是否有其他惊喜礼卡团购需求",
        icon: "none",
      });
      return false;
    }
    const { shopCode, shopName } = this.data;
    let source = shopName;
    const params = {
      ...values,
      shopCode, shopName, source
    }
    httpResource('balanceAdd', params, data => {
      this.setData({
        showTip: true
      }, () => {
        setTimeout(() => {
          this.setData({
            showTip: false
          })
          wx.redirectTo({
            url: '/pages/user/giftCard/index'
          })
        }, 5000)
      })
    }, "POST", null)
  }, 800),

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let isLogin = wx.getStorageSync("u-token");
    if (!isLogin && !this.data.loginLoad) {
      wx.navigateTo({
        url: `/pages/userLogin/index?type=1`,
      });
      return
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.setData({
      loginLoad: false
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.setData({
      loginLoad: false
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onPageScroll(e) {
    const top = e.scrollTop;
    if (e.scrollTop >= 150) {
      if (this.data.opcity < 1) {
        this.setData({
          opcity: 1,
          titleColor: '#333'
        })
      }
    } else {
      const opcity = e.scrollTop / 150;
      this.setData({
        opcity,
        titleColor: '#fff'
      })
    }
  }
})