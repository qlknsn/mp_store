// pages/user/medal/index.js
import { httpResource } from '../../../assets/js/common.js';
import { setCouponDetailInfo } from '../../../assets/js/commonData';
const { throttl } = require('../../../assets/js/eventUtil')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    opcity: 0,
    energy: '', // 当前用户能量
    showDetail: false,
    parameter: {
      title: '我的勋章'
    },
    obtainedMedalList: [],//已获得的勋章列表
    notObtainedMedalList: [],//未获得的勋章列表
    // 勋章详情
    detailInfo: {},
    // 领取勋章
    showModelList: [],
    showMedal: false,
    isLightUp: false,
    rotate: '',
    // 点亮动画结束
    rotateEnd: false,
    // 领取优惠券
    showCoupon: false,
    showCouponList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  openModel(e) {
    const { notObtainedMedalList } = this.data;
    const index = e.currentTarget.dataset.index;
    const item = notObtainedMedalList[index];
    this.setData({
      showModelList: [item],
      showMedal: true
    })
  },
  resetStatus() {
    this.getData();
    this.setData({
      rotateEnd: false,
      rotate: '',
      isLightUp: false,
      showCouponList: []
    })
  },
  // 关闭优惠券model
  closeCoupon() {
    this.setData({
      showCoupon: false,
    }, this.resetStatus)
  },
  // 关闭勋章弹框并打开优惠券弹框
  openCoupon() {
    const { showCouponList } = this.data;
    if (showCouponList.length) {
      this.setData({
        showMedal: false,
        showCoupon: true
      })
    } else {
      this.setData({
        showMedal: false
      }, ()=>{
        this.resetStatus();
      })
    }
  },
  // 关闭勋章弹框
  async closeMedal() {
    // await subscribeMessage('rL-MoUXC5VYa3mnjyOvFJvpcrUqwUpJYBbRURr7miq0');
    const { showCouponList } = this.data;
    if (showCouponList.length) {
      this.setData({
        showMedal: false,
        showCoupon: true
      }, this.getData)
    } else {
      this.setData({
        showMedal: false
      })
    }
  },
  // 点亮勋章
  lightUp: throttl(async function () {
    // await subscribeMessage('rL-MoUXC5VYa3mnjyOvFJvpcrUqwUpJYBbRURr7miq0');
    this.receiveMedal().then(res => {
      this.setData({
        rotate: 'rotate',
        isLightUp: true
      })
    })
  }),
  // 勋章动画结束
  rotateEnd() {
    this.setData({
      rotateEnd: true
    })
  },
  // 勋章详情
  openDetail(e) {
    const { obtainedMedalList } = this.data;
    const index = e.currentTarget.dataset.index;
    const item = obtainedMedalList[index] || {};
    this.setData({
      detailInfo: item,
      showDetail: true
    })
  },
  closeDetail() {
    this.setData({
      detailInfo: {},
      showDetail: false
    })
  },
  // 领取勋章
  async receiveMedal() {
    const { showModelList } = this.data;
    let memberModalIds = '';
    if (showModelList.length == 1) {
      memberModalIds = [showModelList[0].id];
    }
    if (showModelList.length > 1) {
      memberModalIds = showModelList.map(e => e.id);
    };
    let data = [];
    try {
      wx.showLoading({
        title: '加载中',
      })
      data = await httpResource('receiveMedal', { memberModalIds });
      wx.hideLoading();
    } catch {
      wx.hideLoading();
    }
    this.setModelCoupon(data);
    return data
  },
  setModelCoupon(res) {
    const showCouponList = (res || []).map(setCouponDetailInfo);
    this.setData({
      showCouponList
    })
  },
  // 优惠券详情
  goDetail(e) {
    const couponCode = e.currentTarget.dataset.code;
    wx.navigateTo({
      url: `/pages/user/coupon/detail/index?statusIndex=${0}&couponCode=${couponCode}`,
    })
  },
  // 获取勋章数据
  async getData() {
    var userInfo = wx.getStorageSync('userInfo');
    let memId = '';
    if (userInfo && userInfo.userId) {
      memId = userInfo.userId;
    }
    const data = await httpResource('findMemberMedal', { memId }, '', "get")
    const obtainedMedalList = (data.obtainedMedalList || []).filter(e => e.status == 1);
    const nList = (data.obtainedMedalList || []).filter(e => e.status == 0);
    const notObtainedMedalList = [...nList, ...(data.notObtainedMedalList || [])]
    this.setData({
      obtainedMedalList: obtainedMedalList,
      notObtainedMedalList: notObtainedMedalList,
    })
    this.getEnergy();
  },
  // 获取用户当前能量
  async getEnergy() {
    var userInfo = wx.getStorageSync('userInfo');
    let memId = '';
    if (userInfo && userInfo.userId) {
      memId = userInfo.userId;
    }
    const data = await httpResource('findMemberEnergy', { memId }, '', "get");
    this.setData({
      energy: data || 0
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      isLightUp: false,
      rotateEnd: false,
      rotate: '',
    })
    this.getData();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onPageScroll(e) {
    const top = e.scrollTop;
    if (e.scrollTop >= 150) {
      if (this.data.opcity < 1) {
        this.setData({
          opcity: 1
        })
      }
    } else {
      const opcity = e.scrollTop / 150;
      this.setData({
        opcity
      })
    }
  }
})