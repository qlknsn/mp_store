const app = getApp();
const {
  isNeedAutho,
  httpResource,
} = require('../../../assets/js/common.js');

Page({
  /**
   * 页面的初始数据
   */
  data: {
    viewHeight: app.globalData.viewHeight,
    parameter: {
      'title': '售后列表',
    },
    orderNo: null,
    orderId:  null,
    pageCount: 9999,
    pageCount2: 9999,
    orderList: [],
    orderList2: [],
    pageIndex: 1,
    pageIndex2: 1,
    pageSize: 10,
    activeIndex: 1,
  },
  changeTabs: function (e) {
    var that = this,
      index = e.currentTarget.dataset.index;
    if (that.data.activeIndex == index) {
      return false;
    }
    that.setData({
      activeIndex: index,
      orderList: [],
      pageIndex: 1,
      pageIndex2: 1,
      orderList: [],
      orderList2: [],
    })
    if (index == 1) {
      this.getOrderList(1, 1);
    } else {
      this.getOrderList2(1, 1);
    }
  },
  closeNav() {
    this.setData({
      navShow: false
    })
  },

  showModal2(e) {
    // 跳转订单详情
    wx.navigateTo({
      url: `/pages/user/refundDetail/index?orderId=${e.currentTarget.dataset.orderid}`,
    })
  },

  showModal3(e) {
    // 跳转订单详情
    wx.navigateTo({
      url: `/pages/user/refundApplyDetail/index?id=${e.currentTarget.dataset.id}`,
    })
  },

  //低部触发
  lower: function (e) {
    this.getOrderList(this.data.pageIndex, 1);
  },
  lower: function (e) {
    this.getOrderList2(this.data.pageIndex2, 1);
  },
  /**获取门店退款列表 */
  getOrderList: function (pageIndex, type) {
    var that = this;
    if ((pageIndex > that.data.pageCount) && (that.data.pageCount != 0)) {
      wx.showToast({
        title: "已加载全部数据。。。",
        icon: "none",
      });
      return false;
    }
    var filter = {
      orderNo:that.data.orderNo,
      currentPage: pageIndex,
      pageSize: that.data.pageSize,
    };
    httpResource('getRefundList', filter, (data) => {
      var orderListOld = that.data.orderList;
      var orderData = data.records || [];
      if (type == 1) {
        orderData.forEach((value) => {
          value.realAmountAbs = Math.abs(value.realAmount);
          orderListOld.push(value);
        })
        that.setData({
          pageCount: data.pages,
          orderList: orderListOld,
          pageIndex: pageIndex + 1
        })
      } else if (type == 2) {
        orderData.forEach((value) => {
          value.realAmountAbs = Math.abs(value.realAmount);
          orderListOld.push(value);
        })
        that.setData({
          orderList: orderData,
          pageIndex: 2
        })
      }
    }, "POST", null);
  },
  /**获取门店退款申请列表 */
  getOrderList2: function (pageIndex2, type) {
    var that = this;
    if ((pageIndex2 > that.data.pageCount2) && (that.data.pageCount2 != 0)) {
      wx.showToast({
        title: "已加载全部数据。。。",
        icon: "none",
      });
      return false;
    }
    var filter = {
      orderId:that.data.orderId,
      returnSource:2,
      currentPage: pageIndex2,
      pageSize: that.data.pageSize,
    };
    httpResource('refundApplyList', filter, (data) => {
      var orderListOld = that.data.orderList2;
      var orderData = data.records || [];
      if (type == 1) {
        orderData.forEach((value) => {
          value.realAmountAbs = Math.abs(value.realAmount);
          orderListOld.push(value);
        })
        that.setData({
          pageCount2: data.pages,
          orderList2: orderListOld,
          pageIndex2: pageIndex2 + 1
        })
      } else if (type == 2) {
        orderData.forEach((value) => {
          value.realAmountAbs = Math.abs(value.realAmount);
          orderListOld.push(value);
        })
        that.setData({
          orderList2: orderData,
          pageIndex2: 2
        })
      }
    }, "GET", null);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      orderNo: options.orderNo,
      orderId: options.orderId
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.loadOrderList()
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 刷新订单列表数据 
   */
  loadOrderList() {
    var that = this;
    that.setData({
      orderList: [],
      orderList2: []
    })
    isNeedAutho().then(() => {
      if (that.data.activeIndex == 1) {
        that.getOrderList(1, 1);
      } else if (that.data.activeIndex == 2) {
        that.getOrderList2(1, 1);
      }
    })
  }
})