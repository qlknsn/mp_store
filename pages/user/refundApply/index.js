const app = getApp();
import {
    httpResource,
} from '../../../assets/js/common.js';
import {
    throttl
} from '../../../assets/js/eventUtil';

const onThrottl = throttl((that, e) => {
    that.applyBtn(e);
}, 1500)

Page({
    /**
     * 页面的初始数据
     */
    data: {
        applyinfo: {},
        orderLines: {
            lineId: '',
            quantity: '',
            refundWay: 0,
            refundReasonType: '',
        },
        refundReasonTypeList: ["品质问题", "买错了", "价格高了", "不好吃/不好喝", "其他"],
        pickerIndex: null,
        parameter: {
            'title': '退货申请'
        }
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {},
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let applyinfo = wx.getStorageSync('applyinfo');
        this.setData({
            applyinfo: applyinfo,
            ['orderLines.lineId']: applyinfo.orderDetails[0].id,
            ['orderLines.quantity']: applyinfo.orderDetails[0].productNumber
        })
    },
    onHide: function () {

    },
    cartNumDes() {
        let quantity = this.data.orderLines.quantity;
        if (quantity > 1) {
            quantity--;
            this.setData({
                ['orderLines.quantity']: quantity
            })
        }
    },
    cartNumInt() {
        let quantity = this.data.orderLines.quantity;
        if (quantity < this.data.applyinfo.orderDetails[0].productNumber) {
            quantity++;
            this.setData({
                ['orderLines.quantity']: quantity
            })
        }
    },
    /**
     * 复制
     */
    copyVipCodeEvent() {
        wx.setClipboardData({
            data: this.data.applyinfo.channelOrderNo,
            success: function () {
                wx.showToast({
                    title: '复制成功',
                    duration: 2000,
                    icon: "none"
                })
            }
        })
    },
    PickerChange(e) {
        this.setData({
            pickerIndex: e.detail.value,
            ['orderLines.refundReasonType']: this.data.refundReasonTypeList[e.detail.value],
        })
    },
    goback() {
        wx.navigateBack({
            delta: 1
        })
    },
    sumbitBtn(e) {
        onThrottl(this, e);
    },
    applyBtn(e) {
        this.setData({
            ['orderLines.refundReason']: e.detail.value.refundReason,
        })
        let filter = Object.assign({
            orderId: this.data.applyinfo.orderId
        }, {
            orderLines: [this.data.orderLines]
        })
        if (filter.orderLines[0].refundReasonType == '') {
            wx.showToast({
                title: "请选择退货原因!",
                duration: 2000,
                icon: "none"
            })
            return false;
        }
        if (filter.orderLines[0].refundReasonType == '其他' && filter.orderLines[0].refundReason == '') {
            wx.showToast({
                title: "请输入退货原因!",
                duration: 2000,
                icon: "none"
            })
            return false;
        }
        httpResource('refundApplyAdd', filter, (data) => {
            wx.showToast({
                title: "申请成功!",
                duration: 2000,
                icon: "none"
            })
            setTimeout(() => {
                wx.redirectTo({
                    url: `/pages/user/refundApplyDetail/index?id=${data}`,
                })
            }, 800)
        }, 'POST')
    },
})