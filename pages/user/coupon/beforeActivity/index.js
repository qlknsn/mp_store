// pages/user/coupon/beforeActivity/index.js
const { httpResource } = require('../../../../assets/js/common.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      return: 1,
      title: "活动指引",
    },
    actList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  // 是否显示领券活动内容
  async getShowCoupon() {
    try {
      const data = await httpResource('showCoupon', {});
      if (data && data.length) {
        this.setData({ actList: data.filter(item => item.rewardChannel && item.rewardChannel.includes(0)) })
      }
    } catch (error) {

    }
  },
  // 跳转活动列表或详情
  goActInfo(e) {
    const { item } = e.currentTarget.dataset;
    wx.navigateTo({
      url: `/pages/user/coupon/activity/index?actId=${item.actId}`
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getShowCoupon();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})