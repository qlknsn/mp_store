// pages/user/coupon/detail/index.js
import { pxTorpx } from '../../../../assets/js/common.js';
import { createBarcode } from '../../../../assets/js/codeUtil';
const { httpResource } = require('../../../../assets/js/common.js');
import { setCouponDetailInfo } from '../../../../assets/js/commonData';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    urlsArr:['https://img.hotmaxx.cn/web/common/mp-store/coupon/lq.png','https://img.hotmaxx.cn/web/common/mp-store/coupon/xy.png','https://img.hotmaxx.cn/web/common/mp-store/coupon/sx.png'],
    statusIndex:0,
    couponCode:'',
    options:{},
    data:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      options
    })
  },
  getData(){
    let params = {
      currentPage: this.data.currentPage,
      couponCode: this.data.couponCode,
    }
    httpResource('couponDetail',params,(res)=>{
      const data = setCouponDetailInfo(res);
      setTimeout(()=>{
        createBarcode('barcode', data.couponCode, pxTorpx(524), pxTorpx(168));
      },300)
      console.log(data);
      this.setData({
        data
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getScreenBrightness({
      success:  (res) => {
        wx.setStorageSync("screenBrightness", res.value);
        wx.setScreenBrightness({
          value: 1
        })
      }
    })
    this.initData(this.data.options);
  },
  initData(opt){
    if (opt.statusIndex==0||opt.statusIndex) {
      this.setData({statusIndex:opt.statusIndex})
    }
    if(opt.couponCode){
      this.setData({couponCode:opt.couponCode},this.getData)
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    wx.setScreenBrightness({
      value: wx.getStorageSync("screenBrightness")
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})