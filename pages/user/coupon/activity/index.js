// pages/user/coupon/activity/index.js
const { httpResource, getQuery, isNeedAutho } = require('../../../../assets/js/common.js');
import {
  throttl
} from '../../../../assets/js/eventUtil.js'
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      'title': '领券活动',
      home: 1
    },
    actRule: '',
    couponWay: 1,
    shareTitle: '',
    actId: '',
    opcity: 0,
    navHeight: 0,
    isShowSuccessModel: false, // 是否显示领取成功
    successCouponList: [],
    popImg: '',
    showImage: false, //
    groupImgUrl: 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
    shopCode: '',
    images1: '', // 背景图片
    images2: '',
    images3: '', // 券边框
    images4: '', // 领券按钮 大 || 口令按钮
    images5: '', // 领券按钮小
    images6: '',
    images7: '',
    images8: '',
    images9: '',
    rewardDTOList: [], // 优惠券列表
    isDefault: false, // 是否显示错误,
    actStatus: 500, // 活动状态
    actStatusDesc: '活动未开始',
    password: '',
    isShowPwdBtn: true,

  },
  pwdInput(e) {
    this.setData({
      password: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      options,
      navHeight: app.globalData.navHeight,
    });
  },
  openRules() {
    wx.navigateTo({
      url: `/pages/user/coupon/activityRule/index?actRule=${encodeURIComponent(this.data.actRule)}`
    })
  },
  // 初始化数据
  initData(opt) {
    //获取二维码中参数
    if (opt.q || opt.scene) {
      let Url = decodeURIComponent(opt.q || opt.scene);
      let actId = getQuery(Url, "actId");
      if (actId) {
        this.setData({
          actId
        }, () => {
          // 获取活动配置
          this.actInfo();
        })
      }
    } else {
      if (opt.actId) {
        this.setData({
          actId: opt.actId
        }, () => {
          // 获取活动配置
          this.actInfo();
        })
      }
    }
  },
  // 获取活动配置
  async actInfo() {
    try {
      const { actId } = this.data;
      const data = await httpResource('actInfo', { actId });
      this.setData({
        isDefault: true
      })
      if (data) {
        const { actStatus,
          actStatusDesc, pageTitle, actRule, couponWay, shareTitle, pageImgList, rewardDTOList } = data;
        if (actStatus == 0) {
          if (pageImgList && pageImgList.length) {
            pageImgList.forEach((item) => {
              this.setData({
                [`images${item.pagePosition}`]: item.imgUrl
              })
            });
            const isShowPwdBtn = (rewardDTOList || []).some((val) => val.isReward == undefined || val.isReward == 0);
            this.setData({
              actStatus,
              [`parameter.title`]: pageTitle,
              actRule, couponWay, shareTitle,
              rewardDTOList: (rewardDTOList || []),
              isShowPwdBtn,
            })
          }
        } else {
          this.setData({
            actStatus,
            actStatusDesc,
            isDefault: true
          })
        }

      }

    } catch (error) {
      console.log('错误', error)
      this.setData({
        isDefault: true
      })
    }
  },
  // 普通领券
  getCoupon: throttl(async function (e) {
    const { actId } = this.data;
    const { item } = e.currentTarget.dataset;
    if (item.isReward !== 0) return;
    try {
      const { couponTemplateId } = item;
      const templateIds = couponTemplateId ? [{ tempId: couponTemplateId, canRewardNum: 1 }] : [];
      const data = await httpResource('receiveCoupon', {
        actId,
        templateIds,
      });
      if (data && data.couponInfoResps && data.couponInfoResps.length) {
        this.setData({
          successCouponList: data.couponInfoResps,
          // popImg: data.popImg,
          isShowSuccessModel: true,
        })
      }
      console.log("领取结果", data);
    } catch (error) {
      console.log(error);
    }
  }),
  // 口令式领券
  getPwdCoupon: throttl(async function () {
    const { rewardDTOList, actId, password, isShowPwdBtn } = this.data;
    if (!isShowPwdBtn) return;
    if (!password) {
      wx.showToast({
        icon: 'none',
        title: '请输入口令'
      })
      return;
    }
    try {
      const templateIds = rewardDTOList.map(
        (e) => ({ tempId: e.couponTemplateId, canRewardNum: e.canRewardNum })
      );
      const data = await httpResource('receiveCoupon', {
        actId,
        pwd: password,
        templateIds,
      });
      if (data && data.couponInfoResps && data.couponInfoResps.length) {
        this.setData({
          successCouponList: data.couponInfoResps,
          // popImg: data.popImg,
          isShowSuccessModel: true,
        })
      }
      console.log("领取结果", data);
    } catch (error) {
      console.log('领券失败', error);
    }
  }),
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    isNeedAutho().then(() => {
      // 查询活动
      this.initData(this.data.options);
      let currentStore = wx.getStorageSync('current-store');
      if (currentStore) {
        this.setData({
          shopCode: currentStore.shopCode
        })
        this.getShopQrCode(currentStore.shopCode);
      }
    }).catch(() => {
      wx.navigateTo({
        url: `/pages/userLogin/index?type=1`,
      });
    })

  },
  /**
   * 获取社群二维码
   */
  async getShopQrCode(shopCode) {
    const _this = this;
    let data = await httpResource('getShopCommunityCode', {
      shopCode
    }, null, 'GET');
    _this.setData({
      groupImgUrl: data || 'https://img.hotmaxx.cn/web/common/mp-store/community/mp_store.jpg',
    })
  },
  openGroup() {
    this.setData({
      showImage: true
    })
  },

  closeGroup() {
    this.setData({
      showImage: false
    })
  },
  openSuccess() {
    this.setData({
      isShowSuccessModel: true
    })
  },
  closeSuccess() {
    this.setData({
      successCouponList: [],
      isShowSuccessModel: false
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  onPageScroll(e) {
    const top = e.scrollTop;
    if (e.scrollTop >= 150) {
      if (this.data.opcity < 1) {
        this.setData({
          opcity: 1
        })
      }
    } else {
      const opcity = e.scrollTop / 150;
      this.setData({
        opcity
      })
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const { shareTitle, images9, actId } = this.data;
    return {
      title: shareTitle,
      path: `/pages/user/coupon/activity/index?actId=${actId}`,
      imageUrl: images9
    }
  }
})