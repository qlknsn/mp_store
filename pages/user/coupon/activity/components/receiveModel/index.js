// pages/user/coupon/activity/components/receiveModel/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    show: {
      type: 'boolean',
      value: false
    },
    data: {
      type: 'array',
      value: []
    },
    popImg: {
      type: 'string',
      value: ''
    },
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    goCoupon() {
      // 关闭
      this.triggerEvent('close');
      wx.navigateTo({
        url: '/pages/user/coupon/index'
      })
    },
    close() {
      // 关闭
      this.triggerEvent('close');
    },
    move() {
      return
    }
  }
})
