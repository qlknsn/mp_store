// pages/coupon/index.js
import {
  setCouponDetailInfo
} from '../../../assets/js/commonData';
const {
  httpResource
} = require('../../../assets/js/common.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    showNo: false,
    navArr: ['平台券', '第三方券'],
    statusArr: ['未使用', '已使用', '已失效'],
    btnArr: ['立即使用', '已使用', '已失效'],
    statusCodeArr: [1, 2, 3],
    urlsArr: ['https://img.hotmaxx.cn/web/common/mp-store/coupon/lq.png', 'https://img.hotmaxx.cn/web/common/mp-store/coupon/xy.png', 'https://img.hotmaxx.cn/web/common/mp-store/coupon/sx.png'],
    navIndex: 0,
    statusIndex: 0,
    list: [], // 平台券
    list2: [], // 第三方券
    currentPage: 1, // 平台券第几页
    currentPage2: 1, // 第三方券第几页
    isDefault: false,
    actList: [], // 活动列表
  },
  init() {
    this.setData({
      isDefault: false
    }, () => {
      if (this.data.navIndex === 0) {
        this.getList()
      } else {
        this.getList2()
      }
    })
  },
  getList() {
    let params = {
      currentPage: this.data.currentPage,
      pageSize: 20,
      couponType: 1,
      status: this.data.statusCodeArr[this.data.statusIndex]
    }
    httpResource('couponList', params, (data) => {
      let {
        records,
        currentPage,
        total
      } = data;

      if (records) {
        records = records.map(setCouponDetailInfo);
        if (currentPage == 1) {
          this.setData({
            list: records,
            isDefault: records.length ? false : true
          })
          if (records.length < 20) {
            this.setData({
              showNo: true
            })
          }
        } else if (this.data.list.length < total) {
          const list = [...this.data.list, ...records];
          this.setData({
            list,
            isDefault: list.length ? false : true
          })
        } else {
          this.setData({
            showNo: true
          })
        }
      }
    })
  },
  getList2() {
    let params = {
      currentPage: this.data.currentPage2,
      discountType: 12,
      pageSize: 20,
    }
    httpResource('thirdPartyCouponList', params, (data) => {
      let {
        records,
        currentPage,
        total
      } = data;
      if (records) {
        records = records.map(setCouponDetailInfo);
        if (currentPage == 1) {
          this.setData({
            list2: records,
            isDefault: records.length ? false : true
          })
        } else if (this.data.list2.length < total) {
          const list2 = [...this.data.list2, ...records];
          this.setData({
            list2,
            isDefault: list2.length ? false : true
          })
        }
      }
    })
  },
  tabSelect: function (e) {
    let index = e.target.dataset.index;
    let navIndex = this.data.navIndex;
    if (index === navIndex) return;
    this.setData({
      navIndex: index,
      currentPage: 1,
      currentPage2: 1,
      list: [],
      list2: [],
    }, this.init)
  },
  statusSelect: function (e) {
    let index = e.target.dataset.index;
    let statusIndex = this.data.statusIndex;
    if (index === statusIndex) return;
    this.setData({
      statusIndex: index,
      currentPage: 1,
      list: []
    }, this.init)
  },
  // tabSwiper(e){
  //   const index = e.detail.current;
  //   this.setData({
  //     navIndex:index
  //   })
  // },

  // 判断当前时间是否在某个时间段内
  isDuringDate(start, end) {
    let curDate = new Date();
    console.log(start.replace(/\./g, "/"), end.replace(/\./g, "/"));
    let startDate = new Date(start.replace(/\./g, "/"));
    let endDate = new Date(end.replace(/\./g, "/"));
    console.log(curDate, startDate, endDate);
    if (curDate >= startDate && curDate <= endDate) {
      return false
    } else {
      return true
    }

  },
  goDetail(e) {
    const couponCode = e.currentTarget.dataset.code;
    let start = `${e.currentTarget.dataset.start} 00:00:00`
    let end = `${e.currentTarget.dataset.end} 23:59:59`
    console.log(start, end);
    const {
      statusIndex
    } = this.data;
    if (statusIndex !== 0) {
      return;
    }
    console.log(this.isDuringDate(start, end));
    if (!this.isDuringDate(start, end)) {
      wx.navigateTo({
        url: `/pages/user/coupon/detail/index?statusIndex=${this.data.statusIndex}&couponCode=${couponCode}`,
      })
    } else {
      wx.showToast({
        title: '该优惠券未到生效时间，不可以使用哦',
        icon: 'none',
        mask: true,
      });
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      list: [], // 平台券
      list2: [], // 第三方券
      currentPage: 1, // 平台券第几页
      currentPage2: 1, // 第三方券第几页
    }, this.init);
    this.getShowCoupon();
  },
  // 是否显示领券活动内容
  async getShowCoupon() {
    try {
      const data = await httpResource('showCoupon', {});
      if (data && data.length) {
        this.setData({
          actList: data.filter(item => item.rewardChannel && item.rewardChannel.includes(0))
        })
      }
    } catch (error) {

    }
  },
  // 跳转活动列表或详情
  goActInfo() {
    const {
      actList
    } = this.data;
    if (actList.length === 0) return;
    if (actList.length === 1) {
      wx.navigateTo({
        url: `/pages/user/coupon/activity/index?actId=${actList[0].actId}`
      })
    } else {
      wx.navigateTo({
        url: '/pages/user/coupon/beforeActivity/index'
      })
    }

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.navIndex === 0) {
      this.data.currentPage++
      this.getList()
    } else {
      this.data.currentPage2++
      this.getList2()
    }

  },
})