const app = getApp();
const {
  isNeedAutho,
  httpResource,
} = require('../../../assets/js/common.js');
import {
    throttl
} from '../../../assets/js/eventUtil';

const cancelRefund = throttl((that, e) => {
    that.cancelRefundFun(e);
}, 1500)
Page({
  /**
   * 页面的初始数据
   */
  data: {
    viewHeight: app.globalData.viewHeight,
    parameter: {
      'title': '退款单列表',
    },
    orderNo: null,
    orderId:  null,
    pageCount2: 9999,
    orderList2: [],
    pageIndex2: 1,
    pageSize: 10,
    activeIndex: 2,
  },

  closeNav() {
    this.setData({
      navShow: false
    })
  },

  showModal2(e) {
    // 跳转订单详情
    wx.navigateTo({
      url: `/pages/user/refundDetail/index?orderId=${e.currentTarget.dataset.orderid}`,
    })
  },

  showModal3(e) {
    // 跳转订单详情
    wx.navigateTo({
      url: `/pages/user/refundApplyDetail/index?id=${e.currentTarget.dataset.id}`,
    })
  },

  //低部触发
  lower: function (e) {
    this.getOrderList2(this.data.pageIndex2, 1);
  },
  /**获取门店退款列表 */
  getOrderList: function (pageIndex, type) {
    var that = this;
    if ((pageIndex > that.data.pageCount) && (that.data.pageCount != 0)) {
      wx.showToast({
        title: "已加载全部数据。。。",
        icon: "none",
      });
      return false;
    }
    var filter = {
      orderNo:that.data.orderNo,
      currentPage: pageIndex,
      pageSize: that.data.pageSize,
    };
    httpResource('getRefundList', filter, (data) => {
      var orderListOld = that.data.orderList;
      var orderData = data.records || [];
	  orderData.forEach((value) => {
	    value.realAmountAbs = Math.abs(value.realAmount);
	    orderListOld.push(value);
	  })
	  that.setData({
		orderList: orderData,
		pageIndex: 2
	  })
    }, "POST", null);
  },
  /**获取门店退款申请列表 */
  getOrderList2: function (pageIndex2, type) {
    var that = this;
    if ((pageIndex2 > that.data.pageCount2) && (that.data.pageCount2 != 0)) {
      wx.showToast({
        title: "已加载全部数据。。。",
        icon: "none",
      });
      return false;
    }
    var filter = {
      orderId:that.data.orderId,
      returnSource:2,
      currentPage: pageIndex2,
      pageSize: that.data.pageSize,
    };
    httpResource('refundApplyList', filter, (data) => {
      var orderListOld = that.data.orderList2;
      var orderData = data.records || [];
      orderData.forEach((value) => {
        value.realAmountAbs = Math.abs(value.realAmount);
        orderListOld.push(value);
      })
      that.setData({
        orderList2: orderData,
        pageIndex2: 2
      })
    }, "GET", null);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      orderNo: options.orderNo,
      orderId: options.orderId,
	  parameter: {
	    'title': '退款单列表',
		'return': options.return
	  }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.loadOrderList()
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 刷新订单列表数据 
   */
  loadOrderList() {
    var that = this;
    that.setData({
      orderList2: []
    })
    isNeedAutho().then(() => {
       that.getOrderList2(1, 1);
    })
  },
  
  // 取消退款
  submitCancel(e) {
      cancelRefund(this, e);
  },
  cancelRefundFun(e) {
	  let that = this;
	  wx.showModal({
	    title: '提示',
	    content: '请确认是否取消退款',
	    success (res) {
	      if (res.confirm) {
	        let filter = {
	          id: e.currentTarget.dataset.orderinfo.id,
	        };
	        httpResource('cancelRefund', '', (data) => {
				  wx.showToast({
					  title: "申请成功!",
					  duration: 2000,
					  icon: "none"
				  })
				 that.loadOrderList()
	        }, "POST", (res) => {
	              // 请求失败，报错提示
	              wx.showToast({
	                  title: res.msg,
	                  icon: 'none'
	              })
	          }, `/${filter.id}`);
	      } else if (res.cancel) {
	        console.log('用户点击取消')
	      }
	    }
	  })
  }
})