const app = getApp();
import { checkLocation } from "../../../../assets/js/commonData";
const {
	isNeedAutho,
	httpResource,
	getCode,
} = require('../../../../assets/js/common.js');
import {
	throttl
} from '../../../../assets/js/eventUtil.js'
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		viewHeight: app.globalData.viewHeight - 50,
		parameter: {
			'title': '添加地址',
			return: 1
		},
		labelList: [
			{
				name: '家',
				value: 1,
			},
			{
				name: '公司',
				value: 2,
			},
			{
				name: '学校',
				value: 3,
			},
			{
				name: '朋友',
				value: 4,
			},
		],
		items: [
			{
				name: '先生',
				value: 1,
			},
			{
				name: '女士',
				value: 2
			}
		],

		addrId: null, // addressId
		addressInfo: null,
		contactName: null, // 联系人
		gender: 1, // 性别
		contactPhone: null, // 联系电话
		isDefault: true, // 是否默认
		addrDetail: null, // 详细地址	
		addrName: null, // 地址名称
		room: null, // 门牌号
		label: null, // 标签
		longitude: null, // 经度
		latitude: null, // 纬度,
		checkLocation: false
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		if (options && options.addressId) {
			this.setData({
				addrId: options.addressId,
				options
			})
			this.getAddrDetail(options.addressId)
		}
		checkLocation().then((isTrue) => {
			if (isTrue) {
				this.setData({
					checkLocation: true
				})
			} else {
				this.setData({
					checkLocation: false
				})
			}
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {
		this.toast = this.selectComponent("#toast");
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},
	initData(opt) {

	},
	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {
		this.setData({
			showLogin: false,
			showPhone: false,
		})
	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},
	/**
	 * @param {Object} e
	 * 性别
	 */
	radioChange(e) {
		this.setData({
			gender: e.detail.value
		})
	},
	phoneChange(e) {
	},
	/**
	 * 选择送货地址
	 */
	chooseAddHandle() {
		const that = this;
		const { checkLocation } = this.data;
		if (!checkLocation) {
			wx.showModal({
				title: '授权位置',
				content: '您尚未授权位置信息，无法获取附近位置',
				confirmText: '去开启',
				success(res) {
					if (res.confirm) {
						that.setData({
							checkLocation: true
						})
						wx.openSetting();
					} else if (res.cancel) {
						console.log('用户点击取消')
					}
				}
			})
			return;
		};
		wx.chooseLocation({
			success: (res) => {
				that.setData({
					addressInfo: res,
					addrDetail: res.address,
					addrName: res.name,
					longitude: res.longitude,
					latitude: res.latitude
				})
			}
		})
	},
	/**
	 * @param {Object} e
	 * 选择标签
	 */
	chooseLableHandle(e) {
		const { label } = this.data;
		this.setData({
			label: e.currentTarget.dataset.key == label ? null : e.currentTarget.dataset.key
		})
	},
	onChange({ detail }) {
		this.setData({
			isDefault: detail.value
		})
	},
	/**
	 * 保存地址
	 */
	saveHandle: throttl(async function (e) {
		const { addrId } = this.data;
		const params = this.requestParams();
		if (!params.contactName) {
			wx.showToast({
				title: '请输入联系人姓名',
				duration: 2000,
				icon: "none"
			})
			return;
		}
		if (params.contactName.length > 20) {
			wx.showToast({
				title: '联系人20字符以内',
				duration: 2000,
				icon: "none"
			})
			return;
		}
		const phoneReg = /^1\d{10}$/;
		if (!phoneReg.test(params.contactPhone)) {
			wx.showToast({
				title: '请输入正确手机号',
				duration: 2000,
				icon: "none"
			})
			return;
		}
		if (!params.longitude || !params.latitude || !params.addrName) {
			wx.showToast({
				title: '请选择收货地址',
				duration: 2000,
				icon: "none"
			})
			return;
		}
		if (!params.room) {
			wx.showToast({
				title: '请输入门牌号',
				duration: 2000,
				icon: "none"
			})
			return;
		}
		if (params.room.length > 20) {
			wx.showToast({
				title: '门牌号20字符以内',
				duration: 2000,
				icon: "none"
			})
			return;
		}
		if (addrId) {
			params['addrId'] = addrId
			const res = await httpResource('updateAddr', params, null, 'POST');
			if (addrId == app.globalData.deliverableAddress.addrId) {
				wx.setStorageSync('deliverable-address', params)
				app.globalData.deliverableAddress = params
			}
		} else {
			const res = await httpResource('addAddress', params, null, 'POST');
		}

		wx.showToast({
			title: '保存成功',
			duration: 2000,
			icon: "none"
		})
		wx.navigateBack({})
	}),
	/**
	 * 保存地址入参
	 */
	requestParams() {
		const {
			contactName,
			gender,
			contactPhone,
			isDefault,
			label,
			addrDetail,
			addrName,
			room,
			longitude,
			latitude
		} = this.data;
		return {
			contactName,
			gender,
			contactPhone,
			isDefault,
			label,
			addrDetail,
			addrName,
			room,
			longitude,
			latitude
		}
	},
	// 获取地址详情详情
	async getAddrDetail(addrId) {
		const res = await httpResource('addrDetail', { addrId }, null, 'GET');
		this.setData({
			contactName: res.contactName || '',
			addrName: res.addrName || '',
			addrDetail: res.addrDetail || '',
			contactPhone: res.contactPhone || '',
			gender: res.gender || '',
			isDefault: res.isDefault || '',
			label: res.label || '',
			room: res.room || '',
			longitude: res.longitude || '',
			latitude: res.latitude || '',
		})
	}
})