const app = getApp();
const {
	isNeedAutho,
	httpResource,
	getCode,
} = require('../../../../assets/js/common.js');

Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		viewHeight: app.globalData.viewHeight - 50,
		parameter: {
			'title': '地址管理',
			return: 1
		},
		addList: [],
		addListOff: [],
		handleVisible: false,
		shopCode: '', // 门店码
		checkDistance: false, // 是否校验距离
		type: 1, // 1：个人中心 、 2：首页
		addressId: '',
		isLogin: app.globalData.isLogin
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		// 首页
		if (options && options.type == 2) {
			this.setData({
				type: 2,
				addressId: app.globalData.deliverableAddress.addrId || ''
			})
		}
		if (!options || !options.type) {
			this.setData({
				type: null,
				addressId: app.globalData.deliverableAddress.addrId || ''
			})
		} else {
			this.setData({
				type: options.type
			})
		}
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {
		this.toast = this.selectComponent("#toast");
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.setData({
			isLogin: app.globalData.isLogin
		})
		if (!app.globalData.isLogin) return;
		const that = this;
		if (app.globalData.currentStore && app.globalData.currentStore.shopCode) {
			this.setData({
				shopCode: app.globalData.currentStore.shopCode,
			}, () => {
				that.getAddressList(true)
			})
		} else {
			that.getAddressList(true)
		}
	},
	initData(opt) {

	},
	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {
		this.setData({
			showLogin: false,
			showPhone: false,
		})
	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},
	/**
	 * @param {Object} e
	 * 获取地址列表
	 */
	async getAddressList(flag) {
		const { shopCode, checkDistance, type } = this.data;
		let params;
		let addList;
		if (type != 1 && type != 2) {
			const params = {
				shopCode,
				checkDistance: shopCode ? true : false
			};
			console.log('params', params)
			addList = await httpResource('addressList', params, null, "POST");
			this.setData({
				addList
			})
		} else {
			addList = await httpResource('addressList', params, null, "POST");
			this.setData({
				addList
			})
		}
		/**
		 * 默认地址 && 是否在配送范围内
		 */
		const addListOff = []; // 不在配送范围地址
		addList.map((item) => {
			item.isDefault == true ? (() => {
				console.log('2233', item.isDefault, item.addrId, app.globalData.deliverableAddress.addrId)
				if (!app.globalData.deliverableAddress || item.addrId == app.globalData.deliverableAddress.addrId) {
					wx.setStorageSync('deliverable-address', item)
					app.globalData.deliverableAddress = item
				}

			})() : '';
			// 不在配送范围地址
			item.outOfRange ? (() => { addListOff.push(item) })() : '';
		})
		this.setData({
			addListOff
		})
	},
	/**
	 * @param {Object} e
	 * 选择地址
	 */
	chooseHandle(e) {
		const { type } = this.data;
		if (type != 1) {
			console.log('999')
			this.setData({
				addressId: e.currentTarget.dataset.info.addrId
			})
			wx.setStorageSync('deliverable-address', e.currentTarget.dataset.info)
			app.globalData.deliverableAddress = e.currentTarget.dataset.info
			wx.navigateBack({
				delta: 1
			})
		}
	},
	/**
	 * 编辑
	 */
	editHandle(e) {
		wx.navigateTo({
			url: `/pages/user/address/add/index?addressId=${e.currentTarget.dataset.info}`,
		})
	},
	/**
	 * @param {Object} e
	 * 设置为默认
	 */
	async setDefaultHandle(e) {
		const params = {
			addrId: e.currentTarget.dataset.addrid
		};
		await httpResource('setAddrDefault', params, null, "POST");
		wx.showToast({
			title: '操作成功',
			duration: 2000,
			icon: "none"
		})
		this.getAddressList()
		// wx.setStorageSync('deliverable-address', e.currentTarget.dataset.info)
		// app.globalData.deliverableAddress = e.currentTarget.dataset.info

	},
	/**
	 * 删除
	 */
	async deleteHandle(e) {
		const params = {
			addrId: e.currentTarget.dataset.addrid
		};
		await httpResource('addDeleteV2', params, null, "POST");
		wx.showToast({
			title: '删除成功',
			duration: 2000,
			icon: "none"
		})
		if (app.globalData.deliverableAddress && e.currentTarget.dataset.addrid == app.globalData.deliverableAddress.addrId) {
			app.globalData.deliverableAddress = {}
			wx.setStorageSync('deliverable-address', {})
		}
		this.getAddressList()

	},
	/**
	 * 管理操作事件
	 */
	handle() {
		const { handleVisible } = this.data;
		this.setData({
			handleVisible: !handleVisible
		})
	},
	/**
	 * 添加地址
	 */
	addHandle() {
		wx.navigateTo({
			url: `/pages/user/address/add/index`,
		})
	},
	/**
	 * 未在配送范围内地址
	 */
	tipHandle(e) {
		wx.showModal({
			title: '很抱歉',
			content: '无法配送到当前地址，请选择其他地址哦',
			showCancel: false,
			success(res) {
				if (res.confirm) {
					console.log('confirm')
				} else if (res.cancel) {
					console.log('cancel')
				}
			}
		})
	},
	// 去登录
	goLogin() {
		wx.navigateTo({
			url: "/pages/userLogin/index?type=1",
		});
	},
})