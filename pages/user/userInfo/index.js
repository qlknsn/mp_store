const { regions } = require('../../../assets/js/city');
import { httpResource, getNowFormatDate } from '../../../assets/js/common.js';
const { throttl } = require('../../../assets/js/eventUtil');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    canIUseGetUserProfile: false,
    endDate: getNowFormatDate(),
    genderList: ['男', '女'],
    genderCodeList: [1, 2],
    genderCode: 1,
    gender: '请选择性别',
    region: [],
    birthdayEdit: 0,
    birthday: '请选择生日',
    likesList: [
      {
        type: 'like_snacks',
        label: '宝藏零食',
        active: false,
      },
      {
        type: 'like_drink',
        label: '饮品自由',
        active: false,
      },
      {
        type: 'like_department',
        label: '家具百货',
        active: false,
      },
      {
        type: 'like_beauty',
        label: '个护美妆',
        active: false,
      },
    ],
    cardList: [
      {
        label: '学生',
        active: false,
      },
      {
        label: '宝妈',
        active: false,
      },
      {
        label: '上班族',
        active: false,
      },
      {
        label: '自由工作者',
        active: false,
      },
    ],
    multiArray: [regions, regions[0].children],
    showBtn: true,
    id: null,
    headUrl: '',
    nickName: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  onShow() {
    // 检查是否可用新获取用户信息
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
    const userInfo = wx.getStorageSync("userInfo") || {};
    this.setData({
      userInfo
    }, this.initData(userInfo))
  },
  // 编辑赋值
  initData(users) {
    const { genderCodeList, genderList, likesList, cardList } = this.data;
    const { city, gender, like, birthday, province, userInfoId, headUrl, nickName, jobInfo, birthdayEdit } = users;
    this.setData({
      headUrl,
      nickName,
      birthdayEdit: birthdayEdit || 0,
      id: userInfoId
    })
    if (city && province) {
      this.setData({
        region: this.getRegion(province, city)
      })
    }
    if (gender) {
      const genderIndex = genderCodeList.findIndex(e => e == gender);
      this.setData({
        genderCode: gender,
        gender: genderList[genderIndex] || '请选择性别'
      })
    }
    if (like) {
      const arr = like.split(',');
      const list = likesList.map(e => { if (arr.includes(e.label)) { e.active = true }; return e });
      this.setData({
        likesList: list
      })
    }
    if (jobInfo) {
      const arr = jobInfo.split(',');
      const list = cardList.map(e => { if (arr.includes(e.label)) { e.active = true }; return e });
      this.setData({
        cardList: list
      })
    }
    if (birthday) {
      this.setData({
        birthday
      })
    }
  },
  // 获取省市
  getRegion(province, city) {
    let arr = [];
    const provinceIndex = regions.findIndex(e => e.label == province);
    if (provinceIndex == 0 || provinceIndex) {
      const cityList = regions[provinceIndex].children;
      const cityIndex = cityList.findIndex(e => e.label == city);
      arr = [provinceIndex, cityIndex];
      this.setData({
        multiArray: [regions, cityList]
      })
    }
    return arr;
  },
  bindMultiPickerChange: function (e) {
    this.setData({
      region: e.detail.value
    })
  },
  // 城市修改
  bindMultiPickerColumnChange: function (e) {
    var data = {
      multiArray: this.data.multiArray,
      region: this.data.region
    };
    data.region[e.detail.column] = e.detail.value;
    if (e.detail.column == 0) {
      data.multiArray[1] = regions[e.detail.value].children || [];
      data.region[1] = 0;
    }
    this.setData(data);
  },
  selectItem(e) {
    const index = e.currentTarget.dataset.index;
    const { likesList } = this.data;
    if (!likesList[index].active) {
      wx.uma.trackEvent(likesList[index].type)
    }
    this.setData({
      [`likesList[${index}].active`]: (!likesList[index].active)
    })
  },
  selectItem2(e) {
    const index = e.currentTarget.dataset.index;
    const { cardList } = this.data;
    this.setData({
      [`cardList[${index}].active`]: (!cardList[index].active)
    })
  },
  // 性别修改
  genderChange(e) {
    const { genderList, genderCodeList } = this.data;
    this.setData({
      gender: genderList[e.detail.value],
      genderCode: genderCodeList[e.detail.value]
    })
  },
  // 日期修改
  bindDateChange: function (e) {
    this.setData({
      birthday: e.detail.value
    })
  },
  // 提交资料成功返回
  back(data, userInfo) {
    wx.setStorageSync("userInfo", {
      ...userInfo,
      ...data
    });
    wx.showToast({
      title: '保存成功',
      icon: 'none',
    });
    setTimeout(() => {
      wx.navigateBack()
    }, 1500)
  },
  // 提交资料
  formSubmit: throttl(async function (e) {
    const { gender, genderCode, cardList, region, likesList, birthday, multiArray, userInfo, headUrl, nickName } = this.data;
    if (gender === '请选择性别') {
      wx.showToast({
        title: "请选择性别",
        icon: "none",
      });
      return false;
    }
    if (birthday === '请选择生日') {
      wx.showToast({
        title: "请选择生日",
        icon: "none",
      });
      return false;
    }
    if (region[0] != 0 && !region[0]) {
      wx.showToast({
        title: "请选择城市",
        icon: "none",
      });
      return false;
    }
    const likes = likesList.filter(e => e.active);
    if (!likes.length) {
      wx.showToast({
        title: "请选择喜好",
        icon: "none",
      });
      return false;
    }
    if (!headUrl || !nickName) {
      wx.showToast({
        title: `请同步${!headUrl ? '头像' : '昵称'}`,
        icon: "none",
      });
      return false
    }
    const jobInfos = cardList.filter(e => e.active);
    if (!jobInfos.length) {
      wx.showToast({
        title: "请选择身份",
        icon: "none",
      });
      return false;
    }

    const userId = userInfo.userId;
    const params = {
      userId,
      headUrl,
      nickName,
      gender: genderCode,
      birthday,
      province: multiArray[0][region[0]].label,
      city: multiArray[1][region[1]].label,
      like: likes.map(e => e.label).join(','),
      jobInfo: jobInfos.map(e => e.label).join(','),
    }
    if (this.data.id) {
      params.id = this.data.id;
    }
    httpResource('saveMemberExtendInfo', params, data => {
      this.setData({
        showBtn: false
      }, () => this.back(data, userInfo))
    }, "POST", null)
  }),
  // 登录相关
  getUserInfo() {
    wx.getUserProfile({
      desc: '用于完善会员资料',
      success: (res) => {
        const { avatarUrl: headUrl, nickName } = res.userInfo;
        this.setData({
          headUrl, nickName,
        })
      }
    })
  },
  // 兼容版本登录
  oldGetUserInfo(e) {
    if (e.detail.userInfo) {
      const { avatarUrl: headUrl, nickName } = e.detail.userInfo;
      this.setData({
        headUrl, nickName,
      })
    } else {
      wx.showToast({
        title: "请同意微信授权！",
        icon: "none",
        duration: 2000
      });
    }
  }
});
