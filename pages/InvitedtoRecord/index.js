const { httpResource } = require('../../assets/js/common.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    actId: null, //活动ID
    orderNum: 0, //下单总数量
    registerNum: 0, //注册总数量
    loginNum: 0, //登录总数量
    isRspl: false,//是否显示弹框
    currentPage: 1, //第一页
    pageSize: 10, //每页数据条数
    loading: false,
    noMore: false,
    studentDatas: [], //数组
    scrollViewHeight: 0, //获取的高度
    isUpperPulled: false, //是否已被上拉
    isDefault: false,//是否展示缺省图 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      actId: options.actId
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getMyInviteRecord();
    this.getMyInviteList();
  },

  //是否显示弹框
  tapRspl() {
    let _this = this;
    wx.uma.trackEvent("click_order_prompt", { 'actId': this.data.actId });
    _this.data.isRspl = !_this.data.isRspl;
    _this.setData({
      isRspl: _this.data.isRspl
    });
  },

  //我的邀请-总数量
  async getMyInviteRecord() {
    let _this = this;
    let { actId } = _this.data;
    let { userId } = wx.getStorageSync("userInfo");
    let data = await httpResource('getMyInviteRecordTotal', { actId, userId }, null, "post");
    if (data) {
      _this.setData({
        orderNum: data.orderNum,
        registerNum: data.registerNum,
        loginNum: data.loginNum,
      });
    }
  },
  //我的邀请-邀请明细
  async getMyInviteList() {
    //1474570782122713088
    let _this = this;
    let { actId, currentPage, pageSize } = _this.data;
    let { userId } = wx.getStorageSync("userInfo");
    let data = await httpResource('getMyInvite', { actId, userId, currentPage, pageSize }, null, "post");
    let dataItem = data.records || [];
    if (dataItem && dataItem.length != 0) {
      if (_this.data.studentDatas.length < data.total) {
        _this.setData({
          loading: false,
          noMore: false,
          isDefault: false,
          studentDatas: _this.data.studentDatas.concat(dataItem),
        })
      }
      if (_this.data.studentDatas.length == data.total) {
        _this.setData({
          noMore: true,
          loading: true,
        })
      }
    } else {
      _this.setData({
        noMore: true,
        loading: true,
        isDefault: true,
      })
    }
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    let _this = this;
    if (!_this.data.loading && !_this.data.noMore) {
      _this.setData({
        loading: true,
        noMore: true,
        currentPage: _this.data.currentPage + 1,
      }, () => {
        _this.getMyInviteList();
      })
    }
  }
})