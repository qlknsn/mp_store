const { httpResource } = require('../../assets/js/common.js');
import { getProductDetail } from "../../assets/js/commonData";
import Dialog from '@vant/weapp/dialog/dialog'
import { subscribeMessage, getSubState } from "../../config/subConfig";
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    addrId: app.globalData.deliverableAddress.addrId,//地址栏ID
    isDefault: false, //是否展示缺省图
    goodsList: [],//商品数据
    cartId: null,//购物车id 
    isOverallSelect: false,//全局管理
    isSelectAll: false,//全选状态
    selectType: 2,//选择type 2 正常多选 3 全选 
    selectedItemIds: [],//选中的购物车条目ids selectType = 2 时必传
    canSettle: false,//是否可以打包结账
    cannotSettleType: null,//不能打包结算原因 1 未达到启动金额 2 不在配送时间 4 两笔订单未支付
    minimumAmount: 0,//起送金额
    shortAmount: 0,//差多少到起送金额 cannotSettleType = 1 时有值
    deliveryFee: 0,//配送费
    packingFee: 0,//打包费
    totalItemAmount: 0,//商品合计总金额
    totalAmount: 0,//合计金额 (包括商品总金额+打包费+配送费)
    cartItemIds: [],//批量删除的 购物车条目 ids
    isDeleteAll: false,//全局删除状态
    removeNumber: 0,//删除商品数量
    templateList: [], // 模板列表
    titleWidthData: [],//获取商品标题宽度
    receiveType: null,//3自提，4配送到家
    selectTaber: null,//自提，外卖状态
  },

  // 自定义tabbar
  getTabbar() {
    if (typeof this.getTabBar === "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 2,
      });
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let shopCode = app.globalData.currentStore.shopCode;
    let SELECT_TABER = wx.getStorageSync('SELECT_TABER');
    this.getTemplateList();
    this.getTabbar();
    this.getCartList();
    this.setData({
      selectTaber: SELECT_TABER ? SELECT_TABER : app.globalData.selectTaber,
    }, () => {
      if (app.globalData.isLogin) {
        //获取商品总数
        let res = httpResource('cartCount', { shopCode });
        res.then((num) => {
          app.globalData.cartNum = num;
        });
      }
    })
  },

  onHide: function () {
    Dialog.close();
    // this.setData({

    // })
  },

  //查询购物车列表
  async getCartList() {
    let _this = this;
    _this.data.selectedItemIds = [];
    let shopCode = app.globalData.currentStore.shopCode;
    let parasm = JSON.parse(JSON.stringify({ shopCode, version: 2 }));
    try {
      if (!app.globalData.isLogin) {
        this.setData({
          isDefault: true,
        })
        return;
      }
      let data = await httpResource('cartList', parasm, null, "post");
      let { cartId } = data;
      let items = (data.items || []).map(getProductDetail);
      if (data) {
        if (items && items.length != 0) {
          items.forEach((item) => {
            if (item.status == 0) {
              item.isSelect = true;
              _this.data.selectedItemIds.push(item.cartItemId);
              _this.data.isSelectAll = true;
            }
            item.isPurchaseLimit = false;
            item.currentPriceArr = item.salePrice ? item.salePrice.toString().split('.') : ['0', '00'];
          })
          _this.setData({
            goodsList: items,
            isSelectAll: _this.data.isSelectAll,
            cartId: cartId,
            isDefault: false,
            isOverallSelect: false,
          }, () => {
            _this.getTitleWidth();
            _this.getCartCalculate();
          });
        } else {
          _this.setData({
            goodsList: [],
            isSelectAll: false,
            isDefault: true,
          });
        }
      }
    } catch (error) {
      _this.setData({
        goodsList: [],
        isSelectAll: false,
        isDefault: true,
      })
    }
  },

  //获取商品标题宽度
  getTitleWidth() {
    let _this = this;
    wx.createSelectorQuery().selectAll('.title-SQ').boundingClientRect(function (rect) {
      _this.setData({
        titleWidthData: rect
      });
    }).exec()
  },

  //计算金额 每次选中购物车商品调用
  async getCartCalculate() {
    let _this = this;
    let shopCode = app.globalData.currentStore.shopCode;
    try {
      let { receiveType } = _this.data;
      let addrId = app.globalData.deliverableAddress.addrId;
      let selectType = _this.data.selectType;
      let selectedItemIds = _this.data.selectedItemIds;
      if (_this.data.selectTaber == 'ZI_TI') receiveType = 3;
      if (_this.data.selectTaber == 'WAI_MAI') receiveType = 4;
      let parasm = JSON.parse(JSON.stringify({ shopCode, addrId, selectType, selectedItemIds, receiveType, version: 2 }));
      let data = await httpResource('cartCalculate', parasm, null, "post");
      if (data) {
        _this.setData({
          totalAmount: data.totalAmount,
          totalItemAmount: data.totalItemAmount ? data.totalItemAmount : 0,
          packingFee: data.packingFee ? data.packingFee : 0,
          deliveryFee: data.deliveryFee ? data.deliveryFee : 0,
          canSettle: data.canSettle,
          cannotSettleType: data.cannotSettleType,
          shortAmount: data.shortAmount ? data.shortAmount : 0,
          minimumAmount: data.minimumAmount ? data.minimumAmount : 0,
        });
      }
    } catch (error) {
      _this.setData({
        totalAmount: 0,
        totalItemAmount: 0,
        packingFee: 0,
        deliveryFee: 0,
        canSettle: false,
        cannotSettleType: 2,
        shortAmount: 0,
        minimumAmount: 0,
      });
    }
  },

  //全局管理
  overallSelect() {
    let _this = this;
    _this.data.isOverallSelect = !_this.data.isOverallSelect;
    _this.data.selectedItemIds = [];
    if (!_this.data.isOverallSelect) {
      _this.data.goodsList.forEach((item) => {
        if (item.status != 0) {
          item.isSelect = false;
        } else {
          if (item.isSelect) {
            _this.data.selectedItemIds.push(item.cartItemId);
          }
        }
      })
    }
    _this.setData({
      isOverallSelect: _this.data.isOverallSelect,
      goodsList: _this.data.goodsList,
    }, () => {
      if (!_this.data.isOverallSelect) _this.getCartCalculate();
    });
  },

  //input添加商品
  InputRemark(event) {
    let _this = this;
    let { index } = event.currentTarget.dataset;
    let { value } = event.detail;
    let { packageCode, categoryOneCode, categoryTwoCode, categoryThreeCode, purchaseLimit } = _this.data.goodsList[index];
    if (_this.data.goodsList[index].status == 0) {
      if (value != 0 && value <= purchaseLimit) {
        _this.getCartAdd(packageCode, categoryOneCode, value, categoryTwoCode, categoryThreeCode,).then((data) => {
          _this.data.selectedItemIds = [];
          if (data.inventory >= 0) {
            _this.data.goodsList[index].inventory = data.inventory;
          }
          _this.data.goodsList[index].isPurchaseLimit = false;
          _this.data.goodsList[index].itemCount = value;
          _this.data.goodsList.forEach((item) => {
            if (item.isSelect) _this.data.selectedItemIds.push(item.cartItemId);
          })
          _this.setData({
            goodsList: _this.data.goodsList,
          });
        })
          .catch(() => {
            // 输入数字接口报错 恢复原来数量
            _this.setData({
              goodsList: _this.data.goodsList
            });
          });
      } else {
        _this.data.goodsList[index].isPurchaseLimit = true;
        _this.setData({
          goodsList: _this.data.goodsList
        });
      }
    }
  },

  //数字减
  minusCount(event) {
    let _this = this;
    let { index } = event.currentTarget.dataset;
    if (_this.data.goodsList[index].status == 0) {
      if (_this.data.goodsList[index].itemCount > 1) {
        let { packageCode, categoryOneCode, categoryTwoCode, categoryThreeCode, itemCount } = _this.data.goodsList[index];
        itemCount = --itemCount;
        _this.getCartAdd(packageCode, categoryOneCode, itemCount, categoryTwoCode, categoryThreeCode,).then((data) => {
          _this.data.selectedItemIds = [];
          if (data.inventory >= 0) {
            _this.data.goodsList[index].inventory = data.inventory;
          }
          _this.data.goodsList[index].itemCount = itemCount;
          _this.data.goodsList[index].isPurchaseLimit = false;
          _this.data.selectType = 2;
          _this.data.goodsList.forEach((item) => {
            if (item.isSelect) _this.data.selectedItemIds.push(item.cartItemId);
          })
          _this.setData({
            goodsList: _this.data.goodsList
          });
        })
      } else {
        wx.showToast({
          title: '亲～不能再减少了',
          icon: 'none',
          duration: 1500
        })
      }
    }
  },

  //数字加
  addCount(event) {
    let _this = this;
    let { index } = event.currentTarget.dataset;
    if (_this.data.goodsList[index].status == 0) {
      if (_this.data.goodsList[index].itemCount < _this.data.goodsList[index].purchaseLimit) {
        let { packageCode, categoryOneCode, categoryTwoCode, categoryThreeCode, itemCount } = _this.data.goodsList[index];
        itemCount = ++itemCount;
        _this.getCartAdd(packageCode, categoryOneCode, itemCount, categoryTwoCode, categoryThreeCode,).then((data) => {
          _this.data.selectedItemIds = [];
          if (data.inventory >= 0) {
            _this.data.goodsList[index].inventory = data.inventory;
          }
          _this.data.goodsList[index].itemCount = itemCount;
          _this.data.goodsList[index].isPurchaseLimit = false;
          _this.data.selectType = 2;
          _this.data.goodsList.forEach((item) => {
            if (item.isSelect) _this.data.selectedItemIds.push(item.cartItemId);
          })
          _this.setData({
            goodsList: _this.data.goodsList
          });
        })
      } else {
        _this.data.goodsList[index].isPurchaseLimit = true;
        _this.setData({
          goodsList: _this.data.goodsList
        });
      }
    }
  },

  //加购物车
  getCartAdd(packageCode, categoryOneCode, itemCount, categoryTwoCode, categoryThreeCode) {
    return new Promise(async (resolve, reject) => {
      let _this = this;
      let shopCode = app.globalData.currentStore.shopCode;
      let { receiveType } = _this.data;
      if (_this.data.selectTaber == 'ZI_TI') receiveType = 3;
      if (_this.data.selectTaber == 'WAI_MAI') receiveType = 4;
      let parasm = JSON.parse(JSON.stringify({
        shopCode,
        packageCode,
        categoryTwoCode,
        categoryThreeCode,
        categoryOneCode,
        itemCount,
        addType: 1,
        receiveType,
        version: 2,
      }));
      try {
        let data = await httpResource('cartAdd2', parasm, null, "post");
        if (data && data.extraMsg) {
          wx.showToast({
            title: data.extraMsg,
            icon: "none",
            duration: 3000,
          });
        }
        resolve(data)
      } catch (error) {
        reject(error)
      }
      let num = await httpResource('cartCount', { shopCode });
      app.globalData.cartNum = num;
      _this.getCartCalculate();
    })
  },

  //单个删除
  delListItem(event) {
    let _this = this;
    let { index } = event.currentTarget.dataset;
    _this.data.selectedItemIds = [];
    _this.data.cartItemIds = [_this.data.goodsList[index].cartItemId];
    _this.data.goodsList.splice(index, 1);
    _this.data.goodsList.forEach((item) => {
      if (item.status == 0) {
        if (item.isSelect) {
          _this.data.selectedItemIds.push(item.cartItemId);
        }
      }
    })
    _this.setData({
      goodsList: _this.data.goodsList
    }, () => {
      _this.getCartDelete();
    });
  },

  //显示全局删除弹框
  deleteShow() {
    let _this = this;
    let flag = false;
    _this.data.cartItemIds = [];
    _this.data.selectedItemIds = [];
    _this.data.removeNumber = 0;
    _this.data.goodsList.forEach((item) => { if (item.isSelect) flag = true });
    if (flag) {
      _this.data.goodsList.forEach((item) => {
        if (item.isSelect) {
          _this.data.removeNumber += item.itemCount;
          _this.data.cartItemIds.push(item.cartItemId);
          _this.data.selectedItemIds.push(item.cartItemId);
        }
      })
      _this.setData({
        isDeleteAll: true,
        removeNumber: _this.data.removeNumber,
        cartItemIds: _this.data.cartItemIds,
      })
    } else {
      wx.showToast({
        title: '请选择需要删除的商品！',
        icon: 'none',
        duration: 1500,
      })
    }
  },

  //隐藏全局删除弹框
  deleteHide() {
    this.setData({
      isDeleteAll: false
    })
  },

  //全局删除
  deleteAll() {
    let _this = this;
    for (let index = _this.data.goodsList.length - 1; index >= 0; index--) {
      if (_this.data.goodsList[index].isSelect) {
        _this.data.goodsList.splice(index, 1);
      }
    }
    _this.setData({
      goodsList: _this.data.goodsList,
      removeNumber: _this.data.removeNumber,
    }, () => {
      _this.getCartDelete();
    })
  },

  //删除购物车商品
  async getCartDelete() {
    let _this = this;
    let shopCode = app.globalData.currentStore.shopCode;
    let deleteType = 2;
    let cartId = _this.data.cartId;
    let cartItemIds = _this.data.cartItemIds;
    let parasm = JSON.parse(JSON.stringify({ shopCode, deleteType, cartId, cartItemIds }));
    await httpResource('cartDelete', parasm, null, "post");
    let num = await httpResource('cartCount', { shopCode });
    app.globalData.cartNum = num;
    wx.showToast({
      title: '商品删除成功！',
      icon: 'none',
      duration: 1500,
      complete: () => {
        _this.setData({
          isDeleteAll: false,
          isDefault: _this.data.goodsList.length != 0 ? false : true
        }, () => {
          _this.getTitleWidth();
          _this.getCartCalculate();
        })
      }
    })
  },

  //单选选择
  selectGoods(event) {
    let _this = this;
    let { index } = event.currentTarget.dataset;
    let status = _this.data.goodsList[index].status;
    let isOverallSelect = _this.data.isOverallSelect;
    if (status == 0) {
      _this.data.goodsList[index].isSelect = !_this.data.goodsList[index].isSelect;
      _this.data.selectedItemIds = [];
      let isSelectAll = _this.data.goodsList.every(item => item.isSelect);
      _this.data.selectType = 2;
      _this.data.goodsList.forEach((item) => {
        if (item.isSelect) _this.data.selectedItemIds.push(item.cartItemId);
      })
      _this.setData({
        goodsList: _this.data.goodsList,
        isSelectAll: isSelectAll
      }, () => {
        if (!_this.data.isOverallSelect) _this.getCartCalculate();
      });
    } else {
      if (status == 1 && isOverallSelect) {
        _this.data.goodsList[index].isSelect = !_this.data.goodsList[index].isSelect;
        _this.data.selectedItemIds = [];
        let isSelectAll = _this.data.goodsList.every(item => item.isSelect);
        _this.data.selectType = 2;
        _this.data.goodsList.forEach((item) => {
          if (item.isSelect) _this.data.selectedItemIds.push(item.cartItemId);
        })
        _this.setData({
          goodsList: _this.data.goodsList,
          isSelectAll: isSelectAll
        });
      }
    }
  },

  //全选
  tapSelectAll() {
    let _this = this;
    let isOverallSelect = _this.data.isOverallSelect;
    _this.data.selectedItemIds = [];
    _this.data.selectType = 2;
    _this.data.isSelectAll = !_this.data.isSelectAll;
    _this.data.goodsList.forEach((item) => {
      if (item.status == 0) {
        item.isSelect = _this.data.isSelectAll ? true : false;
      } else {
        if (item.status == 1 && isOverallSelect) {
          item.isSelect = _this.data.isSelectAll ? true : false;
        }
      }
      if (item.isSelect) _this.data.selectedItemIds.push(item.cartItemId);
    })
    _this.setData({
      isSelectAll: _this.data.isSelectAll,
      goodsList: _this.data.goodsList
    }, () => {
      if (!_this.data.isOverallSelect) _this.getCartCalculate();
    });
  },
  // 是否登录 未登录去登录
  checkLogin() {
    const isLogin = app.globalData.isLogin;
    if (!isLogin) {
      wx.navigateTo({
        url: "/pages/userLogin/index?type=1",
      });
    }
    return isLogin;
  },
  //跳转首页
  routerHome() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },

  //提交打包结算
  async submitOk() {
    let _this = this;
    wx.showLoading({ title: '提交中...', mask: true });
    // 订阅消息
    try {
      let tmplIds = null;
      let { selectTaber } = _this.data;
      //自提
      if (selectTaber == 'ZI_TI') tmplIds = _this.getTemplateCode("changeSelfPick");
      //外卖
      if (selectTaber == 'WAI_MAI') tmplIds = _this.getTemplateCode("packaging");
      wx.requestSubscribeMessage({
        tmplIds: tmplIds,
        success: (res) => {
          if (tmplIds.some(v => res[v] === 'accept')) {
            httpResource("sendMessage", {
              code: selectTaber == 'ZI_TI' ? "changeSelfPick" : "packaging",
              templateCodeList: tmplIds,
            });
          }
        },
        complete(res) {
          _this.getCartSettlement();
        },
      });
    } catch {
      _this.getCartSettlement();
    }
  },

  //检查销售金额是否满配送
  salesReviews() {
    let { minimumAmount, shortAmount } = this.data;
    Dialog.alert({
      width: '309px',
      message: `满¥${minimumAmount}元起送，还差¥${shortAmount}元`,
      theme: 'round-button',
      confirmButtonText: '去凑单',
      className: 'htm-dialog',
      closeOnClickOverlay: true,
      overlayStyle: {},
    }).then(() => {
      wx.switchTab({
        url: '/pages/index/index',
      })
    });
  },

  //打包结算
  async getCartSettlement() {
    let _this = this;
    let { receiveType } = _this.data;
    let addrId = app.globalData.deliverableAddress.addrId;
    let selectType = 2;
    let cartId = _this.data.cartId;
    let selectedItemIds = _this.data.selectedItemIds;
    let shopCode = app.globalData.currentStore.shopCode;
    if (_this.data.selectTaber == 'ZI_TI') receiveType = 3;
    if (_this.data.selectTaber == 'WAI_MAI') receiveType = 4;
    let parasm = JSON.parse(JSON.stringify({ shopCode, selectType, cartId, addrId, selectedItemIds, receiveType, version: 3 }));
    let data = await httpResource('cartSettlement', parasm, null, "post");
    wx.hideLoading();
    try {
      if (!data.hasAdjust) {
        if (data.cannotSettleType == 4) {
          Dialog.alert({
            width: '260px',
            message: `您有2笔${receiveType == 4 ? '外卖' : receiveType == 3 ? '自提' : ''}订单尚未支付哦～`,
            theme: 'round-button',
            confirmButtonText: '前往支付',
            className: 'htm-dialog',
            overlayStyle: {},
          }).then(() => {
            wx.navigateTo({
              url: `/module_order/orderList/orderList?orderType=4`,
            })
          });
        } else {
          //暂停接单
          if (data.cannotSettleType == 5) {
            wx.showToast({
              title: '非常抱歉，小店繁忙已暂停接单',
              icon: 'none',
              duration: 2000,
            });
            return;
          }
          //生意太火爆
          if (data.cannotSettleType == 6) {
            wx.showToast({
              title: '生意太火爆～小店忙 不过来啦 请稍后在试吧',
              icon: 'none',
              duration: 2000,
            });
            return;
          }
          //低于最低配送金额
          if (data.calculatePrice.cannotSettleType == 1) {
            wx.showToast({
              title: '低于最低配送金额!',
              icon: 'none',
              duration: 2000,
            });
            return;
          }
          //不在配送时间范围
          if (data.calculatePrice.cannotSettleType == 2) {
            wx.showToast({
              title: _this.data.selectTaber == 'ZI_TI' ? '自提时间已过，请明日再来吧' : '不在配送时间范围!',
              icon: 'none',
              duration: 2000,
            });
            return;
          }
          //不在配送费配置配送范围内
          if (data.calculatePrice.cannotSettleType == 3) {
            wx.showToast({
              title: '不在配送费配置配送范围内!',
              icon: 'none',
              duration: 2000,
            });
            return;
          }

          //打包结算
          if (data.calculatePrice.canSettle) {
            wx.setStorage({
              key: "GOODS_PAY",
              data: parasm,
              complete: () => {
                wx.navigateTo({
                  url: '/module_order/order/index'
                })
              }
            })
          }
        }
      } else {
        wx.showToast({
          title: '存在已售罄商品，已为您重新调整商品!',
          icon: 'none',
          duration: 1500,
          complete: () => {
            setTimeout(() => {
              wx.setStorage({
                key: "GOODS_PAY",
                data: parasm,
                complete: () => {
                  wx.navigateTo({
                    url: '/module_order/order/index'
                  })
                }
              })
            }, 1500);
          }
        })
      }
    } catch (error) { }
  },

  // 获取模板消息列表
  async getTemplateList() {
    const templateList = await httpResource("getTemplateList", {}, null, "GET");
    this.setData({
      templateList: templateList || [],
    });
  },

  // 获取模板消息ID
  getTemplateCode(code) {
    const { templateList } = this.data;
    let list = [];
    try {
      list = templateList.find((e) => e.code == code)
        ? templateList
          .find((e) => e.code == code)
          .templateList.map((v) => v.templateId)
        : [];
    } catch (e) {
      list = [];
    }
    return list;
  },

  //跳转详情
  goDetail(e) {
    if (!app.globalData.currentStore.shopCode) return;
    const { barcode } = e.currentTarget.dataset;
    wx.navigateTo({
      url: `/pages/productDetail/index?barcode=${barcode}`
    })
  },
})