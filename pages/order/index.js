const {
  httpResource
} = require('../../assets/js/common.js');
import Dialog from '@vant/weapp/dialog/dialog'
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    goodsPayList: null, //从缓存里面获取确认订单数据
    totalArr: [],
    orderAddress: {}, //送货地址
    address: null, //地址栏
    remark: '', //备注
    shopName: null, //店铺名称
    shopAddress: null, //店铺详细地址
    deliveryTimeStr: '', //店铺营业时间
    businessTimeStr: '', //店铺营业时间
    orderCommodityList: [], //支付订单信息
    canSettle: null, //是否可以结算
    cannotSettleType: null, //不能打包结算原因 1 未达到启动金额 2 不在配送时间 4 两笔订单未支付 5 不在自提时间 6 爆单
    isWrapper: false, //是否显示配送费用说明
    selectTaber: null, //自提，外卖状态
    isTime: false, //自提时间展示选择框展示
    takeTheirText: null, //自提时间展示
    receiverMobile: null,//预留手机号
    receiverExpectedTime: null,//取件日期
    receiverExpectedPeriod: null,//取件时间段
    orderReceive: {},//自提入参
    isTakeOut: false,//外卖，支付展示状态
    receiveType: null,//订单状态 4,外卖订单，3，自提订单，5，团购订单
    residentialArr: [],//附件住宅
    residentialAddress: null,//附件住宅详细地址
    columns: [
      {
        values: [],
        className: 'column1',
        defaultIndex: 0,
      },
      {
        values: [
          { text: '10:00~12:00', disabled: new Date().getHours() >= 12 ? true : false },
          { text: '12:00~21:00', disabled: new Date().getHours() >= 21 ? true : false }],
        className: 'column2',
        defaultIndex: 0,
      },
    ],
    // 众包模式
    isCrowdsourcing: false,
    showCrowdsourcing: false,
    crowdsourcingTime: '尽快送达',
    crowdsourcingTimeNum: '',
    crowdsourcingDefault: true,
    crowdColumns: [
      {
        values: [],
        className: 'column1',
        defaultIndex: 0,
      },
      {
        values: [],
        className: 'column2',
        defaultIndex: 0,
      },
    ],
    // 支付方式 WX：微信支付 GIFT:'礼品卡支付'
    payType: '',
    balanceMoney: 0
  },
  // 众包选择时间
  openCrowdsourcingTime() {
    const { isCrowdsourcing } = this.data;
    if (!isCrowdsourcing) return;
    this.setData({
      showCrowdsourcing: true
    })
  },
  closeCrowdsourcingTime() {
    this.setData({
      showCrowdsourcing: false
    })
  },
  //监听众包时间
  crowdTimeChange(event) {
    const { picker, value } = event.detail;
    picker.setColumnValues(1, value[0].children);
  },

  //选择自提时间
  crowdTimeOk(event) {
    let { value } = event.detail;
    console.log(event.detail);
    this.setData({
      showCrowdsourcing: false,
      crowdsourcingDefault: false,
      crowdsourcingTime: `${value[0].text} ${value[1].text}`,
      crowdsourcingTimeNum: value[1].expectTime
    }, this.getCartSettlement);
  },
  // 重置获取时间
  async resetExpectDateTime() {
    const { isCrowdsourcing, crowdsourcingDefault } = this.data;
    if (isCrowdsourcing && crowdsourcingDefault) {
      await this.getExpectDateTime();
      await this.getCartSettlement();
    }
    return
  },
  // 获取时间
  async getExpectDateTime() {
    try {
      const data = await httpResource('getExpectDateTime', {}, null, "GET");
      const newData = (data || []).map(item => {
        item.text = item.expectDateStr;
        item.children = item.expectTimes.map(e => {
          e.text = e.expectTimeStr
          return e
        })
        return item
      })
      if (newData.length) {
        if (!this.data.crowdsourcingTimeNum) {
          this.setData({
            crowdsourcingTime: newData[0].text + newData[0].children[0].text, //
          })
        }
        if (this.data.crowdsourcingDefault) {
          this.data.crowdsourcingTimeNum = newData[0].children[0].expectTime;
        }
        this.setData({
          'crowdColumns[0].values': newData,
          'crowdColumns[1].values': newData[0].children,
        })
      };
    } catch (error) {

    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let _this = this;
    let addressItem = wx.getStorageSync('deliverable-address');
    let { shopName, deliveryTimeStr, businessTimeStr, address } = wx.getStorageSync('current-store');
    let SELECT_TABER = wx.getStorageSync('SELECT_TABER');
    // 礼品卡余额
    this.getBalanceMoney();
    _this.setData({
      shopName: shopName,
      shopAddress: `${address.city}${address.region}${address.address}`,
      deliveryTimeStr: deliveryTimeStr,
      businessTimeStr: businessTimeStr,
      address: addressItem,
      selectTaber: SELECT_TABER ? SELECT_TABER : app.globalData.selectTaber,
      'columns[0].values': _this.getTime(),
    }, () => {
      if (new Date().getHours() >= 12) {
        _this.setData({
          'columns[1].values[0].disabled': true,
          'columns[1].values[1].disabled': false
        })
      }
      if (new Date().getHours() >= 21) {
        _this.setData({
          'columns[1].values[0].disabled': false,
          'columns[1].values[1].disabled': false
        })
      }
      //判断外卖是否有配送地址
      if (wx.getStorageSync('GOODS_PAY')) {
        if (SELECT_TABER == 'ZI_TI') {
          _this.getCartSettlement();
        }
        //如果没有配送地址弹框，点击跳转添加地址
        if (SELECT_TABER == 'WAI_MAI') {
          if (addressItem.addrId) {
            _this.getCartSettlement();
          } else {
            Dialog.alert({
              title: '温馨提示',
              width: '260px',
              message: '送货方式发生变化，请选择收货地址',
              theme: 'round-button',
              confirmButtonText: '去添加',
              className: 'htm-dialog',
              overlayStyle: {},
            }).then(() => {
              wx.navigateTo({
                url: '/pages/user/address/list/index'
              })
            });
          }
        }
      }
    })

  },

  //获取今天，明天月，日
  getTime() {
    let date = new Date();
    //今天
    date.setTime(date.getTime());
    let day1 = `${date.getMonth() + 1}月${date.getDate()}日`;
    let dayDate1 = date.getDate();
    //明天
    date.setTime(date.getTime() + 24 * 60 * 60 * 1000);
    let day2 = `${date.getMonth() + 1}月${date.getDate()}日`;
    let dayDate2 = date.getDate();
    return [
      { text: day1, day: dayDate1, disabled: new Date().getHours() >= 21 ? true : false, },
      { text: day2, day: dayDate2, disabled: false, }
    ];
  },



  //自提，外卖切换
  selectTab(event) {
    let _this = this;
    let { addrId } = wx.getStorageSync('deliverable-address');
    let { type } = event.currentTarget.dataset;
    _this.setData({
      selectTaber: type,
      isTakeOut: false,
      canSettle: false,
    }, () => {
      //判断地址否存在
      app.globalData.selectTaber = type;
      wx.setStorage({
        key: "SELECT_TABER",
        data: type,
        complete: () => {
          if (!addrId) {
            Dialog.alert({
              title: '温馨提示',
              width: '260px',
              message: '送货方式发生变化，请选择收货地址',
              theme: 'round-button',
              confirmButtonText: '去添加',
              className: 'htm-dialog',
              overlayStyle: {},
            }).then(() => {
              wx.navigateTo({
                url: '/pages/user/address/list/index'
              })
            });
          } else {
            _this.getCartSettlement();
          }
        }
      })
    });
  },

  //兼容性操作，因为没有预订单号，所以把购物车打包结算接口拿过来用
  async getCartSettlement() {
    let _this = this;
    let { selectTaber, crowdsourcingTimeNum } = _this.data;
    let parasm = wx.getStorageSync('GOODS_PAY');
    let { addrId } = wx.getStorageSync('deliverable-address');
    let receiveType = selectTaber == 'ZI_TI' ? 3 : 4;
    //期望送达时间
    if (crowdsourcingTimeNum) {
      parasm.expectedDeliveryTime = _this.data.crowdsourcingTimeNum
    }
    _this.setData({
      canSettle: false,
    })
    let data = await httpResource('cartSettlement', Object.assign(parasm, {
      receiveType,
      version: 3,
      addrId
    }), null, "post");
    console.log(data);
    try {
      if (data) {
        if (!data.hasAdjust) {
          // 三方众包模式
          if (data.calculatePrice && data.calculatePrice.deliveryType == 1) {
            this.setData({
              isCrowdsourcing: true
            })
            // 获取时间
            await this.getExpectDateTime();
            if (!crowdsourcingTimeNum) {
              await this.getCartSettlement();
            }
          }
          if (data.cannotSettleType == 4) {
            Dialog.alert({
              title: '温馨提示',
              width: '260px',
              message: `您有2笔${receiveType == 4 ? '外卖' : receiveType == 3 ? '自提' : ''}订单尚未支付哦～`,
              theme: 'round-button',
              confirmButtonText: '前往支付',
              className: 'htm-dialog',
              overlayStyle: {},
            }).then(() => {
              wx.navigateTo({
                url: `/pages/orderList/orderList?orderType=4`,
              })
            });
          } else {
            // 5 不在自提时间 6 爆单
            if (data.cannotSettleType == 5 || data.cannotSettleType == 6) {
              this.setData({
                cannotSettleType: data.cannotSettleType,
                isTakeOut: true
              })
            } else {
              _this.goodsList(data);
            }
          }
        } else {
          wx.showToast({
            title: '存在已售罄商品，已为您重新调整商品!',
            icon: 'none',
            duration: 2000,
            complete: () => {
              _this.goodsList(data);
            }
          })
        }
      }
    } catch (error) { }
  },

  //商品处理
  goodsList(data) {
    let _this = this;
    let { phone } = wx.getStorageSync('userInfo');
    let goodsArr = data.items;
    let totalAmount = data.calculatePrice.totalAmount;
    let goodsPayList = data;
    let cannotSettleType = data.calculatePrice.cannotSettleType;
    _this.data.orderCommodityList = [];
    if (goodsArr && goodsArr.length != 0) {
      goodsArr.forEach((item) => {
        _this.data.orderCommodityList.push({
          cartItemId: item.cartItemId,
          barcode: item.packageCode,
          productCode: item.goodsCode,
          quantity: item.itemCount,
          sellPrice: item.salePrice,
          productIcon: item.coverImage,
          weight: item.weight
        })
      })
    }
    _this.setData({
      totalArr: totalAmount ? totalAmount.toString().split('.') : ['0', '00'],
      goodsPayList: goodsPayList,
      orderCommodityList: _this.data.orderCommodityList,
      canSettle: data.calculatePrice.canSettle,
      cannotSettleType: cannotSettleType,
      shortAmount: data.calculatePrice.shortAmount,
      minimumAmount: data.calculatePrice.minimumAmount,
      receiverMobile: data.userPhone || phone,
      receiveType: data.calculatePrice.receiveType,
      isTakeOut: (cannotSettleType == 1 || cannotSettleType == 2 || cannotSettleType == 3) ? true : false
    }, () => {
      if (data.calculatePrice.receiveType == 5) {
        _this.getQueryNearbyAddress();
      }
    })
  },

  //查询附近地址
  async getQueryNearbyAddress() {
    let _this = this;
    let { latitude, longitude } = wx.getStorageSync('deliverable-address');
    let data = await httpResource('queryNearbyAddress', { latitude, longitude }, null, "post");
    try {
      if (data && data.length != 0) {
        let obj = {
          addrDetail: null,
          addrName: '没有我的小区（将送至您的收货地址）',
          isDefault: true,
          hasGroupBuy: false,
          latitude,
          longitude
        };
        data.forEach((item) => item.isSelect = false);
        data[0].isSelect = true;
        data.push(obj);
        _this.setData({
          residentialArr: data,
          residentialAddress: data[0].addrName,
          'address.longitude': data[0].longitude,
          'address.latitude': data[0].latitude,
        })
      }
    } catch (err) {
      _this.setData({
        residentialArr: []
      })
    }
  },

  //选择附件住宅
  selectAddress(event) {
    let _this = this;
    let address = wx.getStorageSync('deliverable-address');
    let { index } = event.currentTarget.dataset;
    let residentialArr = _this.data.residentialArr;
    let addrName = null;
    let longitude = null;
    let latitude = null;
    //选择附件住宅
    if (!residentialArr[index].isDefault) {
      addrName = residentialArr[index].addrName;
      longitude = residentialArr[index].longitude;
      latitude = residentialArr[index].latitude;
      wx.uma.trackEvent("click_plot", {
        addrName,
        longitude,
        latitude
      });
    }
    //不选择附件住宅
    if (residentialArr[index].isDefault) {
      longitude = address.longitude;
      latitude = address.latitude;
    }
    residentialArr.forEach((item) => item.isSelect = false);
    residentialArr[index].isSelect = true;
    _this.setData({
      residentialArr: residentialArr,
      residentialAddress: addrName,
      'address.longitude': longitude,
      'address.latitude': latitude,
    })
  },

  //选择自提时间
  selectTake() {
    this.setData({
      isTime: true
    })
  },

  //选择自提时间关闭
  closePopup() {
    this.setData({
      isTime: false
    })
  },

  //监听自提时间
  pickerChange(event) {
    let _this = this;
    let time = event.detail.value;
    let day = new Date().getDate();
    let houres = new Date().getHours();
    //今天
    if (day == time[0].day) {
      if (houres >= 12) {
        _this.setData({
          'columns[1].values[0].disabled': true,
        });
      }
    }
    //明天
    if (day != time[0].day) {
      _this.setData({
        'columns[1].values[0].disabled': false,
        'columns[1].values[1].disabled': false
      });
    }
  },

  //选择自提时间
  confirmOk(event) {
    let { value } = event.detail;
    this.setData({
      isTime: false,
      takeTheirText: `${value[0].text} ${value[1].text}`,
      receiverExpectedTime: value[0].text,
      receiverExpectedPeriod: value[1].text
    });
  },

  //跳转首页添加商品
  routerHome() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },

  //跳转新增地址页
  routerAddress() {
    wx.navigateTo({
      url: '/pages/user/address/list/index'
    })
  },

  //获取备注
  InputRemark(event) {
    let _this = this;
    _this.setData({
      remark: event.detail
    })
  },

  //是否显示配送费用说明
  tostItem() {
    this.setData({
      isWrapper: true
    })
  },

  //是否显示配送费用说明
  tapWrapper() {
    this.setData({
      isWrapper: false,
    })
  },

  //确认支付
  async SubmitOk() {
    let _this = this;
    let { takeTheirText, receiverMobile, selectTaber } = _this.data;
    if (selectTaber == 'ZI_TI') {
      //校验自提是否选择自提时间
      if (!takeTheirText) {
        wx.showToast({
          title: '请选择自提时间!',
          icon: 'none',
          duration: 2000,
        })
        return;
      }
      _this.payAgain();
    } else {
      await this.resetExpectDateTime();
      _this.payAgain();
    }
  },

  //唤起支付
  async payAgain() {
    let _this = this;
    wx.showLoading({
      title: '支付中...',
      mask: true
    });
    try {
      let { nickName } = wx.getStorageSync('userInfo');
      let { orderReceive, selectTaber, orderAddress, orderCommodityList, receiveType, residentialAddress, payType } = _this.data;
      let shopCode = app.globalData.currentStore.shopCode;
      let shopName = app.globalData.currentStore.shopName;
      let payableAmount = _this.data.goodsPayList.calculatePrice.totalAmount;
      let packageFee = _this.data.goodsPayList.calculatePrice.packingFee;
      let deliveryFee = _this.data.goodsPayList.calculatePrice.deliveryFee;
      let orderDeliveryLimitAmount = _this.data.goodsPayList.calculatePrice.minimumAmount;
      let remark = _this.data.remark;

      let orderType = null;
      //自提订单
      if (selectTaber == 'ZI_TI' && receiveType == 3) orderType = 3;
      //外卖订单
      if (selectTaber == 'WAI_MAI' && receiveType == 4) orderType = 4;
      //团购订单
      if (selectTaber == 'WAI_MAI' && receiveType == 5) orderType = 5;
      let sourceCode = 5;
      //外卖，团购，送货地址
      if (selectTaber == 'WAI_MAI') {
        orderAddress = {
          contactName: _this.data.address.contactName,
          gender: _this.data.address.gender,
          contactPhone: _this.data.address.contactPhone,
          address: residentialAddress ? residentialAddress : _this.data.address.addrName,
          houseNum: _this.data.address.room,
          province: _this.data.address.province ? _this.data.address.province : _this.data.address.city,
          city: _this.data.address.city,
          district: _this.data.address.district,
          longitude: _this.data.address.longitude,
          latitude: _this.data.address.latitude,
        };
      }

      //自提入参
      if (selectTaber == 'ZI_TI') {
        orderReceive = {
          receiverMobile: _this.data.receiverMobile,
          receiverExpectedTime: _this.data.receiverExpectedTime,
          receiverExpectedPeriod: _this.data.receiverExpectedPeriod,
          receiver: nickName,
        };
      }

      let parasm = JSON.parse(JSON.stringify({
        shopCode,
        shopName,
        payableAmount,
        packageFee,
        deliveryFee,
        orderDeliveryLimitAmount,
        remark,
        orderType,
        sourceCode,
        orderAddress,
        orderCommodityList,
        orderReceive,
      }));
      // 众包模式
      if (orderType == 4 || orderType == 5) {
        parasm.deliveryType = _this.data.goodsPayList.calculatePrice.deliveryType;
      }
      // 总重量
      parasm.totalWeight = _this.data.goodsPayList.calculatePrice.totalWeight;
      if (_this.data.goodsPayList.calculatePrice.deliveryType == 1) {
        const { crowdsourcingTimeNum } = _this.data;
        parasm.orderId = _this.data.goodsPayList.thridDeliveryInfo.orderId;
        parasm.orderDelivery = _this.data.goodsPayList.thridDeliveryInfo;
        if (crowdsourcingTimeNum) {
          parasm.orderDelivery.expectedDeliveryTimestamp = _this.data.crowdsourcingTimeNum;
        }
        parasm.deliveryChannelFee = _this.data.goodsPayList.calculatePrice.platformDeliveryFee;
        parasm.deliveryAdditionalFee = _this.data.goodsPayList.calculatePrice.additionalDeliveryFee;
      }
      // 默认微信支付
      parasm.paymentType = 3;
      // 惊喜礼卡支付
      if (payType == 'GIFT') {
        parasm.paymentType = 4;
      }
      let data = await httpResource('onlineSubmitOrder', parasm, null, "post");
      wx.hideLoading();


      if (payType == 'WX') {
        // 发起支付
        if (data && data.payInfo != null) {
          this.requestPayment(data);
        } else {
          this.payError();
        }
      } else  // 礼品卡支付
        if (payType == 'GIFT') {
          console.log('礼品卡支付');
          const { walletPayStatus } = data;
          // 支付成功
          if (walletPayStatus == 0) {
            this.paySuccess(data);
            // 部分支付
          } else if (walletPayStatus == 1) {
            if (data && data.payInfo != null) {
              this.requestPayment(data);
            } else {
              this.payError();
            }
            // 支付失败
          } else if (walletPayStatus == 2) {
            this.payError();
          }
        }
    } catch (error) {
      this.payError();
    }
  },
  // 拉起支付
  requestPayment(data) {
    let _this = this;
    wx.requestPayment({
      'timeStamp': data.payInfo.timeStamp,
      'nonceStr': data.payInfo.nonceStr,
      'package': data.payInfo.package,
      'signType': data.payInfo.signType,
      'paySign': data.payInfo.paySign,
      'success': function (res) {
        if (res.errMsg == 'requestPayment:ok') {
          _this.paySuccess(data);
        }
      },
      'fail': function (err) {
        setTimeout(() => {
          _this.resetExpectDateTime();
        }, 1500)
        wx.showToast({
          title: '支付失败，请稍后再试！',
          icon: 'none',
          duration: 2000,
          success: () => {
            wx.removeStorageSync('GOODS_PAY');
            setTimeout(() => {
              wx.redirectTo({
                url: `/pages/orderList/orderList?orderType=4`
              })
            }, 2000)
          }
        })
      }
    })
  },
  // 支付成功跳转
  paySuccess(data) {
    let _this = this;
    wx.showToast({
      title: '支付成功',
      success: () => {
        wx.removeStorageSync('GOODS_PAY');
        let { selectTaber } = _this.data;
        if (selectTaber == 'ZI_TI') {
          wx.redirectTo({
            url: '/pages/pay-successSelf/index?takeCode=' + data.takeCode + "&orderId=" + data.orderId + "&orderNo=" + data.orderNo + "&mobile=" + data.mobile
          })
        } else {
          wx.redirectTo({
            url: '/pages/pay-success/index' + "?orderType=" + data.orderType + "?deliveryType=" + _this.data.goodsPayList.calculatePrice.deliveryType
          })
        }
      }
    })
  },
  // 支付失败跳转
  payError() {
    setTimeout(() => {
      this.resetExpectDateTime();
    }, 1500)
    wx.showToast({
      title: '支付失败，请稍后再试！',
      icon: 'none',
      duration: 2000,
      success: () => {
        wx.removeStorageSync('GOODS_PAY');
        setTimeout(() => {
          wx.redirectTo({
            url: `/pages/orderList/orderList?orderType=4`
          })
        }, 2000)
      }
    })
  },
  // 获取剩余礼品卡金额
  async getBalanceMoney() {
    try {
      const data = await httpResource('balance', {}, null, "GET");
      // 有余额 默认选中余额 否则为微信支付
      if (data && data.balance > 0) {
        this.setData({
          balanceMoney: data.balance,
          payType: 'GIFT'
        })
      } else {
        this.setData({
          payType: 'WX'
        })
      }
    } catch (error) {
      this.setData({
        balanceMoney: '0',
        payType: 'GIFT'
      })
    }
  },
  // 切换支付类型
  changePayType(e) {
    const { type } = e.currentTarget.dataset;
    const { balanceMoney } = this.data;
    if (type === 'GIFT') {
      if (balanceMoney == 0) return;
    }
    this.setData({
      payType: type
    })
  },
})