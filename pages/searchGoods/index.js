// pages/searchGoods/index.js
const { httpResource } = require("../../assets/js/common.js");
const app = getApp();

let isLogin = null 
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navbarData: {
      return: '1',
      title: '搜索商品'
    },
    searchValue: '',
    historyList: [],
    noSrarchData: false,
    isDelete:true
  },
  bindinput(e) {
    this.setData({
      searchValue: e.detail.value
    })
  },
  clearInput() {
    this.setData({
      searchValue: ''
    })
  },
  // 删除历史记录
  deleteAll() {
    this.clearHistory();
    // this.setData({
    //   historyList: []
    // })
  },
  // 选择历史记录
  selectTag(e) {
    const searchValue = e.currentTarget.dataset.value;
    this.setData({
      searchValue
    }, this.searchToPage)
  },
  // 搜索
  async search() {
    const { searchValue, historyList } = this.data;
    if (!historyList.includes(searchValue)) {
      if(isLogin){
        this.addHistory(searchValue);
      }
      
    }
    this.searchToPage();
    console.log('搜索', searchValue)
  },
  // 带参数进入列表页面
  searchToPage() {
    const { searchValue } = this.data;
    wx.navigateTo({
      url: `/pages/searchGoodsList/index?searchValue=${searchValue}`
    })
  },
  // 删除
  async clearHistory() {
    try {
      await httpResource('searchHistoryDelete', {}, null, "GET");
      this.getHistoryData();
    } catch (error) {

    }
  },
  // 新增
  async addHistory(searchWord) {
    try {
      await httpResource('searchHistorySave', { searchWord });
      this.getHistoryData();
    } catch (error) {

    }
  },
  // 列表
  async getHistoryData() {
    let data = await httpResource('searchHistoryList', {}, null, 'GET');
    data = (data || []).map(e => e.searchWord);
    this.setData({
      historyList: data
    })
    console.log(data)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    isLogin = wx.getStorageSync("u-token");
    if(isLogin){
      this.getHistoryData()
      this.setData({
        isDelete:true
      })
    }else{
      this.setData({
        isDelete:false
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
})