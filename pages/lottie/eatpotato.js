module.exports = {
  "v": "5.6.9",
  "fr": 50,
  "ip": 0,
  "op": 50,
  "w": 226,
  "h": 124,
  "nm": "eatpotato",
  "ddd": 0,
  "assets": [{
    "id": "image_0",
    "w": 226,
    "h": 124,
    "u": "https://img-cdn.hotmaxx.cn//web/common/mp-store/user_model/lottie/logo/",
    "p": "img_0.png",
    "e": 0
  }, {
    "id": "image_1",
    "w": 226,
    "h": 124,
    "u": "https://img-cdn.hotmaxx.cn//web/common/mp-store/user_model/lottie/logo/",
    "p": "img_1.png",
    "e": 0
  }, {
    "id": "image_2",
    "w": 226,
    "h": 124,
    "u": "https://img-cdn.hotmaxx.cn//web/common/mp-store/user_model/lottie/logo/",
    "p": "img_2.png",
    "e": 0
  }, {
    "id": "image_3",
    "w": 226,
    "h": 124,
    "u": "https://img-cdn.hotmaxx.cn//web/common/mp-store/user_model/lottie/logo/",
    "p": "img_3.png",
    "e": 0
  }, {
    "id": "image_4",
    "w": 226,
    "h": 124,
    "u": "https://img-cdn.hotmaxx.cn//web/common/mp-store/user_model/lottie/logo/",
    "p": "img_4.png",
    "e": 0
  }, {
    "id": "image_5",
    "w": 226,
    "h": 124,
    "u": "https://img-cdn.hotmaxx.cn//web/common/mp-store/user_model/lottie/logo/",
    "p": "img_5.png",
    "e": 0
  }, {
    "id": "image_6",
    "w": 226,
    "h": 124,
    "u": "https://img-cdn.hotmaxx.cn//web/common/mp-store/user_model/lottie/logo/",
    "p": "img_6.png",
    "e": 0
  }],
  "layers": [{
    "ddd": 0,
    "ind": 1,
    "ty": 2,
    "nm": "csp7",
    "refId": "image_0",
    "sr": 1,
    "ks": {
      "o": {
        "a": 0,
        "k": 100,
        "ix": 11
      },
      "r": {
        "a": 0,
        "k": 0,
        "ix": 10
      },
      "p": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 2
      },
      "a": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 1
      },
      "s": {
        "a": 0,
        "k": [100, 100, 100],
        "ix": 6
      }
    },
    "ao": 0,
    "ip": 42,
    "op": 50,
    "st": 42,
    "bm": 0
  }, {
    "ddd": 0,
    "ind": 2,
    "ty": 2,
    "nm": "csp6",
    "refId": "image_1",
    "sr": 1,
    "ks": {
      "o": {
        "a": 0,
        "k": 100,
        "ix": 11
      },
      "r": {
        "a": 0,
        "k": 0,
        "ix": 10
      },
      "p": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 2
      },
      "a": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 1
      },
      "s": {
        "a": 0,
        "k": [100, 100, 100],
        "ix": 6
      }
    },
    "ao": 0,
    "ip": 38,
    "op": 42,
    "st": 34,
    "bm": 0
  }, {
    "ddd": 0,
    "ind": 3,
    "ty": 2,
    "nm": "csp5",
    "refId": "image_2",
    "sr": 1,
    "ks": {
      "o": {
        "a": 0,
        "k": 100,
        "ix": 11
      },
      "r": {
        "a": 0,
        "k": 0,
        "ix": 10
      },
      "p": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 2
      },
      "a": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 1
      },
      "s": {
        "a": 0,
        "k": [100, 100, 100],
        "ix": 6
      }
    },
    "ao": 0,
    "ip": 32,
    "op": 38,
    "st": 32,
    "bm": 0
  }, {
    "ddd": 0,
    "ind": 4,
    "ty": 2,
    "nm": "csp4",
    "refId": "image_3",
    "sr": 1,
    "ks": {
      "o": {
        "a": 0,
        "k": 100,
        "ix": 11
      },
      "r": {
        "a": 0,
        "k": 0,
        "ix": 10
      },
      "p": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 2
      },
      "a": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 1
      },
      "s": {
        "a": 0,
        "k": [100, 100, 100],
        "ix": 6
      }
    },
    "ao": 0,
    "ip": 24,
    "op": 32,
    "st": 24,
    "bm": 0
  }, {
    "ddd": 0,
    "ind": 5,
    "ty": 2,
    "nm": "csp3",
    "refId": "image_4",
    "sr": 1,
    "ks": {
      "o": {
        "a": 0,
        "k": 100,
        "ix": 11
      },
      "r": {
        "a": 0,
        "k": 0,
        "ix": 10
      },
      "p": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 2
      },
      "a": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 1
      },
      "s": {
        "a": 0,
        "k": [100, 100, 100],
        "ix": 6
      }
    },
    "ao": 0,
    "ip": 16,
    "op": 24,
    "st": 16,
    "bm": 0
  }, {
    "ddd": 0,
    "ind": 6,
    "ty": 2,
    "nm": "csp2",
    "refId": "image_5",
    "sr": 1,
    "ks": {
      "o": {
        "a": 0,
        "k": 100,
        "ix": 11
      },
      "r": {
        "a": 0,
        "k": 0,
        "ix": 10
      },
      "p": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 2
      },
      "a": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 1
      },
      "s": {
        "a": 0,
        "k": [100, 100, 100],
        "ix": 6
      }
    },
    "ao": 0,
    "ip": 8,
    "op": 16,
    "st": 8,
    "bm": 0
  }, {
    "ddd": 0,
    "ind": 7,
    "ty": 2,
    "nm": "csp1",
    "refId": "image_6",
    "sr": 1,
    "ks": {
      "o": {
        "a": 0,
        "k": 100,
        "ix": 11
      },
      "r": {
        "a": 0,
        "k": 0,
        "ix": 10
      },
      "p": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 2
      },
      "a": {
        "a": 0,
        "k": [113, 62, 0],
        "ix": 1
      },
      "s": {
        "a": 0,
        "k": [100, 100, 100],
        "ix": 6
      }
    },
    "ao": 0,
    "ip": 0,
    "op": 8,
    "st": 0,
    "bm": 0
  }],
  "markers": []
}