const { httpResource, uploadImg } = require("../../assets/js/common.js");
import {
  throttl
} from '../../assets/js/eventUtil.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderId: '', // 原订单id
    orderNo: '', // 订单编号
    goodsList: [],
    goodsMoreList: [],
    showMoreGoods: false,
    shopCode: '',
    isAftermarket: false,
    isAllRefundFlag: false,
    deliveryFee: '',
    packageFee: '',
    refundTotalAmount: '',
    imgList: [],
    discountAmount: '17.90',// 退款总金额
    showPicker: false,
    pickerKey: '',
    selectIndex: 0,
    selectList: [],
    storeIndex: 99,
    refundIndex: 99,
    orderType: '',
    orderState: '',
    requestId: '',
    refundStoreList: [
      {
        label: '未收到货',
        value: '1'
      },
      {
        label: '已收到货',
        value: '2'
      },
    ],
    refundTypeList: [
      {
        label: '未收到商品',
        value: '3'
      },
      {
        label: '空包裹',
        value: '4'
      },
    ],
    refundTypeList2: [
      {
        label: '商品变质/发霉/有异物',
        value: '5'
      },
      {
        label: '包装/商品破损',
        value: '6'
      },
      {
        label: '少件/漏送',
        value: '7'
      },
      {
        label: '商品描述与实物不符',
        value: '8'
      },
      {
        label: '生产日期/保质期与商品描述不符',
        value: '9'
      },
      {
        label: '假冒品牌',
        value: '10'
      },

      {
        label: '其他',
        value: '11'
      },
    ],
    refundTypeList3: [
      {
        label: '商家缺货',
        value: '12'
      },
      {
        label: '多买/错买/不想要',
        value: '13'
      },
      {
        label: '协商一致退款',
        value: '14'
      },
      {
        label: '未按承诺时间发货',
        value: '15'
      },
      {
        label: '地址信息填写错误',
        value: '16'
      },
      {
        label: '商家暂时不营业',
        value: '17'
      },
      {
        label: '其他',
        value: '11'
      },
    ],
    refundTypeList4: [
      {
        label: '不喜欢/不想要了',
        value: '19'
      },
      {
        label: '质量问题',
        value: '20'
      },
      {
        label: '使用后身体不舒服',
        value: '21'
      },
      {
        label: '无生产日期/无保质期',
        value: '22'
      },
      {
        label: '已过/临近保质期',
        value: '23'
      },
      {
        label: '非正品/假冒品牌',
        value: '24'
      },
      {
        label: '商品变质/发霉/有异物',
        value: '25'
      },
      {
        label: '味道不对',
        value: '26'
      },
      {
        label: '价格高',
        value: '27'
      },
    ],
    remark: '',
    infoData: {},
    showToast: false,
    showError: false,
    showSuccess: false,
    toastText: ''
  },
  onCloseToast() {
    this.setData({
      showToast: false,
      showError: false,
      showSuccess: true,
      toastText: ''
    })
  },
  onClose() {
    this.setData({
      showPicker: false
    })
  },
  selectChange(e) {
    const index = e.currentTarget.dataset.index;
    this.setData({
      selectIndex: index
    })
  },
  selectConfirm(e) {
    const { selectIndex, pickerKey } = this.data;
    if (pickerKey == 'store') {
      this.setData({
        showPicker: false,
        storeIndex: selectIndex
      })
    } else {
      this.setData({
        showPicker: false,
        refundIndex: selectIndex
      })
    }
  },
  openPicker(e) {
    const key = e.currentTarget.dataset.key;
    const { isAftermarket, refundStoreList, refundTypeList, refundTypeList2, refundTypeList3, storeIndex, refundIndex, orderState, refundTypeList4 } = this.data;
    if (key != 'store' && storeIndex == 99 && isAftermarket) {
      wx.showToast({
        title: '请先选择货物状态',
        icon: 'none',
      });
      return
    }
    if (key == 'store') {
      this.setData({
        refundIndex: 99,
      })
    } else { }
    this.setData({
      pickerKey: key,
      selectIndex: key == 'store' ? storeIndex == 99 ? 0 : storeIndex : refundIndex == 99 ? 0 : refundIndex,
      showPicker: true,
      selectList: key == 'store' ? refundStoreList : storeIndex == 0 ? refundTypeList : isAftermarket ? orderState == 'QI_TIAN_WU_LI_YOU' ? refundTypeList4 : refundTypeList2 : orderState == 'QI_TIAN_WU_LI_YOU' ? refundTypeList4 : refundTypeList3
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.orderId) {
      this.setData({
        orderId: options.orderId,
        orderState: options.state
      })
    }
    if (options.orderNo) {
      this.setData({
        orderNo: options.orderNo,
        orderState: options.state
      })
    }
  },
  PickerChange(e) {
    this.setData({
      storeIndex: e.detail.value,
      refundIndex: 99,
    })
  },
  changeTip() {
    wx.showToast({
      title: '请先选择货物状态',
      icon: 'none',
    })
  },
  // 备注
  textAreaInput(e) {
    const value = e.detail.value;
    this.setData({
      remark: value,
    })
  },
  PickerChange2(e) {
    this.setData({
      refundIndex: e.detail.value,
    })
  },
  changeGoods() {
    const { showMoreGoods } = this.data;
    if (!showMoreGoods) {
      this.setData({
        showMoreGoods: true
      })
    } else {
      this.setData({
        showMoreGoods: false
      })
    }
  },
  removeImg(e) {
    const index = e.currentTarget.dataset.index;
    const {
      imgList
    } = this.data;
    imgList.splice(index, 1);
    this.setData({
      imgList
    })
  },
  previewImage(e) {
    const url = e.currentTarget.dataset.url;
    const {
      imgList
    } = this.data;
    wx.previewImage({
      urls: imgList,
      current: url
    })

  },
  goBack() {
    let _this = this;
    if (_this.data.orderState == 'QI_TIAN_WU_LI_YOU') {
      wx.redirectTo({
        url: '/module_base/goodsRefund/index?orderId=' + this.data.orderId + '&shopCode=' + this.data.shopCode
      })
    } else {
      wx.redirectTo({
        url: '/pages/refundGoodslist/index?orderId=' + this.data.orderId + '&shopCode=' + this.data.shopCode + '&isAftermarket=' + this.data.isAftermarket,
      })
    }
  },
  // 上传图片
  selectImg() {
    wx.chooseImage({
      count: 3,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: async (res) => {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths;
        for (const path of tempFilePaths) {
          try {
            const url = await uploadImg({ filePath: path, code: 'shopOrderRefund' });
            const { imgList } = this.data;
            imgList.push(url);
            this.setData({
              imgList: imgList.slice(0, 3)
            })
          } catch (error) {

          }

        }
      }
    })
  },
  // 获取订单详情 获取参数
  async getOrderDetail(id) {
    httpResource('queryOrderDetail', null, (data) => {
      console.log(data);
      if (data) {
        this.setData({})
      }
      this.setData({
        orderDetail: data,
      })
    }, "GET", null, `/${id}`)
  },
  // 提交
  submit: throttl(async function () {
    let _this = this;
    let httpUrl = _this.data.orderState == 'QI_TIAN_WU_LI_YOU' ? 'withoutSubmit' : 'refundSubmit';
    const { isAftermarket, goodsMoreList, orderType, isAllRefundFlag, orderId, orderNo, imgList, refundTotalAmount, deliveryFee, packageFee, storeIndex, refundIndex, refundStoreList, refundTypeList, refundTypeList2, remark, shopCode, refundTypeList3, requestId } = _this.data;
    if (storeIndex == 99 && isAftermarket) {
      wx.showToast({
        title: '请选择货物状态',
        icon: 'none',
      })
      return
    }
    if (refundIndex == 99) {
      wx.showToast({
        title: '请选择退款原因',
        icon: 'none',
      })
      return
    }
    let storeItem = refundStoreList[storeIndex];
    let refundItem = refundTypeList3[refundIndex];
    if (isAftermarket) {
      refundItem = refundTypeList2[refundIndex];
    }
    if (storeIndex != 99 && storeIndex == 0) {
      refundItem = refundTypeList[refundIndex];
    }
    let userId = '';
    const userInfo = wx.getStorageSync("userInfo");
    if (userInfo && userInfo.userId) {
      userId = userInfo.userId
    }
    let params = null
    if (_this.data.orderState == 'QI_TIAN_WU_LI_YOU') {
      params = {
        requestId,
        userId,
        orderId,
        orderNo,
        shopCode,
        orderType,
        isAllRefundFlag,
        receiveState: storeIndex == 1 ? 1 : 0,
        refundReasonCode: refundItem.value,
        refundReason: refundItem.label,
        refundReasonDesc: remark,
        imgUrlList: imgList,
        refundItemList: goodsMoreList
      }
    } else {
      params = {
        refundOrderList: [{
          userId,
          orderId,
          orderNo,
          shopCode,
          orderType,
          isAllRefundFlag,
          receiveState: storeIndex == 1 ? 1 : 0,
          refundReasonCode: refundItem.value,
          refundReason: refundItem.label,
          refundReasonDesc: remark,
          imgUrlList: imgList,
          refundItemList: goodsMoreList
        }]
      }
    }
    wx.showLoading({ title: '提交中' });
    httpResource(httpUrl, params, (data) => {
      wx.hideLoading();
      const { refundOrderId, refundOrderNo, state, message } = data;
      if (state == 1 || state == 0) {
        this.setData({
          showToast: true,
          showError: false,
          toastText: '提交成功',
          showSuccess: true,
        })
        this.resetToast();
        setTimeout(() => {
          wx.redirectTo({
            url: `/pages/refundDetail/index?orderId=${orderId}&orderNo=${orderNo}&refundOrderId=${refundOrderId}&returnOrderNo=${refundOrderNo}&state=${state}`,
          })
        }, 1000);
      } else {
        this.setData({
          showToast: true,
          showError: true,
          toastText: message,
          showSuccess: false,
        })
        this.resetToast();
        return
      }
    }, "POST", (err) => {
      wx.hideLoading();
      this.setData({
        showToast: true,
        showError: true,
        toastText: err.msg,
        showSuccess: false,
      })
      this.resetToast();
    }, '', false)
  }, 600),
  resetToast(time = 3000) {
    setTimeout(() => {
      this.setData({
        showToast: false,
        showError: false,
        toastText: '',
        showSuccess: false,
      })
    }, time)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getStorage({
      key: 'refundGoodInfo',
      success: (data) => {
        if (data.data) {
          this.setData({
            infoData: data.data,
            requestId: Math.random().toString(36).slice(-6),
          })
          const { orderType, isAftermarket, shopCode, isAllRefundFlag, deliveryFee, packageFee, refundTotalAmount, refundItemList } = data.data;
          if (refundItemList && refundItemList.length) {
            this.setData({
              orderType,
              isAftermarket: isAftermarket == 'true',
              shopCode, isAllRefundFlag, deliveryFee, packageFee, refundTotalAmount,
              goodsList: refundItemList.slice(0, 2),
              goodsMoreList: refundItemList
            })
          }
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})