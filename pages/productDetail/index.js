// pages/productDetail/index.js
const { httpResource } = require("../../assets/js/common.js");
const app = getApp();
import { getImgUrl, setSharePagePath } from "../../assets/js/commonData";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBar: 0,
    defaultImg:
      "https://img.hotmaxx.cn/web/common/mp-store/home-index/goods-default.png",
    barcode: null,
    cartNum: 0,
    shopCode: null,
    navbarData: {
      home: 1
    },
    isShowList: true,
    infoList: [
      {
        icon: 'https://img.hotmaxx.cn/web/common/mp-store/productDetail/brand.png',
        title: '品牌',
        content: ''
      },
      {
        icon: 'https://img.hotmaxx.cn/web/common/mp-store/productDetail/temperature.png',
        title: '贮存条件',
        content: ''
      },
      {
        icon: 'https://img.hotmaxx.cn/web/common/mp-store/productDetail/content.png',
        title: '含量',
        content: ''
      },
      {
        icon: 'https://img.hotmaxx.cn/web/common/mp-store/productDetail/good-address.png',
        title: '产地',
        content: ''
      }
    ],
    detailInfo: {}
  },
  // 是否登录 未登录去登录
  checkLogin() {
    const isLogin = app.globalData.isLogin;
    if (!isLogin) {
      wx.navigateTo({
        url: "/pages/userLogin/index?type=1"
      })
    }
    return isLogin;
  },
  // 加入购物车
  async addCart(e) {
    const status = this.checkLogin();
    const selectTaber = wx.getStorageSync("SELECT_TABER");
    if (!status) return;
    let { barcode: packageCode, goodsName, goodsCode, coverImage, salePrice, marketPrice, categoryCodeFirst: categoryOneCode, categoryCodeSecond: categoryTwoCode,
      categoryCodeThird: categoryThreeCode, inventory } = this.data.detailInfo;
    if (inventory <= 0) return;
    if (selectTaber != 'ZI_TI' && !app.globalData.deliverableAddress.addrId) {
      wx.showToast({
        title: '请先选择地址哦',
        duration: 1000,
        icon: 'none'
      })
      return
    }
    const shopCode = this.data.shopCode || app.globalData.currentStore.shopCode;
    try {
      wx.hideToast();
    } catch (error) { }
    if (!shopCode) {
      wx.showToast({
        title: '请先选择门店哦',
        duration: 1000,
        icon: 'none'
      })
      return
    }
    if (!this.data.barcode) {
      wx.showToast({
        title: '请稍后',
        duration: 1000,
        icon: 'none'
      })
      return
    }
    coverImage = Array.isArray(coverImage) ? coverImage[0] ? coverImage[0] : '' : ''
    const params = JSON.parse(JSON.stringify({
      categoryTwoCode,
      categoryThreeCode,
      version: 2,
      shopCode,
      packageCode,
      goodsName, goodsCode, coverImage, salePrice, marketPrice, categoryOneCode,
      addType: 2,
      receiveType: selectTaber == 'ZI_TI' ? 3 : 4
    }))
    let data = await httpResource('cartAdd2', params);
    if (data && data.extraMsg) {
      wx.showToast({
        title: data.extraMsg,
        icon: "none",
        duration: 3000,
      });
    } else {
      wx.showToast({
        title: "加入成功",
        icon: "none",
        duration: 1000,
      });
    }
    const num = await httpResource('cartCount', { shopCode });
    app.globalData.cartNum = num;
    this.setData({ cartNum: num })
  },
  // 喜欢
  async likeChange(e) {
    const status = this.checkLogin();
    if (!status) {
      return;
    }
    // 是否喜欢
    const { favorite: favorite, barcode } = this.data.detailInfo;
    const api = favorite ? 'unFavorite' : 'favorite';
    await httpResource(api, { barcode }, null, "GET");
    // 上一个页面列表不刷新的
    let pages = getCurrentPages()
    var currPage = null;
    if (pages.length) {
      currPage = pages[pages.length - 2];
      if (currPage) {
        currPage.setData({
          noLikesBarcode: favorite ? barcode : '',
          likesBarcode: favorite ? '' : barcode,
        })
      }
    }
    this.setData({
      [`detailInfo.favorite`]: !favorite,
    }, this.getData)
  },
  // 跳转购物车
  goCart() {
    const status = this.checkLogin();
    if (!status) {
      return;
    }
    const selectTaber = wx.getStorageSync("SELECT_TABER");
    if (selectTaber != 'ZI_TI' && !app.globalData.deliverableAddress.addrId) {
      wx.navigateTo({
        url: '/pages/user/address/list/index?type=2'
      })
      return
    }
    try {
      wx.hideToast();
    } catch (error) {

    }
    const shopCode = this.data.shopCode || app.globalData.currentStore.shopCode;
    if (!shopCode) {
      this.getStoreData();
      return
    }
    wx.switchTab({ url: '/pages/cart/index' })
  },
  async getStoreData() {
    const { latitude, longitude } = app.globalData.deliverableAddress;
    const data = await httpResource("getShopByRange", {
      radius: 100,
      latitude,
      longitude,
    });
    if (data && data.length) {
      // 选择 门店
      app.globalData.currentStore = data[0];
      wx.setStorageSync('current-store', data[0]);
      wx.switchTab({ url: '/pages/cart/index' })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      statusBar: app.globalData.statusBar,
    });
    // if (!app.globalData.isLogin) {
    //   wx.reLaunch({ url: '/pages/index/index' });
    // } else {
    if (options.barcode) {
      const shopCode = options.shopCode ? options.shopCode : '';
      this.setData({ barcode: options.barcode, shopCode }, this.getData);
    }
    this.setData({
      cartNum: app.globalData.cartNum,
    })
    // }
  },
  // 商品详情数据
  async getData() {
    const shopCode = this.data.shopCode || app.globalData.currentStore.shopCode;
    const { barcode, defaultImg } = this.data;
    const data = await httpResource('goodsInfo', { shopCode, barcode }, null, "GET");
    if (data) {
      data.coverImage = (data.coverImage || []).map(url => getImgUrl(url, 1000))
      data.image = data.coverImage[0] ? data.coverImage[0] : defaultImg;
      this.setData({
        detailInfo: data,
      })
      this.setData({
        [`infoList[0].content`]: data.brand,
        [`infoList[1].content`]: data.storeWay,
        [`infoList[2].content`]: data.content ? `${data.content}${data.contentUnit}` : '',
        [`infoList[3].content`]: data.origin,
      }, () => {
        const isShowList = this.data.infoList.filter(e => !!e.content).length > 0;
        this.setData({
          isShowList
        })
      })
    }

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  async shareCount(params) {
    try {
      await httpResource('shareNum', params);
      this.getData()
    } catch (error) {
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (e) {
    const { barcode, detailInfo: { image: imageUrl, goodsName, marketPrice, salePrice: price, } } = this.data;
    const shopCode = this.data.shopCode || app.globalData.currentStore.shopCode;
    const path = setSharePagePath(`/pages/productDetail/index?barcode=${barcode}&shopCode=${shopCode}`, app, 'productDetail');
    this.shareCount({ shopCode, goodsName, marketPrice, price, barcode })
    return {
      title: goodsName,
      path,
      imageUrl,
    };
  }
})