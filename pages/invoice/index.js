import {
  getQuery,
  httpResource,
  isNeedAutho,
  getCode,
} from '../../assets/js/common.js';
import {
  throttl
} from '../../assets/js/eventUtil.js'
var app = getApp();
// pages/invoice/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      'title': '开具发票',
    },
    showDelIcon: false,
    titleList: [{
      titleName: '呵呵哒啊',
      titleId: '1',
      taxCode: 'sjdksdjkds',
      mobile: '15527889999',
      email: 'djskj@qq.com',
      titleType: '2'
    },
    {
      titleName: '看电视看',
      titleId: '2',
      mobile: '19988886666',
      email: '33478834@163.com',
      titleType: '1'
    },
    {
      titleName: '但明明什么都是',
      titleId: '3',
      taxCode: 'dddddd',
      mobile: '16622778766',
      email: 'ddddd@qq.com',
      titleType: '2'
    },
    {
      titleName: '锻炼了动手',
      titleId: '4',
      taxCode: 'bbbbbbb',
      mobile: '13899787655',
      email: 'ccccc@qq.com',
      titleType: '2'
    },
    {
      titleName: 'v那你还大叔大叔',
      titleId: '5',
      taxCode: 'xxxxxxx',
      mobile: '18766662387',
      email: 'cccccc@qq.com',
      titleType: '2'
    }
    ],
    titleColor: '#000',
    background: '#fff',
    tabIndex: '2', // 抬头类型 2 企业 1 个人
    title: '', // 抬头名称
    shui: '', // 税号
    phone: '', // 手机号码
    email: '', // 邮箱
    s: '',
    p: '',
    o: '',
    lcode: null,
    userInfo: {}, // 用户基础信息
    showLogin: false, // userInfo Pop
    showPhone: false, // phoneInfo Pop
    authUserInfo: {}, //
    authPhoneInfo: {}, //
    orderInfo: {}, // 订单信息
    canSubmit: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.s && options.s && options.o) {
      this.setData({
        s: options.s,
        p: options.p,
        o: options.o
      })
    }
    if (options.q) {
      const url = decodeURIComponent(options.q)
      this.setData({
        s: getQuery(url, 's'),
        p: getQuery(url, 'p'),
        o: getQuery(url, 'o')
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  // 打电话
  callPhone() {
    wx.makePhoneCall({
      phoneNumber: "4009659166",
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    isNeedAutho().then(() => {
      this.setData({
        userInfo: wx.getStorageSync("userInfo")
      })
      if (this.data.s && this.data.o) {
        this.orderInfo() // 订单信息
        this.ivvoiceList() // 历史开票
      }
    }).catch(() => {
      this.setData({
        showLogin: true
      })
    })
    getCode().then((code) => {
      this.setData({
        lcode: code
      })
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  tabChange(e) {
    this.setData({
      tabIndex: e.target.dataset.index
    }, () => {
      this.setData({
        title: '',
        shui: '',
        phone: '',
        email: ''
      })
    })
  },
  wxTitle() {
    const version = wx.getSystemInfoSync().SDKVersion
    if (this.compareVersion(version, '1.5.0') >= 0) {
      wx.chooseInvoiceTitle({
        success: res => {
          this.setData({
            title: res.title,
            shui: res.taxNumber
          })
        },
        fail: err => {
          console.error(err)
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },
  compareVersion(v1, v2) {
    v1 = v1.split('.')
    v2 = v2.split('.')
    const len = Math.max(v1.length, v2.length)

    while (v1.length < len) {
      v1.push('0')
    }
    while (v2.length < len) {
      v2.push('0')
    }

    for (let i = 0; i < len; i++) {
      const num1 = parseInt(v1[i])
      const num2 = parseInt(v2[i])

      if (num1 > num2) {
        return 1
      } else if (num1 < num2) {
        return -1
      }
    }

    return 0
  },
  inputChange(e) {
    let dataset = e.currentTarget.dataset
    let value = e.detail.value
    this.setData({
      [`${dataset['item']}`]: e.detail.value
    })
  },
  submit: throttl(function () {
    if (!this.data.title) {
      wx.showToast({
        title: '请填写抬头名称',
        icon: 'none',
        mask: true
      })
      return
    }
    if (!this.data.shui && this.data.tabIndex === '2') {
      wx.showToast({
        title: '请填写税号',
        icon: 'none',
        mask: true
      })
      return
    }
    if (!this.data.phone) {
      wx.showToast({
        title: '请填写手机号码',
        icon: 'none',
        mask: true
      })
      return
    }
    if (!/^1[3-9]\d{9}$/.test(this.data.phone)) {
      wx.showToast({
        title: '手机号格式有误',
        icon: 'none',
        mask: true
      })
      return
    }
    if (!this.data.email) {
      wx.showToast({
        title: '请填写邮箱',
        icon: 'none',
        mask: true
      })
      return
    }
    var reg = new RegExp("^[a-z0-9A-Z]+[- | a-z0-9A-Z . _]+@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-z]{2,}$");
    if (!reg.test(this.data.email.trim())) {
      wx.showToast({
        title: '邮箱格式有误',
        icon: 'none',
        mask: true
      })
      return
    }
    const body = {
      type: this.data.tabIndex,
      title: this.data.title,
      shui: this.data.tabIndex === '2' ? this.data.shui : undefined,
      phone: this.data.phone,
      email: this.data.email
    }
    httpResource(
      'addInvoice', {
      userId: wx.getStorageSync("userInfo").userId,
      shopCode: this.data.orderInfo.shopCode,
      orderCode: this.data.orderInfo.channelOrderNo,
      drawerType: body.type,
      drawerTitle: body.title.replace(/\s*/g, ''),
      taxpayerCode: body.shui,
      drawerPhone: body.phone,
      drawerMail: body.email
    },
      () => {
        // this.setData({
        //   canSubmit: true
        // })
        this.ivvoiceList() // 历史开票记录
        this.orderInfo() // 查询订单信息
      },
      "POST",
      () => {
        // this.setData({
        //   canSbmit: true
        // })
      }
    )
    // if (this.data.canSubmit) {
    //   this.setData({
    //     canSubmit: false
    //   }, () => {

    //   })
    // }
  }),
  doDel() {
    this.setData({
      showDelIcon: !this.data.showDelIcon
    })
  },
  doClick(e) {
    let item = e.target.dataset.item
    this.setData({
      tabIndex: item.titleType.toString(),
      title: item.titleName,
      shui: item.taxCode || '',
      phone: item.mobile,
      email: item.email
    })
  },
  doDelConfirm(e) {
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: (res) => {
        if (res.confirm) {
          //
          let id = e.target.dataset.id
          httpResource(
            'removeInvoice', {
            titleId: id
          },
            () => {
              this.ivvoiceList() // 历史开票记录
            }
          )
        }
      }
    })
  },
  orderInfo() {
    // 查询订单信息
    wx.showLoading({
      title: '加载中'
    })
    httpResource(
      'orderInfo', {
      shopCode: this.data.s,
      channelOrderNo: this.data.o
    },
      (data) => {
        this.setData({
          orderInfo: data || {}
        })
        wx.hideLoading()
      },
      "GET",
      () => {
        wx.hideLoading()
      }
    )
  },
  ivvoiceList() {
    // 历史开票记录
    httpResource(
      'ivvoiceList', {},
      (data) => {
        this.setData({
          titleList: data || []
        })
      }
    )
  },
  init() {
    this.onShow();
  },
})