// pages/mine2/index.js
var app = getApp();
import {
  setCouponDetailInfo,
  setModulesData,
  setSharePagePath
} from "../../assets/js/commonData";
import {
  subscribeMessage,
  getSubState
} from "../../config/subConfig";
import {
  createQrcode2
} from '../../assets/js/codeUtil';
const {
  isNeedAutho,
  httpResource,
  pxTorpx
} = require("../../assets/js/common.js");
import lottie from 'lottie-miniprogram';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLogin: app.globalData.isLogin,
    headerInfo: {}, // 头部信息
    couponLength: 0, // 优惠券数量
    couponList: [], // 推荐的优惠券
    balanceMoney: 0, // 礼品卡金额
    growthValue: 0, // 礼品卡金额
    orderTabList: [],  // 订单列表
    sortList: [], // 页面配置信息
    utilsTabList: [], // 工具列表
    delayTime: 5000, // banner 切换时间
    lanmuList: [], // 栏目位
    options: {}, // 页面参数
    templateList: [],// 模板消息列表
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 500,
    swiperList: [], // 轮播数据
    swiperCurrent: 0,
    selfGetList: [],// 待取货轮播
    memberLevelInfo: {}, // 会员等级信息
    isLoadAnimation: false, // 初始动画效果
    canvasWidth: pxTorpx(138),// canvas宽高
    canvasheight: pxTorpx(142),
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      options
    })
  },
  // 自定义tabbar
  getTabbar() {
    if (typeof this.getTabBar === "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 3,
      });
    }
  },
  onPullDownRefresh: function () {
    // 标题栏显示刷新图标，转圈圈
    wx.showNavigationBarLoading()

    console.log("onPullDownRefresh");

    setTimeout(() => {
      this.onShow();
    }, 200)
    // 标题栏隐藏刷新转圈圈图标
    setTimeout(() => {
      wx.hideNavigationBarLoading();
      wx.stopPullDownRefresh();
    }, 1000);
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getTabbar();
    // 登录状态
    this.setData({
      isLogin: app.globalData.isLogin
    })
    // 页面配置
    this.getPageModules();
    isNeedAutho()
      .then(() => {

        // 获取会员等级
        this.queryBymemberFn();
        // 获取礼品额度
        this.getBalanceMoney();
        // 订单信息
        this.getreceivingNoteList();
        this.setData({
          activeImgEnd: false,
          scrollable: true,
          isLightUp: false,
          rotateEnd: false,
          rotate: "",
          respones: true,
        },
          this.getIndexData
        );
        // 执行爆炸动画
        this.initAnimation();
      })
  },
  // 爆炸效果动画
  initAnimation() {
    wx.createSelectorQuery().select('#user-avatar').node(res => {
      if (!res || !res.node) return;
      const canvas = res.node
      const context = canvas.getContext('2d')
      canvas.width = 300
      canvas.height = 300
      lottie.setup(canvas)
      this.ani = lottie.loadAnimation({
        loop: false,
        autoplay: true,
        animationData: require('./box.js'),
        rendererSettings: {
          context,
        },
      })
      console.log(this.ani)
      // 1秒后修改状态为 吃零食动画
      setTimeout(() => {
        this.ani = null;
        this.setData({
          isLoadAnimation: true
        })
      }, 1020)
    }).exec();
    // this.ani.pay();
  },
  // 获取等级信息
  async queryBymemberFn() {
    let _this = this;
    try {
      let data = await httpResource('queryBymember', { gradeDetailflag: 1 });
      if (data) {
        wx.setStorage({
          key: "GRADE",
          data: data,
          complete: () => {
            // 取当前等级的第一个权益副标题
            const item = (data.privilegeVOList[0] || null);
            if (item) data.subTitle = item.subtitle;
            _this.setData({
              memberLevelInfo: data
            })
          }
        });
      }
    } catch (error) { }
  },
  // 获取栏目位
  async listIconByShopRangeModuleFn(shopCode) {
    try {
      const data = await httpResource('listIconByShopRangeModule', { shopCode, pageCode: 'mine' }, null, "GET", null, '', false);
      return ((data && data.iconList) || []);
    } catch (error) {
      return [];
    }
  },
  // 低碳/新增模块点击事件
  commonTap(e) {
    console.log('点击', e)
    app.globalData.lotterynavigate = '/pages/mine/index'
    const {
      type,
      url,
      appid,
      isbanner,
      tag,
      isutils
    } = e.currentTarget.dataset;
    // banner图片埋点
    if (isbanner && url) {
      wx.uma.trackEvent('member_banner1', {
        'click_module': url
      })
    }
    // 工具栏埋点
    if (isutils && tag) {
      wx.uma.trackEvent('mine_toolbar', {
        'name': tag
      })
    }
    // 无
    if (type === 0) {
      return;
    }
    // 内部页面
    if (type === 1) {
      wx.navigateTo({
        url,
      });
      return;
    }
    // 外部小程序
    if (type === 2) {
      wx.navigateToMiniProgram({
        appId: appid,
        // path:url
      });
      return;
    }
    // H5
    if (type === 3) {
      wx.navigateTo({
        url: `/pages/webview/index?url=${url}`,
      });
      return;
    }
  },
  itemTap(e) {
    this.commonTap({ currentTarget: { dataset: e.detail } })
  },
  // 去礼品卡
  goBalance() {
    wx.navigateTo({
      url: '/pages/user/giftCard/index'
    })
  },
  // 去会员等级首页
  goMemberIndex() {
    wx.navigateTo({
      url: `/module_member/member/index?level=${this.data.memberLevelInfo.gradeId}`
    })
  },
  // 去登录
  goLogin() {
    wx.navigateTo({
      url: "/pages/userLogin/index?type=1",
    });
  },
  // 未登录关注公众号
  goMemberOfficial() {
    wx.navigateTo({
      url: `/pages/webview/index?url=${'https://mp.weixin.qq.com/s/NnleiSczHRGbka7lN88-mA'}`
    })
  },
  // 未登录去社群
  goCommunity() {
    wx.navigateTo({
      url: `/pages/user/community/index`
    })
  },
  // 获取模板消息ID
  getTemplateCode(code) {
    const {
      templateList
    } = this.data;
    let list = [];
    try {
      list = templateList.find((e) => e.code == code) ?
        templateList
          .find((e) => e.code == code)
          .templateList.map((v) => v.templateId) : [];
    } catch (e) {
      list = [];
    }
    return list;
  },
  // 跳转订单列表
  async goOrder() {
    try {
      wx.uma.trackEvent("member_orders");
      const subStatus = getSubState("orderList");
      const templateIds = this.getTemplateCode("orderList");
      if (subStatus && templateIds && templateIds.length) {
        const successList = await subscribeMessage(templateIds, "orderList");
        if (successList && successList.length) {
          httpResource("sendMessage", {
            code: "orderList",
            templateCodeList: successList,
          });
        }

      }
    } finally {
      wx.navigateTo({
        url: "/module_order/orderList/orderList",
      });
    }
  },
  // 跳转优惠券列表
  async goCoupon(e) {
    const type = e.currentTarget.dataset.type;
    try {
      wx.uma.trackEvent(type === 1 ? "member_coupon" : "member_coupons");
      const subStatus = getSubState("couponList");
      const templateIds = this.getTemplateCode("couponList");
      if (subStatus && templateIds && templateIds.length) {
        const successList = await subscribeMessage(templateIds, "couponList");
        if (successList && successList.length) {
          httpResource("sendMessage", {
            code: "couponList",
            templateCodeList: successList,
          });
        }

      }
    } finally {
      wx.navigateTo({
        url: "/pages/user/coupon/index",
      });
    }
  },
  // 跳转能量页
  goEnergy() {
    wx.uma.trackEvent("member_medal");
    wx.navigateTo({
      url: "/pages/user/energy/index",
    });
  },
  // 支付页面
  goCode() {
    wx.navigateTo({
      url: "/module_base/code/index",
    });
  },
  // 用户信息
  goInfo() {
    wx.uma.trackEvent("member_photo");
    wx.navigateTo({
      url: "/pages/user/userInfo/index",
    });
  },
  // 首页接口数据 bol是否更新勋章数据
  getIndexData() {
    const userInfo = wx.getStorageSync("userInfo");
    this.setData({
      userInfo,
    });
    // userMergeInfo
    httpResource(
      "userMergeInfo", {},
      (data) => {
        let {
          memberExtendVO,
          couponVOList,
          memberMedalRelationVO,
          thirdCouponVOList,
          couponCount,
        } = data;
        // 用户扩展信息
        if (memberExtendVO) {
          this.setData({
            userLike: memberExtendVO,
            userInfo,
          },
            this.setUserInfo(memberExtendVO, userInfo)
          );
        }
        let couponList = [];
        let modelList = [];
        // 平台券
        if (couponVOList && couponVOList.length) {
          couponVOList = couponVOList.map(setCouponDetailInfo);
          couponList = [...couponList, ...couponVOList];
        }
        // 第三方券
        if (thirdCouponVOList && thirdCouponVOList.length) {
          thirdCouponVOList = thirdCouponVOList.map(setCouponDetailInfo);
          couponList = [...couponList, ...thirdCouponVOList];
        }
        // 默认使用3条数据
        if (couponList && couponList.length > 0) {
          couponList = couponList.slice(0, 3);
        }
        this.setData({
          couponList,
          couponLength: couponCount || 0,
        });
      },
      "GET",
      null
    );
    this.getNextMedal(userInfo);
    this.getTemplateList();
  },
  setUserInfo(data, userInfo) {
    wx.setStorageSync("userInfo", {
      ...userInfo,
      ...data,
      userInfoId: data.id,
    });
  },
  // 获取模板消息列表
  async getTemplateList() {
    const templateList = await httpResource("getTemplateList", {}, null, "GET");
    this.setData({
      templateList: templateList || [],
    });
  },
  getreceivingNoteList() {
    const userInfo = wx.getStorageSync("userInfo");
    httpResource("receivingNoteList", {
      userId: userInfo.userId
    }, (data) => {
      console.log(data);
      data.forEach((item, index) => {
        setTimeout(() => {
          createQrcode2('qrcode' + "_" + index, item.receivingCode, pxTorpx(200), pxTorpx(200), this);
        }, 200)
      })
      this.setData({
        selfGetList: data
      })
    }, "GET", null)

  },
  // 订单点击事件
  orderTap(e) {
    const tag = e.currentTarget.dataset.tag;
    switch (tag) {
      case "退款/售后":
        wx.uma.trackEvent("member_refund");
        wx.navigateTo({
          url: '/module_order/refundList/refundList',
        });
        break;
      case "待付款":
        wx.navigateTo({
          url: `/module_order/orderList/orderList?orderType=${4}`,
        });
        break;
      case "待送达":
        wx.uma.trackEvent("member_wait");
        wx.navigateTo({
          url: `/module_order/orderList/orderList?orderType=${5}`,
        });
        break;
      case "待取货":
        wx.navigateTo({
          url: `/module_order/orderList/orderList?orderType=${6}`,
        });
        break;
      case "待评价":
        this.evaluateClick();
        break;
      default:
        break;
    }
  },
  // 待评价事件
  async evaluateClick() {
    wx.uma.trackEvent("member_evaluate");
    try {
      const subStatus = getSubState("evaluate");
      const templateIds = this.getTemplateCode("evaluate");
      if (subStatus && templateIds && templateIds.length) {
        const successList = await subscribeMessage(templateIds, "evaluate");
        if (successList && successList.length) {
          httpResource("sendMessage", {
            code: "evaluate",
            templateCodeList: successList,
          });
        }
        wx.navigateTo({
          url: `/module_order/orderList/orderList?orderType=${2}`,
        });
        return
      }
      wx.navigateTo({
        url: `/module_order/orderList/orderList?orderType=${2}`,
      });
    } catch {
      wx.navigateTo({
        url: `/module_order/orderList/orderList?orderType=${2}`,
      });
    }
  },
  // 获取顶部勋章信息
  async getNextMedal(userInfo) {
    let memId = "";
    if (userInfo && userInfo.userId) {
      memId = userInfo.userId;
    }
    const data = await httpResource("getNextMedal", {
      memId
    }, "", "GET");
    let headerInfo = data || {};
    this.setData({
      headerInfo: headerInfo,
    });
  },
  // 获取配置页面数据
  async getPageModules() {

    const data = await httpResource(
      "miniappPage", {
      pageCode: "mine"
    },
      null,
      "GET"
    );

    if (data.pageModules && data.pageModules.length) {
      const list = setModulesData(data.pageModules);
      let sortList = [];
      let orderTabList = [];
      let utilsTabList = [];
      let lanmuList = [];
      let delayTime = 5000;
      let messageContent = "";
      sortList = list
        .map((item) => {
          if (item.moduleName === "消息配置") {
            messageContent = item.messageContent || "";
          }
          if (item.moduleName === "我的订单") {
            orderTabList = (item.data || []).filter((v) => v.showStatus == 0);
          }
          if (item.moduleName === "我的工具栏") {
            utilsTabList = (item.data || []).filter((v) => v.showStatus == 0);
          }
          if (item.moduleName === "栏目位" && item.showStatus == 0) {
            lanmuList = (item.iconList || []).filter((v) => v.showStatus == 0);
          }
          if (item.moduleName === "banner模块") {
            delayTime = Math.ceil(item.timeNums * 1000);
          }
          if (item.sortNo || item.moduleName === "我的工具栏") {
            return item;
          }
        })
        .filter((e) => !!e);
      const filterLabel = ['我的优惠券模块', 'banner模块', '低碳生活模块', '栏目位', '我的工具栏'];
      let swiperList = sortList.filter(item => !filterLabel.includes(item.moduleName));
      const shopCode = app.globalData.currentStore.shopCode;
      if (shopCode) {
        try {
          const imgList = await httpResource('listShopRangeModule', { shopCode, pageCode: 'mine' }, null, "GET", null, '', false);
          if (imgList && imgList.length) {
            swiperList = [...imgList, ...swiperList];
          }
          const iconList = await this.listIconByShopRangeModuleFn(shopCode);
          if (iconList && iconList.length) {
            lanmuList = iconList;
          }
        } catch (error) {

        }
      }
      this.setData({
        delayTime,
        sortList,
        orderTabList,
        utilsTabList,
        messageContent,
        swiperList,
        lanmuList,
        swiperCurrent: 0
      }, () => {
        // 订单数量
        this.getOrderCount();
      });
    }
  },
  // 3:退货
  // 待评价数量
  async getOrderCount() {
    const data = await httpResource("orderListCount", {
      orderTypeList: [2, 3, 4, 5, 6]
    });
    if (data && data.length) {
      let arr = [...this.data.orderTabList]
      for (let i = 0; i < arr.length; i++) {
        if (arr[i].name == '待付款') {
          let item = data.find((e) => e.orderType == 4);
          arr[i].count = item.count
        }
        if (arr[i].name == '待送达') {
          let item = data.find((e) => e.orderType == 5);
          arr[i].count = item.count
        }
        if (arr[i].name == '待取货') {
          let item = data.find((e) => e.orderType == 6);
          arr[i].count = item.count | 0
        }
        if (arr[i].name == '待评价') {
          let item = data.find((e) => e.orderType == 2);
          console.log(item);
          console.log(arr[i]);
          arr[i].count = item.count
        }
        if (arr[i].name == '退款/售后') {
          let item = data.find((e) => e.orderType == 3);
          arr[i].count = item.count
        }
      }
      this.setData({
        orderTabList: arr
      })
    }
  },
  // 获取礼品卡额度
  async getBalanceMoney() {
    try {
      const data = await httpResource("balance", {}, null, "GET");
      if (data && data.balance) {
        this.setData({ balanceMoney: data.balance })
      }
    } catch (error) {
      this.setData({ balanceMoney: 0 })
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.setData({
      isLoadAnimation: false
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const path = setSharePagePath('/pages/index/index', app, 'mine');
    return {
      title: `为你推荐一家宝藏折扣店，全场1折起！`,
      path,
      imageUrl: "https://img-cdn.hotmaxx.cn/web/common/mp-store/common/share-base-img.jpg",
    }
  }
})