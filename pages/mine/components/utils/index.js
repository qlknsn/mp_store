var app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    list: {
      type: Array,
      value: [],
      observer(value) {

        if (value.length) {
          this.setData({
            utilsTop: value.filter((v, index) => index % 2 === 0),
            utilsBottom: value.filter((v, index) => index % 2 === 1),
          }, this.getRatio)
        }
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    utilsTop: [],
    utilsBottom: [],
    slideWidth: '', //滑块宽
    slideLeft: 0, //滑块位置
    totalLength: '', //当前滚动列表总长
    slideShow: false,
    slideRatio: '',
  },
  lifetimes: {
    attached: function () {
      this.getWidth()
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getWidth() {
      let _this = this;
      let systemInfo = wx.getSystemInfoSync();
      _this.setData({
        windowWidth: systemInfo.windowWidth,
      });
      return systemInfo.windowWidth;
    },
    //获取比例
    getRatio() {
      const _this = this;
      if (!_this.data.utilsTop || _this.data.utilsTop.length < 5) {
        this.setData({
          slideShow: false
        })
      } else {
        //分类列表总长度
        let _totalLength = _this.data.utilsTop.length * 175.5;
        console.log(this.data.windowWidth);
        //滚动列表长度与滑条长度比例
        let _ratio = 48 / _totalLength * (750 / (this.data.windowWidth || this.getWidth()));
        //当前显示红色滑条的长度(保留两位小数)
        let _showLength = 750 / _totalLength * 48;
        this.setData({
          slideWidth: _showLength,
          totalLength: _totalLength,
          slideShow: true,
          slideRatio: _ratio
        })
      }
    },
    //slideLeft动态变化
    getleft(e) {
      this.setData({
        slideLeft: Math.ceil(e.detail.scrollLeft * this.data.slideRatio)
      })
    },
    //点击跳转
    commonTap: function (event) {
      this.triggerEvent('tap', event.currentTarget.dataset);
    },
  }
})
