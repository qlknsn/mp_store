const { httpResource } = require('../../../../assets/js/common.js');
import {
  getProductDetail
} from "../../../../assets/js/commonData";
const app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    height: { type: Number, value: 600 },
    noLikesBarcode: {
      type: String, value: '',
      observer(newVal) {
        const { goodsList } = this.data;
        const index = goodsList.findIndex(e => e.barcode = newVal);
        if (index > -1) {
          this.setData({
            [`goodsList[${index}].isFavorite`]: false,
          })
        }
      }
    },
    likesBarcode: {
      type: String, value: '',
      observer(newVal) {
        const { goodsList } = this.data;
        const index = goodsList.findIndex(e => e.barcode = newVal);
        if (index > -1) {
          this.setData({
            [`goodsList[${index}].isFavorite`]: true,
          })
        }
      }
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    defaultImg2: 'https://img.hotmaxx.cn/web/common/mp-store/home-index/goods-default.png',
    refresher: false,
    joshinList: [],
    shopCode: null,
    selectTaber: null,//自提，外卖
  },
  ready: function () {
    let _this = this;
    let SELECT_TABER = wx.getStorageSync('SELECT_TABER');
    _this.setData({
      shopCode: app.globalData.currentStore.shopCode,
      selectTaber: SELECT_TABER
    }, () => {
      _this.getNewGoods();
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 加入购物车
    async addCart(e) {
      const status = this.checkLogin();
      const { selectTaber } = this.data;
      if (!status) return;
      try {
        wx.hideToast();
      } catch (error) { }
      if (selectTaber == 'WAI_MAI') {
        if (!app.globalData.deliverableAddress.addrId) {
          wx.showToast({
            title: '请先选择地址哦',
            duration: 1000,
            icon: 'none'
          })
          return
        }
      }
      const shopCode = app.globalData.currentStore.shopCode;
      if (!shopCode) {
        wx.showToast({
          title: '请先选择门店哦',
          duration: 1000,
          icon: 'none'
        })
        return
      }
      const { item } = e.currentTarget.dataset;
      const { barcode: packageCode, goodsName, goodsCode, coverImage, salePrice, marketPrice, categoryCodeFirst: categoryOneCode,
        categoryCodeSecond: categoryTwoCode,
        categoryCodeThird: categoryThreeCode,
      } = item;
      const params = JSON.parse(
        JSON.stringify({
          categoryTwoCode,
          categoryThreeCode,
          version: 2,
          shopCode,
          packageCode,
          goodsName, goodsCode, coverImage, salePrice, marketPrice, categoryOneCode,
          addType: 2,
          receiveType: selectTaber == 'ZI_TI' ? 3 : 4
        }))
      let data = await httpResource('cartAdd2', params);
      if (data && data.extraMsg) {
        wx.showToast({
          title: data.extraMsg,
          icon: "none",
          duration: 3000,
        });
      } else {
        wx.showToast({
          title: "加入成功",
          icon: "none",
          duration: 1000,
        });
      }
      const num = await httpResource('cartCount', { shopCode });
      app.globalData.cartNum = num;
    },
    // 下拉刷新
    bindrefresherrefresh() {
      this.setData({
        refresher: true
      }, () => {
        setTimeout(() => {
          this.getNewGoods()
        }, 100)
      })
    },
    // 商品数据
    async getNewGoods() {
      let _this = this;
      const shopCode = app.globalData.currentStore.shopCode;
      const parasm = JSON.parse(JSON.stringify({
        shopCode, type: 2, goodType: 2, currentPage: 1,
        pageSize: 50
      }));
      try {
        const data = await httpResource('dayGoods', parasm);
        if (data && data.records) {
          const yesterdayGoods = (data.records || []).map(getProductDetail);
          _this.setData({
            refresher: false,
            joshinList: yesterdayGoods,
          })
        }
      } catch (error) {
        _this.setData({
          refresher: false,
          joshinList: [],
        })
      }
    },
    // 是否登录 未登录去登录
    checkLogin() {
      const isLogin = app.globalData.isLogin;
      if (!isLogin) {
        wx.navigateTo({
          url: "/pages/userLogin/index?type=1"
        })
      }
      return isLogin;
    },
    // 喜欢
    async likeChange(e) {
      const _this = this;
      const status = _this.checkLogin();
      if (!status) { return; }
      // 是否喜欢
      const { favorite: isFavorite, barcode, index } = e.currentTarget.dataset;
      const api = isFavorite ? 'unFavorite' : 'favorite';
      await httpResource(api, { barcode }, null, "GET");
      _this.setData({
        [`joshinList[${index}].isFavorite`]: !isFavorite,
      })
    },
    share() {

    },
    goDetail(e) {
      // const status = this.checkLogin();
      // if (!status) {
      //   return;
      // }
      // if (!app.globalData.isLogin) return;
      if (!app.globalData.currentStore.shopCode) return;
      const barcode = e.currentTarget.dataset.barcode;
      wx.navigateTo({
        url: `/pages/productDetail/index?barcode=${barcode}`
      })
    }
  }
})
