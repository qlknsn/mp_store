const { httpResource } = require("../../../../assets/js/common.js");
const {
  getProductDetail
} = require("../../../../assets/js/commonData.js");
var app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    height: { type: Number, value: 600 },
    noLikesBarcode: {
      type: String, value: '',
      observer(newVal) {
        const { goodsList } = this.data;
        const index = goodsList.findIndex(e => e.barcode = newVal);
        if (index > -1) {
          this.setData({
            [`goodsList[${index}].isFavorite`]: false,
          })
        }
      }
    },
    likesBarcode: {
      type: String, value: '',
      observer(newVal) {
        const { goodsList } = this.data;
        const index = goodsList.findIndex(e => e.barcode = newVal);
        if (index > -1) {
          this.setData({
            [`goodsList[${index}].isFavorite`]: true,
          })
        }
      }
    },
  },
  /**
   * 组件的初始数据
   */
  data: {
    defaultImg2: 'https://img.hotmaxx.cn/web/common/mp-store/home-index/goods-default.png',
    refresher: false,
    loading: false,
    noMore: false,
    loadingFailed: false,
    currentPage: 1,
    goodsList: [],
    shopCode: null,
    selectTaber: null,//自提，外卖
  },

  ready: function () {
    let _this = this;
    let SELECT_TABER = wx.getStorageSync('SELECT_TABER');
    _this.setData({
      shopCode: app.globalData.currentStore.shopCode,
      selectTaber: SELECT_TABER
    }, () => {
      _this.getAllGoods();
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 加入购物车
    async addCart(e) {
      const status = this.checkLogin();
      const { selectTaber } = this.data;
      if (!status) return;
      try {
        wx.hideToast();
      } catch (error) { }
      if (selectTaber == 'WAI_MAI') {
        if (!app.globalData.deliverableAddress.addrId) {
          wx.showToast({
            title: '请先选择地址哦',
            duration: 1000,
            icon: 'none'
          })
          return
        }
      }
      const shopCode = app.globalData.currentStore.shopCode;
      if (!shopCode) {
        wx.showToast({
          title: '请先选择门店哦',
          duration: 1000,
          icon: 'none'
        })
        return
      }
      const { item } = e.currentTarget.dataset;
      const { barcode: packageCode, goodsName, goodsCode, coverImage, salePrice, marketPrice, categoryCodeFirst: categoryOneCode,
        categoryCodeSecond: categoryTwoCode,
        categoryCodeThird: categoryThreeCode,
      } = item;
      const params = JSON.parse(
        JSON.stringify({
          categoryTwoCode,
          categoryThreeCode,
          version: 2,
          shopCode,
          packageCode,
          goodsName, goodsCode, coverImage, salePrice, marketPrice, categoryOneCode,
          addType: 2,
          receiveType: selectTaber == 'ZI_TI' ? 3 : 4
        }))
      let data = await httpResource('cartAdd2', params);
      if (data && data.extraMsg) {
        wx.showToast({
          title: data.extraMsg,
          icon: "none",
          duration: 3000,
        });
      } else {
        wx.showToast({
          title: "加入成功",
          icon: "none",
          duration: 1000,
        });
      }
      const num = await httpResource('cartCount', { shopCode });
      app.globalData.cartNum = num;
    },
    // 下拉刷新
    bindrefresherrefresh() {
      this.setData({
        refresher: true,
        currentPage: 1,
      }, () => {
        setTimeout(() => {
          this.getAllGoods()
        }, 100)
      })
    },
    getAllGoods: function () {
      let _this = this;
      const shopCode = app.globalData.currentStore.shopCode;
      let obj = JSON.parse(JSON.stringify({
        shopCode, currentPage: _this.data.currentPage, pageSize: 20,
      }));
      const res = httpResource("sendHome", obj,);
      res.then((res) => {
        let data = (res.records || []).map(getProductDetail);
        _this.setData({ loading: false, noMore: false });
        if (data && data.length != 0) {
          if (_this.data.currentPage == 1) {
            _this.setData({
              goodsList: data,
              refresher: false
            });
          } else {
            if (_this.data.goodsList.length < res.total) {
              _this.setData({
                goodsList: _this.data.goodsList.concat(data),
                refresher: false
              })
            }
            if (_this.data.goodsList.length == res.total) {
              _this.setData({
                noMore: true,
              })
            }
          }
        } else {
          if (_this.data.currentPage == 1) {
            _this.setData({
              goodsList: [],
            })
          }
          _this.setData({
            noMore: _this.data.currentPage == 1 ? false : this.data.goodsList.length ? true : false,
            refresher: false
          })
        }
        //如果返回的数据为空，那么就没有下一页了
        if (res.records.length == 0) {
          _this.setData({
            isDefault: true,
            refresher: false
          })
        }
      })
    },
    //到达底部
    scrollToLower: function (e) {
      let _this = this;
      if (!_this.data.loading && !_this.data.noMore) {
        _this.setData({
          loading: true,
          currentPage: _this.data.currentPage + 1,
        }, () => {
          _this.getAllGoods();
        })
      }
    },
    // 是否登录 未登录去登录
    checkLogin() {
      const isLogin = app.globalData.isLogin;
      if (!isLogin) {
        wx.navigateTo({
          url: "/pages/userLogin/index?type=1"
        })
      }
      return isLogin;
    },
    // 喜欢
    async likeChange(e) {
      const _this = this;
      const status = _this.checkLogin();
      if (!status) { return; }
      // 是否喜欢
      const { favorite: isFavorite, barcode, index } = e.currentTarget.dataset;
      const api = isFavorite ? 'unFavorite' : 'favorite';
      await httpResource(api, { barcode }, null, "GET");
      _this.setData({
        [`goodsList[${index}].isFavorite`]: !isFavorite,
      })
    },
    share() {

    },
    goDetail(e) {
      // const status = this.checkLogin();
      // if (!status) {
      //   return;
      // }
      // if (!app.globalData.isLogin) return;
      if (!app.globalData.currentStore.shopCode) return;
      const barcode = e.currentTarget.dataset.barcode;
      wx.navigateTo({
        url: `/pages/productDetail/index?barcode=${barcode}`
      })
    }
  }
})
