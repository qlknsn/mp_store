const app = getApp();
const { httpResource } = require("../../assets/js/common.js");
Page({
  /**
   * 页面的初始数据
   */
  data: {
    navHeight: app.globalData.navHeight,
    navbarData: {
      home: '1'
    },
    tabTitleArr: [
      { tabTitle: '优选好物', navIndex: 0 },
      { tabTitle: '每日上新', navIndex: 1 },
      // { tabTitle: '我喜欢', navIndex: 2 },
    ],
    navIndex: 0,
    tabLeft: "64",
    currentPage: 1,
    goodsList: [],
    joshinList: [],
    shopCode: null,
    loading: false,
    noMore: false,
    loadingFailed: false,
    contentHeight: 0,
    noLikesBarcode: '',
    likesBarcode: '',
    selectTaber: null,//自提，外卖
  },
  getHeight() {
    const query = wx.createSelectorQuery();
    query.select('.header-bg').boundingClientRect()
    query.select('.goods-tab').boundingClientRect()
    query.exec((res) => {
      const height = app.globalData.screenHeight - res[0].height - res[1].height + 10;
      this.setData({
        contentHeight: height
      })
      console.log(height)
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.navIndex) {
      this.setData({
        navIndex: options.navIndex
      }, () => {
        this.changeline();
      })
    }
    const data = wx.getSystemInfoSync();
    this.setData({
      moveViewLeft: 0,
      moveViewTop: data.windowHeight - 100,
      shopCode: app.globalData.currentStore.shopCode
    })
  },
  //tab切换
  changeTab: function (e) {
    this.setData({
      navIndex: e.currentTarget.dataset.index
    })
    this.changeline()
  },
  changeline: function () {
    this.setData({
      tabLeft: this.data.navIndex == 1 ? 324 : 64
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */

  onReady: function () {
    this.getHeight()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let SELECT_TABER = wx.getStorageSync('SELECT_TABER');
    this.setData({
      selectTaber: SELECT_TABER
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('触底')
  },
  async shareCount(params) {
    try {
      await httpResource('shareNum', params)
    } catch (error) {
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (e) {
    if (e.from === "button") {
      const { img: imageUrl, item: { goodsName, goodsCode, barcode, marketPrice, salePrice: price } } = e.target.dataset;
      const shopCode = app.globalData.currentStore.shopCode || 0;
      const { navIndex } = this.data;
      wx.uma.trackEvent(navIndex == 0 ? 'home_share' : 'new_share', {
        goods_code: goodsCode,
        goods_name: goodsName,
      });
      this.shareCount({ shopCode, goodsName, marketPrice, price, barcode });
      return {
        title: goodsName,
        path: "/pages/index/index",
        imageUrl,
      };
    }
    return {
      title: `为你推荐一家宝藏折扣店，全场1折起！`,
      path: "/pages/index/index",
      imageUrl: "https://img-cdn.hotmaxx.cn/web/common/mp-store/common/share-base-img.jpg",
    }
  }
})