// pages/userIndex/index.js
var app = getApp();
import {
  setCouponDetailInfo,
  getUserLocation,
  setModulesData,
} from "../../assets/js/commonData";
import {
  subscribeMessage,
  getSubState
} from "../../config/subConfig";
import {
  createBarcode,
  createQrcode2
} from '../../assets/js/codeUtil';
const {
  isNeedAutho,
  httpResource,
  getCode,
  Subtr,
  accAdd,
  pxTorpx
} = require("../../assets/js/common.js");
const {
  throttl
} = require("../../assets/js/eventUtil");
Page({
  /**
   * 页面的初始数据
   */
  data: {
    isLogin: app.globalData.isLogin,
    navH: app.globalData.navHeight,
    navbarData: {
      class: "0",
      home: 1,
      title: "会员中心",
    },
    userLike: {}, // 用户资料 是否晚上信息 控制信息显示
    couponLength: 0, // 优惠券数量
    couponList: [], // 推荐的优惠券
    modelList: [], // 已获取的勋章
    headerInfo: {}, // 顶部勋章信息
    storeInfo: {}, // 附近10km门店信息
    lcode: null,
    respones: false,
    // 登录相关
    userInfo: {}, // 用户基础信息
    showLoginStatus: false,
    // bavbar 透明度
    opcity: 0,
    options: {},
    // 领取勋章
    showModelList: [],
    showMedal: false,
    isLightUp: false,
    rotate: "",
    // 点亮动画结束
    rotateEnd: false,
    activeImgEnd: false, // 点亮翻转动画完成
    // 领取优惠券
    showCoupon: false,
    showCouponList: [],
    // 模板消息列表
    templateList: [],
    // 页面配置相关
    orderTabList: [],
    sortList: [],
    delayTime: 5000,
    utilsTabList: [],
    messageContent: "",
    scrollable: true,
    showStoreTip: true, // 未授权提示状态
    orderEvaluationNum: 0, // 待评价数量
    favoriteCount: 0, // 喜欢的数量

    // 轮播
    background: ['demo-text-1', 'demo-text-2', 'demo-text-3'],
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 5000,
    duration: 500,
    modelAnimation: null, // 勋章动画
    // 轮播数据
    swiperList: [],
    swiperCurrent: 0,
    // 待取货轮播
    selfGetList: [],
    balanceMoney: '',
  },
  getreceivingNoteList() {
    if (!app.globalData.isLogin) return;
    // receivingNoteList
    const userInfo = wx.getStorageSync("userInfo");

    httpResource("receivingNoteList", {
      userId: userInfo.userId
    }, (data) => {
      // data = [{
      //   orderStatus: 2,
      //   orderStatusStr: '',
      //   shopCode: '',
      //   shopName: '七宝宝龙店',
      //   qty: '2',
      //   receivingCode: 'AHTM337389029783492087',
      //   qrcode: ""
      // }]
      console.log(data);
      data.forEach((item, index) => {
        setTimeout(() => {
          createQrcode2('qrcode' + "_" + index, item.receivingCode, pxTorpx(200), pxTorpx(200), this);
        }, 200)
      })
      // console.log(data);
      this.setData({
        selfGetList: data
      })
    }, "GET", null)

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getreceivingNoteList()
    this.setData({
      options,
    });

  },
  //清空弹框信息
  resetStatus() {
    this.setData({
      rotateEnd: false,
      rotate: "",
      isLightUp: false,
      showCouponList: [],
    },
      this.getIndexData
    );
  },
  // 关闭优惠券model
  closeCoupon() {
    this.setData({
      showCoupon: false,
    },
      this.resetStatus
    );
  },
  // 关闭勋章弹框并打开优惠券弹框
  openCoupon() {
    const {
      showCouponList
    } = this.data;
    if (showCouponList.length) {
      this.setData({
        showMedal: false,
        showCoupon: true,
      });

    } else {
      this.setData({
        showMedal: false,
      },
        this.getIndexData
      );
    }
  },
  // 关闭勋章弹框
  async closeMedal() {
    const {
      showCouponList
    } = this.data;
    if (showCouponList.length) {
      this.setData({
        showMedal: false,
        showCoupon: true,
      },
        this.getIndexData
      );
    } else {
      this.setData({
        showMedal: false,
      });
    }
  },
  // 获取配置页面数据
  async getPageModules() {

    const data = await httpResource(
      "miniappPage", {
      pageCode: "mine"
    },
      null,
      "GET"
    );

    if (data.pageModules && data.pageModules.length) {
      const list = setModulesData(data.pageModules);
      let sortList = [];
      let orderTabList = [];
      let utilsTabList = [];
      let delayTime = 5000;
      let messageContent = "";
      sortList = list
        .map((item) => {
          if (item.moduleName === "消息配置") {
            messageContent = item.messageContent || "";
          }
          if (item.moduleName === "我的订单") {
            orderTabList = (item.data || []).filter((v) => v.showStatus == 0);
          }
          if (item.moduleName === "我的工具栏") {
            utilsTabList = (item.data || []).filter((v) => v.showStatus == 0);
          }
          if (item.moduleName === "banner模块") {
            delayTime = Math.floor(item.timeNums * 1000);
          }
          if (item.sortNo) {
            return item;
          }
        })
        .filter((e) => !!e);
      const filterLabel = ['我的优惠券模块', 'banner模块', '低碳生活模块'];
      let swiperList = sortList.filter(item => !filterLabel.includes(item.moduleName));
      const shopCode = app.globalData.currentStore.shopCode;
      if (shopCode) {
        try {
          const imgList = await httpResource('listShopRangeModule', { shopCode, pageCode: 'mine' }, null, "GET", null, '', false);
          if (imgList && imgList.length) {
            swiperList = [...imgList, ...swiperList];
          }
        } catch (error) {

        }
      }
      this.setData({
        delayTime,
        sortList,
        orderTabList,
        utilsTabList,
        messageContent,
        swiperList,
        swiperCurrent: 0
      }, () => {
        // 订单数量
        this.getOrderCount();
      });
    }
  },
  // 3:退货
  // 待评价数量
  async getOrderCount() {
    const data = await httpResource("orderListCount", {
      orderTypeList: [2, 3, 4, 5, 6]
    });
    if (data && data.length) {
      let arr = [...this.data.orderTabList]
      for (let i = 0; i < arr.length; i++) {
        if (arr[i].name == '待付款') {
          let item = data.find((e) => e.orderType == 4);
          arr[i].count = item.count
        }
        if (arr[i].name == '待送达') {
          let item = data.find((e) => e.orderType == 5);
          arr[i].count = item.count
        }
        if (arr[i].name == '待取货') {
          let item = data.find((e) => e.orderType == 6);
          arr[i].count = item.count | 0
        }
        if (arr[i].name == '待评价') {
          let item = data.find((e) => e.orderType == 2);
          console.log(item);
          console.log(arr[i]);
          arr[i].count = item.count
        }
        if (arr[i].name == '退款/售后') {
          let item = data.find((e) => e.orderType == 3);
          arr[i].count = item.count
        }
      }
      this.setData({
        orderTabList: arr
      })
      // 过滤类型
      // const item = data.find((e) => e.orderType == 2);
      // if (item && item.count > 0) {
      //   this.setData({
      //     orderEvaluationNum: item.count,
      //   });
      // } else {
      //   this.setData({
      //     orderEvaluationNum: 0,
      //   });
      // }
    }
  },
  // 喜欢的商品数量
  async getFavoriteCount() {
    let favoriteCount = 0;
    try {
      favoriteCount = await httpResource("favoriteGoodsCount", {
        orderTypeList: [2]
      }, null, "GET");
    } catch (error) { }
    this.setData({
      favoriteCount
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () { },
  // 优惠券详情
  goDetail(e) {
    const couponCode = e.currentTarget.dataset.code;
    wx.navigateTo({
      url: `/pages/user/coupon/detail/index?statusIndex=${0}&couponCode=${couponCode}`,
    });
  },
  // 跳转订单列表
  async goOrder() {
    try {
      wx.uma.trackEvent("member_orders");
      const subStatus = getSubState("orderList");
      const templateIds = this.getTemplateCode("orderList");
      if (subStatus && templateIds && templateIds.length) {
        const successList = await subscribeMessage(templateIds, "orderList");
        if (successList && successList.length) {
          httpResource("sendMessage", {
            code: "orderList",
            templateCodeList: successList,
          });
        }

      }
    } finally {
      wx.navigateTo({
        url: "/module_order/orderList/orderList",
      });
    }
  },
  // 跳转优惠券列表
  async goCoupon(e) {
    const type = e.currentTarget.dataset.type;
    try {
      wx.uma.trackEvent(type === 1 ? "member_coupon" : "member_coupons");
      const subStatus = getSubState("couponList");
      const templateIds = this.getTemplateCode("couponList");
      if (subStatus && templateIds && templateIds.length) {
        const successList = await subscribeMessage(templateIds, "couponList");
        if (successList && successList.length) {
          httpResource("sendMessage", {
            code: "couponList",
            templateCodeList: successList,
          });
        }

      }
    } finally {
      wx.navigateTo({
        url: "/pages/user/coupon/index",
      });
    }
  },
  // 跳转勋章列表
  async goMedal() {
    try {
      wx.uma.trackEvent("member_medal");
      const subStatus = getSubState("medalList");
      const templateIds = this.getTemplateCode("medalList");
      if (subStatus && templateIds && templateIds.length) {
        const successList = await subscribeMessage(templateIds, "medalList");
        if (successList && successList.length) {
          httpResource("sendMessage", {
            code: "medalList",
            templateCodeList: successList,
          });
        }

      }
    } finally {
      wx.navigateTo({
        url: "/pages/user/medal/index",
      });
    }
  },
  // 打电话
  callPhone() {
    wx.makePhoneCall({
      phoneNumber: "4009659166",
    });
  },
  // 用户信息
  goInfo() {
    wx.uma.trackEvent("member_photo");
    wx.navigateTo({
      url: "/pages/user/userInfo/index",
    });
  },
  // 跳转能量页
  goEnergy() {
    wx.uma.trackEvent("member_medal");
    wx.navigateTo({
      url: "/pages/user/energy/index",
    });
  },
  goStore() {
    wx.navigateTo({
      url: "/pages/map/index",
    });
  },
  goCode() {
    wx.navigateTo({
      url: "/module_base/code/index",
    });
  },
  // 自定义tabbar
  getTabbar() {
    if (typeof this.getTabBar === "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 3,
      });
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getTabbar();
    this.setData({
      isLogin: app.globalData.isLogin
    })
    isNeedAutho()
      .then(() => {
        app.globalData.showLoginStatus = true;
        // 页面配置
        this.getPageModules();
        // 获取礼品额度
        this.getBalanceMoney();
        this.setData({
          activeImgEnd: false,
          scrollable: true,
          isLightUp: false,
          rotateEnd: false,
          rotate: "",
          showLoginStatus: false,
          respones: true,
        },
          this.getIndexData
        );
        //
      })
      .catch((e) => {
        // wx.setStorageSync('userInfo', {})
      });
  },
  setUserInfo(data, userInfo) {
    wx.setStorageSync("userInfo", {
      ...userInfo,
      ...data,
      userInfoId: data.id,
    });
  },
  // 首页接口数据 bol是否更新勋章数据
  getIndexData() {
    const userInfo = wx.getStorageSync("userInfo");
    this.setData({
      userInfo,
    });
    // userMergeInfo
    httpResource(
      "userMergeInfo", {},
      (data) => {
        let {
          memberExtendVO,
          couponVOList,
          memberMedalRelationVO,
          thirdCouponVOList,
          couponCount,
        } = data;
        // 用户扩展信息
        if (memberExtendVO) {
          this.setData({
            userLike: memberExtendVO,
            userInfo,
          },
            this.setUserInfo(memberExtendVO, userInfo)
          );
        }
        let couponList = [];
        let modelList = [];
        // 平台券
        if (couponVOList && couponVOList.length) {
          couponVOList = couponVOList.map(setCouponDetailInfo);
          couponList = [...couponList, ...couponVOList];
        }
        // 第三方券
        if (thirdCouponVOList && thirdCouponVOList.length) {
          thirdCouponVOList = thirdCouponVOList.map(setCouponDetailInfo);
          couponList = [...couponList, ...thirdCouponVOList];
        }
        // 默认使用3条数据
        if (couponList && couponList.length > 0) {
          couponList = couponList.slice(0, 3);
        }
        // 勋章列表
        if (memberMedalRelationVO) {
          let {
            obtainedMedalList
          } = memberMedalRelationVO;
          obtainedMedalList = obtainedMedalList || [];
          modelList = obtainedMedalList.filter((e) => e.status == 1);
          // 已领取 未点亮
          const showModelList = obtainedMedalList.filter((e) => e.status == 0);
          // 显示领取弹框
          if (showModelList.length) {
            this.setData({
              showMedal: true,
              showModelList,
            }, () => {
              // 延迟200后执行
              setTimeout(() => {
                this.lightUp();
              }, 200)
            });
          }
        }
        this.setData({
          couponList,
          couponLength: couponCount || 0,
          modelList,
        });
      },
      "GET",
      null
    );
    this.getNextMedal(userInfo);

    // 喜欢的商品数量
    this.getFavoriteCount();
    this.getTemplateList();
  },
  openSetting() {
    wx.openSetting();
  },
  // 获取顶部勋章信息
  async getNextMedal(userInfo) {
    let memId = "";
    if (userInfo && userInfo.userId) {
      memId = userInfo.userId;
    }
    const data = await httpResource("getNextMedal", {
      memId
    }, "", "GET");
    let headerInfo = data || {};
    if (headerInfo) {
      const progress = Math.ceil(
        (headerInfo.curEnergyValue * 100) /
        accAdd(headerInfo.curEnergyValue, headerInfo.diffValue || 0)
      );
      headerInfo.progress = progress > 100 ? 100 : progress;
    }
    this.setData({
      headerInfo: headerInfo,
    });
  },
  // 获取模板消息列表
  async getTemplateList() {
    const templateList = await httpResource("getTemplateList", {}, null, "GET");
    this.setData({
      templateList: templateList || [],
    });
  },
  // 获取模板消息ID
  getTemplateCode(code) {
    const {
      templateList
    } = this.data;
    let list = [];
    try {
      list = templateList.find((e) => e.code == code) ?
        templateList
          .find((e) => e.code == code)
          .templateList.map((v) => v.templateId) : [];
    } catch (e) {
      list = [];
    }
    return list;
  },
  // 订阅后通知后端
  async sendTemplateMsg(templateIds) {
    await httpResource("sendMessage", {
      templateCodeList: templateIds
    });
  },
  // 登录
  init() {
    this.onShow();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.setData({
      scrollable: false,
      activeImgEnd: false
    });
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () { },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () { },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () { },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: `为你推荐一家宝藏折扣店，全场1折起！`,
      path: "/pages/index/index",
      imageUrl: "https://img-cdn.hotmaxx.cn/web/common/mp-store/common/share-base-img.jpg",
    }
  },
  // 获取头像底部icon距离
  medalAnimation() {
    return new Promise((resolve, reject) => {
      const query = wx.createSelectorQuery();
      query.select(".member-icon").boundingClientRect();
      query.select(".model-active-img").boundingClientRect();
      query.exec((res) => {
        if (res[0] == null || res[1] == null) {
          reject(res)
        }
        const {
          top,
          left,
          width,
          height
        } = res[0];
        const {
          top: aTop,
          left: aLeft
        } = res[1];
        var Animation = wx.createAnimation({
          duration: 400,
          timingFunction: 'linear',
        });
        // 
        console.log(left - aLeft, top - aTop)
        Animation.width(width).height(height).translate((left - aLeft), (top - aTop)).step();
        this.setData({
          modelAnimation: Animation.export()
        }, () => {
          setTimeout(() => {
            this.openCoupon();
          }, 1410)
          console.log('icon信息', res, Animation)
        })
        // console.log('icon信息', res, Animation)
        resolve(res);
      });
    })
  },
  // 点亮勋章
  lightUp: throttl(async function () {
    this.receiveMedal().then((res) => {
      this.setData({
        rotate: "rotate",
        isLightUp: true,
      });
    }).catch(() => {
      this.setData({
        isLightUp: false,
      });
      wx.hideLoading();
    })
  }),
  // 领取勋章
  async receiveMedal() {
    const {
      showModelList
    } = this.data;
    let memberModalIds = "";
    if (showModelList.length == 1) {
      memberModalIds = [showModelList[0].id];
    }
    if (showModelList.length > 1) {
      memberModalIds = showModelList.map((e) => e.id);
    }
    let data = [];
    // try {
    wx.showLoading({
      title: "加载中",
    });
    data = await httpResource("receiveMedal", {
      memberModalIds
    });
    wx.hideLoading();
    // } catch {
    //   wx.hideLoading();
    // }
    this.setModelCoupon(data);
    return data;
  },
  setModelCoupon(res) {
    const showCouponList = (res || []).map(setCouponDetailInfo);
    this.setData({
      showCouponList,
    });
  },
  // 勋章动画结束
  rotateEnd() {
    this.setData({
      rotateEnd: true,
    });
  },
  rotateActiveEnd() {
    this.setData({
      activeImgEnd: true
    })
    setTimeout(() => {
      this.medalAnimation();
    }, 0)
  },
  onPageScroll(e) {
    const top = e.scrollTop;
    if (e.scrollTop >= 150) {
      if (this.data.opcity < 1) {
        this.setData({
          opcity: 1,
        });
      }
    } else {
      const opcity = e.scrollTop / 150;
      this.setData({
        opcity,
      });
    }
  },
  // 待评价事件
  async evaluateClick() {
    wx.uma.trackEvent("member_evaluate");
    try {
      const subStatus = getSubState("evaluate");
      const templateIds = this.getTemplateCode("evaluate");
      if (subStatus && templateIds && templateIds.length) {
        const successList = await subscribeMessage(templateIds, "evaluate");
        if (successList && successList.length) {
          httpResource("sendMessage", {
            code: "evaluate",
            templateCodeList: successList,
          });
        }
        wx.navigateTo({
          url: `/module_order/orderList/orderList?orderType=${2}`,
        });
        return
      }
      wx.navigateTo({
        url: `/module_order/orderList/orderList?orderType=${2}`,
      });
    } catch {
      wx.navigateTo({
        url: `/module_order/orderList/orderList?orderType=${2}`,
      });
    }
  },
  // 订单点击事件
  orderTap(e) {
    const tag = e.currentTarget.dataset.tag;
    switch (tag) {
      case "退款/售后":
        wx.uma.trackEvent("member_refund");
        wx.navigateTo({
          url: '/module_order/refundList/refundList',
        });
        break;
      case "待付款":
        wx.navigateTo({
          url: `/module_order/orderList/orderList?orderType=${4}`,
        });
        break;
      case "待送达":
        wx.uma.trackEvent("member_wait");
        wx.navigateTo({
          url: `/module_order/orderList/orderList?orderType=${5}`,
        });
        break;
      case "待取货":
        wx.navigateTo({
          url: `/module_order/orderList/orderList?orderType=${6}`,
        });
        break;
      case "待评价":
        this.evaluateClick();
        break;
      default:
        break;
    }
  },
  // 关闭提示
  closeStoreTip() {
    this.setData({
      showStoreTip: false,
    });
  },
  // 低碳/新增模块点击事件
  commonTap(e) {
    app.globalData.lotterynavigate = '/pages/mine/index'
    const {
      type,
      url,
      appid,
      isbanner,
      tag,
      isutils
    } = e.currentTarget.dataset;
    // banner图片埋点
    if (isbanner && url) {
      wx.uma.trackEvent('member_banner1', {
        'click_module': url
      })
    }
    // 工具栏埋点
    if (isutils && tag) {
      wx.uma.trackEvent('mine_toolbar', {
        'name': tag
      })
    }
    // 无
    if (type === 0) {
      return;
    }
    // 内部页面
    if (type === 1) {
      wx.navigateTo({
        url,
      });
      return;
    }
    // 外部小程序
    if (type === 2) {
      wx.navigateToMiniProgram({
        appId: appid,
        // path:url
      });
      return;
    }
    // H5
    if (type === 3) {
      wx.navigateTo({
        url: `/pages/webview/index?url=${url}`,
      });
      return;
    }
  },
  // 工具栏点击事件
  utilsTap(e) {
    const tag = e.currentTarget.dataset.tag;
    switch (tag) {
      case "关注公众号":
        wx.uma.trackEvent('member_official');
        wx.navigateTo({
          url: `/pages/webview/index?url=${'https://mp.weixin.qq.com/s/NnleiSczHRGbka7lN88-mA'}`
        })
        console.log("关注公众号");
        break;
      case "我喜欢":
        this.goProdoctList();
        console.log("我喜欢");
        break;
      case "进社群":
        wx.uma.trackEvent("member_club");
        wx.navigateTo({
          url: `/pages/user/community/index`
        })
        break;
      case "收货地址":
        wx.navigateTo({
          url: `/pages/user/address/list/index?type=1`
        })
        break;
      default:
        break;
    }
  },
  // 我喜欢的
  goProdoctList() {
    wx.uma.trackEvent("member_like");
    wx.navigateTo({
      url: '/pages/user/likeGoods/index'
    })
  },
  // 获取礼品卡额度
  async getBalanceMoney() {
    try {
      const data = await httpResource("balance", {}, null, "GET");
      if (data && data.balance) {
        this.setData({ balanceMoney: data.balance })
      }
    } catch (error) {
      this.setData({ balanceMoney: 0 })
    }
  },
  // 去礼品卡
  goBalance() {
    wx.navigateTo({
      url: '/pages/user/giftCard/index'
    })
  },
  // 去登录
  goLogin() {
    wx.navigateTo({
      url: "/pages/userLogin/index?type=1",
    });
  },
  // 未登录关注公众号
  goMemberOfficial() {
    wx.navigateTo({
      url: `/pages/webview/index?url=${'https://mp.weixin.qq.com/s/NnleiSczHRGbka7lN88-mA'}`
    })
  },
  // 未登录去社群
  goCommunity() {
    wx.navigateTo({
      url: `/pages/user/community/index`
    })
  },
});