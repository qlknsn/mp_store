const { httpResource } = require('../../assets/js/common.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: true,
    vertical: false,
    autoplay: false,
    interval: 2000,
    duration: 500,
    actId: null,//活动ID
    imagePath: null,//保存图片
    base64: null,//太阳码
    base64Url: null,//太阳码数据
    friendAdviceContent: null,//邀请语录
    swiperImgList: [],
    currentIndex: 0,//轮播图下标
    statusBar: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _this = this;
    _this.setData({
      actId: options.actId,
      statusBar: app.globalData.statusBar,
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getActivity();
    this.getCode();
  },
  //根据id查询活动图片
  async getActivity() {
    let _this = this;
    let { actId } = _this.data;
    let data = await httpResource('getActImgById', { actId: actId }, null, "get");
    if (data.actStatus == 0) {
      if (data.actStartStatus == 1) {
        if (data.imgs && data.imgs.length != 0) {
          let imagesList = [];
          data.imgs.forEach((item) => {
            if (item.imageType == 3) {
              imagesList.push(item.imgUrl);
            }
          });
          _this.setData({
            swiperImgList: imagesList,
            friendAdviceContent: data.friendAdviceContent
          });
        }
      }
    }
  },
  //获取太阳码
  async getCode() {
    let _this = this;
    let { actId } = _this.data;
    let { userId } = wx.getStorageSync("userInfo");
    let page = 'pages/index/index';
    let data = await httpResource('generateMiniQRCode', { params: { act: `${actId}_${userId}` }, page }, null, "post");
    _this.setData({
      base64: 'data:image/png;base64,' + data,
      base64Url: data,
    })
  },

  //监听轮播图
  changeImages(event) {
    let { current } = event.detail;
    this.setData({
      currentIndex: current
    });
  },

  //将canvas转换为图片保存到本地，然后将图片路径传给image图片的src
  createNewImg() {
    let that = this;
    wx.uma.trackEvent("click_save", { 'actId': this.data.actId });
    let { swiperImgList, currentIndex } = that.data;
    wx.showLoading({ title: '正在生成海报...', mask: true });
    console.log(swiperImgList[currentIndex]);
    wx.getImageInfo({
      src: swiperImgList[currentIndex],
      success: function (res) {
        console.log(res);
        let path = res.path;
        let context = wx.createCanvasContext('mycanvas');
        // 绘制主背景白色
        context.setFillStyle("#ffffff")
        context.fillRect(0, 0, 375, 667);
        context.drawImage(path, 0, 0, 375, 667);
        //绘制右下角小程序二维码
        const fsm = wx.getFileSystemManager();
        let showImgData = that.data.base64Url;//二维码数据
        showImgData = showImgData.replace(/\ +/g, "");
        showImgData = showImgData.replace(/[\r\n]/g, "");
        const buffer = wx.base64ToArrayBuffer(showImgData);
        const fileName = wx.env.USER_DATA_PATH + '/share_img.png'
        fsm.writeFileSync(fileName, buffer, 'binary')
        showImgData = fileName;  //处理完成
        context.beginPath();
        context.arc(100 / 2 + 30, 100 / 2 + 540, 100 / 2, 0, Math.PI * 2);
        context.setLineWidth(10);
        context.setStrokeStyle('#fff');
        context.stroke();//画空心圆
        context.closePath();
        context.save();
        context.arc(100 / 2 + 30, 100 / 2 + 540, 100 / 2, 0, Math.PI * 2);
        context.clip(); //裁剪上面的圆形
        context.drawImage(showImgData, 30, 540, 100, 100);
        context.draw();
        //将生成好的图片保存到本地
        setTimeout(function () {
          wx.canvasToTempFilePath({
            canvasId: 'mycanvas',
            x: 0,
            y: 0,
            destWidth: 375 * 4,
            destHeight: 667 * 4,
            success: function (res) {
              wx.hideToast();
              wx.saveImageToPhotosAlbum({
                filePath: res.tempFilePath,
                success(res) {
                  wx.hideLoading();
                  wx.showToast({
                    title: '保存成功！',
                    icon: 'none',
                    duration: 1500
                  })
                }
              })
            },
            fail: function (res) {
              wx.hideLoading();
              wx.showToast({
                title: '保存失败！',
                icon: 'none',
                duration: 1500
              })
            }
          });
        }, 200);
      },
      fail: function (res) { }
    })
  },
  //复制分享朋友圈
  copywxtap(e) {
    wx.uma.trackEvent("click_copy", { 'actId': this.data.actId });
    wx.setClipboardData({
      data: e.currentTarget.dataset.text,
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            wx.showToast({
              title: '复制成功',
              icon: 'none',
              duration: 1500
            })
          }
        })
      }
    })
  },
  //后退
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  },
})