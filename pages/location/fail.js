// pages/location/fail.js
import { getUserLocation } from "../../assets/js/commonData";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      'title': '',
      return: 1,
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  openSetting() {
    wx.openSetting({})
  },
  selectStore() {
    wx.navigateTo({
      url: '/pages/map/index'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    getUserLocation().then(() => {
      wx.navigateBack();
      wx.removeStorageSync('toLoction');
    })
  },
})