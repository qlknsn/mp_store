var app = getApp();
const {
  getCode,
  mpLogin,
  httpResource
} = require("../../assets/js/common.js");
import {
  throttl
} from '../../assets/js/eventUtil.js'

Page({

  /**
   * 页面的初始数据s
   */
  data: {
    parameter: {
      'title': '登录',
      return: 1,
    },
    isCheck: false,
  },
  changeCheck() {
    this.setData({
      isCheck: !this.data.isCheck
    })
  },
  goAgreement() {
    wx.navigateTo({
      url: `/module_base/privacyAgreement/index`,
    })
  },
  goUserAgreement() {
    wx.navigateTo({
      url: `/module_base/userAgreement/index`,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 获取微信用户信息，新API - getUserProfile
   */
  getUserPhone: throttl(async function (e) {
    if (!this.data.isCheck) {
      wx.showToast({
        title: '请先勾选协议',
        icon: 'none',
        duration: 3000,
        mask: true
      })
      return false;
    }
    if (e.detail.errMsg !== 'getPhoneNumber:ok') {
      return false;
    }
    const shopCode = wx.getStorageSync("shopCode") || '';
    const shopName = wx.getStorageSync("shopName") || '';
    const source = wx.getStorageSync("source") || '';
    const { type, actId, inviteUserId, isActivity } = app.globalData;
    try {
      const code = await getCode();
      let parms = {
        code: code,
        encreptedData: e.detail.encryptedData,
        iv: e.detail.iv,
        shopCode,
        shopName,
        phone: '',
        source,
        type,
        inviteUserId,
        actId,
        channel: 'WX'
      };
      wx.showLoading({ title: '加载中' });
      // 注册
      let data = await httpResource('userRegister', parms);
      wx.hideLoading()
      const logiCode = await getCode();
      let param = { code: logiCode, type, actId, inviteUserId, isActivity };
      mpLogin(param, () => {
        app.globalData.isLogin = true;
        wx.navigateBack()
      })
    } catch (error) {
      wx.hideLoading()
      console.log('注册错误', error)
    }
  }),
})