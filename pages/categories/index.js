// 分类数据
const categoryData = [
  {
    name: "新品",
    icon: "https://img.hotmaxx.cn/web/common/mp-store/home-category/new2.png",
    childrenList: [
      {
        name: "新品首发",
        code: 1,
      },
      {
        name: "每日上新",
        code: 2,
      },
    ],
  },
];
const { httpResource } = require("../../assets/js/common.js");
import { getProductDetail, setSharePagePath } from "../../assets/js/commonData";
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    navHeight: 0,
    shopCode: "",
    shopName: "",
    respones: false,
    defaultImg2:
      "https://img.hotmaxx.cn/web/common/mp-store/home-index/goods-default.png",
    showAll: false, // 是否显示全部分类
    childrenId: "", // 滚动位置
    twoChildrenId: "", // 二级分类滚动位置
    categoriesData: [], // 分类数据
    isCategoriesData: true, //是否有分类数据
    twoCategoriesList: [],
    likesBarcode: "",
    noLikesBarcode: "",
    cat1Code: "",
    cat1Index: 0,
    cat2Code: "",
    cat2Index: 0,
    scrollTopNum: 0,
    currentPage: 1,
    goodsList: [],
    receiveType: null, //3自提，4配送到家
    noMore: false, // 是否到底
    refresher: false, // 下拉刷新
    // 排序
    utilsIndex: 0,
    utilsList: [
      {
        title: "综合",
        value: "",
      },
      {
        title: "销量",
        value: "fall", // rise 上 fall
      },
      {
        title: "价格",
        value: "fall", // rise 上 fall
      },
    ],
  },
  // 是否登录 未登录去登录
  checkLogin() {
    const isLogin = app.globalData.isLogin;
    if (!isLogin) {
      wx.navigateTo({
        url: "/pages/userLogin/index?type=1",
      });
    }
    return isLogin;
  },
  // 加入购物车
  async addCart(e) {
    const status = this.checkLogin();
    const selectTaber = wx.getStorageSync("SELECT_TABER");
    if (!status) return;
    if (selectTaber != "ZI_TI") {
      if (!app.globalData.deliverableAddress.addrId) {
        wx.showToast({
          title: "请先选择地址哦",
          duration: 1000,
          icon: "none",
        });
        return;
      }
    }
    const shopCode = app.globalData.currentStore.shopCode;
    try {
      wx.hideToast();
    } catch (error) { }
    if (!shopCode) {
      wx.showToast({
        title: "请先选择门店哦",
        duration: 1000,
        icon: "none",
      });
      return;
    }
    const { item } = e.currentTarget.dataset;
    const {
      barcode: packageCode,
      goodsName,
      goodsCode,
      coverImage,
      salePrice,
      marketPrice,
      categoryCodeFirst: categoryOneCode, categoryCodeSecond: categoryTwoCode,
      categoryCodeThird: categoryThreeCode,
    } = item;
    const params = JSON.parse(
      JSON.stringify({
        categoryTwoCode,
        categoryThreeCode,
        version: 2,
        shopCode,
        packageCode,
        goodsName,
        goodsCode,
        coverImage,
        salePrice,
        marketPrice,
        categoryOneCode,
        addType: 2,
        receiveType: selectTaber == "ZI_TI" ? 3 : 4,
      })
    );
    let data = await httpResource("cartAdd2", params);
    if (data && data.extraMsg) {
      wx.showToast({
        title: data.extraMsg,
        icon: "none",
        duration: 3000,
      });
    } else {
      wx.showToast({
        title: "加入成功",
        icon: "none",
        duration: 1000,
      });
    }
    const num = await httpResource("cartCount", { shopCode });
    app.globalData.cartNum = num;
  },
  onClose() {
    this.setData({
      showAll: false,
    });
  },
  // 自定义tabbar
  getTabbar() {
    if (typeof this.getTabBar === "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 1,
      });
    }
  },
  // 重新加载
  reload() {
    const { categoriesData } = this.data;
    const twoCategoriesList = categoriesData[0].childrenList;
    const cat1Code = categoriesData[0].code;
    const cat2Code = categoriesData[0].childrenList[0].code;
    this.setData(
      {
        cat1Index: 0,
        cat2Index: 0,
        twoCategoriesList,
        cat1Code,
        cat2Code,
        scrollTopNum: 0,
        currentPage: 1,
      },
      this.getNewGoods
    );
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      statusBar: app.globalData.statusBar,
      navHeight: app.globalData.navHeight,
    });
    this.getCategory();
  },
  // 打开全部分类
  openAll() {
    this.setData({
      showAll: true,
    });
  },
  // 一级分类点击
  async oneTap(e, isTowLast = false) {
    this.scrollTop = 0;
    this.nextState = false;
    this.data.currentPage = 1;
    const { categoriesData } = this.data;
    const { index, mask, code } = e.currentTarget.dataset;
    wx.uma.trackEvent("Pri_categories", { Pri_categories: code });
    const item = categoriesData[index];
    if (index == 0) {
      if (mask) {
        const num =
          index - 2 >= 0 ? index - 2 : index - 1 >= 0 ? index - 1 : index;
        this.setData({
          showAll: false,
          childrenId: `c${num}`,
        });
      }
      // 新品首发 昨日上新
      const list = item.childrenList;
      const firstItem = isTowLast ? list[list.length - 1] : list[0];
      const cat2Code = firstItem.code;
      const cat2Index = isTowLast ? list.length - 1 : 0;
      wx.uma.trackEvent("Sec_categories", {
        Pri_categories: code,
        Sec_categories: cat2Code,
      });
      this.setData(
        {
          cat1Index: index,
          scrollTopNum: 0,
          cat2Index,
          cat1Code: code,
          cat2Code,
          twoCategoriesList: list,
        },
        this.getNewGoods
      );
    } else {
      const shopCode = app.globalData.currentStore.shopCode;
      const { categoriesData } = this.data;
      // 根据code 获取下标 首页跳转情况
      let index2 = categoriesData.findIndex((item) => item.code == code);
      if (index2 == -1) {
        // 首页数据和当前页面 数据不一致 重新请求数据
        await this.getCategory();
        let pam = {
          currentTarget: { dataset: { code: code, mask: true, index: index } },
        };
        this.oneTap(pam);
        return;
      }
      if (mask) {
        const num =
          index2 - 2 >= 0 ? index2 - 2 : index2 - 1 >= 0 ? index2 - 1 : index2;
        this.setData({
          showAll: false,
          childrenId: `c${num}`,
        });
      }
      // const key = shopCode ? "categoryAll2" : "categoryAll";
      const params = JSON.parse(
        JSON.stringify({
          code,
          shopCode,
        })
      );
      let list = await httpResource("categoryAll", params, null, "GET");
      if (list.length) {
        const firstItem = isTowLast ? list[list.length - 1] : list[0];
        const cat2Code = firstItem.code;
        const cat2Index = isTowLast ? list.length - 1 : 0;
        wx.uma.trackEvent("Sec_categories", {
          Pri_categories: code,
          Sec_categories: cat2Code,
        });

        this.setData(
          {
            cat1Index: index2,
            scrollTopNum: 0,
            cat1Code: categoriesData[index2].code,
            cat2Index,
            cat2Code,
            twoCategoriesList: list,
          },
          this.getGoods
        );
      } else {
        this.setData({
          cat1Index: index,
          twoCategoriesList: [],
          goodsList: [],
          cat2Index: 0,
        });
      }
    }
  },
  // 二级分类点击
  twoTap(e) {
    this.nextState = false;
    this.scrollTop = 0;
    this.nextState = false;
    this.data.currentPage = 1;
    const { cat1Index, twoCategoriesList } = this.data;
    const { code, index } = e.currentTarget.dataset;
    const centerNum = Math.floor(twoCategoriesList.length);
    const num = index - 2 >= 0 ? index - 2 : index - 1 >= 0 ? index - 1 : index;
    this.setData({
      twoChildrenId: `ch${num}`,
    });
    console.log(`ch${num}`);
    this.setData(
      {
        cat2Index: index,
        scrollTopNum: 0,
        cat2Code: code,
      },
      () => {
        if (cat1Index != 0) {
          this.getGoods();
        } else {
          this.getNewGoods();
        }
      }
    );
  },
  // 下拉刷新
  bindrefresherrefresh() {
    const { cat1Index } = this.data;
    this.setData(
      {
        refresher: true,
        currentPage: 1,
      },
      () => {
        setTimeout(() => {
          if (cat1Index != 0) {
            this.getGoods();
          } else {
            this.getNewGoods();
          }
        }, 300);
      }
    );
  },
  //到达底部加载
  scrollToLower: function (e) {
    const { cat1Index } = this.data;
    if (this.nextState) {
      console.log(1);
    } else {
      this.data.currentPage++;
      if (cat1Index != 0) {
        this.getGoods();
      } else {
        this.getNewGoods();
      }
    }
  },
  // 滑动顶部回退
  scrollTopLower() {
    // 滑动顶部
    console.log("滑动顶部");
  },
  // 获取全部分类
  async getCategory() {
    let _this = this;
    const shopCode = app.globalData.currentStore.shopCode;
    const params = JSON.parse(
      JSON.stringify({
        code: "",
        shopCode,
      })
    );
    let data = await httpResource("categoryAll", params, null, "GET");
    if (data && data.length) {
      this.setData(
        {
          categoriesData: [...categoryData, ...data],
          isCategoriesData: true,
        },
        () => {
          if (!wx.getStorageSync("classify_home")) {
            _this.reload();
          }
          setTimeout(() => {
            this.getScrollHeight();
          }, 500)
        }
      );
    } else {
      this.setData(
        {
          categoriesData: [],
          isCategoriesData: false,
        },
        () => {
          if (!wx.getStorageSync("classify_home")) {
            //_this.reload();
          }
        }
      );
    }
  },

  // 获取商品
  async getGoods() {
    const shopCode = app.globalData.currentStore.shopCode;
    const { cat1Code, cat2Code } = this.data;
    let params = JSON.parse(
      JSON.stringify({
        shopCode,
        currentPage: this.data.currentPage,
        pageSize: 20,
        categoryCodeSecond: cat2Code,
      })
    );
    try {
      const res = await httpResource("sendHome", params);
      let data = (res.records || []).map(getProductDetail);
      console.log("商品数据", data);
      this.setData({ noMore: false });
      if (data && data.length != 0) {
        this.setData({
          respones: false,
        });
        if (this.data.currentPage == 1) {
          this.setData({
            goodsList: data,
            refresher: false,
            respones: true,
          });
        } else {
          this.setData({
            goodsList: this.data.goodsList.concat(data),
            refresher: false,
            respones: true,
          });
        }
      } else {
        if (this.data.currentPage == 1) {
          this.setData({
            goodsList: [],
          });
          this.nextState = true;
        } else {
          this.nextState = true;
        }
        this.setData({
          noMore: this.data.currentPage == 1 ? false : true,
          refresher: false,
          respones: true,
        });
      }
    } catch (error) {
      this.setData({
        goodsList: [],
        noMore: false,
        respones: true,
      });
    }
  },
  getScrollHeight() {
    wx.createSelectorQuery()
      .select("#scroll-content")
      .boundingClientRect((rect) => {
        //滚动的高度=scroll-view内view的高度
        this.viewHeight = rect.height;
      })
      .exec();
  },
  getScrollHeight() {
    wx.createSelectorQuery()
      .select("#scroll-content")
      .boundingClientRect((rect) => {
        //滚动的高度=scroll-view内view的高度
        if (rect) this.viewHeight = rect.height;
      })
      .exec();
  },
  // 上一个
  prev() {
    const { cat1Index, cat2Index, categoriesData, twoCategoriesList } =
      this.data;
    // 最后一个一级分类和二级分类
    if (cat1Index == 0 && cat2Index == 0) {
      console.log("第一个");
      return;
    }
    // 二级是最后一个
    if (cat2Index == 0) {
      const oneIndex = cat1Index - 1;
      const item = categoriesData[oneIndex];
      this.oneTap(
        { currentTarget: { dataset: { code: item.code, index: oneIndex } } },
        true
      );
    } else {
      // 二级加一
      const twoIndex = cat2Index - 1;
      const item = twoCategoriesList[twoIndex];
      this.twoTap({
        currentTarget: { dataset: { code: item.code, index: twoIndex } },
      });
    }
  },
  // 下一个
  next() {
    const { cat1Index, cat2Index, categoriesData, twoCategoriesList } =
      this.data;
    let oneMaxIndex = categoriesData.length - 1;
    let twoMaxIndex = twoCategoriesList.length - 1;
    // 最后一个一级分类和二级分类
    if (cat1Index == oneMaxIndex && cat2Index == twoMaxIndex) {
      return;
    }
    // 二级是最后一个
    if (cat2Index == twoMaxIndex) {
      const oneIndex = cat1Index + 1;
      const item = categoriesData[oneIndex];
      this.oneTap({
        currentTarget: { dataset: { code: item.code, index: oneIndex } },
      });
    } else {
      // 二级加一
      const twoIndex = cat2Index + 1;
      const item = twoCategoriesList[twoIndex];
      this.twoTap({
        currentTarget: { dataset: { code: item.code, index: twoIndex } },
      });
    }
  },
  // 新品、昨日上新
  async getNewGoods() {
    const shopCode = app.globalData.currentStore.shopCode;
    const { cat2Code } = this.data;
    let params = JSON.parse(
      JSON.stringify({
        shopCode,
        currentPage: this.data.currentPage,
        pageSize: 20,
        goodType: cat2Code,
      })
    );
    try {
      const res = await httpResource("dayGoods", params);
      let data = (res.records || []).map(getProductDetail);
      console.log("商品数据", data);
      this.setData({
        noMore: false,
      });
      if (data && data.length != 0) {
        this.setData({
          respones: false,
        });
        if (this.data.currentPage == 1) {
          this.setData({
            goodsList: data,
            refresher: false,
            respones: true,
          });
        } else {
          this.setData({
            goodsList: this.data.goodsList.concat(data),
            refresher: false,
            respones: true,
          });
        }
      } else {
        if (this.data.currentPage == 1) {
          this.setData({
            goodsList: [],
          });
          this.nextState = true;
        } else {
          this.nextState = true;
          // this.next()
        }
        this.setData({
          noMore: this.data.currentPage == 1 ? false : true,
          refresher: false,
          respones: true,
        });
      }
    } catch (error) {
      this.setData({
        goodsList: [],
        noMore: false,
        respones: true,
      });
    }
  },
  // 喜欢
  async likeChange(e) {
    const status = this.checkLogin();
    if (!status) {
      return;
    }
    // 是否喜欢
    const { favorite: isFavorite, barcode, index } = e.currentTarget.dataset;
    const api = isFavorite ? "unFavorite" : "favorite";
    await httpResource(api, { barcode }, null, "GET");
    this.setData({
      [`goodsList[${index}].isFavorite`]: !isFavorite,
    });
  },
  // 触摸相关
  lastTapTime: 0,
  nextState: false,
  startY: 0,
  scrollTop: 0,
  viewHeight: 0,
  viewScrollHeight: 0,
  // 滑动开始
  touchStart(e) {
    this.lastTapTime = e.timeStamp;
    this.startY = e.changedTouches[0].pageY;
    console.log("开始", e);
  },
  // 滑动结束
  touchEnd(e) {
    const emdTime = e.timeStamp;
    const endY = e.changedTouches[0].pageY;
    const height = endY - this.startY;
    this.startY = 0;
    const { respones } = this.data;
    if (respones && height > 120 && this.scrollTop <= 0) {
      this.prev();
    }
    if (
      (respones && height < -120 && this.scrollTop <= 0) ||
      (this.nextState &&
        respones &&
        this.scrollTop >= this.viewScrollHeight - this.viewHeight - 50 &&
        height < -120)
    ) {
      this.next();
    }
    console.log(
      "结束",
      '滑动距离' + height,
      '是否可以进行下一个分类' + this.nextState,
      respones,
      '列表滑动高度' + this.viewScrollHeight,
      '滑动距离' + this.scrollTop,
      '列表高度' + this.viewHeight,
      this.viewScrollHeight - this.viewHeight - 50
    );
  },
  // 滑动事件
  bindscrollFn(e) {
    console.log("滑动", e);
    this.scrollTop = e.detail.scrollTop;
    this.viewScrollHeight = e.detail.scrollHeight;
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    setTimeout(() => {
      this.getScrollHeight();
    }, 1000)
  },
  changeLikeGoods() {
    const { noLikesBarcode, likesBarcode, goodsList } = this.data;
    if (noLikesBarcode) {
      const index = goodsList.findIndex((e) => (e.barcode = noLikesBarcode));
      if (index > -1) {
        this.setData({
          [`goodsList[${index}].isFavorite`]: false,
        });
      }
    }
    if (likesBarcode) {
      const index = goodsList.findIndex((e) => (e.barcode = likesBarcode));
      if (index > -1) {
        this.setData({
          [`goodsList[${index}].isFavorite`]: true,
        });
      }
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let _this = this;
    this.getTabbar();
    if (
      this.data.shopCode &&
      this.data.shopCode != app.globalData.currentStore.shopCode
    ) {
      this.getCategory();
    } else {
      const { categoriesData } = this.data;
      if (categoriesData.length == 1) {
        this.getCategory();
      }
    }
    _this.setData(
      {
        shopCode: app.globalData.currentStore.shopCode,
        shopName: app.globalData.currentStore.shopName,
      },
      () => {
        _this.changeLikeGoods();
        if (wx.getStorageSync("classify_home")) {
          let { code, index } = wx.getStorageSync("classify_home");
          let param = {
            currentTarget: {
              dataset: { code: code, mask: true, index: index },
            },
          };
          _this.oneTap(param);
        }
      }
    );
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () { },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () { },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () { },
  async shareCount(params) {
    try {
      await httpResource("shareNum", params);
    } catch (error) { }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (e) {
    const path = setSharePagePath('/pages/index/index', app, 'categories');
    if (e.from === "button") {
      const {
        img: imageUrl,
        item: { goodsName, goodsCode, barcode, marketPrice, salePrice: price },
      } = e.target.dataset;
      const shopCode = app.globalData.currentStore.shopCode || 0;
      this.shareCount({ shopCode, goodsName, marketPrice, price, barcode });
      wx.uma.trackEvent("class_share", {
        goods_code: goodsCode,
        goods_name: goodsName,
      });
      return {
        title: goodsName,
        path,
        imageUrl,
      };
    }
    return {
      title: `为你推荐一家宝藏折扣店，全场1折起！`,
      path,
      imageUrl:
        "https://img-cdn.hotmaxx.cn/web/common/mp-store/common/share-base-img.jpg",
    };
  },
  resetUtilsList() {
    this.setData({
      utilsList: [
        {
          title: "综合",
          value: "",
        },
        {
          title: "销量",
          value: "fall", // rise 上 fall
        },
        {
          title: "价格",
          value: "fall", // rise 上 fall
        },
      ],
    });
  },
  utilsChange(e) {
    const index = e.currentTarget.dataset.index;
    const { utilsIndex, utilsList } = this.data;
    const row = utilsList[index];
    // 有排序
    if (index > 0) {
      // 当前index
      if (utilsIndex === index) {
        const value = row.value == "fall" ? "rise" : "fall"; // rise 上 fall
        this.setData(
          {
            [`utilsList[${index}].value`]: value,
          },
          this.utilsGetData
        );
      } else {
        this.setData(
          {
            utilsIndex: index,
          },
          this.utilsGetData
        );
        this.resetUtilsList();
      }
    } else {
      if (utilsIndex === index) return;
      this.setData(
        {
          utilsIndex: 0,
        },
        this.utilsGetData
      );
      this.resetUtilsList();
    }
  },
  utilsGetData() {
    this.scrollTop = 0;
    this.data.currentPage = 1;
    this.nextState = false;
    const { cat1Index } = this.data;
    if (cat1Index != 0) {
      this.getGoods();
    } else {
      this.getNewGoods();
    }
  },
  share() { },
  goDetail(e) {
    // const status = this.checkLogin();
    // if (!status) {
    //   return;
    // }
    // if (!app.globalData.isLogin) return;
    if (!app.globalData.currentStore.shopCode) return;
    const barcode = e.currentTarget.dataset.barcode;
    wx.navigateTo({
      url: `/pages/productDetail/index?barcode=${barcode}`,
    });
  },
  goSearch() {
    const shopCode = app.globalData.currentStore.shopCode;
    if (!shopCode) {
      wx.showToast({
        title: "请先选择门店哦",
        duration: 1000,
        icon: "none",
      });
      return;
    }
    wx.navigateTo({
      url: `/pages/searchGoods/index`,
    });
  },
  //切换门店
  goStore() {
    wx.navigateTo({
      url: "/pages/map/index",
    });
  },
});
