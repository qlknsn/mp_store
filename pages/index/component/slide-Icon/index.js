var app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    categoryList: {
      type: Array,
      value: [],
      observer(value) {
        this.getRatio()
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    slideWidth: '', //滑块宽
    slideLeft: 0, //滑块位置
    totalLength: '', //当前滚动列表总长
    slideShow: false,
    slideRatio: '',
  },
  lifetimes: {
    attached: function () {
      let _this = this;
      let systemInfo = wx.getSystemInfoSync();
      _this.setData({
        windowHeight: systemInfo.windowHeight - 35,
        windowWidth: systemInfo.windowWidth,
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    //根据分类获取比例
    getRatio() {
      const _this = this;
      if (!_this.data.categoryList || _this.data.categoryList.length <= 5) {
        this.setData({
          slideShow: false
        })
      } else {
        //分类列表总长度
        let _totalLength = _this.data.categoryList.length * 140;
        //滚动列表长度与滑条长度比例
        let _ratio = 70 / _totalLength * (750 / this.data.windowWidth);
        //当前显示红色滑条的长度(保留两位小数)
        let _showLength = 750 / _totalLength * 70;
        this.setData({
          slideWidth: _showLength,
          totalLength: _totalLength,
          slideShow: true,
          slideRatio: _ratio
        })
      }
    },
    //slideLeft动态变化
    getleft(e) {
      this.setData({
        slideLeft: e.detail.scrollLeft * this.data.slideRatio
      })
    },
    //跳转分类
    topNavTo: function (event) {
      let { code, index } = event.currentTarget.dataset;
      wx.setStorage({
        key: "classify_home",
        data: { code, index },
        complete: () => {
          wx.switchTab({
            url: '/pages/categories/index',
          })
        }
      })
    },
  }
})
