var app = getApp();
import {
  getQuery,
  getUserLocation,
  checkLocation,
  setModulesData,
  commonTap,
  getProductDetail,
  setSharePagePath
} from "../../assets/js/commonData";
import { subscribeMessage, getSubState } from "../../config/subConfig";
const { getCode, mpLogin, httpResource } = require("../../assets/js/common.js");
import Dialog from '@vant/weapp/dialog/dialog'
import {
  debounce
} from '../../assets/js/eventUtil.js'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    templateList: [], // 模板列表
    opcity: 0,
    response: false,
    responseHome: false,
    defaultImg1:
      "https://img.hotmaxx.cn/web/common/mp-store/home-index/goods-default.png",
    options: {}, // url数据
    // navbar信息
    isLogin: false,
    statusBar: "",
    longitude: null, // 经度
    latitude: null, // 纬度
    isShowLocBtn: false, // 定位权限
    isShowAddressBtn: false, // 不在配送范围
    storeInfo: {}, // 当前门店信息
    addressInfo: {
      // title: '七宝龙兴小区'
    }, // 当前地址信息
    scrollable: true, // notice-bar
    showMessageContent: false,
    // 页面配置信息
    imgList: [],
    oldImgList: [],
    swiperCurrent: 0,
    delayTime: 2000,
    messageContent: "",
    yesterdayIcon: "",
    newDayIcon: "",
    // 昨日上新
    joshinList: [],
    // 新品推荐
    newGoodsList: [],
    // new arrival 新到货
    newArrivalList: [],
    isNewArrivalList: true,//是否优选好物有数据
    listLeft: [],
    listRight: [],
    popupImgInfo: {}, // 弹框图片
    showPopup: false, // 首页弹框
    isLoad: false,
    showAddressPopup: false, // 选择配送地址
    isLoadAddressPopup: false, // 弹窗一次
    showStorePopup: false, // 选择附近门店
    isLoadStorePopup: false, // 弹窗一次
    showStoreList: [],
    selectTaber: "ZI_TI", //自提，外卖状态
    distanceStr: null, //自提距离
    categoryList: [], //滑动模块数据
    isFixed: false,//是否吸顶
    isIosTop: false,
    popTimer: null, // 页面隐藏关闭定时器
  },
  otherData: {
    leftHeight: 0, // 两列中左list的总高度
    rightHeight: 0, // 两列中右list的总高度
  },
  // 点击事件
  swiperItemTap(e) {
    const { url } = e.currentTarget.dataset;
    app.globalData.lotterynavigate = '/pages/index/index'
    if (url) {
      wx.uma.trackEvent("product_banner1", { click_advertisement: url });
    }
    console.log(e);
    commonTap(e);
  },
  popupTap(e) {
    commonTap(e);
    this.onPopupClose();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  async login() {
    let loginCode = await getCode();
    let { type, actId, inviteUserId, isActivity } = app.globalData;
    let param = { code: loginCode, type, actId, inviteUserId, isActivity };
    if (!app.globalData.isLogin) {
      try {
        mpLogin(param, async () => {
          this.setData({
            isLogin: true,
          });
          // 1调用经纬度
          this.getLoc(true);
          app.globalData.isLogin = true;
        }, () => {
          // 1调用经纬度
          this.getLoc(true);
        });
      } catch (error) { }
    } else {
      //如果是邀请有礼活动，已经登录了在从新走一遍登录
      if (isActivity) {
        mpLogin(param, async () => {
          this.setData({
            isLogin: true,
          }, () => {
            app.globalData.isLogin = true;
          });
        });
      }
      this.getLoc(true);
      const shopCode = app.globalData.currentStore.shopCode;
      if (shopCode) {
        const num = await httpResource("cartCount", { shopCode });
        app.globalData.cartNum = num;
      }
    }
  },
  onLoad: function (options) {
    // 获取页面配置
    this.getPageModules();
    // 确认进入
    if (app.globalData.isWelcome) this.login();
    this.setData({
      options,
    }, () => {
      this.getTemplateList();
      this.getCategory();
    });
  },

  //吸顶滚动监听
  scrollSticky(event) {
    let { isFixed } = event.detail;
    this.setData({
      isFixed: isFixed
    })
  },

  // 获取全部分类
  async getCategory() {
    let _this = this;
    let shopCode = app.globalData.currentStore.shopCode;
    let data = await httpResource("categoryAll", { code: "", shopCode }, null, "GET");
    if (data && data.length) {
      let categoryData = [
        {
          name: "新品",
          icon: "https://img.hotmaxx.cn/web/common/mp-store/home-category/new2.png",
        },
      ];
      _this.setData({
        categoryList: [...categoryData, ...data],
      });
    }
  },
  // 初始化nabbar高度
  initData() {
    const { storeInfo: { shopCode } } = this.data;
    const { shopCode: newShopCode } = app.globalData.currentStore;
    if (shopCode && newShopCode && shopCode != newShopCode) {
      const oldActivityId = wx.getStorageSync(`popupImgId`);
      this.getPopupData(newShopCode, oldActivityId);
      if (app.globalData.isLogin) {
        this.getCartNum();
        this.setImgList(newShopCode);
      }
    }
    if (!shopCode && newShopCode) {
      const oldActivityId = wx.getStorageSync(`popupImgId`);
      this.getPopupData(newShopCode, oldActivityId);
    }
    this.setData({
      statusBar: app.globalData.statusBar,
      isLogin: app.globalData.isLogin,
      storeInfo: app.globalData.currentStore || {},
    });
  },
  // 获取购物车数量
  async getCartNum() {
    const shopCode = app.globalData.currentStore.shopCode;
    if (shopCode) {
      const num = await httpResource("cartCount", { shopCode });
      app.globalData.cartNum = num;
    }
  },
  // 初始化数据
  initStoreData(opt) {
    //获取二维码中参数
    if (opt.q) {
      let Url = decodeURIComponent(opt.q);
      let shopCode = getQuery(Url, "shopCode");
      let shopName = getQuery(Url, "shopName");
      let source = getQuery(Url, "source");
      if (shopCode) {
        wx.setStorageSync("shopCode", shopCode);
      }
      if (shopName) {
        wx.setStorageSync("shopName", shopName);
      }
      if (source) {
        wx.setStorageSync("source", source);
      }
    }
  },
  // 商品数据
  async getNewGoods() {
    const shopCode = app.globalData.currentStore.shopCode;
    const parasm = JSON.parse(
      JSON.stringify({
        shopCode,
        goodType: 1,
        currentPage: 1,
        pageSize: 50,
      })
    );
    try {
      const data = await httpResource("dayGoods", parasm);
      const data2 = await httpResource("dayGoods", Object.assign(parasm, { pageSize: 10, goodType: 2 }));
      if (data && data.records) {
        const newGoods = (data.records || []).map(getProductDetail) || [];
        this.setData({
          newGoodsList: newGoods,
          response: true,
        });
      }
      if (data2) {
        const yesterdayGoods =
          (data2.records || []).map(getProductDetail) || [];
        this.setData({
          joshinList: yesterdayGoods.slice(0, 8),
          response: true,
        });
      }
    } catch (error) {
      this.setData({
        joshinList: [],
        newGoodsList: [],
        response: true,
      });
    }
  },
  //瀑布流布局
  async waterfallFlow() {
    //在获取list后调用
    const { newGoodsList, listLeft, listRight } = this.data;
    for (let item of newGoodsList) {
      this.otherData.leftHeight <= this.otherData.rightHeight
        ? listLeft.push(item)
        : listRight.push(item); //判断两边高度，来觉得添加到那边
      await this.getBoxHeight(listLeft, listRight);
    }
  },
  getBoxHeight(listLeft, listRight) {
    return new Promise((resolve, reject) => {
      this.setData({ listLeft, listRight });
      const query = wx.createSelectorQuery();
      query.select(".left-item").boundingClientRect();
      query.select(".right-item").boundingClientRect();
      query.exec((res) => {
        if (res[0] == null) {
          reject(res);
        }
        this.otherData.leftHeight = res[0].height; //获取左边列表的高度
        this.otherData.rightHeight = res[1].height; //获取右边列表的高度
        resolve();
      });
    });
  },
  //
  openCouponModel() {
    if (!app.globalData.isLogin) return;
    const child = this.selectComponent('#coupon-model');
    child.getData()
  },
  // 获取优惠券弹窗实例
  getCouponModelInfo() {
    const child = this.selectComponent('#coupon-model');
    const { isLoad, show } = child.data;
    if (isLoad && show) child.data.modelArr.push('getPopupData');
    return { isLoad, show }
  },
  getCouponModelData() {
    return new Promise((resolve, reject) => {
      try {
        httpResource("popCoupon", {}, data => {
          if (data && data.actId) {
            const { rewardChannel } = data;
            const isShow = rewardChannel.includes(0);
            if (isShow) {
              resolve(true)
            } else {
              resolve(false)
            }
          } else {
            resolve(false)
          }
        }, "POST", () => {
          resolve(false)
        });
      } catch (error) {
        resolve(false)
      }
    });
  },
  nextPopModel() {
    if (!this.isDefaultLoadPop) {
      this.getPopupData();
    }
  },
  modelList: [],
  isDefaultLoadPop: false,
  // 获取首页弹窗数据
  getPopupData: debounce(async function (code, id = '') {
    const isShowCouponModel = await this.getCouponModelData();
    if (this.modelList.length === 0 && isShowCouponModel) {
      this.modelList = ['getPopupData'];
      return
    }
    this.isDefaultLoadPop = true;
    const storeCode = code || app.globalData.currentStore.shopCode;
    const data = await httpResource("getPopup", { storeCode: storeCode });
    let { popupCount, showFlag, activityId, moduleLink, popupChannel } = data;
    // 是否是微信弹窗
    const isShow = popupChannel && popupChannel.includes(0);
    moduleLink = moduleLink || [];
    const { isLoad, show } = this.getCouponModelInfo();
    console.log('定时', isLoad, show, this.data.popTimer);
    // if (!isLoad) {
    //   if (this.data.popTimer) {
    //     clearTimeout(this.data.popTimer);
    //     this.data.popTimer = null;
    //   }
    //   this.data.popTimer = setTimeout(() => {
    //     this.getPopupData(code, id);
    //   }, 1000);
    //   return
    // } else {
    //   if (this.data.popTimer) {
    //     clearTimeout(this.data.popTimer);
    //     this.data.popTimer = null;
    //   }
    // }
    // console.log(isLoad, show);
    // if (isLoad && show) {
    //   return
    // }
    if (showFlag && moduleLink.length && isShow) {
      const oldActivityId = wx.getStorageSync(`popupImgId`);
      const oldDate = wx.getStorageSync(`popupImgDate`);
      const date = new Date(new Date().setHours(0, 0, 0, 0)).getTime();
      if (date > oldDate || oldActivityId != activityId) {
        wx.setStorageSync("popupImgId", activityId);
        wx.setStorageSync("popupImgDate", date);
        wx.setStorageSync("popupImgCount", 0);
        this.setData({
          showPopup: true,
          popupImgInfo: moduleLink[0],
        });
      }
      // 真状态
      let count = wx.getStorageSync(`popupImgCount`) || 0;
      if (popupCount > count) {
        wx.setStorageSync("popupImgCount", Number(count) + 1);
        this.setData({
          showPopup: true,
          popupImgInfo: moduleLink[count % moduleLink.length],
        });
      }
    }
  }, 800),
  // 自定义tabbar
  getTabbar() {
    if (typeof this.getTabBar === "function" && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 0,
        show: app.globalData.isWelcome ? true : false,
      });
    }
  },
  welcomeGetData() {
    this.onShow();
    setTimeout(() => {
      this.onLoad({});
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getTabbar();
    this.initData();
    // 默认操作-获取二维码参数
    this.initStoreData(this.data.options);
    const { storeInfo: { shopCode }, isLoad, isLogin } = this.data;
    // 确认进入
    if (app.globalData.isWelcome) {
      try {
        // 经纬度
        if (isLoad) this.getLoc();
      } catch (error) {
        console.log("首页报错", error);
      }
    }
    //检查缓存里面是否有自提状态
    if (wx.getStorageSync("SELECT_TABER")) {
      this.setData({
        selectTaber: wx.getStorageSync("SELECT_TABER")
      });
    } else {
      this.setData({
        selectTaber: app.globalData.selectTaber
      }, () => {
        wx.setStorage({
          key: "SELECT_TABER",
          data: app.globalData.selectTaber,
        })
      });
    }
    //自提获取距离
    if (wx.getStorageSync("current-store")) {
      let { distanceStr } = wx.getStorageSync("current-store");
      let { statusBarHeight } = wx.getStorageSync("systemInfo");
      this.setData({
        distanceStr: distanceStr,
        isIosTop: statusBarHeight > 20 ? true : false
      });
    }
    this.getCategory();
    if (isLogin) {
      this.openCouponModel();
    }
  },
  // 打开配置
  openSetting() {
    wx.openSetting();
  },
  // 关闭提示
  closeStoreTip() {
    this.setData({
      isShowLocBtn: false,
    });
  },
  // 切换门店
  openAddressList() {
    wx.navigateTo({
      url: "/pages/map/index",
    });
  },
  // 关闭提示
  closeAddressTip() {
    this.setData({
      isShowAddressBtn: false,
    });
  },
  // 关闭弹窗
  onPopupClose() {
    this.setData({
      showPopup: false,
    });
  },
  // 去选择门店
  goStore() {
    wx.navigateTo({
      url: "/pages/map/index",
    });
  },
  // 获取配货地址和门店
  async getStoreInfo(refresh, showAddress = false) {
    let _this = this;
    let { selectTaber } = _this.data;
    // 本地缓存没有
    if (selectTaber == 'WAI_MAI' && !app.globalData.deliverableAddress.addrId) {
      // 查询默认地址
      if (app.globalData.isLogin) {
        const item = await httpResource("addressDefault", {}, null, "GET");
        if (item) {
          // 设置缓存
          _this.setData({
            addressInfo: item
          }, () => {
            app.globalData.deliverableAddress = item;
            wx.setStorageSync('deliverable-address', item);
            checkLocation().then((isTrue) => {
              console.log('isTruexxx', isTrue)
              if (isTrue) _this.getStoreData(refresh);
            })
          })
        } else {
          _this.setData({
            addressInfo: {}
          }, () => {

            // 弹起选择地址页面,外卖
            if (selectTaber == 'WAI_MAI') {
              if (!_this.data.isLoadAddressPopup) {
                _this.setData({
                  isLoadAddressPopup: true,
                  showAddressPopup: true
                })
              }
            }
            //自提
            if (selectTaber == 'ZI_TI') {
              if (!app.globalData.currentStore.shopCode) {
                _this.setData({
                  isLoadAddressPopup: true,
                  showAddressPopup: true
                })
              }
            }
          })
        }
      }
      this.getNewGoods();
      this.getAllGoods();
      if (refresh) {
        this.getPopupData();
      }
      return;
    } else {
      if (app.globalData.deliverableAddress.addrId) {
        this.setData({
          addressInfo: app.globalData.deliverableAddress,
        });
      }
      this.getStoreData(refresh);
    }
    if (showAddress) {
      return;
    }
    this.getStoreData(refresh);
  },
  async getStoreData(refresh) {
    let { selectTaber } = this.data;
    if (!app.globalData.currentStore.shopCode) {
      let { latitude, longitude } = app.globalData.deliverableAddress;
      const { longitude: log, latitude: lat } = app.globalData;
      if (selectTaber == 'ZI_TI') {
        latitude = lat;
        longitude = log;
      }
      if (!longitude || !latitude) {
        longitude = 121.48704;
        latitude = 31.211519;
      }
      if (!this.data.isLoadStorePopup) {
        const data = await httpResource("getShopByRange", {
          radius: 100,
          latitude,
          longitude,
        });
        if (data && data.length) {
          // 选择 门店
          this.setData({
            storeInfo: data[0],
            showStoreList: data.slice(0, 2),
            isLoadStorePopup: true,
            showStorePopup: true,
            distanceStr: data[0].distanceStr,
          }, () => {
            this.setImgList(data[0].shopCode);
            app.globalData.currentStore = data[0];
            wx.setStorageSync("current-store", data[0]);
          }
          );
        }
      }
    } else {
      const { shopCode } = app.globalData.currentStore;
      if (refresh) {
        if (app.globalData.isLogin) {
          this.openCouponModel();
        }
        this.getPopupData();
      }
      if (selectTaber == 'WAI_MAI') {
        const { latitude, longitude } = app.globalData.deliverableAddress;
        if (latitude && longitude) {
          // 校验地址
          this.checkAddress(shopCode, longitude, latitude);
        }
      }

      this.getNewGoods();
      this.getAllGoods();
    }
  },
  // 获取经纬度
  getLoc(refresh) {
    let _this = this;
    getUserLocation().then((res) => {
      let { latitude, longitude } = res;
      app.globalData.latitude = latitude;
      app.globalData.longitude = longitude;
      _this.getStoreInfo(refresh);
      _this.setData({
        latitude,
        isLoad: true,
        longitude, // 纬度
        isShowLocBtn: latitude && longitude ? false : true,
      });
    }).catch((err) => {
      const { storeInfo: { shopCode } } = _this.data;
      if (err.errMsg.indexOf("频繁调用会增加电量") == -1) {
        _this.setData({
          isLoad: true,
        }, () => {
          checkLocation().then((isTrue) => {
            _this.getStoreInfo(false, true);
            console.log("============位置错误", err, isTrue);
            _this.setData({ isShowLocBtn: !isTrue });
            if (!isTrue) {
              if (!app.globalData.toLoction) {
                app.globalData.toLoction = true;
                wx.setStorageSync("toLoction", true);
                // 跳转定位授权
                wx.navigateTo({
                  url: "/pages/location/fail",
                });
              }
            }
          });
          _this.getNewGoods();
          _this.getAllGoods();
        });
      }
    });
  },
  // 去登录
  goLogin() {
    wx.navigateTo({
      url: "/pages/userLogin/index?type=1",
    });
  },
  // 近日上新页面
  async goJoshin() {
    wx.uma.trackEvent("product_more");
    if (!app.globalData.isLogin) {
      wx.navigateTo({
        url: "/pages/productList/index?navIndex=1",
      });
    } else {
      try {
        const subStatus = getSubState("viewMore");
        const templateIds = this.getTemplateCode("viewMore");
        if (subStatus && templateIds && templateIds.length) {
          const successList = await subscribeMessage(templateIds, "viewMore");
          if (successList && successList.length) {
            await httpResource("sendMessage", {
              code: "viewMore",
              templateCodeList: successList,
            });
            wx.navigateTo({
              url: "/pages/productList/index?navIndex=1",
            });
          } else {
            wx.navigateTo({
              url: "/pages/productList/index?navIndex=1",
            });
          }
        } else {
          wx.navigateTo({
            url: "/pages/productList/index?navIndex=1",
          });
        }
      } catch {
        wx.navigateTo({
          url: "/pages/productList/index?navIndex=1",
        });
      }
    }
  },
  // 获取模板消息ID
  getTemplateCode(code) {
    const { templateList } = this.data;
    let list = [];
    try {
      list = templateList.find((e) => e.code == code)
        ? templateList
          .find((e) => e.code == code)
          .templateList.map((v) => v.templateId)
        : [];
    } catch (e) {
      list = [];
    }
    return list;
  },
  // 打开详情文字
  openMessage() {
    if (this.data.messageContent) {
      this.setData({
        showMessageContent: true,
      });
    }
  },
  // 关闭详情文字
  onClose() {
    this.setData({
      showMessageContent: false,
    });
  },
  // 获取配置页面数据
  async getPageModules() {
    const data = await httpResource(
      "miniappPage",
      { pageCode: "new_product_selection" },
      null,
      "GET"
    );
    if (data.pageModules && data.pageModules.length) {
      const list = setModulesData(data.pageModules);
      let imgList = [];
      let oldImgList = [];
      let delayTime = 1000;
      let yesterdayIcon = "";
      let newDayIcon = "";
      let messageContent = "";
      list.forEach((item) => {
        if (item.moduleName === "消息配置") {
          messageContent = item.messageContent || "";
        }
        if (item.moduleName === "banner图配置" && item.showStatus === 0) {
          imgList = item.list || [];
          oldImgList = JSON.parse(JSON.stringify(imgList));
          delayTime = Math.floor(item.timeNums * 1000);
        }
        if (item.moduleName === "我的图标") {
          const yesterdayIconItem = item.data.find(
            (val) => val.name === "昨日上新" && val.showStatus === 0
          );
          const newDayIconItem = item.data.find(
            (val) => val.name === "新品首发" && val.showStatus === 0
          );
          yesterdayIcon = yesterdayIconItem && yesterdayIconItem.imgUrl;
          newDayIcon = newDayIconItem && newDayIconItem.imgUrl;
        }
      });
      const shopCode = app.globalData.currentStore.shopCode;
      if (shopCode) {
        try {
          const swiperList = await httpResource('listShopRangeModule', { shopCode, pageCode: 'new_product_selection' }, null, "GET", null, '', false);
          if (swiperList && swiperList.length) {
            imgList = [...swiperList, ...imgList];
          }
        } catch (error) {

        }
      }
      this.setData({
        imgList,
        oldImgList,
        swiperCurrent: 0,
        delayTime,
        yesterdayIcon,
        newDayIcon,
        messageContent,
      });
    }
    console.log("首页数据", data);
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.setData({
      showAddressPopup: false,
      showStorePopup: false,
      showMessageContent: false, // 重置弹框
    });
    clearTimeout(this.data.popTimer);
    this.data.popTimer = null;
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.setData({
      showMessageContent: false, // 重置弹框
    });
    clearTimeout(this.data.popTimer);
    this.data.popTimer = null;
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () { },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () { },
  async shareCount(params) {
    try {
      await httpResource("shareNum", params);
    } catch (error) { }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (e) {
    const path = setSharePagePath('/pages/index/index', app, 'index');
    if (e.from === "button") {
      const {
        img: imageUrl,
        item: { goodsName, goodsCode, barcode, marketPrice, salePrice: price },
      } = e.target.dataset;
      const shopCode = app.globalData.currentStore.shopCode || 0;
      this.shareCount({ shopCode, goodsName, marketPrice, price, barcode });
      wx.uma.trackEvent("product_share", {
        goods_code: goodsCode,
        goods_name: goodsName,
      });
      return {
        title: goodsName,
        path,
        imageUrl,
      };
    }
    return {
      title: `为你推荐一家宝藏折扣店，全场1折起！`,
      path,
      imageUrl: "https://img-cdn.hotmaxx.cn/web/common/mp-store/common/share-base-img.jpg",
    }
  },
  // 配送到家 新到货
  async getAllGoods() {
    const shopCode = app.globalData.currentStore.shopCode;
    const parasm = JSON.parse(
      JSON.stringify({
        shopCode,
        currentPage: 1,
        pageSize: 50,
      })
    );
    if (this.data.responseHome) this.setData({ responseHome: false });
    try {
      const data = await httpResource("sendHome", parasm);
      if (data && data.records && data.records.length) {
        const list = (data.records || []).map(getProductDetail).slice(0, 6);
        this.setData({
          newArrivalList: list,
          isNewArrivalList: true,
        });
        if (!this.data.responseHome) {
          this.setData({ responseHome: true });
        }
      } else {
        if (!this.data.responseHome) {
          this.setData({ responseHome: true });
        }
        this.setData({
          newArrivalList: [],
          isNewArrivalList: false,
        });
      }
    } catch (error) {
      if (!this.data.responseHome) {
        this.setData({ responseHome: true });
      }
      this.setData({
        newArrivalList: [],
        isNewArrivalList: false
      });
    }
  },
  // 是否登录 未登录去登录
  checkLogin() {
    const isLogin = app.globalData.isLogin;
    if (!isLogin) {
      wx.navigateTo({
        url: "/pages/userLogin/index?type=1",
      });
    }
    return isLogin;
  },
  // 喜欢
  async likeChange(e) {
    const status = this.checkLogin();
    if (!status) return;
    // 是否喜欢
    const {
      favorite: isFavorite,
      barcode,
      index,
      arr,
    } = e.currentTarget.dataset;
    const api = isFavorite ? "unFavorite" : "favorite";
    await httpResource(api, { barcode }, null, "GET");
    switch (arr) {
      case "arrival":
        this.setData({
          [`newArrivalList[${index}].isFavorite`]: !isFavorite,
        });
        break;
      case "joshin":
        this.setData({
          [`joshinList[${index}].isFavorite`]: !isFavorite,
        });
        break;
      case "new-goods":
        this.setData({
          [`newGoodsList[${index}].isFavorite`]: !isFavorite,
        });
        break;
      default:
        break;
    }
  },
  // 我喜欢的
  goProdoctList() {
    wx.navigateTo({
      url: "/pages/productList/index?navIndex=0",
    });
  },
  onPageScroll(e) {
    const top = e.scrollTop;
    if (e.scrollTop >= 150) {
      if (this.data.opcity < 1) {
        this.setData({
          opcity: 1,
        });
      }
    } else {
      const opcity = e.scrollTop / 150;
      this.setData({
        opcity,
      });
    }
  },
  // 加入购物车
  async addCart(e) {
    let _this = this;
    const status = _this.checkLogin();
    let toLoction = wx.getStorageSync('toLoction');
    if (!status) return;
    //自提不需要校验用户地址
    if (_this.data.selectTaber != 'ZI_TI') {
      if (!app.globalData.deliverableAddress.addrId) {
        _this.setData({ showAddressPopup: true });
        return;
      }
    }
    const shopCode = app.globalData.currentStore.shopCode;
    try {
      wx.hideToast();
    } catch (error) { }
    //自提未授权地址信息
    if (_this.data.selectTaber == 'ZI_TI' && toLoction) {
      Dialog.alert({
        width: '309px',
        message: "未授权位置信息，无法获取附近门店",
        theme: 'round-button',
        confirmButtonText: '去开启',
        className: 'htm-dialog',
        closeOnClickOverlay: true,
        overlayStyle: {},
      }).then(() => {
        // 跳转定位授权
        wx.navigateTo({
          url: "/pages/location/fail",
        });
      });
      return;
    }
    //自提未选择门店
    if (_this.data.selectTaber == 'ZI_TI' && !shopCode) {
      Dialog.alert({
        width: '309px',
        message: "请先选择门店",
        theme: 'round-button',
        confirmButtonText: '去选择',
        className: 'htm-dialog',
        closeOnClickOverlay: true,
        overlayStyle: {},
      }).then(() => {
        wx.navigateTo({
          url: `/pages/map/index`,
        })
      });
      return;
    }
    const { item } = e.currentTarget.dataset;
    const {
      barcode: packageCode,
      goodsName,
      goodsCode,
      coverImage,
      salePrice,
      marketPrice,
      categoryCodeFirst: categoryOneCode,
      categoryCodeSecond: categoryTwoCode,
      categoryCodeThird: categoryThreeCode,
    } = item;
    const params = JSON.parse(
      JSON.stringify({
        categoryTwoCode,
        categoryThreeCode,
        version: 2,
        shopCode,
        packageCode,
        goodsName,
        goodsCode,
        coverImage,
        salePrice,
        marketPrice,
        categoryOneCode,
        addType: 2,
        receiveType: _this.data.selectTaber == 'ZI_TI' ? 3 : 4
      })
    );
    let data = await httpResource("cartAddHome", params);
    if (data && data.extraMsg) {
      wx.showToast({
        title: data.extraMsg,
        icon: "none",
        duration: 3000,
      });
    } else {
      wx.showToast({
        title: "加入成功",
        icon: "none",
        duration: 1000,
      });
    }
    const num = await httpResource("cartCount", { shopCode });
    app.globalData.cartNum = num;
  },
  // 关闭地址弹窗
  onAddressPopupClose() {
    this.setData({
      showAddressPopup: false,
    });
  },
  // 选择地址列表
  selectAddress() {
    const { selectTaber } = this.data;
    //外卖
    if (selectTaber == 'WAI_MAI') {
      const status = this.checkLogin();
      if (!status) return;
      wx.navigateTo({
        url: "/pages/user/address/list/index?type=2",
      });
    }
    //门店
    if (selectTaber == 'ZI_TI') {
      wx.navigateTo({
        url: "/pages/map/index",
      });
    }
  },
  // 关闭选择门店弹窗
  onStorePopupClose() {
    this.setData({
      showStorePopup: false,
    },
      () => {
        const { showStoreList } = this.data;
        const item = showStoreList[0];
        this.getPopupData();
        this.getNewGoods();
        this.getAllGoods();
        this.setImgList(item.shopCode);
        if (app.globalData.isLogin) {
          this.openCouponModel();
        }
      }
    );
  },
  // 选择当前门店
  selectCurrentStore(e) {
    const index = e.currentTarget.dataset.index;
    const { showStoreList } = this.data;
    const item = showStoreList[index];
    app.globalData.currentStore = item;
    wx.setStorageSync("current-store", item);
    this.setData(
      {
        showStorePopup: false,
        storeInfo: item,
        distanceStr: item.distanceStr,
      },
      () => {
        this.getPopupData();
        this.getNewGoods();
        this.getAllGoods();
        this.setImgList(item.shopCode);
        if (app.globalData.isLogin) {
          this.openCouponModel();
        }
      }
    );
  },
  // 获取模板消息列表
  async getTemplateList() {
    const templateList = await httpResource("getTemplateList", { code: 'viewMore' }, null, "GET");
    this.setData({
      templateList: templateList || [],
    });
  },
  // 校验门店是否在配送范围
  async checkAddress(shopCode, longitude, latitude) {
    if (!app.globalData.isLogin) return;
    const params = {
      shopCode,
      longitude,
      latitude,
    };
    const data = await httpResource("checkAddress", params, null, "GET");
    if (!data) {
      this.setData({
        isShowAddressBtn: true,
      });
    } else {
      this.setData({
        isShowAddressBtn: false,
      });
    }
    console.log("检验地址", data, shopCode, longitude, latitude);
  },
  goDetail(e) {
    let toLoction = wx.getStorageSync('toLoction');
    // if (!app.globalData.isLogin) return;
    // if (toLoction) {
    //   Dialog.alert({
    //     width: '309px',
    //     message: "未授权位置信息，无法获取附近门店",
    //     theme: 'round-button',
    //     confirmButtonText: '去开启',
    //     className: 'htm-dialog',
    //     closeOnClickOverlay: true,
    //     overlayStyle: {},
    //   }).then(() => {
    //     // 跳转定位授权
    //     wx.navigateTo({
    //       url: "/pages/location/fail",
    //     });
    //   });
    //   return;
    // }
    if (!app.globalData.currentStore.shopCode) {
      this.setData({
        showAddressPopup: true
      });
      return;
    }
    const barcode = e.currentTarget.dataset.barcode;
    wx.navigateTo({
      url: `/pages/productDetail/index?barcode=${barcode}`,
    });
  },
  share() { },
  goSearch() {
    const shopCode = app.globalData.currentStore.shopCode;
    if (!shopCode) {
      wx.showToast({
        title: "请先选择门店哦",
        duration: 1000,
        icon: "none",
      });
      return;
    }
    wx.navigateTo({
      url: `/pages/searchGoods/index`,
    });
  },
  //自提，外卖tab切换
  selectTab(event) {
    let { type } = event.currentTarget.dataset;
    this.setData({
      selectTaber: type,
    }, () => {
      app.globalData.selectTaber = type;
      wx.setStorage({
        key: "SELECT_TABER",
        data: type,
      })
    });
  },
  //跳转会员
  routerMember() {
    wx.navigateTo({
      url: "/module_base/code/index",
    });
  },
  //跳转优惠券
  routerCoupon() {
    wx.navigateTo({
      url: "/pages/user/coupon/index",
    });
  },
  // 不同门店列表
  async setImgList(shopCode) {
    try {
      const imgList = await httpResource('listShopRangeModule', { shopCode, pageCode: 'new_product_selection' }, null, "GET", null, '', false);
      const { oldImgList } = this.data;
      if (imgList && imgList.length) {
        this.setData({
          imgList: [...imgList, ...oldImgList,],
          swiperCurrent: 0,
        })
      } else {
        this.setData({
          imgList: oldImgList,
          swiperCurrent: 0,
        })
      }
    } catch (error) {

    }
  }
});
