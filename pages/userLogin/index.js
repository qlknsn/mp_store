var app = getApp();
const {
  getCode,
  mpLogin,
  httpResource
} = require("../../assets/js/common.js");
import {
  throttl
} from '../../assets/js/eventUtil.js'

Page({

  /**
   * 页面的初始数据s
   */
  data: {
    parameter: {
      'title': '登录',
      return: 1,
    },
    switchTabPages: ['/pages/mine/index', '/pages/cart/index', '/pages/categories/index'],
    switchTabIndex: 0,
    isCheck: false,
    type: 0,
    actCode: null,//Boss后台配置Code，完成用户引导
    blindbox: null,//盲盒Boss后台配置抽奖Code，完成用户引导
    lottery: null,//大转盘Boss后台配置抽奖Code，完成用户引导
    gasiaponcode: null,//九宫格Boss后台配置扭蛋抽奖Code，完成用户引导
    niudanShareid: null,//扭蛋Boss后台配置扭蛋抽奖分享Code，完成用户引导
    taskChainId: null,//Boss后台配置抽奖人Code，完成用户引导
    srcUserId: null,//Boss后台配置抽奖人Code，完成用户引导
  },
  changeCheck() {
    this.setData({
      isCheck: !this.data.isCheck
    })
  },
  goAgreement() {
    wx.navigateTo({
      url: `/module_base/privacyAgreement/index`,
    })
  },
  goUserAgreement() {
    wx.navigateTo({
      url: `/module_base/userAgreement/index`,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.type) {
      this.setData({ type: Number(options.type) })
    }
    //Boss后台配置Code，完成用户引导
    if (options.actCode) {
      this.setData({ actCode: options.actCode });
    }
    if (options.lottery) {
      this.setData({ lottery: options.lottery });
    }
    if (options.blindbox) {
      this.setData({ blindbox: options.blindbox });
    }
    if (options.gasiaponcode) {
      this.setData({ gasiaponcode: options.gasiaponcode });
    }
    if (options.sudoku) {
      this.setData({ sudoku: options.sudoku });
    }
    if (options.niudanShareid) {
      this.setData({ niudanShareid: options.niudanShareid });
    }
    if (options.taskChainId) {
      this.setData({ taskChainId: options.taskChainId });
    }
    if (options.srcUserId) {
      this.setData({ srcUserId: options.srcUserId });
    }
    if (options.switchTabIndex) {
      this.setData({ type: Number(options.switchTabIndex) });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 获取微信用户信息，新API - getUserProfile
   */
  getUserPhone: throttl(async function (e) {
    let _this = this;
    if (!this.data.isCheck) {
      wx.showToast({
        title: '请先勾选协议',
        icon: 'none',
        duration: 3000,
        mask: true
      })
      return false;
    }
    if (e.detail.errMsg !== 'getPhoneNumber:ok') return false;
    const shopCode = wx.getStorageSync("shopCode") || '';
    const shopName = wx.getStorageSync("shopName") || '';
    const source = wx.getStorageSync("source") || '';
    const { type, actId, inviteUserId, isActivity } = app.globalData;
    try {
      const code = await getCode();
      let parms = {
        code: code,
        encreptedData: e.detail.encryptedData,
        iv: e.detail.iv,
        shopCode,
        shopName,
        phone: '',
        source,
        type,
        actId,
        inviteUserId,
        channel: 'WX'
      };
      wx.showLoading({ title: '加载中' });
      // 注册
      let data = await httpResource('userRegister', parms);
      wx.hideLoading()
      const loginCode = await getCode();
      let param = { code: loginCode, type, actId, inviteUserId, isActivity };
      mpLogin(param, () => {
        app.globalData.isLogin = true;
        if (_this.data.actCode && _this.data.actCode != null) {
          wx.redirectTo({
            url: `/pages/invitation/index?actId=${_this.data.actCode}`
          })
        } else if (_this.data.lottery && _this.data.lottery != null) {
          wx.redirectTo({
            url: `/module_games/lotteryDraw/lotteryDraw?actId=${_this.data.lottery}&taskChainId=${_this.data.taskChainId}&srcUserId=${_this.data.srcUserId}`
          })
        } else if (_this.data.gasiaponcode && _this.data.gasiaponcode != null) {
          wx.redirectTo({
            url: `/module_games/gasiapon/index?actId=${_this.data.gasiaponcode}&taskChainId=${_this.data.niudanShareid}&srcUserId=${_this.data.srcUserId}`
          })
        }else if (_this.data.sudoku && _this.data.sudoku != null) {
          wx.redirectTo({
            url: `/module_games/sudoku/sudoku?actId=${_this.data.sudoku}&taskChainId=${_this.data.taskChainId}&srcUserId=${_this.data.srcUserId}`
          })
        }else if (_this.data.blindbox && _this.data.blindbox != null) {
          wx.redirectTo({
            url: `/module_games/blindbox/blindbox?actId=${_this.data.blindbox}&taskChainId=${_this.data.taskChainId}&srcUserId=${_this.data.srcUserId}`
          })
        } else {
          // 跳转页面
          this.goPath();
        }
      })
    } catch (error) {
      // 两秒后关闭提示错误
      setTimeout(() => {
        wx.hideLoading()
      }, 2000)
      console.log('注册错误', error)
      wx.uma.trackEvent("wechat_onError", { 'register': JSON.stringify(error) });
    }
  }),
  goPath() {
    const { type, switchTabPages, switchTabIndex } = this.data;
    switch (type) {
      case 0:
        wx.switchTab({ url: switchTabPages[switchTabIndex] })
        break;
      case 1:
        wx.navigateBack()
        break;
      default:
        wx.navigateBack()
        break;
    }
  }
})