var app = getApp();
export const subConfig = {
    couponList: {},
    medalList: {},
    orderList: {},
    packaging: {},// 打包结算
    evaluate: {},// 待评价
    viewMore: {},// 昨日上新 - 查看更多
}
// 每天弹一次消息 - 设置状态
export const initSubState = () => {
    Object.keys(subConfig).forEach(key => {
        if (!wx.getStorageSync(`sub-${key}-date`)) {
            wx.setStorageSync(`sub-${key}-date`, '');
        }
        if (!wx.getStorageSync(`sub-${key}-status`)) {
            wx.setStorageSync(`sub-${key}-status`, false);
        }
    })
}

export const setSubState = (key) => {
    const date = new Date(new Date().setHours(0, 0, 0, 0)).getTime();
    wx.setStorageSync(`sub-${key}-date`, date);
    wx.setStorageSync(`sub-${key}-status`, true);
}
export const getSubState = (key) => {
    let state = false;
    const oldDate = wx.getStorageSync(`sub-${key}-date`);
    const date = new Date(new Date().setHours(0, 0, 0, 0)).getTime();
    // 真状态
    let status = wx.getStorageSync(`sub-${key}-status`);
    if (date > oldDate) {
        wx.setStorageSync(`sub-${key}-status`, false);
        status = false
    }
    if (date > oldDate && !status) {
        state = true;
    }

    return state
}
// 发生模板消息 更新订阅状态
export const subscribeMessage = (templateId, key) => {
    return new Promise((reslove, reject) => {
        wx.requestSubscribeMessage({
            tmplIds: templateId,
            success: (res) => {
                // if (templateId.some(v => res[v]!== 'accept' ) ) {
                //     return false
                // }
                const successList = templateId.filter(v => res[v] === 'accept');
                // 更新订阅
                setSubState(key);
                if (templateId.some(v => res[v] === 'accept')) {
                    wx.uma.trackEvent("message_allow", { 'templateId': templateId, 'code': key });
                    reslove(successList)
                } else {
                    wx.uma.trackEvent("message_refuse", { 'templateId': templateId, 'code': key });
                    reslove([])
                }
            },
            fail: (e) => {
                console.log('失败', e)
                reject(e)
            }
        })
    })
}