// 获取当前帐号信息
const accountInfo = wx.getAccountInfoSync();
// 线上appid wx35e6b745bcb73b2c
// 测试小程序appid wx63d401c108c6fb36
// env类型

export const env = accountInfo.miniProgram.envVersion || 'release';
// 请求地址前缀配置
export const baseUrlConfig = {
  // 开发环境
  develop: {
    url: {
      default: 'https://openapi-gateway-dev.hotmaxx.cn', // 开发
      // default: 'https://yapi.hotmaxx.cn/mock/85', // 开发
    }
  },
  // 体验版本
  trial: {
    url: {
      default: 'https://openapi-gateway-uat.hotmaxx.cn', // 测试地址
    }
  },
  //生产
  release: {
    url: {
      default: 'https://openapi-gateway.hotmaxx.cn' // 正式地址
    }
  }
}
// 请求地址前缀
//export const baseUrl = baseUrlConfig['release'].url.default
export const baseUrl = baseUrlConfig['trial'].url.default
