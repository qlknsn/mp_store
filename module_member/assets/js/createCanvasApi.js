
// 保存海报到相册
function saveImage() {
    return new Promise((resolve, reject) => {
        wx.getSetting({
            success(res) {
                if (res.authSetting['scope.writePhotosAlbum']) {
                    resolve();
                } else if (res.authSetting['scope.writePhotosAlbum'] === undefined) {
                    wx.authorize({
                        scope: 'scope.writePhotosAlbum',
                        success() {
                            resolve();
                        },
                        fail() {
                            return authConfirm()
                        }
                    })
                } else {
                    return authConfirm()
                }
            }
        })
    })
}
function authConfirm() {
    return new Promise((resolve, reject) => {
        wx.showModal({
            content: '检测到您没打开保存图片权限，是否去设置打开？',
            confirmText: "确认",
            cancelText: "取消",
            success: function (res) {
                if (res.confirm) {
                    wx.openSetting({
                        success(res) {
                            if (res.authSetting['scope.writePhotosAlbum']) {
                                resolve();
                            } else {
                                wx.showToast({
                                    title: '您没有授权，无法保存到相册',
                                    icon: 'none'
                                })
                            }
                        }
                    })
                } else {
                    wx.showToast({
                        title: '您没有授权，无法保存到相册',
                        icon: 'none'
                    })
                }
            }
        });
    })
}
const loadImage = (url, canvas) => {
    return new Promise((resolve, reject) => {
        let img = canvas.createImage();
        img.src = url;
        img.onload = () => resolve(img);
        img.onerror = reject;
    })
}
const getImages = (src) => {
    return new Promise((reslove, reject) => {
        wx.getImageInfo({
            src,
            success: (res) => reslove(res),
            fail: (e) => reject(e),
        });
    });
}

// 获取文本宽度
function getTW(ctx, text, size, bold) {
    ctx.font = bold ? `normal bold ${bold} ${size}px` : `${bold} ${size}px`;
    const tw = ctx.measureText(text);
    return tw.width;
}

module.exports = {
    saveImage: saveImage,
    loadImage: loadImage,
    getImages: getImages,
    getTW: getTW
};