// module_member/benefits/index.js
const {
  httpResource,
  isNeedAutho
} = require("../../assets/js/common.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      'title': '',
      return: 1,
      home: 1
    },
    gradeVal: 1, // 会员等级
    options: {},
    memberLevelInfo: {},// 会员等级信息
    swiperIndex: 0,// 权益下标
    tagIndex: 0, // tag下标
    isLoad: false, // 只在页面加载的时候跳转一次
    list: [],
    childrenId: "", // 滚动位置

  },
  swiperChange(e) {
    const { tagList, list } = this.data;
    const row = list[e.detail.current];
    const tagIndex = tagList.findIndex(item => item.privilegeCode == row.privilegeCode) || 0;
    this.setData({
      swiperIndex: e.detail.current,
      tagIndex,
      childrenId: `nav-item${tagIndex}`,
    })
  },
  tagTap(e) {
    const code = e.currentTarget.dataset.code;
    const index = e.currentTarget.dataset.index;
    const { list } = this.data;
    const swiperIndex = list.findIndex(item => item.privilegeCode == code) || 0;
    this.setData({
      swiperIndex,
      tagIndex: index
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      options
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    isNeedAutho().then(() => {
      this.queryBymemberFn();
    })
  },
  goSevenDaysAgreement() {
    wx.navigateTo({
      url: '/module_base/sevenDaysAgreement/index'
    })
  },
  // 获取等级信息
  async queryBymemberFn() {
    try {
      const data = await httpResource('queryBymember', { gradeDetailflag: 1 });
      let newData = data;
      if (newData) {
        this.setData({
          memberLevelInfo: newData,
          gradeVal: newData.gradeId,
        }, () => {
          this.gradeInfoFn();
        })
      }
    } catch (error) {

    }
  },
  // 初始化参数
  initData(opt) {
    if (opt.code && !this.data.isLoad) {
      const { list } = this.data;
      const index = list.findIndex(item => item.privilegeCode == opt.code) || 0;
      this.setData({
        swiperIndex: index,
        isLoad: true
      })
    }
  },
  getIndex() {
    let index = 0;

  },
  // 获取权益配置列表
  async gradeInfoFn() {
    try {
      const data = await httpResource('gradeInfo', {});
      const { gradeVal } = this.data;
      let item = (data || []).find(item => item.gradeCode == gradeVal);
      if (item) {
        this.setData({
          tagList: (item.privilegeVOList || []).filter(item => item.privilegeCode != 'GD'),
          list: this.getList((item.privilegeVOList || []).filter(item => item.privilegeCode != 'GD'))
        }, () => {
          this.initData(this.data.options);
        })
        console.log(item.privilegeVOList, this.getList(item.privilegeVOList || []))
      }
    } catch (error) {

    }
  },
  // 处理列表
  getList(arr) {
    let data = [];
    arr.forEach(item => {
      if (item.descList && item.descList.length) {
        item.descList.forEach(val => {
          data.push({
            ...item,
            url: val.url,
            imgDesc: val.imgDesc,
          })
        })

      }
    })
    return data;
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
})