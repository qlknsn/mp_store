// pages/user/signIn/index.js
var app = getApp();
import { httpResource } from '../../assets/js/common.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parameter: {
      'title': '',
      return: 1,
      home: 1
    },
    navHeight: 0,
    days: [],//天
    signUp: [], //接口返回打卡
    cur_year: null, //年
    cur_month: null,//月
    showPopup: false, // 签到成功状态
    growValue: '',
  },
  onPopupOpen() {
    this.setData({
      showPopup: true
    })
  },
  onPopupClose() {
    this.setData({
      showPopup: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      navHeight: app.globalData.navHeight,
    });
    const date = new Date();
    const cur_year = date.getFullYear();
    const cur_month = date.getMonth() + 1;
    const weeks_ch = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
    this.calculateEmptyGrids(cur_year, cur_month);
    this.calculateDays(cur_year, cur_month);
    //获取签到状态
    this.getAttendanceCalendar();
    this.setData({
      cur_year,
      cur_month,
      weeks_ch
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  //签到历史
  getAttendanceCalendar: function () {
    // wx.showLoading({ title: '加载中...' });
    let _this = this;
    httpResource('signList', {}, data => {
      _this.setData({
        signUp: data.signList
      }, () => {
        _this.onJudgeSign();
      });
    });
  },

  // 获取当月共多少天
  getThisMonthDays: function (year, month) {
    return new Date(year, month, 0).getDate()
  },

  // 获取当月第一天星期几
  getFirstDayOfWeek: function (year, month) {
    return new Date(Date.UTC(year, month - 1, 1)).getDay();
  },

  // 计算当月1号前空了几个格子，把它填充在days数组的前面
  calculateEmptyGrids: function (year, month) {
    var that = this;
    //计算每个月时要清零
    that.setData({ days: [] });
    const firstDayOfWeek = this.getFirstDayOfWeek(year, month);
    if (firstDayOfWeek > 0) {
      for (let i = 0; i < firstDayOfWeek; i++) {
        var obj = {
          date: null,
          isSign: false,
          isSelect: false,
        };
        that.data.days.push(obj);
      }
      this.setData({
        days: that.data.days
      });
      //清空
    } else {
      this.setData({ days: [] });
    }
  },

  // 绘制当月天数占的格子，并把它放到days数组中
  calculateDays: function (year, month) {
    var _this = this;
    const date = new Date().getDate();
    const thisMonthDays = _this.getThisMonthDays(year, month);
    for (let i = 1; i <= thisMonthDays; i++) {
      let iMsg = i < 10 ? `0${i}` : i;
      var obj = {
        date: i,
        isBefore: i < date ? true : false,
        dateMsg: date == i ? '今天' : `${month}.${iMsg}`,
        isSign: false,
        isSelect: false,
      };
      _this.data.days.push(obj);
    }
    _this.setData({
      days: _this.data.days
    });
  },

  //匹配判断当月与当月哪些日子签到
  onJudgeSign: function () {
    let _this = this;
    let currentDate = new Date();
    //当前年
    let currentYear = currentDate.getFullYear();
    //当前月
    let currentMonth = currentDate.getMonth() + 1;
    //当前日
    let currentDay = currentDate.getDate();
    let signs = _this.data.signUp.find(item => item.year == currentYear && item.month == currentMonth);
    let signsArr = [];
    if (signs) {
      signsArr = signs.signDay;
    }
    let daysArr = _this.data.days;
    for (let i = 0; i < daysArr.length; i++) {
      // 是否打卡
      if (signsArr.includes(`${daysArr[i].date}`)) {
        daysArr[i].isSelect = true;
      }
    }
    _this.setData({
      days: daysArr
    }, () => {
      setTimeout(() => {
        wx.hideLoading({ fail() { } });
      }, 2000)
    });
  },

  // 切换控制年月，上一个月，下一个月
  handleCalendar: function (e) {
    wx.showLoading({ title: '加载中...' });
    let _this = this;
    const handle = e.currentTarget.dataset.handle;
    const cur_year = _this.data.cur_year;
    const cur_month = _this.data.cur_month;
    if (handle === 'prev') {
      let newMonth = cur_month - 1;
      let newYear = cur_year;
      if (newMonth < 1) {
        newYear = cur_year - 1;
        newMonth = 12;
      }
      _this.calculateEmptyGrids(newYear, newMonth);
      _this.calculateDays(newYear, newMonth);
      _this.setData({
        cur_year: newYear,
        cur_month: newMonth,
        workHours: '',
      }, () => {
        _this.onJudgeSign();
      })
    } else {
      let newMonth = cur_month + 1;
      let newYear = cur_year;
      if (newMonth > 12) {
        newYear = cur_year + 1;
        newMonth = 1;
      }
      _this.calculateEmptyGrids(newYear, newMonth);
      _this.calculateDays(newYear, newMonth);
      _this.setData({
        cur_year: newYear,
        cur_month: newMonth,
        workHours: '',
      }, () => {
        _this.onJudgeSign();
      })
    }
  },

  //选着当前天数
  async SelectClock(event) {
    const { days } = this.data;
    const signinDays = days.filter(item => item.isSelect).map(item => item.date);
    const date = new Date().getDate();
    if (signinDays.includes(date)) {
      wx.showToast({
        icon: 'none',
        title: '今天已经签到过了!'
      })
      return
    }
    wx.showLoading({ title: '加载中...' });
    try {
      let _this = this;
      for (let i = 0; i < _this.data.days.length; i++) {
        _this.data.days[i].isSelect = false;
        if (_this.data.days[i].dateMsg == '今天') {
          _this.data.days[i].isSelect = true;
        }
      }
      const data = await httpResource('signIn', {});
      if (data) {
        this.setData({
          growValue: data.growValue,
          signUp: data.signList,
          showPopup: true,
          days: _this.data.days,
        })
        wx.hideLoading({ fail() { } })
      }
    } catch (error) {
      console.log('错误', error)
      setTimeout(() => {
        wx.hideLoading({ fail() { } });
      }, 2000)
    }

  },
})