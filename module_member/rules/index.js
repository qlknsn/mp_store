// module_member/rules/index.js
const app = getApp();
const {
  httpResource,
  isNeedAutho
} = require("../../assets/js/common.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navHeight: 0,
    parameter: { // navbar
      'title': '',
      return: 1,
      home: 1
    },
    isShow: false,// 等级特权显示
    gradeVal: '', // 会员等级
    list: [
      {
        defaultUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/SJLY.png',
        activetUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/SJLY-act.png',
        privilegeCode: "SJLY",
        privilegeName: "升级礼遇",
        unlockFlag: 0,
      },
      {
        defaultUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/JXXQS.png',
        activetUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/JXXQS-act.png',
        privilegeCode: "JXXQS",
        privilegeName: "惊喜星期三",
        unlockFlag: 0,
      },
      {
        defaultUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/LPKTQ.png',
        activetUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/LPKTQ-act.png',
        privilegeCode: "LPKTQ",
        privilegeName: "礼品卡特权",
        unlockFlag: 0,
      },
      {
        defaultUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/HYFW.png',
        activetUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/HYFW-act.png',
        privilegeCode: "HYFW",
        privilegeName: "会员服务",
        unlockFlag: 0,
      },
      {
        defaultUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/SRTQ.png',
        activetUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/SRTQ-act.png',
        privilegeCode: "SRTQ",
        privilegeName: "生日特权",
        unlockFlag: 0,
      },
      {
        defaultUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/JXQY.png',
        activetUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/ruleIcons/JXQY-act.png',
        privilegeCode: "JXQY",
        privilegeName: "惊喜权益",
        unlockFlag: 0,
      },
      {
        defaultUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%BC%96%E7%BB%84%2039%402x.png',
        activetUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%BC%96%E7%BB%84%2039%402x.png',
        privilegeCode: "GD",
        privilegeName: "更多",
        unlockFlag: 0,
      },
    ],
    privilegeTop: 0,
    memberLevelInfo: {},// 会员等级信息
    unlockCount: '',// 解锁权益数量
  },
  // 初始化基础数据
  initData() {
    this.setData({
      navHeight: app.globalData.navHeight
    });
  },
  // 等级特权显示
  changeShow() {
    this.setData({
      isShow: !this.data.isShow
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.initData();
    isNeedAutho().then(() => {
      this.queryBymemberFn();
    })
  },
  // 获取等级信息
  async queryBymemberFn() {
    try {
      const data = await httpResource('queryBymember', { gradeDetailflag: 1 });
      if (data) {
        // 拼接数据
        let obj = {};
        const { list } = this.data;
        (list || []).forEach(item => {
          obj[item.privilegeCode] = item
        })
        const dataList = data.privilegeVOList.map(item => Object.assign(obj[item.privilegeCode], item));
        // 获取权益数量
        const unlockCount = list.filter(item => item.unlockFlag == 1).length;
        this.setData({
          memberLevelInfo: data,
          gradeVal: data.gradeId,
          list: dataList,
          unlockCount
        })
        setTimeout(() => {
          this.getDomTop();
        }, 500)
      }
    } catch (error) {

    }
  },
  // 点击跳转加速位置
  scrollToPrivilege() {
    const { privilegeTop } = this.data;
    wx.pageScrollTo({
      scrollTop: privilegeTop - 80
    })
  },
  // 获取加速升级位置
  getDomTop() {
    const query = wx.createSelectorQuery();
    query.select(".privilege").boundingClientRect();
    query.exec((res) => {
      const {
        top,
      } = res[0];
      this.setData({
        privilegeTop: top || 0
      })
    });
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})