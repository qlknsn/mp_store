// module_member/member/components/share/index.js
import {
  loadImage, getImages
} from "../../../assets/js/createCanvasApi";
import {
  pxTorpx
} from "../../../../assets/js/common";
const app = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    show: {
      type: 'boolean',
      value: false,
      observer(value) {
        if (value) {
          setTimeout(() => {
            this.medalAnimation(value);
          }, 50)
        }
      }
    },
    data: {
      type: 'object',
      value: {},
      modelAnimation: null
    },
  },
  lifetimes: {
    ready: function () {

    },
    attached: function () {
      // 在组件实例进入页面节点树时执行
      // this.medalAnimation();
    },
    detached: function () {
    },
    show: function () {
    },
    hide: function () {
      this.close();
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    isShare: false,
    width: pxTorpx(700),
    height: pxTorpx(938),
    canvas: null, // 画布 canvas
    dpr: 1
  },
  Animation: null,
  /**
   * 组件的方法列表
   */
  methods: {
    // 获取头像底部icon距离
    medalAnimation(isOpen) {
      return new Promise((resolve, reject) => {
        const query = wx.createSelectorQuery().in(this);
        query.select(".img-card").boundingClientRect();
        query.exec((res) => {
          const { data } = this.data;
          if (res[0] == null || res[0].width == 0 || !data.width) {
            setTimeout(() => {
              this.medalAnimation(isOpen);
            }, 30)
            reject(res)
          }
          const {
            top,
            left,
            right,
            width,
            height
          } = res[0];
          this.Animation = wx.createAnimation({
            duration: 400,
            timingFunction: 'ease',
          });
          console.log(data.width, width);
          console.log(data.height, height);
          // .scale(data.width / width, data.height / height)
          this.Animation.left(data.left - ((width - data.width) / 2)).top(data.top - ((height - data.height) / 2)).right(data.right).scale(data.width / width, data.height / height).opacity(1).step({ duration: isOpen ? 1 : 399 });
          if (isOpen) {
            this.Animation.left(left).top(top).right(right).scale(1, 1).step({ duration: 399 });
          } else {
            this.Animation.left(left).top(top).right(right).scale(1, 1).opacity(0).step({ duration: 1 });
          }
          this.setData({
            modelAnimation: this.Animation.export()
          })
          setTimeout(() => {
            resolve(res);
          }, 400)
        });
      })
    },
    // 设置canvas本地路径
    setFile() {
      return new Promise((reslove, reject) => {
        wx.canvasToTempFilePath({
          canvas: this.data.canvas, // 使用2D 需要传递的参数
          success(res) {
            reslove(res.tempFilePath)
          }
        })
      });
    },
    async saveFile() {
      const canvas = this.data.canvas;
      if (canvas) {
        wx.showLoading({
          title: '正在生成海报...',
          mask: true
        });
        let that = this;
        const path = await this.setFile();
        wx.saveImageToPhotosAlbum({
          filePath: path, //canvasToTempFilePath返回的tempFilePath
          success: (res) => {
            wx.hideLoading();
            wx.showToast({
              title: '保存成功！',
              icon: 'none',
              duration: 1500
            })
            this.close();
          },
          fail: (err) => {
            wx.hideLoading();
            wx.showToast({
              title: '保存失败！',
              icon: 'none',
              duration: 1500
            })
          },
          complete: () => { },
        });
      } else {
        console.log('生成中')
      }
    },
    openShare() {
      this.setData({
        isShare: true
      })
      this.create();
    },
    closeShare() {
      this.setData({
        isShare: false
      })
    },
    close() {
      this.setData({
        isShare: false,
      })
      this.medalAnimation(false).then(() => {
        this.setData({
          modelAnimation: null
        })
        this.triggerEvent('close')
      })
    },
    // 跳转会员特权
    goRules(e) {
      wx.navigateTo({
        url: '/module_member/benefits/index'
      })
      this.close();
    },
    // 生成canvas
    create() {
      const query = wx.createSelectorQuery().in(this)
      query.select('#memberCanvas')
        .fields({ node: true, size: true })
        .exec((res) => {
          const canvas = res[0].node;
          const ctx = canvas.getContext('2d');
          const dpr = wx.getSystemInfoSync().pixelRatio
          canvas.width = res[0].width * dpr;
          canvas.height = res[0].height * dpr;
          this.data.dpr = dpr;
          ctx.scale(dpr, dpr);
          console.log(res, dpr, canvas.width, canvas.height, canvas, ctx);
          if (!this.data.data.gradeIcon) return;
          this.createModule(ctx, canvas, this.data.data)
        })
    },
    async createModule(ctx, canvas, data) {
      // ctx.fillStyle = '#000';
      // ctx.fillRect(pxTorpx(0), pxTorpx(0), pxTorpx(700), pxTorpx(928));
      let bg = await getImages('https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%BC%96%E7%BB%84%2020%402x.png');
      console.log(bg);
      await this.createImg(bg.path, ctx, canvas, 0, 224, 700, 704);
      let icon = await getImages(data.gradeIcon);
      await this.createImg(icon.path, ctx, canvas, 150, 0, 400, 448);
      // 标题
      this.createText(`${data.gradeName}`, '#FFFFFF', true, 42, 'PingFangSC-Medium, PingFang SC', 350, 480, ctx, 'center');
      // 文案
      this.createText(`${data.gradeIconDesc}`, '#FFFFFF', false, 24, 'PingFangSC-Medium, PingFang SC', 350, 550, ctx, 'center');
      // 日期
      this.createText(`${data.shareDate} 获得`, '#FFFFFF', false, 24, 'PingFangSC-Medium, PingFang SC', 350, 620, ctx, 'center');

      ctx.save();
      ctx.beginPath(); //开始绘制
      ctx.arc(76, 738, 36, 0, Math.PI * 2, false);
      ctx.clip();
      // 头像
      const avatar = await getImages(data.headUrl || 'https://img.hotmaxx.cn/web/common/mp-store/default/avatar-default.png');
      await this.createImg(avatar.path, ctx, canvas, 40, 702, 72, 72);
      ctx.restore();
      // 昵称
      this.createText(data.nickName, '#333333', false, 28, 'PingFangSC-Medium, PingFang SC', 124, 718, ctx);
      // 边框
      ctx.beginPath();
      ctx.fillStyle = '#ECECEC';
      ctx.moveTo(0, 802);
      ctx.lineTo(700, 802);
      ctx.lineWidth = 1;
      ctx.strokeStyle = '#ECECEC';
      ctx.stroke();
      // 长按文案提示
      this.createText('长按查看我的成就和权益', '#666666', false, 22, 'PingFangSC-Medium, PingFang SC', 40, 828, ctx);
      // hotmaxx icon icon
      let hotmaxxIcon = await getImages('https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E4%BD%8D%E5%9B%BE%402x.png');
      await this.createImg(hotmaxxIcon.path, ctx, canvas, 40, 880, 196, 24);
      // 小程序码
      let qrcode = await getImages('https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/qrcode33333.png');
      await this.createImg(qrcode.path, ctx, canvas, 560, 810, 104, 104);
      this.data.canvas = canvas;
    },
    // 渲染文字
    createText(str, color, bold, size, font, x, y, ctx, textAlign = 'start') {
      ctx.font = bold ? `normal bold ${size}px ${font}` : `${size}px ${font}`;
      ctx.fillStyle = color;
      ctx.textAlign = textAlign;
      ctx.textBaseline = 'top'
      ctx.fillText(str, x, y)
    },
    // 渲染图片
    async createImg(url, ctx, canvas, x, y, w, h) {
      const img = await loadImage(url, canvas);
      ctx.drawImage(img, x, y, w, h);
    },
    move() {
      return
    }
  },
})
