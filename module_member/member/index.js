// module_member/member/index.js
const {
  httpResource,
  isNeedAutho
} = require("../../assets/js/common.js");
import {
  setSharePagePath
} from "../../assets/js/commonData";
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navHeight: 0,
    parameter: { // navbar
      'title': '',
      return: 1,
      home: 1
    },
    opcity: 0,
    gradeList: [],
    swiperIndex: 0, // 切换等级
    swiperBaseImgList: ['https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%BC%96%E7%BB%84%2028%402x.png', 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E5%8D%A1%E7%89%87%E8%83%8C%E6%99%AF%402x.png', 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%BC%96%E7%BB%84%2045%402x.png', 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E8%83%8C%E6%99%AF%402x.png', 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E5%8D%A1%E7%89%87%402x.png'], // swiper背景图片
    swiperOkImgList: ['https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%BC%96%E7%BB%84%2030%402x.png', 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%BC%96%E7%BB%84%2029%402x.png', 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%BC%96%E7%BB%84%2045%402x%20(1).png', 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%BC%96%E7%BB%84%2043%402x.png', 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%BC%96%E7%BB%84%2022%402x.png'], // 已达成背景图
    // 当前等级
    gradeVal: 1,
    userInfo: {}, // 用户基础信息
    // 不同等级配置
    gradeObj: {
      1: {
        infoColor: '#4E9171',
        color: '#9fd0b1',
        speedUpUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%9F%A9%E5%BD%A2%402x.png',
        taskIconUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%9F%A9%E5%BD%A2%402x%20(1).png'
      },
      2: {
        infoColor: '#627DAC',
        color: '#919EBA',
        speedUpUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%9F%A9%E5%BD%A2%402x%20(2).png',
        taskIconUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%9F%A9%E5%BD%A2%402x%20(3).png'
      },
      3: {
        infoColor: '#9A6A67',
        color: '#B18D8A',
        speedUpUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%9F%A9%E5%BD%A2%402x%20(4).png',
        taskIconUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%9F%A9%E5%BD%A2%402x%20(5).png',
      },
      4: {
        infoColor: '#AD7A4B',
        color: '#F6B675',
        speedUpUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%9F%A9%E5%BD%A2%402x%20(6).png',
        taskIconUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%9F%A9%E5%BD%A2%402x%20(7).png',
      },
      5: {
        infoColor: '#F8E1B1',
        color: '#222222',
        speedUpUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%9F%A9%E5%BD%A2%402x%20(8).png',
        taskIconUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/%E7%9F%A9%E5%BD%A2%402x%20(9).png',
      },
    },
    // 任务code枚举
    growInList: [],// 任务列表
    taskObj: {
      '1': '去购物',
      '3': '去签到',
      '4': '去邀请',
      '5': '去评价',
      '6': '去分享',
      '7': '去完善',
    },
    isLoad: false, // 是否加载第一次
    memberLevelInfo: {}, // 会员等级信息
    shareData: {}, // 分享弹框数据
    shareShow: false, //分享弹框开关
  },
  swiperChange(e) {
    this.setData({
      swiperIndex: e.detail.current,
    })
  },
  tagTap(e) {
    const { code } = e.currentTarget.dataset;
    this.setData({
      swiperIndex: code
    })
  },
  // 初始化基础数据
  initData() {
    // 用户信息
    const userInfo = wx.getStorageSync("userInfo");
    this.setData({
      userInfo,
      navHeight: app.globalData.navHeight
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options && options.level) {
      this.setData({
        gradeVal: options.level,
        swiperIndex: options.level - 1,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    setTimeout(() => {
      this.getDomTop()
    }, 500)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.initData();
    isNeedAutho().then(() => {
      this.queryBymemberFn();
      this.growInfoFn();
    }).catch(() => {
      wx.reLaunch({
        url: '/pages/index/index'
      })
    })
  },
  // 点击头像显示分享弹窗
  openShare() {
    // 获取勋章icon 的位置
    const query = wx.createSelectorQuery();
    query.select(".member-medal").boundingClientRect();
    query.exec((res) => {
      if (res[0] == null || res[0].width == 0) {
        return
      }
      const {
        top,
        left,
        right,
        width,
        height
      } = res[0];
      const { gradeVal, gradeList, memberLevelInfo, userInfo } = this.data;
      const item = gradeList.find(item => item.gradeCode == gradeVal);
      if (item && memberLevelInfo.shareDate) {
        // 赋值数据
        this.setData({
          shareData: Object.assign(item, memberLevelInfo, userInfo, {
            top,
            left,
            right,
            width,
            height
          }),
          shareShow: true
        })
        console.log(Object.assign(item, memberLevelInfo, userInfo))
      }
    });

  },
  closeShare() {
    this.setData({
      shareShow: false
    })
    setTimeout(() => {
      this.setData({
        shareData: {},
      })
    }, 50)
  },
  // 跳转权益轮播
  goBenefits(e) {
    const { code } = e.currentTarget.dataset;
    if (code == 'GD') return;
    wx.navigateTo({
      url: `/module_member/benefits/index?code=${code}`
    })

  },
  // 任务点击事件
  taskTap(e) {
    const { type, url } = e.currentTarget.dataset;
    switch (type) {
      case '1':
        // 去购物 跳转首页
        wx.reLaunch({
          url: '/pages/index/index'
        })
        break;
      case '3':
        // 签到
        wx.navigateTo({
          url: '/module_member/signIn/index'
        })
        break;
      case '4':
        // 邀请
        if (url) {
          wx.navigateTo({
            url
          })
        }
        break;
      case '5':
        // 评价-跳转待评价
        wx.navigateTo({
          url: `/module_order/orderList/orderList?orderType=${2}`,
        });
        break;
      case '6':
        // 去分享 跳转首页
        wx.reLaunch({
          url: '/pages/index/index'
        })
        break;
      case '7':
        // 完善资料 修改信息
        wx.navigateTo({
          url: '/pages/user/userInfo/index'
        })
        break;

      default:
        break;
    }
  },
  // 点击跳转加速位置
  scrollToSpeedUp() {
    const { speedUpTop } = this.data;
    wx.pageScrollTo({
      scrollTop: speedUpTop - 80
    })
  },
  // 获取加速升级位置
  getDomTop() {
    const query = wx.createSelectorQuery();
    query.select(".speed-up").boundingClientRect();
    query.exec((res) => {
      const {
        top,
      } = res[0];
      this.setData({
        speedUpTop: top || 0
      })
    });
  },
  // 跳转会员规则
  goRules(e) {
    wx.navigateTo({
      url: '/module_member/rules/index'
    })
  },
  // 获取加入天数
  getRegisterDays() {
    // 用户信息
    let userInfo = wx.getStorageSync("userInfo");
    // userMergeInfo
    httpResource(
      "userMergeInfo", {},
      (data) => {
        let {
          memberExtendVO,
        } = data;
        // 用户扩展信息
        if (memberExtendVO) {
          userInfo.registerDays = memberExtendVO.registerDays;
          this.setData({
            userInfo
          });
          this.setUserInfo(memberExtendVO, userInfo)
        }
      },
      "GET",
      null
    );
  },
  setUserInfo(data, userInfo) {
    wx.setStorageSync("userInfo", {
      ...userInfo,
      ...data,
      userInfoId: data.id,
    });
  },
  // 获取等级信息
  async queryBymemberFn() {
    try {
      const data = await httpResource('queryBymember', { gradeDetailflag: 1 });
      let newData = Object.assign(data, {
        cardDate: (data.gradeEnd || '').substring(2, 10).replace(/-/g, '.'),
        shareDate: (data.gradeBegin || '').substring(0, 10).replace(/-/g, '/'),
      })
      if (newData) {
        if (!this.data.isLoad) {
          this.setData({
            gradeVal: newData.gradeId || 1,
            swiperIndex: newData.gradeId ? newData.gradeId - 1 : 0,
          })
        }
        this.setData({
          isLoad: true,
          memberLevelInfo: newData,

        }, () => {
          this.gradeInfoFn();
        })

      }
    } catch (error) {

    }
  },
  // 获取权益配置列表
  async gradeInfoFn() {
    try {
      const data = await httpResource('gradeInfo', {});
      const { memberLevelInfo } = this.data;
      let list = (data || []).map((item, index) => {
        const progress = Math.ceil((memberLevelInfo.growValue * 100) / (item.gradeCode == 5 ? item.growValue : (data || [])[index + 1].growValue));
        console.log(memberLevelInfo.growValue, item.growValue)
        item.progress = progress > 100 ? 100 : progress;
        return item
      })
      if (list.length) {
        this.setData({
          gradeList: list
        })
      }
    } catch (error) {

    }
  },
  // 获取互动配置列表
  async growInfoFn() {
    try {
      const data = await httpResource('growInfo', {});
      if (data) {
        this.setData({
          growInList: data
        })
      }
    } catch (error) {

    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const path = setSharePagePath('/module_member/member/index', app, 'memberIndex');
    return {
      title: '快来加入好特卖会员，一起来发现宝藏商品',
      path,
      imageUrl: 'https://htm-bucket-pro.oss-cn-hangzhou.aliyuncs.com/web/common/mp-store/mine2/member-share-img.png'
    }
  },
  onPageScroll(e) {
    const top = e.scrollTop;
    if (e.scrollTop >= 150) {
      if (this.data.opcity < 1) {
        this.setData({
          opcity: 1,
          [`parameter.title`]: '会员中心'
        });
      }
    } else {
      const opcity = e.scrollTop / 150;
      this.setData({
        opcity,
      });
      if (opcity < 0.4) {
        this.setData({
          [`parameter.title`]: ''
        })
      }
    }
  },
})